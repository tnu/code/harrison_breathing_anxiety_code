%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% BREATHING SCANNER TASK: FIT MODELS %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 26/03/2020
% -------------------------------------------------------------------------
% TO RUN:   models      = pbihb_fitModels(location, runSims)
% INPUTS:   location    = 'local' or 'euler'
%           varargin    = 1 to run simulations or 0 for no simulations
% OUTPUTS:  models      = Structure with model fits and comparisons 
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script fits the models for the 7 Tesla functional data collected in
% the PBIHB study. The models used are Rescorla Wagner, 2-level HGF and
% 3-level HGF.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function models = pbihb_fitModels(location, varargin)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD AND CODE BEHAVIOURAL DATA FROM ALL PARTICIPANTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Secify PPIDs and options for whole group
models.options = pbihb_setOptions('all', location);

% Set paths and load data for all PPIDs
for a = 1:length(models.options.PPIDs.total)
    setupTemp = pbihb_setOptions(models.options.PPIDs.total{a}, location);
    models.rawData{a} = load(fullfile(setupTemp.paths.task_directory, setupTemp.names.taskData));
end

% Set paths and load data for all pilot PPIDs
for a = 1:length(models.options.PPIDs.pilots)
    setupTemp = pbihb_setOptions(models.options.PPIDs.pilots{a}, location);
    models.pilots.rawData{a} = load(fullfile(setupTemp.paths.task_directory, setupTemp.names.taskData));
end

% Re-code data back to pairings for model fits and calculate score
for a = 1:length(models.options.PPIDs.total)
    score = 0;
    models.data{a}.u = models.rawData{a}.params.pairings;
    models.data{a}.y = [];
    for n = 1:length(models.rawData{a}.data.pred_answer)
        if models.rawData{a}.params.cue(n,1) == 1                                                    % If cue = 1
            models.data{a}.y(n,1) = models.rawData{a}.data.pred_answer(n);                           % Then prediction starts as resistance in first block (original answer)
        elseif models.rawData{a}.params.cue(n,1) == 2                                                % If cue = 2
            models.data{a}.y(n,1) = 1 - models.rawData{a}.data.pred_answer(n);                       % Then prediction starts as no resistance in first block (flipped answer)
        end
        if models.data{a}.y(n,1) == models.data{a}.u(n,1)
            score = score + 1;
        end
    end
    models.data{a}.score = score / length(models.rawData{a}.data.pred_answer) * 100;
end

% Re-code data back to pairings for model fits for pilots and calculate score
for a = 1:length(models.options.PPIDs.pilots)
    score = 0;
    models.pilots.data{a}.u = models.pilots.rawData{a}.params.pairings;
    models.pilots.dataMat.u(:,a) = models.pilots.data{a}.u;
    models.pilots.dataMat.predictRT(:,a) = models.pilots.rawData{a}.data.pred_rt;
    models.pilots.data{a}.y = [];
    for n = 1:length(models.pilots.rawData{a}.data.pred_answer)
        if models.pilots.rawData{a}.params.cue(n,1) == 1                                             % If cue = 1
            models.pilots.data{a}.y(n,1) = models.pilots.rawData{a}.data.pred_answer(n);             % Then prediction starts as resistance in first block (original answer)
            models.pilots.dataMat.y(n,a) = models.pilots.data{a}.y(n,1);                             % Add to pilot data matrix
        elseif models.pilots.rawData{a}.params.cue(n,1) == 2                                         % If cue = 2
            models.pilots.data{a}.y(n,1) = 1 - models.pilots.rawData{a}.data.pred_answer(n);         % Then prediction starts as no resistance in first block (flipped answer)
            models.pilots.dataMat.y(n,a) = models.pilots.data{a}.y(n,1);                             % Add to pilot data matrix
        end
        if models.pilots.data{a}.y(n,1) == models.pilots.data{a}.u(n,1)
            score = score + 1;
        end
    end
    models.pilots.data{a}.score = score / length(models.pilots.rawData{a}.data.pred_answer) * 100;
end

% Check directory existance and make if required
if ~exist(models.options.paths.group_directory, 'dir')
    mkdir(models.options.paths.group_directory);
end
if ~exist(models.options.paths.group_directory_models, 'dir')
    mkdir(models.options.paths.group_directory_models);
end

% Save the pilot data matrix
pilotsOut = models.pilots.dataMat;
save(models.options.saveNames.pilotData, '-struct', 'pilotsOut');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FIT PILOT PARTICIPANTS USING ML
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Include necessary parameters (no simulations etc.)
models.pilots.pilotFits = pbihb_prepModels_master('data', pilotsOut, ...
    'saveDir', models.options.paths.group_directory_models, 'pilots', 1, ...
    'pilot_sub', 8, 'ML_est', 1, 'MapOptQ', 1, 'plot', 1, 'StopSim', 1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FIT CANDIDATE MODELS FOR ALL PARTICIPANTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Fit each model for each PPID
for a = 1:length(models.options.PPIDs.total)
    for model = 1:size(models.pilots.pilotFits.ModelSpace,2) % data FITTING model
        models.est.names{model} = models.pilots.pilotFits.setup(model).name;
        models.est.fits{model}{a} = modelPrep_tapas_fitModel(models.data{a}.y,... % Data
                models.data{a}.u,... % Input sequence
                models.pilots.pilotFits.setup(model).prc_config,... % Prc fitting model
                models.pilots.pilotFits.setup(model).obs_config,... % Obs fitting model
                'tapas_quasinewton_optim_config', 0); % MLE = 0 !!!
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN MODEL COMPARISONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pull out all LME's
for a = 1:length(models.options.PPIDs.total)
    for model = 1:size(models.pilots.pilotFits.ModelSpace,2)
        models.est.LME(a,model) = models.est.fits{model}{a}.optim.LME;
    end
end

% Run BMS on total group
[models.est.comp.total.alpha, models.est.comp.total.exp_r, models.est.comp.total.xp, models.est.comp.total.pxp, models.est.comp.total.bor] = spm_BMS(models.est.LME, 1e6, 0, 0, 1);

% Run BMS on groups separately
[models.est.comp.low.alpha, models.est.comp.low.exp_r, models.est.comp.low.xp, models.est.comp.low.pxp, models.est.comp.low.bor] = spm_BMS(models.est.LME(models.options.PPIDs.low.idx,:), 1e6, 0, 0, 1);
[models.est.comp.mod.alpha, models.est.comp.mod.exp_r, models.est.comp.mod.xp, models.est.comp.mod.pxp, models.est.comp.mod.bor] = spm_BMS(models.est.LME(models.options.PPIDs.mod.idx,:), 1e6, 0, 0, 1);

% Print model comparison results to screen
fprintf('\nMODEL COMPARISON RESULTS:\n');
fprintf('Whole group XPs for models (%s : %s : %s) = %1.2f %1.2f %1.2f \n', models.est.names{1}, models.est.names{2}, models.est.names{3}, models.est.comp.total.xp(1), models.est.comp.total.xp(2), models.est.comp.total.xp(3))
fprintf('Whole group PXPs for models (%s : %s : %s) = %1.2f %1.2f %1.2f \n', models.est.names{1}, models.est.names{2}, models.est.names{3}, models.est.comp.total.pxp(1), models.est.comp.total.pxp(2), models.est.comp.total.pxp(3))
fprintf('Low group XPs for models (%s : %s : %s) = %1.2f %1.2f %1.2f \n', models.est.names{1}, models.est.names{2}, models.est.names{3}, models.est.comp.low.xp(1), models.est.comp.low.xp(2), models.est.comp.low.xp(3))
fprintf('Low group PXPs for models (%s : %s : %s) = %1.2f %1.2f %1.2f \n', models.est.names{1}, models.est.names{2}, models.est.names{3}, models.est.comp.low.pxp(1), models.est.comp.low.pxp(2), models.est.comp.low.pxp(3))
fprintf('Moderate group XPs for models (%s : %s : %s) = %1.2f %1.2f %1.2f \n', models.est.names{1}, models.est.names{2}, models.est.names{3}, models.est.comp.mod.xp(1), models.est.comp.mod.xp(2), models.est.comp.mod.xp(3))
fprintf('Moderate group PXPs for models (%s : %s : %s) = %1.2f %1.2f %1.2f \n\n', models.est.names{1}, models.est.names{2}, models.est.names{3}, models.est.comp.mod.pxp(1), models.est.comp.mod.pxp(2), models.est.comp.mod.pxp(3))

% Find winning model
winner = find(models.est.comp.total.pxp > 0.9);
if ~isempty(winner)
    models.est.comp.total.winner.idx = winner;
    models.est.comp.total.winner.name = models.est.names{winner};
    fprintf('MODEL COMPARISON: WINNING MODEL (WHOLE-GROUP BMS) = %s\n', models.est.comp.total.winner.name)
else
    models.est.comp.total.winner.idx = find(strcmp(models.est.names, 'Rescorla-Wagner'));
    models.est.comp.total.winner.name = 'Rescorla-Wagner';
    fprintf('MODEL COMPARISON: NO WINNING MODEL (WHOLE-GROUP BMS) --> RW USED\n')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMPUTE MODEL GOODNESS OF FIT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate statistics for each subject
for a = 1:length(models.options.PPIDs.total)
    for model = 1:size(models.pilots.pilotFits.ModelSpace,2)
        % Compute "goodness of model fit" statistics
        models.est.goodness.logLlRatio_chance(a,model) = (-models.est.fits{model}{a}.optim.negLl) / (length(models.est.fits{model}{a}.y) * log(0.5));
        models.est.goodness.pseudoR2(a,model) = 1 - models.est.goodness.logLlRatio_chance(a,model);
        models.est.goodness.avgLogLlperTrial(a,model) = (-models.est.fits{model}{a}.optim.negLl) / length(models.est.fits{model}{a}.y);
        models.est.goodness.avgLogLlperTrialExp(a,model) = exp(models.est.goodness.avgLogLlperTrial(a,model));
        models.est.goodness.logLlDiff_chance(a,model) = (-models.est.fits{model}{a}.optim.negLl) - (length(models.est.fits{model}{a}.y) * log(0.5));
        models.est.goodness.k(a,model) = exp(models.est.goodness.logLlDiff_chance(a,model));
        [models.est.goodness.lRatioTest.h(a,model), models.est.goodness.lRatioTest.p(a,model)] = lratiotest(-models.est.fits{model}{a}.optim.negLl, (length(models.est.fits{model}{a}.y) * log(0.5)), 2, 0.05);
    end
end

% Find subjects to exclude
models.est.goodness.excludeIdx = find(models.est.goodness.lRatioTest.h(:,models.est.comp.total.winner.idx) == 0);

% Create indices excluding subjects with poor model fits
models.est.goodness.totalEx = 1:60;
models.est.goodness.totalEx(models.est.goodness.excludeIdx) = [];
models.est.goodness.lowEx = models.options.PPIDs.low.idx;
models.est.goodness.modEx = models.options.PPIDs.mod.idx;
for a = 1:length(models.est.goodness.excludeIdx)
    if any(models.est.goodness.lowEx == models.est.goodness.excludeIdx(a))
        ex = find(models.est.goodness.excludeIdx(a) == models.est.goodness.lowEx);
        models.est.goodness.lowEx(ex) = [];
    end
    if any(models.est.goodness.modEx == models.est.goodness.excludeIdx(a))
        ex = find(models.est.goodness.excludeIdx(a) == models.est.goodness.modEx);
        models.est.goodness.modEx(ex) = [];
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLOT AND SAVE MODEL FIT RESULTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Save results
save(models.options.saveNames.modelFits, 'models');

% Plot example trajectories and model comparison figure
% pbihb_figuresMatlab_trajectories(models.options);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN SIMULATIONS USING PILOT FITS IF SPECIFIED
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check if simulations specified
if exist('varargin', 'var') && varargin{1} == 1
    % Run the simulations
    pbihb_simModels(location);
end

end