%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% BREATHING SCANNER TASK: SINGLE SUBJECT TASK ANALYSIS %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 06/02/2019
% -------------------------------------------------------------------------
% TO RUN:   pbihb_taskLowerLevel(PPID, location)
% INPUTS:   PPID       = PPID of subject to analyse
%           location   = 'local' or 'euler'
% OUTPUTS:  All structures associated with single subject analysis steps
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script runs the single subject analysis steps on Euler for the 7 
% Tesla functional data collected in the PBIHB study.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_taskLowerLevel(PPID, location)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set all options for preprocessing with specific location
options = pbihb_setOptions(PPID, location);

% Initialise SPM and PhysIO
pbihb_initSPM;

% Run preprocessing of physiology data and PhysIO
[physValues, physTraces] = pbihb_physiology(options, 'full');
[physValues] = pbihb_physioToolBox(options, 'task', physValues, physTraces);

% Create noise matrix
pbihb_createNoiseMatrix(options, physValues, 'physio', 'ica_orig');

% Create GLMs for basic and model-based analyses
pbihb_createGlmBasic(options, physValues);
pbihb_createGlmModel(options, physValues);
 
% Unzip 4D functional file
pbihb_zippingFunctions(options, 'unzip')

% Run lower level SPM analyses for functional activity
pbihb_spmAnalysis(options, 'basic');
pbihb_spmAnalysis(options, 'model');

% % Extra lower level SPM analyses for sanity checks --> If using then more
% % run time will be needed for these jobs on Euler
% pbihb_spmAnalysis(options, 'basicSPM');
% pbihb_spmAnalysis(options, 'modelPred');
% pbihb_spmAnalysis(options, 'modelErr');
% pbihb_spmAnalysis(options, 'modelFactors');

% Run timeseries extraction
pbihb_extractTimeseries(options);

% Zip all created analysis files
disp('Zipping all created analysis files...');
system(['gzip ', options.paths.analysis_directories.root, '/*/*.nii']);
disp('... Finished!');

% Create functional connectivity GLMs
pbihb_createGlmModelFC(options, 'aIns', 'predictPos');
pbihb_createGlmModelFC(options, 'aIns', 'predictNeg');
pbihb_createGlmModelFC(options, 'aIns', 'errorPos');
pbihb_createGlmModelFC(options, 'aIns', 'errorNeg');
pbihb_createGlmModelFC(options, 'PAG', 'errorPos');
pbihb_createGlmModelFC(options, 'PAG', 'errorNeg');

% Run lower level SPM analyses for functional connectivity
pbihb_spmAnalysis(options, 'FC', 'aIns', 'predictPos');
pbihb_spmAnalysis(options, 'FC', 'aIns', 'predictNeg');
pbihb_spmAnalysis(options, 'FC', 'aIns', 'errorPos');
pbihb_spmAnalysis(options, 'FC', 'aIns', 'errorNeg');
pbihb_spmAnalysis(options, 'FC', 'PAG', 'errorPos');
pbihb_spmAnalysis(options, 'FC', 'PAG', 'errorNeg');

% Run middle level SPM analyses for functional connectivity
% --> aIns predictions
% --> aIns errors
% --> PAG errors
pbihb_spmAnalysisFE(options, 'aIns', 'both');
pbihb_spmAnalysisFE(options, 'PAG', 'error');

% Re-zip 4D functional file
pbihb_zippingFunctions(options, 'zip');

% Zip all further created analysis files
disp('Zipping all created analysis files...');
system(['gzip ', options.paths.analysis_directories.root, '/*/*.nii']);
disp('... Finished!');

% For rest:
% [physTraces, physValues] = pbihb_physioToolBox(options, 'rest', [], []);

end