%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%% BREATHING SCANNER TASK: CHECK CORRELATIONS %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 24/04/2020
% -------------------------------------------------------------------------
% TO RUN:   corr        = pbihb_checkCorr(location)
% INPUTS:   location    = 'local' or 'euler'
% OUTPUTS:  Matrix and figures for correlations across model matrix
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script checks the correlations across the 'model' matrix used to 
% analyse the fMRI data collected in the PBIHB study. Results include 
% average correlation values (and std) across all participants, plus
% figures to demonstrate these values.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function corr = pbihb_checkModelCorr(location)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Secify options
corr.options = pbihb_setOptions('all', location);

% Check save directory existance and make if required
if ~exist(corr.options.paths.group_directory_imagingGroupGLMs, 'dir')
   mkdir(corr.options.paths.group_directory_imagingGroupGLMs)
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Import all model matrices and calculate correlations
for PPIDidx = 1:length(corr.options.PPIDs.total)
        setupTemp = pbihb_setOptions(corr.options.PPIDs.total{PPIDidx}, corr.options.paths.location);
        loadTemp = load(setupTemp.saveNames.glmModelMatrix);
        corr.data.modelMatrices{PPIDidx} = loadTemp.R;
        modelNames = loadTemp.names;
        [corr.data.modelRvalues(:,:,PPIDidx), corr.data.modelPvalues(:,:,PPIDidx)] = corrcoef(corr.data.modelMatrices{PPIDidx}(:,1:(length(modelNames)*3)));
        loadTemp = load(setupTemp.saveNames.glmBasicMatrix);
        corr.data.basicMatrices{PPIDidx} = loadTemp.R;
        basicNames = loadTemp.names;
        [corr.data.basicRvalues(:,:,PPIDidx), corr.data.basicPvalues(:,:,PPIDidx)] = corrcoef(corr.data.basicMatrices{PPIDidx}(:,1:(length(basicNames)*3)));
end

% Specify full length of names (with spaces for derivatives)
corr.data.modelNames.reduced = modelNames;
a = 1;
for x = 1:length(corr.data.modelNames.reduced)
    corr.data.modelNames.full{a} = corr.data.modelNames.reduced{x};
    for b = 1:2
        corr.data.modelNames.full{a+b} = '';
    end
    a = a + 3;
end
corr.data.basicNames.reduced = basicNames;
a = 1;
for x = 1:length(corr.data.basicNames.reduced)
    corr.data.basicNames.full{a} = corr.data.basicNames.reduced{x};
    for b = 1:2
        corr.data.basicNames.full{a+b} = '';
    end
    a = a + 3;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PULL OUT PREDICTIONS AND ERRORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get indices for predictions and errors
corr.data.modelNames.ofInterest.reduced = {'predictPos', 'predictNeg', 'errorPos', 'errorNeg'};
a = 1;
for x = 1:length(corr.data.modelNames.ofInterest.reduced)
    idx(a) = find(contains(corr.data.modelNames.full, corr.data.modelNames.ofInterest.reduced{x}));
    corr.data.modelNames.ofInterest.full{a} = corr.data.modelNames.full{idx(a)};
    idx(a+1) = idx(a) + 1;
    corr.data.modelNames.ofInterest.full{a+1} = '';
    idx(a+2) = idx(a+1) + 1;
    corr.data.modelNames.ofInterest.full{a+2} = '';
    a = a + 3;
end

% Pull out R and p values
corr.data.ofInterest.modelRvalues = corr.data.modelRvalues(idx,idx,:);
corr.data.ofInterest.modelPvalues = corr.data.modelPvalues(idx,idx,:);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PULL OUT PREDICTION DECISIONS AND ERRORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get indices for prediction decisions and errors
corr.data.basicNames.ofInterest.reduced = {'cueYes', 'cueNo', 'resistanceSurprise', 'noResistanceSurprise'};
a = 1;
for x = 1:length(corr.data.basicNames.ofInterest.reduced)
    idx(a) = find(contains(corr.data.basicNames.full, corr.data.basicNames.ofInterest.reduced{x}));
    corr.data.basicNames.ofInterest.full{a} = corr.data.basicNames.full{idx(a)};
    idx(a+1) = idx(a) + 1;
    corr.data.basicNames.ofInterest.full{a+1} = '';
    idx(a+2) = idx(a+1) + 1;
    corr.data.basicNames.ofInterest.full{a+2} = '';
    a = a + 3;
end

% Pull out R and p values
corr.data.ofInterest.basicRvalues = corr.data.basicRvalues(idx,idx,:);
corr.data.ofInterest.basicPvalues = corr.data.basicPvalues(idx,idx,:);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CALCULATE SUMMARY CORRELATION VALUES FOR PREDICTIONS AND ERRORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% R to Z transform data
corr.data.modelZvalues = atanh(corr.data.modelRvalues);
corr.data.ofInterest.modelZvalues = atanh(corr.data.ofInterest.modelRvalues);

% Calculate mean R and p values (total)
corr.summary.meanModelZvalues = mean(corr.data.modelZvalues,3);
corr.summary.meanModelRvalues = tanh(corr.summary.meanModelZvalues);
corr.summary.meanModelPvalues = mean(corr.data.modelPvalues,3);

% Calculate std of Z and p values (total)
corr.summary.stdModelZvalues = std(corr.data.modelZvalues,[],3);
corr.summary.stdModelPvalues = std(corr.data.modelPvalues,[],3);

% Isolate only significant p-values (total)
[row,col] = find(corr.summary.meanModelPvalues < 0.01);
corr.summary.sigModelPvalues = NaN(size(corr.summary.meanModelPvalues));
for a = 1:length(row)
    corr.summary.sigModelPvalues(row(a),col(a)) = corr.summary.meanModelPvalues(row(a),col(a));
end

% Calculate mean R and p values (predictions and errors)
corr.summary.ofInterest.meanModelZvalues = mean(corr.data.ofInterest.modelZvalues,3);
corr.summary.ofInterest.meanModelRvalues = tanh(corr.summary.ofInterest.meanModelZvalues);
corr.summary.ofInterest.meanModelPvalues = mean(corr.data.ofInterest.modelPvalues,3);

% Calculate std of R and p values (predictions and errors)
corr.summary.ofInterest.stdModelZvalues = std(corr.data.ofInterest.modelZvalues,[],3);
corr.summary.ofInterest.stdModelPvalues = std(corr.data.ofInterest.modelPvalues,[],3);

% Isolate only significant p-values (predictions and errors)
[row,col] = find(corr.summary.ofInterest.meanModelPvalues < 0.01);
corr.summary.ofInterest.sigModelPvalues = NaN(size(corr.summary.ofInterest.meanModelPvalues));
for a = 1:length(row)
    corr.summary.ofInterest.sigModelPvalues(row(a),col(a)) = corr.summary.ofInterest.meanModelPvalues(row(a),col(a));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CALCULATE SUMMARY CORRELATION VALUES FOR PREDICTION DECISIONS AND ERRORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% R to Z transform data
corr.data.basicZvalues = atanh(corr.data.basicRvalues);
corr.data.ofInterest.basicZvalues = atanh(corr.data.ofInterest.basicRvalues);

% Calculate mean R and p values (total)
corr.summary.meanBasicZvalues = mean(corr.data.basicZvalues,3);
corr.summary.meanBasicRvalues = tanh(corr.summary.meanBasicZvalues);
corr.summary.meanBasicPvalues = mean(corr.data.basicPvalues,3);

% Calculate std of Z and p values (total)
corr.summary.stdBasicZvalues = std(corr.data.basicZvalues,[],3);
corr.summary.stdBasicPvalues = std(corr.data.basicPvalues,[],3);

% Isolate only significant p-values (total)
[row,col] = find(corr.summary.meanBasicPvalues < 0.01);
corr.summary.sigBasicPvalues = NaN(size(corr.summary.meanBasicPvalues));
for a = 1:length(row)
    corr.summary.sigBasicPvalues(row(a),col(a)) = corr.summary.meanBasicPvalues(row(a),col(a));
end

% Calculate mean R and p values (predictions and errors)
corr.summary.ofInterest.meanBasicZvalues = mean(corr.data.ofInterest.basicZvalues,3);
corr.summary.ofInterest.meanBasicRvalues = tanh(corr.summary.ofInterest.meanBasicZvalues);
corr.summary.ofInterest.meanBasicPvalues = mean(corr.data.ofInterest.basicPvalues,3);

% Calculate std of R and p values (predictions and errors)
corr.summary.ofInterest.stdBasicZvalues = std(corr.data.ofInterest.basicZvalues,[],3);
corr.summary.ofInterest.stdBasicPvalues = std(corr.data.ofInterest.basicPvalues,[],3);

% Isolate only significant p-values (predictions and errors)
[row,col] = find(corr.summary.ofInterest.meanBasicPvalues < 0.01);
corr.summary.ofInterest.sigBasicPvalues = NaN(size(corr.summary.ofInterest.meanBasicPvalues));
for a = 1:length(row)
    corr.summary.ofInterest.sigBasicPvalues(row(a),col(a)) = corr.summary.ofInterest.meanBasicPvalues(row(a),col(a));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE SUMMARY FIGURES FOR PREDICTIONS AND ERRORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create figure for mean R values (total)
figure()
    set(gcf, 'Position', [0 0 600 500]);
    pivot = 0;
    ticks = 1:length(corr.data.modelNames.full);
    % Mean R values
    bwr1 = @(n, pivot) interp1([-1 0 1], [0 0 1; 1 1 1; 1 0 0], linspace(-1, 1, n), 'linear');
    imagesc(corr.summary.meanModelRvalues, [-1 1]);
    colormap(gca, bwr1(64));
    colorbar;
    axis square
    title('GLM MEAN CORRELATION MATRIX (R)', 'FontSize', 15);
    set(gca, 'XTick', ticks, 'XTickLabels', corr.data.modelNames.full);
    xtickangle(45);
    set(gca, 'YTick', ticks, 'YTickLabels', corr.data.modelNames.full);
    hold on;
    b = 1;
    for a = 1:(length(corr.data.modelNames.reduced)-1)
        x = b + 2.5;
        line([0,(length(corr.data.modelNames.full)+0.5)], [x,x], 'Color', 'k', 'LineWidth', 1);
        line([x,x], [0,(length(corr.data.modelNames.full)+0.5)], 'Color', 'k', 'LineWidth', 1);
        b = a * 3 + 1;
    end
    print(corr.options.saveNames.glmModelRvalues, '-dtiff');
    
% Create a figure for significant p values (total)
figure()
    set(gcf, 'Position', [0 0 600 500]);
    bwr2 = @(n)interp1([0 0.01], [0.3 0.5 1; 1 1 1], linspace(0, 0.01, n), 'linear');
    subImg2 = imagesc(corr.summary.sigModelPvalues, [0 0.01]);
    set(subImg2, 'AlphaData', ~isnan(corr.summary.sigModelPvalues))
    colormap(gca, bwr2(64));
    colorbar;
    set(gca, 'clim', [0 0.01])
    axis square
    title('GLM CORRELATION P-VALUES', 'FontSize', 15);
    set(gca, 'XTick', ticks, 'XTickLabels', corr.data.modelNames.full);
    xtickangle(45);
    set(gca, 'YTick', ticks, 'YTickLabels', corr.data.modelNames.full);
    hold on;
    b = 1;
    for a = 1:(length(corr.data.modelNames.reduced)-1)
        x = b + 2.5;
        line([0,(length(corr.data.modelNames.full)+0.5)], [x,x], 'Color', 'k', 'LineWidth', 1);
        line([x,x], [0,(length(corr.data.modelNames.full)+0.5)], 'Color', 'k', 'LineWidth', 1);
        b = a * 3 + 1;
    end
    print(corr.options.saveNames.glmModelPvalues, '-dtiff');
    
% Create figure for mean R values (predictions and errors only)
figure()
    set(gcf, 'Position', [0 0 600 500]);
    pivot = 0;
    ticks = 1:length(corr.data.modelNames.ofInterest.full);
    % Mean R values
    bwr1 = @(n, pivot) interp1([-1 0 1], [0 0 1; 1 1 1; 1 0 0], linspace(-1, 1, n), 'linear');
    imagesc(corr.summary.ofInterest.meanModelRvalues, [-1 1]);
    colormap(gca, bwr1(64));
    colorbar;
    axis square
    title('GLM MEAN CORRELATION MATRIX (R)', 'FontSize', 15);
    set(gca, 'XTick', ticks, 'XTickLabels', corr.data.modelNames.ofInterest.full);
    xtickangle(45);
    set(gca, 'YTick', ticks, 'YTickLabels', corr.data.modelNames.ofInterest.full);
    hold on;
    b = 1;
    for a = 1:(length(corr.data.modelNames.ofInterest.reduced)-1)
        x = b + 2.5;
        line([0,(length(corr.data.modelNames.ofInterest.full)+0.5)], [x,x], 'Color', 'k', 'LineWidth', 1);
        line([x,x], [0,(length(corr.data.modelNames.ofInterest.full)+0.5)], 'Color', 'k', 'LineWidth', 1);
        b = a * 3 + 1;
    end
    print(corr.options.saveNames.glmModelRvaluesPredErr, '-dtiff');
    
% Create a figure for significant p values (predictions and errors only)
figure()
    set(gcf, 'Position', [0 0 600 500]);
    bwr2 = @(n)interp1([0 0.01], [0.3 0.5 1; 1 1 1], linspace(0, 0.01, n), 'linear');
    subImg2 = imagesc(corr.summary.ofInterest.sigModelPvalues, [0 0.01]);
    set(subImg2, 'AlphaData', ~isnan(corr.summary.ofInterest.sigModelPvalues))
    colormap(gca, bwr2(64));
    colorbar;
    set(gca, 'clim', [0 0.01])
    axis square
    title('GLM CORRELATION P-VALUES', 'FontSize', 15);
    set(gca, 'XTick', ticks, 'XTickLabels', corr.data.modelNames.ofInterest.full);
    xtickangle(45);
    set(gca, 'YTick', ticks, 'YTickLabels', corr.data.modelNames.ofInterest.full);
    hold on;
    b = 1;
    for a = 1:(length(corr.data.modelNames.ofInterest.reduced)-1)
        x = b + 2.5;
        line([0,(length(corr.data.modelNames.ofInterest.full)+0.5)], [x,x], 'Color', 'k', 'LineWidth', 1);
        line([x,x], [0,(length(corr.data.modelNames.ofInterest.full)+0.5)], 'Color', 'k', 'LineWidth', 1);
        b = a * 3 + 1;
    end
    print(corr.options.saveNames.glmModelPvaluesPredErr, '-dtiff');

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE SUMMARY FIGURES FOR PREDICTION DECISIONS AND ERRORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create figure for mean R values (total)
figure()
    set(gcf, 'Position', [0 0 600 500]);
    pivot = 0;
    ticks = 1:length(corr.data.basicNames.full);
    % Mean R values
    bwr1 = @(n, pivot) interp1([-1 0 1], [0 0 1; 1 1 1; 1 0 0], linspace(-1, 1, n), 'linear');
    imagesc(corr.summary.meanBasicRvalues, [-1 1]);
    colormap(gca, bwr1(64));
    colorbar;
    axis square
    title('GLM MEAN CORRELATION MATRIX (R)', 'FontSize', 15);
    set(gca, 'XTick', ticks, 'XTickLabels', corr.data.basicNames.full);
    xtickangle(45);
    set(gca, 'YTick', ticks, 'YTickLabels', corr.data.basicNames.full);
    hold on;
    b = 1;
    for a = 1:(length(corr.data.basicNames.reduced)-1)
        x = b + 2.5;
        line([0,(length(corr.data.basicNames.full)+0.5)], [x,x], 'Color', 'k', 'LineWidth', 1);
        line([x,x], [0,(length(corr.data.basicNames.full)+0.5)], 'Color', 'k', 'LineWidth', 1);
        b = a * 3 + 1;
    end
    print(corr.options.saveNames.glmBasicRvalues, '-dtiff');
    
% Create a figure for significant p values (total)
figure()
    set(gcf, 'Position', [0 0 600 500]);
    bwr2 = @(n)interp1([0 0.01], [0.3 0.5 1; 1 1 1], linspace(0, 0.01, n), 'linear');
    subImg2 = imagesc(corr.summary.sigBasicPvalues, [0 0.01]);
    set(subImg2, 'AlphaData', ~isnan(corr.summary.sigBasicPvalues))
    colormap(gca, bwr2(64));
    colorbar;
    set(gca, 'clim', [0 0.01])
    axis square
    title('GLM CORRELATION P-VALUES', 'FontSize', 15);
    set(gca, 'XTick', ticks, 'XTickLabels', corr.data.basicNames.full);
    xtickangle(45);
    set(gca, 'YTick', ticks, 'YTickLabels', corr.data.basicNames.full);
    hold on;
    b = 1;
    for a = 1:(length(corr.data.basicNames.reduced)-1)
        x = b + 2.5;
        line([0,(length(corr.data.basicNames.full)+0.5)], [x,x], 'Color', 'k', 'LineWidth', 1);
        line([x,x], [0,(length(corr.data.basicNames.full)+0.5)], 'Color', 'k', 'LineWidth', 1);
        b = a * 3 + 1;
    end
    print(corr.options.saveNames.glmBasicPvalues, '-dtiff');
    
% Create figure for mean R values (predictions and errors only)
figure()
    set(gcf, 'Position', [0 0 600 500]);
    pivot = 0;
    ticks = 1:length(corr.data.basicNames.ofInterest.full);
    % Mean R values
    bwr1 = @(n, pivot) interp1([-1 0 1], [0 0 1; 1 1 1; 1 0 0], linspace(-1, 1, n), 'linear');
    imagesc(corr.summary.ofInterest.meanBasicRvalues, [-1 1]);
    colormap(gca, bwr1(64));
    colorbar;
    axis square
    title('GLM MEAN CORRELATION MATRIX (R)', 'FontSize', 15);
    set(gca, 'XTick', ticks, 'XTickLabels', corr.data.basicNames.ofInterest.full);
    xtickangle(45);
    set(gca, 'YTick', ticks, 'YTickLabels', corr.data.basicNames.ofInterest.full);
    hold on;
    b = 1;
    for a = 1:(length(corr.data.basicNames.ofInterest.reduced)-1)
        x = b + 2.5;
        line([0,(length(corr.data.basicNames.ofInterest.full)+0.5)], [x,x], 'Color', 'k', 'LineWidth', 1);
        line([x,x], [0,(length(corr.data.basicNames.ofInterest.full)+0.5)], 'Color', 'k', 'LineWidth', 1);
        b = a * 3 + 1;
    end
    print(corr.options.saveNames.glmBasicRvaluesPredErr, '-dtiff');
    
% Create a figure for significant p values (predictions and errors only)
figure()
    set(gcf, 'Position', [0 0 600 500]);
    bwr2 = @(n)interp1([0 0.01], [0.3 0.5 1; 1 1 1], linspace(0, 0.01, n), 'linear');
    subImg2 = imagesc(corr.summary.ofInterest.sigBasicPvalues, [0 0.01]);
    set(subImg2, 'AlphaData', ~isnan(corr.summary.ofInterest.sigBasicPvalues))
    colormap(gca, bwr2(64));
    colorbar;
    set(gca, 'clim', [0 0.01])
    axis square
    title('GLM CORRELATION P-VALUES', 'FontSize', 15);
    set(gca, 'XTick', ticks, 'XTickLabels', corr.data.basicNames.ofInterest.full);
    xtickangle(45);
    set(gca, 'YTick', ticks, 'YTickLabels', corr.data.basicNames.ofInterest.full);
    hold on;
    b = 1;
    for a = 1:(length(corr.data.basicNames.ofInterest.reduced)-1)
        x = b + 2.5;
        line([0,(length(corr.data.basicNames.ofInterest.full)+0.5)], [x,x], 'Color', 'k', 'LineWidth', 1);
        line([x,x], [0,(length(corr.data.basicNames.ofInterest.full)+0.5)], 'Color', 'k', 'LineWidth', 1);
        b = a * 3 + 1;
    end
    print(corr.options.saveNames.glmBasicPvaluesPredErr, '-dtiff');
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE OUTPUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save(corr.options.saveNames.glmModelCorr, 'corr');

end