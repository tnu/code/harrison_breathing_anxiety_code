%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% BREATHING SCANNER TASK: INTEGRATIVE ANALYSIS %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 29/07/2020
% -------------------------------------------------------------------------
% TO RUN:   pbihb_multiModalPCA(location)
% INPUTS:   location   = 'local' or 'euler'
% OUTPUTS:  A structure containing the PCA analysis across three modes of
%           measurement: Questionnaires, FDT and insula brain activity
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script runs the integrative, multi-modal analysis for the PBIHB
% study. This analysis runs a PCA on the relevant anxiety / depression and
% interoceptive questionnaires, FDT measures and brain activity in the aIns
% related to positive / negative predictions and errors.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pcaResults = pbihb_multiModalPCA(location)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set the options
pcaResults.options = pbihb_setOptions('all', location);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD THE DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load FDT and questionnaire analysis
load(pcaResults.options.saveNames.fdt);

% Load the aIns activity data
aIns.predPos = load(pcaResults.options.saveNames.aIns.posPred);
aIns.predNeg = load(pcaResults.options.saveNames.aIns.negPred);
aIns.errPos = load(pcaResults.options.saveNames.aIns.posErr);
aIns.errNeg = load(pcaResults.options.saveNames.aIns.negErr);

% Load model fits
load(pcaResults.options.saveNames.modelFits);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GATHER DATA MATRIX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check PPID order for model data against questionnaires & fdt
if ~isempty(setdiff(fdt.options.PPIDs.total, models.options.PPIDs.total))
    error('PPIDs from model fits do not match... Exiting');
end

% Pull out affect questionnaire scores
questNamesAff = {'staiS', 'gad7', 'asi', 'cesd'};
pcaResults.data.affectQ.names = {'State anxiety', 'Anxiety disorder', 'Anxiety sensitivity', 'Depression'};
pcaResults.data.affectQ.namesShort = {'State anx.', 'Anx. disor.', 'Anx. sens.', 'Depress.'};
idxAffect = find(contains(fdt.quest.names.total, questNamesAff));
pcaResults.data.affectQ.scores = fdt.quest.finalScores.total(:,idxAffect);

% Pull out interoceptive questionnaire scores
questNamesIntero = {'bpq', 'maia', 'pcsR', 'pvqR'};
pcaResults.data.interoQ.names = {'Body perception', 'Intero. awareness', 'Br. catastophising', 'Breathing vigilance'};
pcaResults.data.interoQ.namesShort = {'Body perc.', 'Int. aware.', 'Catastroph.', 'Vigilance'};
idxIntero = find(contains(fdt.quest.names.total, questNamesIntero));
pcaResults.data.interoQ.scores = fdt.quest.finalScores.total(:,idxIntero);

% Pull out FDT scores
pcaResults.data.FDT.names = {'Perceptual threshold', 'Decision bias', 'Metacognitive bias', 'Meta. performance'};
pcaResults.data.FDT.namesShort = {'Br. thresh.', 'Dec. bias', 'Meta. bias', 'Meta. perf.'};
pcaResults.data.FDT.scores = fdt.params.values.total;

% Check PPID order for model data against aIns activity
if ~isempty(setdiff(aIns.predPos(:,1), str2double(models.options.PPIDs.total)))
    error('PPIDs from model fits do not match... Exiting');
end

% Pull out aIns activity
pcaResults.data.aIns.names = {'Positive predictions', 'Negative predictions', 'Positive errors', 'Negative errors'};
pcaResults.data.aIns.namesShort = {'Pos. pred.', 'Neg. pred', 'Pos. err.', 'Neg. err.'};
pcaResults.data.aIns.scores(:,1) = aIns.predPos(:,2);
pcaResults.data.aIns.scores(:,2) = aIns.predNeg(:,2);
pcaResults.data.aIns.scores(:,3) = aIns.errPos(:,2);
pcaResults.data.aIns.scores(:,4) = aIns.errNeg(:,2);

% Create a full matrix
pcaResults.matrix.names = {pcaResults.data.affectQ.names{:}, pcaResults.data.interoQ.names{:}, pcaResults.data.FDT.names{:}, pcaResults.data.aIns.names{:}};
pcaResults.matrix.namesShort = {pcaResults.data.affectQ.namesShort{:}, pcaResults.data.interoQ.namesShort{:}, pcaResults.data.FDT.namesShort{:}, pcaResults.data.aIns.namesShort{:}};
pcaResults.matrix.scoresFull = [pcaResults.data.affectQ.scores, pcaResults.data.interoQ.scores, pcaResults.data.FDT.scores, pcaResults.data.aIns.scores];


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXCLUDE NECESSARY SUBJECTS WITH POOR MODEL FITS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify indices for included PPIDs
pcaResults.options.PPIDidx.totalEx = models.est.goodness.totalEx;
pcaResults.options.PPIDidx.lowEx = models.est.goodness.lowEx;
pcaResults.options.PPIDidx.modEx = models.est.goodness.modEx;
pcaResults.options.PPIDidx.lowNew = 1:length(pcaResults.options.PPIDidx.lowEx);
pcaResults.options.PPIDidx.modNew = (length(pcaResults.options.PPIDidx.lowEx)+1):length(pcaResults.options.PPIDidx.totalEx);

% Remove excluded PPIDs from total matrix
pcaResults.matrix.scores = pcaResults.matrix.scoresFull(pcaResults.options.PPIDidx.totalEx,:);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CALCULATE CORRELATION MATRIX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate the correlation matrix
[pcaResults.corrcoef.R, pcaResults.corrcoef.p] = corrcoef(pcaResults.matrix.scores);
% [pcaResults.corrcoef.R, pcaResults.corrcoef.p] = corr(pcaResults.matrix.scores, 'Type', 'Spearman'); % Robust non-parametric correlation matrix to check results

% Calculate FDR-corrected p-values
% First remove the lower diagonal and nan the values of 1
ii = ones(size(pcaResults.corrcoef.p));
idx = triu(ii);
pcaResults.corrcoef.pFDR = pcaResults.corrcoef.p;
pcaResults.corrcoef.pFDR(~idx) = NaN;
pcaResults.corrcoef.pFDR(pcaResults.corrcoef.pFDR == 1) = NaN;
pFDRvec = pcaResults.corrcoef.pFDR(:);
pFDR = mafdr(pFDRvec);
c = 1;

% Put FDR values back into matrix
for a = 1:length(pcaResults.corrcoef.p)
    pcaResults.corrcoef.pFDR(1:16,a) = pFDR(c:(c+15));
    c = c + 16;
end

% Add reflected side of the matrix
oppFDR = rot90(flip(pcaResults.corrcoef.pFDR, 1), -1);
[idxRow, idxCol] = find(~isnan(oppFDR));
for a = 1:length(idxRow)
    pcaResults.corrcoef.pFDR(idxRow(a),idxCol(a)) = oppFDR(idxRow(a),idxCol(a));
end
pcaResults.corrcoef.pFDR(isnan(pcaResults.corrcoef.pFDR)) = 1;


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN PCA ON DATA MATRIX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Z-score matrix
pcaResults.matrix.Zscores = zscore(pcaResults.matrix.scores);

% Run PCA
[pcaResults.coeff, pcaResults.score, pcaResults.latent, ~, pcaResults.explained, ~] = pca(pcaResults.matrix.Zscores);
pcaResults.explained_relative = pcaResults.explained ./ [100.0; 100.0 - cumsum(pcaResults.explained(1:end-1))];

% Calculate a correlation matrix between scores and measures
pcaResults.scoresVmeasuresR = corr(pcaResults.score, pcaResults.matrix.Zscores);

%%
% 
% n_permutations = 1000;
% for n_perm = 1:n_permutations
%     for comp = 1:size(pcaResults.matrix.Zscores,2)
%         % Regress out previous (unshuffled) components
%         %zscores = pcaResults.matrix.Zscores - pcaResults.matrix.Zscores * pcaResults.coeff(:, 1:comp-1) * pcaResults.coeff(:, 1:comp-1)';
%         %zscores = pcaResults.matrix.Zscores;
%         % Shuffle the columns independently
%         %for column = 1:size(zscores, 2)
%         %   idx = randperm(size(zscores, 1));
%         %   zscores(:, column) = zscores(idx, column);
%         %end
%         % Regress out previous (unshuffled) components
%         %zscores = zscores - zscores * pcaResults.coeff(:, 1:comp-1) * pcaResults.coeff(:, 1:comp-1)';
%         scores = pcaResults.score(:, comp:end);
%         coeff = pcaResults.coeff(:, comp:end);
%         latent = pcaResults.latent(comp:end);
%         weights = sqrt(latent * (size(scores,1) - 1));
%         scores = scores ./ weights';
%         coeff = coeff .* weights';
%         % Shuffle the columns independently
% %         for column = 1:size(scores, 2)
% %            idx = randperm(size(scores, 1));
% %            scores(:, column) = scores(idx, column);
% %         end
%         for column = 1:size(coeff, 1)
%            idx = randperm(size(coeff, 2));
%            coeff(column, :) = coeff(column, idx);
%         end
%         zscores = scores * coeff';
%         % Compute next PC
%         [coeff, scores, latent, ~, ~, ~] = pca(zscores, 'NumComponents', 1);
%         pcaResults.permute.coeff(n_perm,:,comp) = coeff(:,1);
%         pcaResults.permute.scores(n_perm,:,comp) = scores(:,1);
%         pcaResults.permute.latent(n_perm,comp) = latent(1);
%     end
% end
% 
% figure(); hold on; plot(pcaResults.permute.latent'); plot(pcaResults.latent, 'k', 'LineWidth', 3)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN PERMUTATIONS AND CALCULATE NULL CONFIDENCE INTERVALS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set and save the seed
rng(0, 'twister')
pcaResults.options.seed = rng;

% Shuffle and PCA!
for a = 1:1000
    % Shuffle the columns independently
    for column = 1:size(pcaResults.matrix.Zscores,2)
        idx = randperm(size(pcaResults.matrix.Zscores,1));
        pcaResults.permute.Zscores{a}(:,column) = pcaResults.matrix.Zscores(idx,column);
    end
    % Run the PCA on shuffled data
    [pcaResults.permute.coeff(:,:,a), pcaResults.permute.scores(:,:,a), pcaResults.permute.latent(a,:), ~, pcaResults.permute.explained(a,:), ~] = pca(pcaResults.permute.Zscores{a});
    explained = pcaResults.permute.explained(a,:);
    pcaResults.permute.explained_relative(a,:) = explained ./ [100.0, 100.0 - cumsum(explained(1:end-1))];
end

% Calculate permutation means and ste
pcaResults.permute.explainedMean = nanmean(pcaResults.permute.explained);
pcaResults.permute.explainedSte = nanstd(pcaResults.permute.explained) / sqrt(length(pcaResults.permute.explained));

% Calculate confidence intervals and p-values for the observed variance explained
for a = 1:length(pcaResults.explained)
    pcaResults.permute.explainedCI(a,:) = prctile(sort(pcaResults.permute.explained(:,a), 1, 'descend', 'ComparisonMethod', 'abs'), [2.5, 97.5]);
    pcaResults.explained_pvalues(a) = (sum(pcaResults.permute.explained(:,a) > pcaResults.explained(a)) + 1) / 1001;
end

% Determine number of significant components
pcaResults.numComponents = sum(pcaResults.explained_pvalues < (0.05/16));


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CALCULATE GROUP DIFFERENCE SCORES FOR SIGNIFICANT COMPONENTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compare each significant component
for a = 1:pcaResults.numComponents
    
    % Create summary metrics for scores
    pcaResults.groupComp.summary{a}.lowMean = mean(pcaResults.score(pcaResults.options.PPIDidx.lowNew,a));
    pcaResults.groupComp.summary{a}.lowStd = std(pcaResults.score(pcaResults.options.PPIDidx.lowNew,a));
    pcaResults.groupComp.summary{a}.lowSte = std(pcaResults.score(pcaResults.options.PPIDidx.lowNew,a)) / sqrt(length(pcaResults.options.PPIDidx.lowNew));
    pcaResults.groupComp.summary{a}.lowMed = median(pcaResults.score(pcaResults.options.PPIDidx.lowNew,a));
    pcaResults.groupComp.summary{a}.lowIqr = iqr(pcaResults.score(pcaResults.options.PPIDidx.lowNew,a));
    pcaResults.groupComp.summary{a}.modMean = mean(pcaResults.score(pcaResults.options.PPIDidx.modNew,a));
    pcaResults.groupComp.summary{a}.modStd = std(pcaResults.score(pcaResults.options.PPIDidx.modNew,a));
    pcaResults.groupComp.summary{a}.modSte = std(pcaResults.score(pcaResults.options.PPIDidx.modNew,a)) / sqrt(length(pcaResults.options.PPIDidx.modNew));
    pcaResults.groupComp.summary{a}.modMed = median(pcaResults.score(pcaResults.options.PPIDidx.modNew,a));
    pcaResults.groupComp.summary{a}.modIqr = iqr(pcaResults.score(pcaResults.options.PPIDidx.modNew,a));
    
    % Compare the groups
    pcaResults.groupComp.alphaCorrected = 0.05 / pcaResults.numComponents;
    pcaResults.groupComp.tails = 'two';
    pcaResults.groupComp.adtests{a}.low = adtest(pcaResults.score(pcaResults.options.PPIDidx.lowNew,a));
    pcaResults.groupComp.adtests{a}.mod = adtest(pcaResults.score(pcaResults.options.PPIDidx.modNew,a));
    if pcaResults.groupComp.adtests{a}.low == 1 || pcaResults.groupComp.adtests{a}.mod == 1
        pcaResults.groupComp.component{a}.test = 'Wcxn_rankSum';
        [pcaResults.groupComp.component{a}.p, pcaResults.groupComp.component{a}.h, pcaResults.groupComp.component{a}.stats] = ranksum(pcaResults.score(pcaResults.options.PPIDidx.lowNew,a), pcaResults.score(pcaResults.options.PPIDidx.modNew,a), 'alpha', pcaResults.groupComp.alphaCorrected);
    else
        comp.models.groupComp.component{a}.test = 'tTest2';
        [pcaResults.groupComp.component{a}.h, pcaResults.groupComp.component{a}.p, pcaResults.groupComp.component{a}.ci, pcaResults.groupComp.component{a}.stats] = ttest2(pcaResults.score(pcaResults.options.PPIDidx.lowNew,a), pcaResults.score(pcaResults.options.PPIDidx.modNew,a), 'Alpha', pcaResults.groupComp.alphaCorrected);
    end
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE OUTPUTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save(pcaResults.options.saveNames.pcaResults, 'pcaResults');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE FIGURES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Make the figures
pbihb_figuresMatlab_multiModalPCA(pcaResults.options)


end