############################################################################################

# PBIHB: RUN RANDOMISE ANALYSES

############################################################################################

# DESCRIPTION:
# This function runs randomise for the small volume analyses of the PBIHB study
# Usage: ./runRandomise $data $scripts $higherL $lowerL $mask
#	data = path to main data folder
#	scripts = path to PBIHB scripts folder
#	higherL = name of higher level design ('groups' 'groupsEx' 'regress' or 'explore')
#	lowerL = name of lower level design ('basic' 'model' or 'modelFactors')
#	conName = name of lower level contrast (number or name e.g. con0001 or cueTot)
#	mask = name of mask to use ('PAG' or 'full')

############################################################################################

# Define the function
runRandomise(){

	# Define local variables from inputs
	local data=$1
	local scripts=$2
	local higherL=$3
	local lowerL=$4
	local conName=$5
	local mask=$6
	
	# make sure filename supplied as command line arg else die
	[ $# -eq 0 ] && { echo "Usage: $0 data scripts higherL lowerL conName mask"; exit 1; }

	# Create full path for output data
	outputDir=$(find "${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_${higherL}_${lowerL}/" -name "*${conName}*" | head -n1)
	outputData=${outputDir}/randomise_${conName}/randomise_${conName}

	# Create full path for input data
	inputData=${outputDir}/lowerLevel_*${conName}_*4Dfile.nii.gz

	# Create full paths for design specs
	if [ "${higherL}" = "groups" ]; then
		designMatTxt=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupDiffModel.txt
		designMat=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupDiffModel.mat
		designConTxt=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupDiffModel_con.txt
		designCon=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupDiffModel_con.con
	elif [ "${higherL}" = "groupsEx" ]; then
		designMatTxt=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupDiffModelEx.txt
		designMat=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupDiffModelEx.mat
		designConTxt=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupDiffModel_conEx.txt
		designCon=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupDiffModel_conEx.con
	elif [ "${higherL}" = "regress" ]; then
		designMatTxt=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupRegressModel.txt
		designMat=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupRegressModel.mat
		designConTxt=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupRegressModel_con.txt
		designCon=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupRegressModel_con.con
	elif [ "${higherL}" = "explore" ]; then
		designMatTxt=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupExploreModel.txt
		designMat=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupExploreModel.mat
		designConTxt=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupExploreModel_con.txt
		designCon=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupGLMs/PBIHB_groupExploreModel_con.con
	fi

	# Specify the mask
	if [ "${mask}" = "full" ]; then
		mask=${scripts}/pbihb_masks/PBIHB_aInsBN_PAG_mask.nii.gz
	elif [ "${mask}" = "PAG" ]; then
		mask=${scripts}/pbihb_masks/PBIHB_PAG_mask.nii.gz
	fi
	
	echo ---------------------------------------
	echo RANDOMISE ANALYSIS
	echo ---------------------------------------

	# Print variables to screen
	echo input data = $inputData
	echo output data = $outputData
	echo design.mat = $designMat
	echo design.con = $designCon
	echo mask = $mask

	echo output directory = $outputDir

	# Run randomise analysis
	echo Running randomise...
	randomise_parallel -i $inputData -o $outputData -d $designMat -t $designCon -m $mask -T

	echo ---------------------------------------
	echo RANDOMISE ANALYSIS FINISHED!
	echo ---------------------------------------

}

# Invoke the function and pass arguments
runRandomise $1 $2 $3 $4 $5 $6
