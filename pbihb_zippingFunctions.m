%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% BREATHING SCANNER TASK: UNZIP SMOOTHED FUNCTIONAL FILE %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 26/04/2020
% -------------------------------------------------------------------------
% TO RUN:    pbihb_zippingFunctions(options, type)
% INPUTS:    options    = PPID-specific matrix output from the
%                         pbihb_setOptions function
%            type       = 'zip' or 'unzip'
% OUTPUTS:   zipped or unzipped (smoothed) EPI file
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script checks for an unzips the smoothed 4D functional file from the
% imaging data collected in the PBIHB study.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_zippingFunctions(options, type)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify files
sEPI = (fullfile(options.paths.image_directory, options.names.images.task_EPI_stdSm));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% UNZIP SMOOTHED EPI IF SPECIFIED
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Unzip the file if it is zipped
if strcmp(type, 'unzip') == 1 && isfile([sEPI, '.gz']) && ~isfile(sEPI)
    disp('Unzipping smoothed standard EPI file...');
    gunzip([sEPI, '.gz'], options.paths.image_directory)
    disp('...Finished!');
elseif strcmp(type, 'unzip') == 1 && ~isfile([sEPI, '.gz'])
    disp('EXITING UNZIP: No smoothed standard space EPI scan exists');
    return
elseif strcmp(type, 'unzip') == 1 && isfile(sEPI)
    disp('EXITING UNZIP: Unzipped smoothed standard space EPI scan exists');
    return
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ZIP SMOOTHED EPI IF SPECIFIED
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Zip the file if it is unzipped
if strcmp(type, 'zip') == 1 && isfile(sEPI) && isfile([sEPI, '.gz'])
    disp('Deleting unzipped standard EPI file...');
    delete(sEPI)
    disp('... Finished!');
elseif strcmp(type, 'zip') == 1 && isfile(sEPI) && ~isfile([sEPI, '.gz'])
    disp('Zipping smoothed standard EPI file...');
    system(['gzip ', sEPI]);
    disp('... Finished!');
elseif strcmp(type, 'zip') == 1 && ~isfile(sEPI)
    disp('EXITING ZIP: No unzipped smoothed standard space EPI scan exists');
    return
end

end