############################################################################################

# SIMULATIONS AND ANALYSES FOR PBIHB LEARNING TASK: TO RUN ON EULER

############################################################################################

# DESCRIPTION:
# This is the master script for the simulation and empirical analyses based around the 
# questionnaires, filter detection task (both conducted in the behavioural lab) as well as
# the learning task (conducted in the scanner) of the PBIHB study.

############################################################################################

# INSTRUCTIONS:
# 1) COPY ALL REQUIRED CODE TO EULER:
#	- Copy the main script folder for the study to somewhere on Euler
#	  e.g. bsub -W 6:00 -R "rusage[mem=20000]" -o copyFiles cp -r /cluster/work/tnu/ofaull/PBIHB_setup/ /cluster/home/ofaull/studies/PBIHB
#	- Download and set up JAGS (see 'InstallEuler.md' file in HMeta-d-installEuler)
# 2) COPY ALL DATA TO EULER:
#	- Copy all minimally preprocessed data to somewhere on Euler
# 3) CHANGE THE PATHS TO DATA AND SCRIPTS:
#	- Change the paths in pbihb_setOptions.m to the location of both scripts and data
#	- Change the paths in the script below to the location of both scripts and data
# 3) RUN THE CODE:
#	- type pbihb_runEuler.sh in the terminal **from the main script folder** to run

############################################################################################
############################################################################################

# SET THE PATHS - ADJUST THIS SECTION ONLY

data=/cluster/work/tnu/ofaull/PBIHB_anon/fmri
#data=/cluster/home/ofaull/studies/PBIHB/fmri
scripts=/cluster/home/ofaull/PBIHB/pbihb_scanner_analysis

############################################################################################
############################################################################################

# SPECIFY THE VARIABLES

# Load correct Matlab and FSL versions
#module purge
module load new
module load matlab/R2017b
module load gcc/4.8.2 openblas/0.2.13_seq qt/4.8.4 vtk/6.1.0 fsl/6.0.1
module load r/3.3.0
#module list

# Set PPIDs to analyse
subj=(5104 5123 5112 5124 5113 5130 5105 5107 5119 5109 5126 5118 5120 5108 5101 \
5114 5127 5129 5117 5128 5103 5111 5122 5110 5115 5116 5121 5106 5125 5102 \
5224 5205 5201 5229 5208 5227 5210 5228 5217 5202 5204 5214 5226 5221 5222 \
5230 5206 5207 5216 5212 5223 5203 5215 5218 5213 5220 5219 5211 5225 5209)

############################################################################################
# ANALYSES TO RUN

# Fit the models
bsub -J pbihb_modelFits -W 4:00 -R "rusage[mem=20000]" -o pbihb_euler_logFiles/PBIHB_fitModels_rep \
	matlab -singleCompThread -r "pbihb_fitModels('euler')" ;

# Simulate from the model priors
bsub -J pbihb_modelSims -w "done(pbihb_modelFits)" -W 10:00 -R "rusage[mem=20000]" -o pbihb_euler_logFiles/PBIHB_simModels_rep \
	matlab -singleCompThread -r "pbihb_simModels('euler')" ;

# Set single subject preprocessing and analyses running for task
for a in ${subj[@]}; do
	bsub -J preproc_${a} -w "done(pbihb_modelFits)" -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_${a}_euler_task \
		"./pbihb_imagePrep.sh $a $data $scripts ; \
		./pbihb_fslPreproc.sh $a $data $scripts task ; \
		./pbihb_fslNorm.sh $a $data $scripts task ; \
		./pbihb_fslSmooth.sh $a $data $scripts task 3" ; \
	bsub -J spm_${a} -w "done(preproc_${a})" -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_${a}_euler_task \
		matlab -singleCompThread -r "pbihb_taskLowerLevel('${a}', 'euler')" ;
done

# Set the group analyses running for the task
bsub -J groupTask -w "done(spm_*)" -W 12:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_euler_groupTask_rep \
	matlab -singleCompThread -r "pbihb_taskGroup('euler')" ;

# Set the randomise analyses running
bsub -w "done(groupTask)" -o pbihb_euler_logFiles/PBIHB_runRandomise_rep "./pbihb_eulerRandomise.sh $data $scripts" ;

# Set the insula contrast extraction running
bsub -J pbihb_maxIns -w "done(groupTask)" -o pbihb_euler_logFiles/PBIHB_getMaxInsulaCons_rep "./pbihb_getMaxInsula.sh $data ${subj[@]}" ;

# Set the group analyses running for the non-fMRI data
bsub -J pbihb_nonMRI -w "done(spm_*)" -W 4:00 -R "rusage[mem=10000]" -o pbihb_euler_logFiles/PBIHB_groupResultsNonMRI_rep \
	matlab -singleCompThread -r "pbihb_compGroups('euler')" ; \
bsub -w "done(pbihb_nonMRI)" -o pbihb_euler_logFiles/PBIHB_groupResultsNonMRI_rep \
	cp pbihb_euler_logFiles/PBIHB_groupResultsNonMRI_rep $data/PBIHB_BLT_results/PBIHB_groupComp/ ;

# Set the FDT and questionnaire analyses running
bsub -J pbihb_FDT -W 4:00 -R "rusage[mem=10000]" -o pbihb_euler_logFiles/PBIHB_groupResultsFDT_rep \
	matlab -singleCompThread -r "pbihb_FDT_analysis('euler')" ;

# Set the combined analysis running
bsub -J pbihb_multi -w "done(pbihb_maxIns)" -R "rusage[mem=10000]" -o pbihb_euler_logFiles/PBIHB_multiModalPCA_rep \
	matlab -singleCompThread -r "pbihb_multiModalPCA('euler')" ;

############################################################################################
# WORKING TASK ANALYSIS PIPELINE:

# Fit the models
#bsub -J pbihb_modelFits -W 4:00 -R "rusage[mem=20000]" -o pbihb_euler_logFiles/PBIHB_fitModels \
#	matlab -singleCompThread -r "pbihb_fitModels('euler')" ;

# Simulate from the model priors
#bsub -J pbihb_modelSims -w "done(pbihb_modelFits)" -W 10:00 -R "rusage[mem=20000]" -o pbihb_euler_logFiles/PBIHB_simModels \
#	matlab -singleCompThread -r "pbihb_simModels('euler')" ;

# Set single subject preprocessing and analyses running for task
#for a in ${subj[@]}; do
#	bsub -J preproc_${a} -w "done(pbihb_modelFits)" -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_${a}_euler_task \
#		"./pbihb_imagePrep.sh $a $data $scripts ; \
#		./pbihb_fslPreproc.sh $a $data $scripts task ; \
#		./pbihb_fslNorm.sh $a $data $scripts task ; \
#		./pbihb_fslSmooth.sh $a $data $scripts task 3" ; \
#	bsub -J spm_${a} -w "done(preproc_${a})" -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_${a}_euler_task \
#		matlab -singleCompThread -r "pbihb_taskLowerLevel('${a}', 'euler')" ;
#done

# Set the group analyses running for the task
#bsub -J groupTask -w "done(spm_*)" -W 12:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_euler_groupTask \
#	matlab -singleCompThread -r "pbihb_taskGroup('euler')" ;

# Set the randomise analyses running
#bsub -w "done(groupTask)" -o pbihb_euler_logFiles/PBIHB_runRandomise "./pbihb_eulerRandomise.sh $data $scripts" ;

# Set the insula contrast extraction running
#bsub -J pbihb_maxIns -w "done(groupTask)" -o pbihb_euler_logFiles/PBIHB_getMaxInsulaCons "./pbihb_getMaxInsula.sh $data ${subj[@]}" ;

# Set the group analyses running for the non-fMRI data
#bsub -J pbihb_nonMRI -w "done(spm_*)" -W 4:00 -R "rusage[mem=10000]" -o pbihb_euler_logFiles/PBIHB_groupResultsNonMRI \
#	matlab -singleCompThread -r "pbihb_compGroups('euler')" ; \
#bsub -w "done(pbihb_nonMRI)" -o pbihb_euler_logFiles/PBIHB_groupResultsNonMRI \
#	cp pbihb_euler_logFiles/PBIHB_groupResultsNonMRI $data/PBIHB_BLT_results/PBIHB_groupComp/ ;

# Set the FDT and questionnaire analyses running
#bsub -J pbihb_FDT -W 12:00 -R "rusage[mem=10000]" -o pbihb_euler_logFiles/PBIHB_groupResultsFDT \
#	matlab -singleCompThread -r "pbihb_FDT_analysis('euler')" ;

# Set the combined analysis running
#bsub -J pbihb_multi -w "done(pbihb_maxIns)" -R "rusage[mem=10000]" -o pbihb_euler_logFiles/PBIHB_multiModalPCA \
#	matlab -singleCompThread -r "pbihb_multiModalPCA('euler')" ;

############################################################################################
# WORKING REST ANALYSIS PIPELINE:

# Set single subject preprocessing running for rest
#for a in ${subjRest[@]}; do
#	bsub -W 6:00 -R "rusage[mem=20000]" -o pbihb_euler_logFiles/preproc_PBIHB_${a}_euler_rest \
#		"./pbihb_imagePrep.sh $a $data $scripts ; \
#		./pbihb_fslPreproc.sh $a $data $scripts rest ;
#		./pbihb_fslNorm.sh $a $data $scripts rest ; \
#		./pbihb_fslSmooth.sh $a $data $scripts rest 3" ;
#done

############################################################################################

# PLAN

# Run image preparation - bash for FSL
# Run preprocessing - bash for FSL
# Run physiology and physio scripts - matlab

# Run the pilot data fits and simulations - matlab
# Run the actual data fits - matlab
# Set up the model-based single subject GLMs and fit model - matlab
# Set up the model-based group GLM and fit model - matlab
# Run parametric statistics for model-based group GLM - matlab
# Run non-parametric statistics for model-based group GLM - bash for FSL

# Set up the standard single subject GLMs and fit model - matlab
# Set up the standard group GLM and fit model - matlab
# Run parametric statistics for standard group GLM - matlab
# Run non-parametric statistics for standard group GLM - bash for FSL


# EXTRA COMMANDS
# bsub -W 10:00 -R "rusage[mem=20000]" -o pbihb_euler_logFiles/preproc_PBIHB_${a}_euler_task matlab -singleCompThread -r "pbihb_preprocEuler('${a}', 'task')" ;
# bsub -W 10:00 -R "rusage[mem=20000]" -o pbihb_euler_logFiles/preproc_PBIHB_${a}_euler_rest matlab -singleCompThread -r "pbihb_preprocEuler('${a}', 'rest')" ;
# "./pbihb_fslPreproc.sh $a $data $scripts rest"

# Set test fix analyses running
#for a in ${subj[@]}; do
#	bsub -W 6:00 -R "rusage[mem=20000]" -o pbihb_euler_logFiles/fix_PBIHB_${a}_task \
#		"./pbihb_runFix.sh $a $data $scripts task" ;
#done
