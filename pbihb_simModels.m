%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% BREATHING SCANNER TASK: SIM MODELS %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 26/03/2020
% -------------------------------------------------------------------------
% TO RUN:   simSummary  = pbihb_simModels(location)
% INPUTS:   location    = 'local' or 'euler'
% OUTPUTS:  simSummary  = Structure with summary of simulations 
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script runs the model simulations for the 7 Tesla functional data 
% collected in the PBIHB study, based on the distribution of maximum 
% likelihood estimates from the pilot participants. The models used are 
% Rescorla Wagner, 2-level HGF and 3-level HGF.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function simSummary = pbihb_simModels(location)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PRINT MESSAGE TO SCREEN
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Print message to screen about simulations
fprintf('\n\n\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('SIMULATION OUTPUT BELOW:\n');
disp('-------------------------------------------------------------------------------------------');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD PRE-PROCESSED DATA FROM PILOT PARTICIPANTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Secify options for whole group
simSummary.options = pbihb_setOptions('all', location);

% Save the pilot data matrix
if isfile(simSummary.options.saveNames.pilotData)
    pilots = load(simSummary.options.saveNames.pilotData);
else
    error('No pre-processed pilot data matrix... Exiting');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN SIMULATIONS USING PILOT FITS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set the seed
rng(0, 'twister')
simSummary.options.seeds.init = rng;

% Specify the seeds to use for simulations
for a = 1:simSummary.options.sims.numGroups
    simSummary.options.seeds.sims(a) = round(rand * 10000);
end

% Specify ze values
zeta = simSummary.options.sims.ze;

% Run simulations
for ze = 1:length(zeta)
    for loop = 1:simSummary.options.sims.numGroups
        sims.zetaValues{ze}.simRuns{loop} = pbihb_prepModels_master('data', pilots, ...
            'saveDir', simSummary.options.paths.group_directory_models, ...
            'pilots', 1, 'pilot_sub', 8, 'ML_est', 1, 'MapOptQ', 0, ...
            'nSub', 60, 'plot', 0, 'StopSim', 0, ...
            'zeta', zeta(ze), 'seed', simSummary.options.seeds.sims(loop));
    end
end

% Calculate averages over seeds and add setup info
simSummary.avg = modelPrep_avgSimResults(sims, zeta, simSummary.options.sims.numGroups);
simSummary.setup = sims.zetaValues{1}.simRuns{1}.setup;
simSummary.SimSpecs = sims.zetaValues{1}.simRuns{1}.SimSpecs;
simSummary.ModelSpace = sims.zetaValues{1}.simRuns{1}.ModelSpace;
simSummary.chanceThresh = sims.zetaValues{ze}.simRuns{1}.ModelComparison.Classification.chanceThresh;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE THE RESULTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Save simulation summaries
save(simSummary.options.saveNames.modelSimSummary, 'simSummary');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLOT THE RESULTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Plot results
for ze = 1:length(zeta)
    % Plot confusion matrix
    modelPrep_plotConfusionMatrix(simSummary.avg.zetas{ze}.LME.winPerc.avg, 'LME Winner Classification',...
        simSummary.ModelSpace, simSummary.chanceThresh, ...
        simSummary.avg.zetas{ze}.LME.acc.balanced)
    saveNameCM = [simSummary.options.saveNames.modelSimFigCM, '_ze', num2str(zeta(ze))];
    print(saveNameCM, '-dtiff');
    close;
    % Plot perceptual model parameter recovery
    modelPrep_plotParamRecovery(simSummary, simSummary.avg.zetas{ze}.ParamRec.param, 'prc')
    saveNameParamPrc = [simSummary.options.saveNames.modelSimFigParamPrc, '_ze', num2str(zeta(ze))];
    print(saveNameParamPrc, '-dtiff');
    close;
    % Plot obervation model parameter recovery
    modelPrep_plotParamRecovery(simSummary, simSummary.avg.zetas{ze}.ParamRec.param, 'obs')
    saveNameParamObs = [simSummary.options.saveNames.modelSimFigParamObs, '_ze', num2str(zeta(ze))];
    print(saveNameParamObs, '-dtiff');
    close;
end
    

end