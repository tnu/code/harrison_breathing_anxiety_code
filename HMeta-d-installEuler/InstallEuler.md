Download source files from
<https://sourceforge.net/projects/mcmc-jags/files/JAGS/3.x/Source/>

```shell
tar -xzvf JAGS-3.4.0.tar.gz
cd JAGS-3.4.0/

# Set up compiler
module load gcc openblas
module list

# Build and install!
mkdir "${HOME}/jags"
LIBS=-lopenblas ./configure --prefix="${HOME}/jags"
make
make install
```

# Open matjags.m (in HMeta-d toolbox) and re-set the location of jags to where you have downloaded it on Euler,
# by changing the 2 instances of the following:
# original line: jagsPrefix = ''; %sprintf('/usr/local/bin/');
# example change to: jagsPrefix = sprintf('/cluster/home/ofaull/jags/bin/');
