%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% BREATHING SCANNER TASK: PHYSIOLOGY PREPROCESSING %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 06/02/2019
% -------------------------------------------------------------------------
% TO RUN:   [physValues, physTraces] = pbihb_physiology(options, varargin)
% INPUTS:   options           = PPID-specific matrix output from the
%                               pbihb_setOptions function
%           varargin          = 'test' or 'full'
% OUTPUTS:  physValues        = Structure with physiological traces from 
%                               the learning task + summary
%           physTraces        = Structure with physiological values from
%                               the learning task
%           figures           = Summary figures are generated to display 
%                               the correlation matrices for important 
%                               physiological variables vs. motion traces
% -------------------------------------------------------------------------
% DESCRIPTION:
% This function is designed to process the physiology data collected for
% the PBIHB study. This requires a matlab output matrix from a preprocessed
% version of the LabChart file recorded during scanning. Key preprocessing
% checks need to be made before this analysis (see details below).
% -------------------------------------------------------------------------
% PREPROCESSING CHECKS BEFORE/DURING EXPORT OF LABCHART FILE
%   1) Check lag adjusted for CO2 by -10.5 seconds
%   2) Check lag adjusted for O2 by -10.1 seconds
%   3) Check event markers for scanner triggers (or add 'START' comment)
%   4) Check event markers for maximum end-tidal triggers
%   5) Check event markers for maximum inspiratory pressure triggers
%   6) Check event markers for maximum breathing belt triggers
%   7) Check detection and measurements for breathing rate and depth
%   8) Add comments to all channels at each stimulus onset (= 'RESISTANCE')
%   9) Check output channels:
%       - data(1,:) = time in seconds
%       - data(2,:) = raw scanner triggers
%       - data(3,:) = inspiratory pressure trace
%       - data(4,:) = respiratory bellows trace
%       - data(5,:) = carbon dioxide trace
%       - data(6,:) = oxygen trace
%       - data(7,:) = processed triggers from scanner
%       - data(8,:) = processed end tidal triggers
%       - data(9,:) = processed inspiratory peak triggers
%       - data(10,:) = processed breathing trace peak triggers
%       - data(11,:) = processed breathing rate values
%       - data(12,:) = processed breathing depth values
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [physValues, physTraces] = pbihb_physiology(options, varargin)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD AND SETUP PHYSIOLOGY --> CHECK LABCHART CHANNELS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

% Save options to matrices
physTraces.options = options;
physValues.options = options;
physTraces.type = 'task';
physValues.type = 'task';

% Load physiology files
labChart.data = dlmread(fullfile(physTraces.options.paths.physiology_directory, physTraces.options.names.labChartData.task));
fileID = fopen(fullfile(physTraces.options.paths.physiology_directory, physTraces.options.names.labChartComments.task));
physiology.labChart.comments = textscan(fileID, '%n %s');
fclose(fileID);

% Replace any NaNs in data with previous number (in case of out of range)
for c = 1:(size(labChart.data,2))
    % Find first non-NaN row
    indxFirst = find(~isnan(labChart.data(:,c)),1,'first');
    if ~isempty(indxFirst) == 1
        % Find all NaN rows
        indxNaN = find(isnan(labChart.data(:,c)));
        % Find NaN rows beyond first non-NaN
        indx = indxNaN(indxNaN > indxFirst);
        % For each of these, copy previous value
        for r = (indx(:))'
            labChart.data(r,c) = labChart.data(r-1,c);
        end
    end
end

% Sort physiology data according to LabChart channels
physiology.time = labChart.data(:,1);
physiology.inspiratoryPressure.rawTrace = labChart.data(:,3);
physiology.breathing.rawTrace = labChart.data(:,4);
physiology.endTidalCO2.rawTrace = labChart.data(:,5);
physiology.endTidalO2.rawTrace = labChart.data(:,6);
physiology.MRItrigs.fromScanner = labChart.data(:,7);
physiology.endTidalTrigs.peakTrigs = labChart.data(:,8);
physiology.inspiratoryTrigs.peakTrigs = labChart.data(:,9);
physiology.breathingTrigs.peakTrigs = labChart.data(:,10);
physiology.breathing.rate.rawTrace = labChart.data(:,11);
physiology.breathing.depth.rawTrace = labChart.data(:,12);
physiology.resistance.onsetTime = physiology.labChart.comments{1};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD TASK DATA FOR TIMINGS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

physValues.values.behaviour = load(fullfile(options.paths.task_directory, options.names.taskData));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IDENTIFY MRI TRIGGERS --> USE ACTUAL TRIGGERS IF PRESENT OR MANUAL INSERT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

% Locate the start index for the MRI trigs
physiology.MRItrigs.trigs = physiology.MRItrigs.fromScanner;
physiology.MRItrigs.trigIndex = find(physiology.MRItrigs.trigs == 1);
physiology.MRItrigs.startIndex = physiology.MRItrigs.trigIndex(1);

% Check scanner triggers match the number of volumes and LabChart comments to find the other triggers
if length(physiology.MRItrigs.trigIndex) == physTraces.options.taskScan.vols && strcmp(physiology.labChart.comments{1,2}(1,1), 'RESISTANCE') == 1
    physiology.MRItrigs.finishIndex = physiology.MRItrigs.trigIndex(end) + physTraces.options.taskScan.tr * physTraces.options.physiology.samplingRate; % Add extra TR to final trigger index for the end of the scan
elseif strcmp(physiology.labChart.comments{1,2}(1,1), 'START') == 1 % If the first comment is 'START', work from the first comment
    % Take the start index from the first comment
    physiology.MRItrigs.startTime = physiology.resistance.onsetTime(1);
    physiology.MRItrigs.startIndex = find(physiology.time == physiology.MRItrigs.startTime);
    % Remove the first comment from the resistance onset index
    physiology.resistance.onsetTime = physiology.resistance.onsetTime(2:end);
    % Manually insert the finish index from the scanner timings
    physiology.MRItrigs.finishIndex = round(physiology.MRItrigs.startIndex + physTraces.options.taskScan.tr * physTraces.options.taskScan.vols * physTraces.options.physiology.samplingRate); % Start + TR x volumes x sampling rate
    % Manually insert a trigger every TR
    physiology.MRItrigs.trigs = zeros([length(physiology.MRItrigs.fromScanner) 1]);
    physiology.MRItrigs.trigs(physiology.MRItrigs.startIndex:round(physTraces.options.taskScan.tr * physTraces.options.physiology.samplingRate):physiology.MRItrigs.finishIndex) = 1;
    % Overwrite trigIndex
    physiology.MRItrigs.trigIndex = find(physiology.MRItrigs.trigs == 1);
else
    fprintf('\nWARNING: PROBLEM WITH TRIGGER IDENTIFICATION, EXITING PHYSIOLOGY PREPROCESSING\n')
    return
end

% Insert scanning time
physTraces.timeInSeconds = (1:(physiology.MRItrigs.finishIndex - physiology.MRItrigs.startIndex + 1))' / physTraces.options.physiology.samplingRate;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PROCESS MOUTH PRESSURE VALUES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find and interpolate between maximal inspiratory pressure values
physiology.inspiratoryTrigs.peakIndex = find(physiology.inspiratoryTrigs.peakTrigs == 1);
physiology.inspiratoryPressure.peakValues = physiology.inspiratoryPressure.rawTrace(physiology.inspiratoryTrigs.peakIndex); % Value of max press 
physiology.inspiratoryPressure.peakTime = physiology.time(physiology.inspiratoryTrigs.peakIndex); % Time of max press value
physiology.inspiratoryPressure.peakTrace = (interp1(physiology.inspiratoryPressure.peakTime, physiology.inspiratoryPressure.peakValues, physiology.time,'linear')) * -1; % Interpolate between peaks

% Calculate times for onset of resistance stimuli from the start of the scan
physValues.values.inspiratoryPressure.stimulus.onsetTimeVisual = physValues.values.behaviour.data.events.stim_on(physValues.values.behaviour.params.resist == 1)';
physTraces.physiology.indices.stimulusOnsetIndexVisual = round(physValues.values.inspiratoryPressure.stimulus.onsetTimeVisual * physTraces.options.physiology.samplingRate);
if strcmp(physTraces.options.taskScan.resistTimings, 'inhalation') == 1
    physValues.values.inspiratoryPressure.stimulus.onsetTime = physiology.resistance.onsetTime - physiology.time(physiology.MRItrigs.startIndex);
elseif strcmp(physTraces.options.taskScan.resistTimings, 'valves') == 1
    physValues.values.inspiratoryPressure.stimulus.onsetTime = physValues.values.behaviour.data.events.stim_on(physValues.values.behaviour.params.resist == 1)';
end
physTraces.physiology.indices.stimulusOnsetIndex = round(physValues.values.inspiratoryPressure.stimulus.onsetTime * physTraces.options.physiology.samplingRate);

% Calculate times for onset of no resistance stimuli from the start of the scan
physiology.breathing.rawTrace_smooth = fastsmooth(physiology.breathing.rawTrace, (physTraces.options.physiology.samplingRate/2), 1, 1); % Smooth the breathing trace using 1% of values as span
physiology.breathing.rawTrace_deriv = gradient(physiology.breathing.rawTrace_smooth); % Take the gradient of the smoothed line
physValues.values.inspiratoryPressure.noStimulus.onsetTimeVisual = physValues.values.behaviour.data.events.stim_on(physValues.values.behaviour.params.resist == 0)';
physTraces.physiology.indices.noStimulusOnsetIndexVisual = round(physValues.values.inspiratoryPressure.noStimulus.onsetTimeVisual * physTraces.options.physiology.samplingRate);
physValues.values.inspiratoryPressure.noStimulus.durationVisual = physValues.values.behaviour.data.events.stim_off(physValues.values.behaviour.params.resist == 0)' - physValues.values.behaviour.data.events.stim_on(physValues.values.behaviour.params.resist == 0)';
for n = 1:length(physValues.values.behaviour.data.events.stim_on(physValues.values.behaviour.params.resist == 0))
    idx1 = physTraces.physiology.indices.noStimulusOnsetIndexVisual(n) + physiology.MRItrigs.startIndex;
    idx2 = round(idx1 + physValues.values.inspiratoryPressure.noStimulus.durationVisual(n) * physTraces.options.physiology.samplingRate);
    try % Find where they took their first inhalation after the visual stimulus
        physTraces.physiology.indices.noStimulusOnsetIndex(n,1) = find((physiology.breathing.rawTrace_deriv(idx1:idx2) < -0.005), 1) + idx1 - physiology.MRItrigs.startIndex;
    catch % If none, place stimulus at the end of the trial instead
        physTraces.physiology.indices.noStimulusOnsetIndex(n,1) = idx2 - physiology.MRItrigs.startIndex;
    end
    if physTraces.physiology.indices.noStimulusOnsetIndex(n,1) >= physTraces.timeInSeconds(end) * physTraces.options.physiology.samplingRate
        physValues.values.inspiratoryPressure.noStimulus.onsetTime(n,1) = physTraces.physiology.indices.noStimulusOnsetIndex(n,1) / physTraces.options.physiology.samplingRate;
    else
        physValues.values.inspiratoryPressure.noStimulus.onsetTime(n,1) = physTraces.timeInSeconds(physTraces.physiology.indices.noStimulusOnsetIndex(n,1));
    end
end

% Calculate duration of each resistance stimulus and print warnings if necessary
physValues.values.inspiratoryPressure.stimulus.duration = physValues.values.behaviour.data.events.stim_off(physValues.values.behaviour.params.resist == 1)' - physValues.values.inspiratoryPressure.stimulus.onsetTime;
physValues.values.inspiratoryPressure.noStimulus.duration = physValues.values.behaviour.data.events.stim_off(physValues.values.behaviour.params.resist == 0)' - physValues.values.inspiratoryPressure.noStimulus.onsetTime;
if sum(physValues.values.inspiratoryPressure.stimulus.duration <= 0) > 0 || sum(physValues.values.inspiratoryPressure.stimulus.duration > 5.3) > 0
    fprintf('\nWARNING: SOME STIMULUS DURATIONS MAY BE ZERO OR GREATER THAN 5! CHECK BREATHING TRIGGERS IN LABCHART FILE\n')
else
    fprintf('\n... STIMULUS DURATIONS VALID\n')
end
if sum(physValues.values.inspiratoryPressure.noStimulus.duration <= 0) > 0
    fprintf('\nWARNING: SOME NON-STIMULUS DURATIONS MAY BE ZERO OR LESS! CHECK BREATHING TRIGGERS IN LABCHART FILE\nTHESE TRIALS HAVE BEEN REPLACED BY EVENTS AT THE END OF THE CIRCLE CUE\n')
    physValues.values.inspiratoryPressure.noStimulus.duration(physValues.values.inspiratoryPressure.noStimulus.duration < 0.5) = 0.5; % Make 0.5 sec the minimum duration
else
    fprintf('\n... NON-STIMULUS DURATIONS VALID\n')
end

% Exit here if in test mode
if exist('varargin', 'var') && strcmp(varargin{1}, 'test') == 1
    return
end

% Combine times for onset of all stimuli from the start of the scan
physValues.values.inspiratoryPressure.allStimuli.onsetTime = sort([physValues.values.inspiratoryPressure.stimulus.onsetTime; physValues.values.inspiratoryPressure.noStimulus.onsetTime]);
physValues.values.inspiratoryPressure.allStimuli.onsetIndex = sort([physTraces.physiology.indices.stimulusOnsetIndex; physTraces.physiology.indices.noStimulusOnsetIndex]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PROCESS BREATHING RATE VALUES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Highpass and add breathing belt trace to scan data
physiology.breathing.rawTrace_filtered = filter([1-((1/physTraces.options.physiology.samplingRate)/(physTraces.options.physiology.samplingRate/10)) ((1/physTraces.options.physiology.samplingRate)/(physTraces.options.physiology.samplingRate/10))-1],[1 ((1/physTraces.options.physiology.samplingRate)/(physTraces.options.physiology.samplingRate/10))-1], physiology.breathing.rawTrace);

% Find indices for the breathing peaks
physiology.breathingTrigs.peakIndex = find(physiology.breathingTrigs.peakTrigs == 1);

% Find and interpolate between breathing rate values
physiology.breathing.rate.peakValues = physiology.breathing.rate.rawTrace(physiology.breathingTrigs.peakIndex); % Breathing rate value at maximal inspiration point
physiology.breathing.rate.peakTime = physiology.time(physiology.breathingTrigs.peakIndex); % Time of breathing rate value
physiology.breathing.rate.peakTrace = interp1(physiology.breathing.rate.peakTime, physiology.breathing.rate.peakValues, physiology.time, 'linear'); % Interpolate between peaks


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PROCESS BREATHING DEPTH VALUES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find and interpolate between breathing depth values
physiology.breathing.depth.peakValues = physiology.breathing.depth.rawTrace(physiology.breathingTrigs.peakIndex); % Breathing depth value
physiology.breathing.depth.peakTime = physiology.time(physiology.breathingTrigs.peakIndex); % Time of breathing depth value
physiology.breathing.depth.peakTrace = interp1(physiology.breathing.depth.peakTime, physiology.breathing.depth.peakValues, physiology.time,'linear'); % Interpolate between peaks


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PROCESS END-TIDAL CO2 VALUES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find and interpolate between end tidal CO2 values
physiology.endTidalTrigs.peakIndex = find(physiology.endTidalTrigs.peakTrigs == 1);
physiology.endTidalCO2.peakValues = physiology.endTidalCO2.rawTrace(physiology.endTidalTrigs.peakIndex); % Value of end-tidal CO2 
physiology.endTidalCO2.peakTime = physiology.time(physiology.endTidalTrigs.peakIndex); % Time of end-tidal CO2
physiology.endTidalCO2.peakTrace = interp1(physiology.endTidalCO2.peakTime, physiology.endTidalCO2.peakValues, physiology.time,'linear'); % Interpolate between peaks


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PROCESS END-TIDAL O2 VALUES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find and interpolate between end tidal O2 values
physiology.endTidalO2.peakValues = physiology.endTidalO2.rawTrace(physiology.endTidalTrigs.peakIndex); % Value of end-tidal O2 
physiology.endTidalO2.peakTime = physiology.time(physiology.endTidalTrigs.peakIndex); % Time of end-tidal O2
physiology.endTidalO2.peakTrace = interp1(physiology.endTidalO2.peakTime, physiology.endTidalO2.peakValues, physiology.time,'linear'); % Interpolate between peaks


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXTRACT SCANNING PHYSIOLOGY TRACES / VALUES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Scanner triggers
physTraces.physiology.MRItrigs = physiology.MRItrigs.trigs(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);

% Inspiratory pressure
physTraces.physiology.inspiratoryPressure.rawTrace = physiology.inspiratoryPressure.rawTrace(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
physTraces.physiology.inspiratoryPressure.peakTrace = physiology.inspiratoryPressure.peakTrace(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
physTraces.physiology.inspiratoryPressure.peakTrigs = physiology.inspiratoryTrigs.peakTrigs(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
physTraces.physiology.inspiratoryPressure.peakIndex = find(physTraces.physiology.inspiratoryPressure.peakTrigs == 1); % Index for max press
physTraces.physiology.inspiratoryPressure.peakValues = physTraces.physiology.inspiratoryPressure.rawTrace(physTraces.physiology.inspiratoryPressure.peakIndex); % Value of max press
physTraces.physiology.inspiratoryPressure.peakTime = physTraces.timeInSeconds(physTraces.physiology.inspiratoryPressure.peakIndex); % Time of max press value

% Breathing trace
physTraces.physiology.breathing.rawTrace = physiology.breathing.rawTrace(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
physTraces.physiology.breathing.rawTrace_filtered = physiology.breathing.rawTrace_filtered(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
physTraces.physiology.breathing.peakTrigs = physiology.breathingTrigs.peakTrigs(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
physTraces.physiology.breathing.peakIndex = find(physTraces.physiology.breathing.peakTrigs == 1); % Index for max inspiration
physTraces.physiology.breathing.peakValues = physTraces.physiology.breathing.rawTrace_filtered(physTraces.physiology.breathing.peakIndex); % Value of max inspiration
physTraces.physiology.breathing.peakTime = physTraces.timeInSeconds(physTraces.physiology.breathing.peakIndex); % Time of max inspiration value

% Breathing rate
physTraces.physiology.breathing.rate.peakTrace = physiology.breathing.rate.peakTrace(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
physTraces.physiology.breathing.rate.peakIndex = physTraces.physiology.breathing.peakIndex;
physTraces.physiology.breathing.rate.peakValues = physTraces.physiology.breathing.rate.peakTrace(physTraces.physiology.breathing.peakIndex);

% Breathing depth
physTraces.physiology.breathing.depth.peakTrace = physiology.breathing.depth.peakTrace(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
physTraces.physiology.breathing.depth.peakIndex = physTraces.physiology.breathing.peakIndex;
physTraces.physiology.breathing.depth.peakValues = physTraces.physiology.breathing.depth.peakTrace(physTraces.physiology.breathing.peakIndex);

% End tidal CO2
physTraces.physiology.endTidalCO2.rawTrace = physiology.endTidalCO2.rawTrace(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
physTraces.physiology.endTidalCO2.peakTrigs = physiology.endTidalTrigs.peakTrigs(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
physTraces.physiology.endTidalCO2.peakIndex = find(physTraces.physiology.endTidalCO2.peakTrigs == 1);
physTraces.physiology.endTidalCO2.peakTrace = physiology.endTidalCO2.peakTrace(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
physTraces.physiology.endTidalCO2.peakValues = physTraces.physiology.endTidalCO2.rawTrace(physTraces.physiology.endTidalCO2.peakIndex);
physTraces.physiology.endTidalCO2.peakTime = physTraces.timeInSeconds(physTraces.physiology.endTidalCO2.peakIndex);

% End tidal O2
physTraces.physiology.endTidalO2.rawTrace = physiology.endTidalO2.rawTrace(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
physTraces.physiology.endTidalO2.peakTrigs = physTraces.physiology.endTidalCO2.peakTrigs;
physTraces.physiology.endTidalO2.peakIndex = physTraces.physiology.endTidalCO2.peakIndex;
physTraces.physiology.endTidalO2.peakTrace = physiology.endTidalO2.peakTrace(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
physTraces.physiology.endTidalO2.peakValues = physTraces.physiology.endTidalO2.rawTrace(physTraces.physiology.endTidalO2.peakIndex);
physTraces.physiology.endTidalO2.peakTime = physTraces.physiology.endTidalCO2.peakTime;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CALCULATE AVERAGE PHYSIOLOGY VALUES FOR REST PERIODS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get onset indices for ITI rest periods
physTraces.physiology.indices.itiOnsetIndex = round(physValues.values.behaviour.data.events.iti_on * physTraces.options.physiology.samplingRate)';

% Shorten last ITI duration to end of scan
physValues.values.behaviour.params.dur.iti(80) = floor(physTraces.timeInSeconds(end) - physValues.values.behaviour.data.events.iti_on(80));
if physValues.values.behaviour.params.dur.iti(80) <= 0
    physTraces.physiology.indices.itiOnsetIndex(80) = [];
    physValues.values.behaviour.params.dur.iti(80) = [];
end
if physValues.values.behaviour.data.events.iti_on(79) - physTraces.timeInSeconds(end) <= 0
    physTraces.physiology.indices.itiOnsetIndex(79) = [];
    physValues.values.behaviour.params.dur.iti(79) = [];
end

% Calculate average values for each ITI
for n = 1:length(physTraces.physiology.indices.itiOnsetIndex)
    % Inspiratory pressure
    physValues.values.inspiratoryPressure.iti.avgPressure.values(n,1) = mean(physTraces.physiology.inspiratoryPressure.rawTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.inspiratoryPressure.iti.maxPressure.values(n,1) = min(physTraces.physiology.inspiratoryPressure.rawTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * physTraces.options.physiology.samplingRate))));
    % Breathing rate
    physValues.values.breathingRate.iti.avgRate.values(n,1) = mean(physTraces.physiology.breathing.rate.peakTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.breathingRate.iti.maxRate.values(n,1) = max(physTraces.physiology.breathing.rate.peakTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.breathingRate.iti.stdRate.values(n,1) = std(physTraces.physiology.breathing.rate.peakTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.breathingRate.iti.coefvRate.values(n,1) = physValues.values.breathingRate.iti.stdRate.values(n,1) / physValues.values.breathingRate.iti.avgRate.values(n,1);
    % Breathing depth
    physValues.values.breathingDepth.iti.avgDepth.values(n,1) = mean(physTraces.physiology.breathing.depth.peakTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.breathingDepth.iti.maxDepth.values(n,1) = max(physTraces.physiology.breathing.depth.peakTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.breathingDepth.iti.stdDepth.values(n,1) = std(physTraces.physiology.breathing.depth.peakTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.breathingDepth.iti.coefvDepth.values(n,1) = physValues.values.breathingDepth.iti.stdDepth.values(n,1) / physValues.values.breathingDepth.iti.avgDepth.values(n,1);
    % End tidal CO2
    physValues.values.endTidalCO2.iti.avgEndTidalCO2.values(n,1) = nanmean(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.endTidalCO2.iti.maxEndTidalCO2.values(n,1) = max(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.endTidalCO2.iti.minEndTidalCO2.values(n,1) = min(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * physTraces.options.physiology.samplingRate))));
    % End tidal O2
    physValues.values.endTidalO2.iti.avgEndTidalO2.values(n,1) = nanmean(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.endTidalO2.iti.maxEndTidalO2.values(n,1) = max(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.endTidalO2.iti.minEndTidalO2.values(n,1) = min(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * physTraces.options.physiology.samplingRate))));
end

% Calculate overall summary values for rest periods
    % Inspiratory pressure
    physValues.summary.iti.absolute.avgPressure = mean(physValues.values.inspiratoryPressure.iti.avgPressure.values);
    physValues.summary.iti.absolute.maxPressure = mean(physValues.values.inspiratoryPressure.iti.maxPressure.values);
    % Breathing rate
    physValues.summary.iti.absolute.avgBreathingRate = mean(physValues.values.breathingRate.iti.avgRate.values);
    physValues.summary.iti.absolute.maxBreathingRate = mean(physValues.values.breathingRate.iti.maxRate.values);
    physValues.summary.iti.absolute.stdBreathingRate = mean(physValues.values.breathingRate.iti.stdRate.values);
    physValues.summary.iti.absolute.coefvBreathingRate = mean(physValues.values.breathingRate.iti.coefvRate.values);
    % Breathing depth
    physValues.summary.iti.absolute.avgBreathingDepth = mean(physValues.values.breathingDepth.iti.avgDepth.values);
    physValues.summary.iti.absolute.maxBreathingDepth = mean(physValues.values.breathingDepth.iti.maxDepth.values);
    physValues.summary.iti.absolute.stdBreathingDepth = mean(physValues.values.breathingDepth.iti.stdDepth.values);
    physValues.summary.iti.absolute.coefvBreathingDepth = mean(physValues.values.breathingDepth.iti.coefvDepth.values);
    % End tidal CO2
    physValues.summary.iti.absolute.avgEndTidalCO2 = nanmean(physValues.values.endTidalCO2.iti.avgEndTidalCO2.values);
    physValues.summary.iti.absolute.maxEndTidalCO2 = nanmean(physValues.values.endTidalCO2.iti.maxEndTidalCO2.values);
    physValues.summary.iti.absolute.minEndTidalCO2 = nanmean(physValues.values.endTidalCO2.iti.minEndTidalCO2.values);
    % End tidal O2
    physValues.summary.iti.absolute.avgEndTidalO2 = nanmean(physValues.values.endTidalO2.iti.avgEndTidalO2.values);
    physValues.summary.iti.absolute.maxEndTidalO2 = nanmean(physValues.values.endTidalO2.iti.maxEndTidalO2.values);
    physValues.summary.iti.absolute.minEndTidalO2 = nanmean(physValues.values.endTidalO2.iti.minEndTidalO2.values);

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CALCULATE BREATHING VARIABILITY AND SIGHS FOR REST PERIODS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate number of sighs in each rest period (>= 1.5 x average resting breathing depth)
physValues.values.sighs.cutOffRatio = 1.5;
for n = 1:length(physTraces.physiology.indices.itiOnsetIndex)
    physValues.values.sighs.iti.ratioToMeanBreathingDepths{n} = find((physTraces.physiology.breathing.peakTime >= physValues.values.behaviour.data.events.iti_on(n)) & (physTraces.physiology.breathing.peakTime <= physValues.values.behaviour.data.events.iti_off(n)));
    physValues.values.sighs.iti.breathTimes{n} = physTraces.physiology.breathing.peakTime;
    physValues.values.sighs.iti.breathTimes{n} = physValues.values.sighs.iti.breathTimes{n}(physValues.values.sighs.iti.ratioToMeanBreathingDepths{n});
    physValues.values.sighs.iti.ratioToMeanBreathingDepths{n} = physTraces.physiology.breathing.depth.peakValues(physValues.values.sighs.iti.ratioToMeanBreathingDepths{n}) / physValues.summary.iti.absolute.avgBreathingDepth;
    physValues.values.sighs.iti.numberOfSighs(n) = sum(physValues.values.sighs.iti.ratioToMeanBreathingDepths{n} >= physValues.values.sighs.cutOffRatio);
    physValues.values.sighs.iti.timeOfSigh{n} = physValues.values.sighs.iti.breathTimes{n}(physValues.values.sighs.iti.ratioToMeanBreathingDepths{n} >= physValues.values.sighs.cutOffRatio);
end

% Calculate number of sighs in each rating period
for n = 1:length(physValues.values.behaviour.data.events.rating_on)
    physValues.values.sighs.rating.ratioToMeanBreathingDepths{n} = find((physTraces.physiology.breathing.peakTime >= physValues.values.behaviour.data.events.rating_on(n)) & (physTraces.physiology.breathing.peakTime <= physValues.values.behaviour.data.events.rating_off(n)));
    physValues.values.sighs.rating.breathTimes{n} = physTraces.physiology.breathing.peakTime;
    physValues.values.sighs.rating.breathTimes{n} = physValues.values.sighs.rating.breathTimes{n}(physValues.values.sighs.rating.ratioToMeanBreathingDepths{n});
    physValues.values.sighs.rating.ratioToMeanBreathingDepths{n} = physTraces.physiology.breathing.depth.peakValues(physValues.values.sighs.rating.ratioToMeanBreathingDepths{n}) / physValues.summary.iti.absolute.avgBreathingDepth;
    physValues.values.sighs.rating.numberOfSighs(n) = sum(physValues.values.sighs.rating.ratioToMeanBreathingDepths{n} >= physValues.values.sighs.cutOffRatio);
    physValues.values.sighs.rating.timeOfSigh{n} = physValues.values.sighs.rating.breathTimes{n}(physValues.values.sighs.rating.ratioToMeanBreathingDepths{n} >= physValues.values.sighs.cutOffRatio);
end

% Calculate total number of sighs and summary metrics accordingly
physValues.values.sighs.iti.itiWithSighs = find(physValues.values.sighs.iti.numberOfSighs >= 1);
physValues.values.sighs.iti.itiWithoutSighs = find(physValues.values.sighs.iti.numberOfSighs == 0);
physValues.summary.sighs.sighTotalIti = sum(physValues.values.sighs.iti.numberOfSighs);
physValues.summary.sighs.sighTotalRatings = sum(physValues.values.sighs.rating.numberOfSighs);
physValues.summary.sighs.sighTotal = physValues.summary.sighs.sighTotalIti + physValues.summary.sighs.sighTotalRatings;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CALCULATE AVERAGE PHYSIOLOGY VALUES FOR NO-RESISTANCE PERIODS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate absolute average and peak values for each non-resistance stimulus
for n = 1:length(physValues.values.inspiratoryPressure.noStimulus.onsetTime)
    if physTraces.timeInSeconds(end) - physValues.values.inspiratoryPressure.noStimulus.onsetTime(n) > 0
        % Inspiratory pressure
        physValues.values.inspiratoryPressure.noStimulus.avgPressure.values(n,1) = mean(physTraces.physiology.inspiratoryPressure.rawTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate))));
        physValues.values.inspiratoryPressure.noStimulus.maxPressure.values(n,1) = min(physTraces.physiology.inspiratoryPressure.rawTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate))));
        physValues.values.inspiratoryPressure.noStimulus.avgPressure.valuesMinusRest(n,1) = physValues.values.inspiratoryPressure.noStimulus.avgPressure.values(n,1) - physValues.summary.iti.absolute.avgPressure;
        physValues.values.inspiratoryPressure.noStimulus.maxPressure.valuesMinusRest(n,1) = physValues.values.inspiratoryPressure.noStimulus.maxPressure.values(n,1) - physValues.summary.iti.absolute.avgPressure;
        % Breathing rate
        physValues.values.breathingRate.noStimulus.avgRate.values(n,1) = mean(physTraces.physiology.breathing.rate.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate))));
        physValues.values.breathingRate.noStimulus.maxRate.values(n,1) = max(physTraces.physiology.breathing.rate.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate))));
        physValues.values.breathingRate.noStimulus.stdRate.values(n,1) = std(physTraces.physiology.breathing.rate.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate))));
        physValues.values.breathingRate.noStimulus.coefvRate.values(n,1) = physValues.values.breathingRate.noStimulus.stdRate.values(n,1) / physValues.values.breathingRate.noStimulus.avgRate.values(n,1);
        % Breathing depth
        physValues.values.breathingDepth.noStimulus.avgDepth.values(n,1) = mean(physTraces.physiology.breathing.depth.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate))));
        physValues.values.breathingDepth.noStimulus.maxDepth.values(n,1) = max(physTraces.physiology.breathing.depth.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate))));
        physValues.values.breathingDepth.noStimulus.stdDepth.values(n,1) = std(physTraces.physiology.breathing.depth.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate))));
        physValues.values.breathingDepth.noStimulus.coefvDepth.values(n,1) = physValues.values.breathingDepth.noStimulus.stdDepth.values(n,1) / physValues.values.breathingDepth.noStimulus.avgDepth.values(n,1);
        % End tidal CO2
        physValues.values.endTidalCO2.noStimulus.avgEndTidalCO2.values(n,1) = nanmean(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate))));
        physValues.values.endTidalCO2.noStimulus.maxEndTidalCO2.values(n,1) = max(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate))));
        physValues.values.endTidalCO2.noStimulus.minEndTidalCO2.values(n,1) = min(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate))));
        % End tidal O2
        physValues.values.endTidalO2.noStimulus.avgEndTidalO2.values(n,1) = nanmean(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate))));
        physValues.values.endTidalO2.noStimulus.maxEndTidalO2.values(n,1) = max(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate))));
        physValues.values.endTidalO2.noStimulus.minEndTidalO2.values(n,1) = min(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate))));
    end
end

% Calculate overall absolute average and peak values for no resistance
    % Inspiratory pressure
    physValues.summary.noStimulus.absolute.avgPressure = mean(physValues.values.inspiratoryPressure.noStimulus.avgPressure.values);
    physValues.summary.noStimulus.absolute.maxPressure = mean(physValues.values.inspiratoryPressure.noStimulus.maxPressure.values);
    physValues.summary.noStimulus.absolute.avgPressureMinusRest = mean(physValues.values.inspiratoryPressure.noStimulus.avgPressure.valuesMinusRest);
    physValues.summary.noStimulus.absolute.maxPressureMinusRest = mean(physValues.values.inspiratoryPressure.noStimulus.maxPressure.valuesMinusRest);
    % Breathing rate
    physValues.summary.noStimulus.absolute.avgBreathingRate = mean(physValues.values.breathingRate.noStimulus.avgRate.values);
    physValues.summary.noStimulus.absolute.maxBreathingRate = mean(physValues.values.breathingRate.noStimulus.maxRate.values);
    physValues.summary.noStimulus.absolute.stdBreathingRate = mean(physValues.values.breathingRate.noStimulus.stdRate.values);
    physValues.summary.noStimulus.absolute.coefvBreathingRate = mean(physValues.values.breathingRate.noStimulus.coefvRate.values);
    % Breathing depth
    physValues.summary.noStimulus.absolute.avgBreathingDepth = mean(physValues.values.breathingDepth.noStimulus.avgDepth.values);
    physValues.summary.noStimulus.absolute.maxBreathingDepth = mean(physValues.values.breathingDepth.noStimulus.maxDepth.values);
    physValues.summary.noStimulus.absolute.stdBreathingDepth = mean(physValues.values.breathingDepth.noStimulus.stdDepth.values);
    physValues.summary.noStimulus.absolute.coefvBreathingDepth = mean(physValues.values.breathingDepth.noStimulus.coefvDepth.values);
    % End tidal CO2
    physValues.summary.noStimulus.absolute.avgEndTidalCO2 = nanmean(physValues.values.endTidalCO2.noStimulus.avgEndTidalCO2.values);
    physValues.summary.noStimulus.absolute.maxEndTidalCO2 = nanmean(physValues.values.endTidalCO2.noStimulus.maxEndTidalCO2.values);
    physValues.summary.noStimulus.absolute.minEndTidalCO2 = nanmean(physValues.values.endTidalCO2.noStimulus.minEndTidalCO2.values);
    % End tidal O2
    physValues.summary.noStimulus.absolute.avgEndTidalO2 = nanmean(physValues.values.endTidalO2.noStimulus.avgEndTidalO2.values);
    physValues.summary.noStimulus.absolute.maxEndTidalO2 = nanmean(physValues.values.endTidalO2.noStimulus.maxEndTidalO2.values);
    physValues.summary.noStimulus.absolute.minEndTidalO2 = nanmean(physValues.values.endTidalO2.noStimulus.minEndTidalO2.values);

% Calculate relative average and peak values for each non-resistance stimulus compared to rest
for n = 1:length(physValues.values.inspiratoryPressure.noStimulus.onsetTime)
    if physTraces.timeInSeconds(end) - physValues.values.inspiratoryPressure.noStimulus.onsetTime(n) > 0
        % Inspiratory pressure
        physValues.values.inspiratoryPressure.noStimulus.avgPressure.percentageChange(n,1) = mean(physTraces.physiology.inspiratoryPressure.rawTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / mean(physValues.summary.iti.absolute.avgPressure) * 100;
        physValues.values.inspiratoryPressure.noStimulus.maxPressure.percentageChange(n,1) = min(physTraces.physiology.inspiratoryPressure.rawTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / mean(physValues.summary.iti.absolute.maxPressure) * 100;
        % Breathing rate
        physValues.values.breathingRate.noStimulus.avgRate.percentageChange(n,1) = mean(physTraces.physiology.breathing.rate.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / mean(physValues.summary.iti.absolute.avgBreathingRate) * 100;
        physValues.values.breathingRate.noStimulus.maxRate.percentageChange(n,1) = max(physTraces.physiology.breathing.rate.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / mean(physValues.summary.iti.absolute.maxBreathingRate) * 100;
        % Breathing depth
        physValues.values.breathingDepth.noStimulus.avgDepth.percentageChange(n,1) = mean(physTraces.physiology.breathing.depth.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / mean(physValues.summary.iti.absolute.avgBreathingDepth) * 100;
        physValues.values.breathingDepth.noStimulus.maxDepth.percentageChange(n,1) = max(physTraces.physiology.breathing.depth.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / mean(physValues.summary.iti.absolute.maxBreathingDepth) * 100;
        % End tidal CO2
        physValues.values.endTidalCO2.noStimulus.avgEndTidalCO2.percentageChange(n,1) = nanmean(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / nanmean(physValues.summary.iti.absolute.avgEndTidalCO2) * 100;
        physValues.values.endTidalCO2.noStimulus.maxEndTidalCO2.percentageChange(n,1) = max(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / nanmean(physValues.summary.iti.absolute.maxEndTidalCO2) * 100;
        physValues.values.endTidalCO2.noStimulus.minEndTidalCO2.percentageChange(n,1) = min(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / nanmean(physValues.summary.iti.absolute.minEndTidalCO2) * 100;
        % End tidal O2
        physValues.values.endTidalO2.noStimulus.avgEndTidalO2.percentageChange(n,1) = nanmean(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / nanmean(physValues.summary.iti.absolute.avgEndTidalO2) * 100;
        physValues.values.endTidalO2.noStimulus.maxEndTidalO2.percentageChange(n,1) = max(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / nanmean(physValues.summary.iti.absolute.maxEndTidalO2) * 100;
        physValues.values.endTidalO2.noStimulus.minEndTidalO2.percentageChange(n,1) = min(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / nanmean(physValues.summary.iti.absolute.minEndTidalO2) * 100;
    end
end

% Calculate overall relative average and peak values for no resistance compared to rest
    % Inspiratory pressure
    physValues.summary.noStimulus.relative.avgPressure = mean(physValues.values.inspiratoryPressure.noStimulus.avgPressure.percentageChange);
    physValues.summary.noStimulus.relative.maxPressure = mean(physValues.values.inspiratoryPressure.noStimulus.maxPressure.percentageChange);
    % Breathing rate
    physValues.summary.noStimulus.relative.avgBreathingRate = mean(physValues.values.breathingRate.noStimulus.avgRate.percentageChange);
    physValues.summary.noStimulus.relative.maxBreathingRate = mean(physValues.values.breathingRate.noStimulus.maxRate.percentageChange);
    % Breathing depth
    physValues.summary.noStimulus.relative.avgBreathingDepth = mean(physValues.values.breathingDepth.noStimulus.avgDepth.percentageChange);
    physValues.summary.noStimulus.relative.maxBreathingDepth = mean(physValues.values.breathingDepth.noStimulus.maxDepth.percentageChange);
    % End tidal CO2
    physValues.summary.noStimulus.relative.avgEndTidalCO2 = nanmean(physValues.values.endTidalCO2.noStimulus.avgEndTidalCO2.percentageChange);
    physValues.summary.noStimulus.relative.maxEndTidalCO2 = nanmean(physValues.values.endTidalCO2.noStimulus.maxEndTidalCO2.percentageChange);
    physValues.summary.noStimulus.relative.minEndTidalCO2 = nanmean(physValues.values.endTidalCO2.noStimulus.minEndTidalCO2.percentageChange);
    % End tidal O2
    physValues.summary.noStimulus.relative.avgEndTidalO2 = nanmean(physValues.values.endTidalO2.noStimulus.avgEndTidalO2.percentageChange);
    physValues.summary.noStimulus.relative.maxEndTidalO2 = nanmean(physValues.values.endTidalO2.noStimulus.maxEndTidalO2.percentageChange);
    physValues.summary.noStimulus.relative.minEndTidalO2 = nanmean(physValues.values.endTidalO2.noStimulus.minEndTidalO2.percentageChange);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CALCULATE BREATHING VARIABILITY AND DEBS FOR NO-RESISTANCE PERIODS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate number of DEBs in each no-stimulus period (>= 1.5 x average no stimulus breathing depth)
physValues.values.debs.cutOffRatio = 1.5;
for n = 1:length(physValues.values.inspiratoryPressure.noStimulus.onsetTime)
    if physTraces.timeInSeconds(end) - physValues.values.inspiratoryPressure.noStimulus.onsetTime(n) > 0
        physValues.values.debs.noStimulus.ratioToMeanBreathingDepths{n} = find((physTraces.physiology.breathing.peakTime >= physValues.values.inspiratoryPressure.noStimulus.onsetTime(n)) & (physTraces.physiology.breathing.peakTime <= (physValues.values.inspiratoryPressure.noStimulus.onsetTime(n) + physValues.values.inspiratoryPressure.noStimulus.duration(n))));
        physValues.values.debs.noStimulus.breathTimes{n} = physTraces.physiology.breathing.peakTime;
        physValues.values.debs.noStimulus.breathTimes{n} = physValues.values.debs.noStimulus.breathTimes{n}(physValues.values.debs.noStimulus.ratioToMeanBreathingDepths{n});   
        physValues.values.debs.noStimulus.ratioToMeanBreathingDepths{n} = physTraces.physiology.breathing.depth.peakValues(physValues.values.debs.noStimulus.ratioToMeanBreathingDepths{n}) / physValues.summary.noStimulus.absolute.avgBreathingDepth;
        physValues.values.debs.noStimulus.numberOfDebs(n) = sum(physValues.values.debs.noStimulus.ratioToMeanBreathingDepths{n} >= physValues.values.debs.cutOffRatio);
        physValues.values.debs.noStimulus.timeOfDebs{n} = physValues.values.debs.noStimulus.breathTimes{n}(physValues.values.debs.noStimulus.ratioToMeanBreathingDepths{n} >= physValues.values.debs.cutOffRatio);
    end
end

% Calculate total number of sighs and summary metrics accordingly
physValues.values.debs.noStimulus.noStimulusWithDebs = find(physValues.values.debs.noStimulus.numberOfDebs >= 1);
physValues.values.debs.noStimulus.noStimulusWithoutDebs = find(physValues.values.debs.noStimulus.numberOfDebs == 0);
physValues.summary.noStimulus.debsTotal = sum(physValues.values.debs.noStimulus.numberOfDebs);

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CALCULATE AVERAGE PHYSIOLOGY VALUES FOR RESISTANCE PERIODS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate absolute average and peak pressure values for each resistance stimulus
for n = 1:length(physValues.values.inspiratoryPressure.stimulus.onsetTime)
    % Inspiratory pressure
    physValues.values.inspiratoryPressure.stimulus.avgPressure.values(n,1) = mean(physTraces.physiology.inspiratoryPressure.rawTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.inspiratoryPressure.stimulus.maxPressure.values(n,1) = min(physTraces.physiology.inspiratoryPressure.rawTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.inspiratoryPressure.stimulus.avgPressure.valuesMinusRest(n,1) = physValues.values.inspiratoryPressure.stimulus.avgPressure.values(n,1) - physValues.summary.iti.absolute.avgPressure;
    physValues.values.inspiratoryPressure.stimulus.maxPressure.valuesMinusRest(n,1) = physValues.values.inspiratoryPressure.stimulus.maxPressure.values(n,1) - physValues.summary.iti.absolute.avgPressure;
    % Breathing rate
    physValues.values.breathingRate.stimulus.avgRate.values(n,1) = mean(physTraces.physiology.breathing.rate.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.breathingRate.stimulus.maxRate.values(n,1) = max(physTraces.physiology.breathing.rate.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.breathingRate.stimulus.stdRate.values(n,1) = std(physTraces.physiology.breathing.rate.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.breathingRate.stimulus.coefvRate.values(n,1) = physValues.values.breathingRate.stimulus.stdRate.values(n,1) / physValues.values.breathingRate.stimulus.avgRate.values(n,1);
    % Breathing depth
    physValues.values.breathingDepth.stimulus.avgDepth.values(n,1) = mean(physTraces.physiology.breathing.depth.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.breathingDepth.stimulus.maxDepth.values(n,1) = max(physTraces.physiology.breathing.depth.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.breathingDepth.stimulus.stdDepth.values(n,1) = std(physTraces.physiology.breathing.depth.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.breathingDepth.stimulus.coefvDepth.values(n,1) = physValues.values.breathingDepth.stimulus.stdDepth.values(n,1) / physValues.values.breathingDepth.stimulus.avgDepth.values(n,1);
    % End tidal CO2
    physValues.values.endTidalCO2.stimulus.avgEndTidalCO2.values(n,1) = nanmean(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.endTidalCO2.stimulus.maxEndTidalCO2.values(n,1) = max(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.endTidalCO2.stimulus.minEndTidalCO2.values(n,1) = min(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate))));
    % End tidal O2
    physValues.values.endTidalO2.stimulus.avgEndTidalO2.values(n,1) = nanmean(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.endTidalO2.stimulus.maxEndTidalO2.values(n,1) = max(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate))));
    physValues.values.endTidalO2.stimulus.minEndTidalO2.values(n,1) = min(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate))));
end

% Calculate overall absolute average and peak values for resistance
    % Inspiratory pressure
    physValues.summary.stimulus.absolute.avgPressure = mean(physValues.values.inspiratoryPressure.stimulus.avgPressure.values);
    physValues.summary.stimulus.absolute.maxPressure = mean(physValues.values.inspiratoryPressure.stimulus.maxPressure.values);
    physValues.summary.stimulus.absolute.avgPressureMinusRest = mean(physValues.values.inspiratoryPressure.stimulus.avgPressure.valuesMinusRest);
    physValues.summary.stimulus.absolute.maxPressureMinusRest = mean(physValues.values.inspiratoryPressure.stimulus.maxPressure.valuesMinusRest);
    % Breathing rate
    physValues.summary.stimulus.absolute.avgBreathingRate = mean(physValues.values.breathingRate.stimulus.avgRate.values);
    physValues.summary.stimulus.absolute.maxBreathingRate = mean(physValues.values.breathingRate.stimulus.maxRate.values);
    physValues.summary.stimulus.absolute.stdBreathingRate = mean(physValues.values.breathingRate.stimulus.stdRate.values);
    physValues.summary.stimulus.absolute.coefvBreathingRate = mean(physValues.values.breathingRate.stimulus.coefvRate.values);
    % Breathing depth
    physValues.summary.stimulus.absolute.avgBreathingDepth = mean(physValues.values.breathingDepth.stimulus.avgDepth.values);
    physValues.summary.stimulus.absolute.maxBreathingDepth = mean(physValues.values.breathingDepth.stimulus.maxDepth.values);
    physValues.summary.stimulus.absolute.stdBreathingDepth = mean(physValues.values.breathingDepth.stimulus.stdDepth.values);
    physValues.summary.stimulus.absolute.coefvBreathingDepth = mean(physValues.values.breathingDepth.stimulus.coefvDepth.values);
    % End tidal CO2
    physValues.summary.stimulus.absolute.avgEndTidalCO2 = nanmean(physValues.values.endTidalCO2.stimulus.avgEndTidalCO2.values);
    physValues.summary.stimulus.absolute.maxEndTidalCO2 = nanmean(physValues.values.endTidalCO2.stimulus.maxEndTidalCO2.values);
    physValues.summary.stimulus.absolute.minEndTidalCO2 = nanmean(physValues.values.endTidalCO2.stimulus.minEndTidalCO2.values);
    % End tidal O2
    physValues.summary.stimulus.absolute.avgEndTidalO2 = nanmean(physValues.values.endTidalO2.stimulus.avgEndTidalO2.values);
    physValues.summary.stimulus.absolute.maxEndTidalO2 = nanmean(physValues.values.endTidalO2.stimulus.maxEndTidalO2.values);
    physValues.summary.stimulus.absolute.minEndTidalO2 = nanmean(physValues.values.endTidalO2.stimulus.minEndTidalO2.values);

% Calculate relative average and peak values for each resistance stimulus compared to rest
for n = 1:length(physValues.values.inspiratoryPressure.stimulus.onsetTime)
    % Inspiratory pressure
    physValues.values.inspiratoryPressure.stimulus.avgPressure.percentageChange(n,1) = mean(physTraces.physiology.inspiratoryPressure.rawTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n)*  physTraces.options.physiology.samplingRate)))) / mean(physValues.summary.iti.absolute.avgPressure) * 100;
    physValues.values.inspiratoryPressure.stimulus.maxPressure.percentageChange(n,1) = min(physTraces.physiology.inspiratoryPressure.rawTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / mean(physValues.summary.iti.absolute.maxPressure) * 100;
    % Breathing rate
    physValues.values.breathingRate.stimulus.avgRate.percentageChange(n,1) = mean(physTraces.physiology.breathing.rate.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / mean(physValues.summary.iti.absolute.avgBreathingRate) * 100;
    physValues.values.breathingRate.stimulus.maxRate.percentageChange(n,1) = max(physTraces.physiology.breathing.rate.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / mean(physValues.summary.iti.absolute.maxBreathingRate) * 100;
    % Breathing depth
    physValues.values.breathingDepth.stimulus.avgDepth.percentageChange(n,1) = mean(physTraces.physiology.breathing.depth.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / mean(physValues.summary.iti.absolute.avgBreathingDepth) * 100;
    physValues.values.breathingDepth.stimulus.maxDepth.percentageChange(n,1) = max(physTraces.physiology.breathing.depth.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / mean(physValues.summary.iti.absolute.maxBreathingDepth) * 100;
    % End tidal CO2
    physValues.values.endTidalCO2.stimulus.avgEndTidalCO2.percentageChange(n,1) = nanmean(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / nanmean(physValues.summary.iti.absolute.avgEndTidalCO2) * 100;
    physValues.values.endTidalCO2.stimulus.maxEndTidalCO2.percentageChange(n,1) = max(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / nanmean(physValues.summary.iti.absolute.maxEndTidalCO2) * 100;
    physValues.values.endTidalCO2.stimulus.minEndTidalCO2.percentageChange(n,1) = min(physTraces.physiology.endTidalCO2.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / nanmean(physValues.summary.iti.absolute.minEndTidalCO2) * 100;
    % End tidal O2
    physValues.values.endTidalO2.stimulus.avgEndTidalO2.percentageChange(n,1) = nanmean(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / nanmean(physValues.summary.iti.absolute.avgEndTidalO2) * 100;
    physValues.values.endTidalO2.stimulus.maxEndTidalO2.percentageChange(n,1) = max(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / nanmean(physValues.summary.iti.absolute.maxEndTidalO2) * 100;
    physValues.values.endTidalO2.stimulus.minEndTidalO2.percentageChange(n,1) = min(physTraces.physiology.endTidalO2.peakTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * physTraces.options.physiology.samplingRate)))) / nanmean(physValues.summary.iti.absolute.minEndTidalO2) * 100;
end
    
% Calculate overall relative average and peak values for resistance compared to rest
    % Inspiratory pressure
    physValues.summary.stimulus.relative.avgPressure = mean(physValues.values.inspiratoryPressure.stimulus.avgPressure.percentageChange);
    physValues.summary.stimulus.relative.maxPressure = mean(physValues.values.inspiratoryPressure.stimulus.maxPressure.percentageChange);
    % Breathing rate
    physValues.summary.stimulus.relative.avgBreathingRate = mean(physValues.values.breathingRate.stimulus.avgRate.percentageChange);
    physValues.summary.stimulus.relative.maxBreathingRate = mean(physValues.values.breathingRate.stimulus.maxRate.percentageChange);
    % Breathing depth
    physValues.summary.stimulus.relative.avgBreathingDepth = mean(physValues.values.breathingDepth.stimulus.avgDepth.percentageChange);
    physValues.summary.stimulus.relative.maxBreathingDepth = mean(physValues.values.breathingDepth.stimulus.maxDepth.percentageChange);
    % End tidal CO2
    physValues.summary.stimulus.relative.avgEndTidalCO2 = nanmean(physValues.values.endTidalCO2.stimulus.avgEndTidalCO2.percentageChange);
    physValues.summary.stimulus.relative.maxEndTidalCO2 = nanmean(physValues.values.endTidalCO2.stimulus.maxEndTidalCO2.percentageChange);
    physValues.summary.stimulus.relative.minEndTidalCO2 = nanmean(physValues.values.endTidalCO2.stimulus.minEndTidalCO2.percentageChange);
    % End tidal O2
    physValues.summary.stimulus.relative.avgEndTidalO2 = nanmean(physValues.values.endTidalO2.stimulus.avgEndTidalO2.percentageChange);
    physValues.summary.stimulus.relative.maxEndTidalO2 = nanmean(physValues.values.endTidalO2.stimulus.maxEndTidalO2.percentageChange);
    physValues.summary.stimulus.relative.minEndTidalO2 = nanmean(physValues.values.endTidalO2.stimulus.minEndTidalO2.percentageChange);
    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMBINE STIMULI VALUES BACK INTO CORRECT ORDER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Identify resistance and no resistance indices
resist = find(physValues.values.behaviour.params.resist == 1)';
no_resist = find(physValues.values.behaviour.params.resist == 0)';
if physTraces.timeInSeconds(end) - physValues.values.inspiratoryPressure.noStimulus.onsetTime(end) < 0
    no_resist(end) = []; % One participant lost their final no-resistance trial due to timing issues with the scanner computer
end

% Combine inspiratory pressure
physValues.values.inspiratoryPressure.allStimuli.avgPressure(resist,1) = physValues.values.inspiratoryPressure.stimulus.avgPressure.values;
physValues.values.inspiratoryPressure.allStimuli.maxPressure(resist,1) = physValues.values.inspiratoryPressure.stimulus.maxPressure.values;
physValues.values.inspiratoryPressure.allStimuli.avgPressure(no_resist,1) = physValues.values.inspiratoryPressure.noStimulus.avgPressure.values;
physValues.values.inspiratoryPressure.allStimuli.maxPressure(no_resist,1) = physValues.values.inspiratoryPressure.noStimulus.maxPressure.values;
physValues.values.inspiratoryPressure.allStimuli.avgPressureMinusRest(resist,1) = physValues.values.inspiratoryPressure.stimulus.avgPressure.valuesMinusRest;
physValues.values.inspiratoryPressure.allStimuli.maxPressureMinusRest(resist,1) = physValues.values.inspiratoryPressure.stimulus.maxPressure.valuesMinusRest;
physValues.values.inspiratoryPressure.allStimuli.avgPressureMinusRest(no_resist,1) = physValues.values.inspiratoryPressure.noStimulus.avgPressure.valuesMinusRest;
physValues.values.inspiratoryPressure.allStimuli.maxPressureMinusRest(no_resist,1) = physValues.values.inspiratoryPressure.noStimulus.maxPressure.valuesMinusRest;
physValues.values.inspiratoryPressure.allStimuli.duration(resist,1) = physValues.values.inspiratoryPressure.stimulus.duration;
physValues.values.inspiratoryPressure.allStimuli.duration((physValues.values.behaviour.params.resist == 0),1) = physValues.values.inspiratoryPressure.noStimulus.duration;

% Combine breathing rate
physValues.values.breathingRate.allStimuli.avgRate(resist,1) = physValues.values.breathingRate.stimulus.avgRate.values;
physValues.values.breathingRate.allStimuli.maxRate(resist,1) = physValues.values.breathingRate.stimulus.maxRate.values;
physValues.values.breathingRate.allStimuli.avgRate(no_resist,1) = physValues.values.breathingRate.noStimulus.avgRate.values;
physValues.values.breathingRate.allStimuli.maxRate(no_resist,1) = physValues.values.breathingRate.noStimulus.maxRate.values;

% Combine breathing depth (as a percentage change from rest)
physValues.values.breathingDepth.allStimuli.avgDepthPercentageChange(resist,1) = physValues.values.breathingDepth.stimulus.avgDepth.percentageChange;
physValues.values.breathingDepth.allStimuli.maxDepthPercentageChange(resist,1) = physValues.values.breathingDepth.stimulus.maxDepth.percentageChange;
physValues.values.breathingDepth.allStimuli.avgDepthPercentageChange(no_resist,1) = physValues.values.breathingDepth.noStimulus.avgDepth.percentageChange;
physValues.values.breathingDepth.allStimuli.maxDepthPercentageChange(no_resist,1) = physValues.values.breathingDepth.noStimulus.maxDepth.percentageChange;

% Combine end tidal CO2
physValues.values.endTidalCO2.allStimuli.avgEndTidalCO2(resist,1) = physValues.values.endTidalCO2.stimulus.avgEndTidalCO2.values;
physValues.values.endTidalCO2.allStimuli.maxEndTidalCO2(resist,1) = physValues.values.endTidalCO2.stimulus.maxEndTidalCO2.values;
physValues.values.endTidalCO2.allStimuli.minEndTidalCO2(resist,1) = physValues.values.endTidalCO2.stimulus.minEndTidalCO2.values;
physValues.values.endTidalCO2.allStimuli.avgEndTidalCO2(no_resist,1) = physValues.values.endTidalCO2.noStimulus.avgEndTidalCO2.values;
physValues.values.endTidalCO2.allStimuli.maxEndTidalCO2(no_resist,1) = physValues.values.endTidalCO2.noStimulus.maxEndTidalCO2.values;
physValues.values.endTidalCO2.allStimuli.minEndTidalCO2(no_resist,1) = physValues.values.endTidalCO2.noStimulus.minEndTidalCO2.values;

% Combine end tidal O2
physValues.values.endTidalO2.allStimuli.avgEndTidalO2(resist,1) = physValues.values.endTidalO2.stimulus.avgEndTidalO2.values;
physValues.values.endTidalO2.allStimuli.maxEndTidalO2(resist,1) = physValues.values.endTidalO2.stimulus.maxEndTidalO2.values;
physValues.values.endTidalO2.allStimuli.minEndTidalO2(resist,1) = physValues.values.endTidalO2.stimulus.minEndTidalO2.values;
physValues.values.endTidalO2.allStimuli.avgEndTidalO2(no_resist,1) = physValues.values.endTidalO2.noStimulus.avgEndTidalO2.values;
physValues.values.endTidalO2.allStimuli.maxEndTidalO2(no_resist,1) = physValues.values.endTidalO2.noStimulus.maxEndTidalO2.values;
physValues.values.endTidalO2.allStimuli.minEndTidalO2(no_resist,1) = physValues.values.endTidalO2.noStimulus.minEndTidalO2.values;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXTRACT AVERAGE PHYSIOLOGY VALUES PER SCANNING VOLUME
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for n = 1:(physTraces.options.taskScan.vols)
    % Inspiratory pressure
    physValues.values.inspiratoryPressure.valuesPerVolumeRawTrace(n,1) = mean(physiology.inspiratoryPressure.rawTrace(physiology.MRItrigs.trigIndex(n):(physiology.MRItrigs.trigIndex(n) + physTraces.options.taskScan.tr * physTraces.options.physiology.samplingRate)));
    physValues.values.inspiratoryPressure.valuesPerVolumePeakTrace(n,1) = mean(physiology.inspiratoryPressure.peakTrace(physiology.MRItrigs.trigIndex(n):(physiology.MRItrigs.trigIndex(n) + physTraces.options.taskScan.tr * physTraces.options.physiology.samplingRate)));
    % Breathing rate
    physValues.values.breathing.rate.valuesPerVolume(n,1) = mean(physiology.breathing.rate.peakTrace(physiology.MRItrigs.trigIndex(n):(physiology.MRItrigs.trigIndex(n) + physTraces.options.taskScan.tr * physTraces.options.physiology.samplingRate)));
    % Breathing depth (raw)
    physValues.values.breathing.depth.valuesPerVolume(n,1) = mean(physiology.breathing.depth.peakTrace(physiology.MRItrigs.trigIndex(n):(physiology.MRItrigs.trigIndex(n) + physTraces.options.taskScan.tr * physTraces.options.physiology.samplingRate)));
    % End tidal CO2
    physValues.values.endTidalCO2.valuesPerVolume(n,1) = nanmean(physiology.endTidalCO2.peakTrace(physiology.MRItrigs.trigIndex(n):(physiology.MRItrigs.trigIndex(n) + physTraces.options.taskScan.tr * physTraces.options.physiology.samplingRate)));
    % End tidal O2
    physValues.values.endTidalO2.valuesPerVolume(n,1) = nanmean(physiology.endTidalO2.peakTrace(physiology.MRItrigs.trigIndex(n):(physiology.MRItrigs.trigIndex(n) + physTraces.options.taskScan.tr * physTraces.options.physiology.samplingRate)));
end

% Double check for NaNs in gas traces
physValues.values.endTidalCO2.valuesPerVolume = fillmissing(physValues.values.endTidalCO2.valuesPerVolume,'previous');
physValues.values.endTidalO2.valuesPerVolume = fillmissing(physValues.values.endTidalO2.valuesPerVolume,'previous');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE CORRELATION MATRICES AND PLOTS WITH MOTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load and demean motion parameters
physTraces.motion.raw = load(physTraces.options.saveNames.motion.task_txt);
physTraces.motion.means = mean(physTraces.motion.raw);
physTraces.motion.demeaned = physTraces.motion.raw - physTraces.motion.means;
physTraces.motion.x = physTraces.motion.demeaned(:,1);
physTraces.motion.y = physTraces.motion.demeaned(:,2);
physTraces.motion.z = physTraces.motion.demeaned(:,3);
physTraces.motion.pitch = physTraces.motion.demeaned(:,4);
physTraces.motion.roll = physTraces.motion.demeaned(:,5);
physTraces.motion.yaw = physTraces.motion.demeaned(:,6);

% Physiology and motion plots and correlations
physTraces.correlations.variableNames = {'MotX', 'MotY', 'MotZ', 'MotP', 'MotR', 'MotYa', 'Press', 'BRate', 'BDepth', 'CO2', 'O2'};
physTraces.correlations.dataRaw = [physTraces.motion.x, physTraces.motion.y, physTraces.motion.z, physTraces.motion.pitch, physTraces.motion.roll, physTraces.motion.yaw, physValues.values.inspiratoryPressure.valuesPerVolumePeakTrace, physValues.values.breathing.rate.valuesPerVolume, physValues.values.breathing.depth.valuesPerVolume, physValues.values.endTidalCO2.valuesPerVolume, physValues.values.endTidalO2.valuesPerVolume];
physTraces.correlations.dataStd = zscore(physTraces.correlations.dataRaw);
physTraces.correlations.tableRaw = table(physTraces.motion.x, physTraces.motion.y, physTraces.motion.z, physTraces.motion.pitch, physTraces.motion.roll, physTraces.motion.yaw, physValues.values.inspiratoryPressure.valuesPerVolumePeakTrace, physValues.values.breathing.rate.valuesPerVolume, physValues.values.breathing.depth.valuesPerVolume, physValues.values.endTidalCO2.valuesPerVolume, physValues.values.endTidalO2.valuesPerVolume, 'VariableNames', physTraces.correlations.variableNames);
[physTraces.correlations.Rvalues, physTraces.correlations.Pvalues] = corrplot(physTraces.correlations.tableRaw, 'testR', 'on');
print(physTraces.options.saveNames.correlations, '-dtiff');

% Add values to summary matrix
physValues.summary.motionCorr.variableNames = physTraces.correlations.variableNames;
physValues.summary.motionCorr.Rvalues = physTraces.correlations.Rvalues;
physValues.summary.motionCorr.Pvalues = physTraces.correlations.Pvalues;

% Plot and save key motion and pressure traces
figure
    ax1 = subplot(3,1,1);
    plot(ax1, physTraces.motion.x, 'Color', 'b');
    hold on
    plot(ax1, physTraces.motion.y, 'Color', [0 0.5 0]);
    plot(ax1, physTraces.motion.z, 'Color', 'r');
    title('Translational motion')
    legend('X', 'Y', 'Z');
    ylabel('Motion (mm)');
    xlabel('Volumes');
    ax2 = subplot(3,1,2);
    plot(ax2, physTraces.motion.pitch, 'Color', 'b');
    hold on
    plot(ax2, physTraces.motion.roll, 'Color', [0 0.5 0]);
    plot(ax2, physTraces.motion.yaw, 'Color', 'r');
    title('Rotational motion')
    legend('P', 'R', 'Y');
    ylabel('Motion (mm)');
    xlabel('Volumes');
    ax3 = subplot(3,1,3);
    plot(ax3, physTraces.motion.x, 'Color', 'b');
    hold on
    plot(ax3, physTraces.motion.y, 'Color', [0 0.5 0]);
    plot(ax3, physTraces.motion.z, 'Color', 'r');
    plot(ax3, physTraces.motion.pitch.*100, 'Color', 'b');
    plot(ax3, physTraces.motion.roll.*100, 'Color', [0 0.5 0]);
    plot(ax3, physTraces.motion.yaw.*100, 'Color', 'r');
    plot(ax3, physValues.values.inspiratoryPressure.valuesPerVolumePeakTrace./10, 'Color', 'k');
    title('Overlayed with mouth pressure trace')
    ylabel('Arbitrary Units');
    xlabel('Volumes');
    hold off
    print(physTraces.options.saveNames.motionVsPressure, '-dtiff');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE PROCESSED PHYSIOLOGY MATRICES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save(physTraces.options.saveNames.physTaskTraces, 'physTraces', '-v7.3', '-nocompression');
save(physTraces.options.saveNames.physTaskValues, 'physValues');

end
