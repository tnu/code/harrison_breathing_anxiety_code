############################################################################################

# PBIHB: SET OFF RANDOMISE ANALYSES

############################################################################################

# DESCRIPTION:
# This script runs randomise (via pbihb_randomise function) for the PBIHB study
# Usage: ./eulerRandomise $data $scripts
#	data = path to main data folder
#	scripts = path to PBIHB scripts folder
# INFO FOR pbihb_randomise:
# Usage: ./runRandomise $data $scripts $higherL $lowerL
#	data = path to main data folder
#	scripts = path to PBIHB scripts folder
#	higherL = name of higher level design ('groups' 'groupsEx' 'regress' or 'explore')
#	lowerL = name of lower level design ('basic' 'model' or 'modelFactors')
#	conName = name of lower level contrast (number or name e.g. con0001 or cueTot)
#	mask = name of mask to use ('PAG' or 'full')

############################################################################################

# Set off the randomise
eulerRandomise(){

	# Define local variables from inputs
	local data=$1
	local scripts=$2
	
	# make sure filename supplied as command line arg else die
	[ $# -eq 0 ] && { echo "Usage: $0 data scripts"; exit 1; }

	############################################################################################
	# BASIC DESIGN

	# Set off randomise for group diff + basic design: resistance
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_basic_resist \
		"./pbihb_randomise.sh $data $scripts groups basic resistanceBlock PAG";

	# Set off randomise for group diff + basic design: resistance surprise
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_basic_resistSurprise \
		"./pbihb_randomise.sh $data $scripts groups basic resistanceSurprise PAG";

	# Set off randomise for group diff + basic design: no resistance surprise
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_basic_noResistSurprise \
		"./pbihb_randomise.sh $data $scripts groups basic noResistanceSurprise PAG";

	# Set off randomise for group diff + basic design: prediction average yes / no
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_basic_predictYesNoAvg \
		"./pbihb_randomise.sh $data $scripts groups basic avgCue PAG";

	# Set off randomise for group diff + basic design: prediction yes > no
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_basic_predictYesNoDiff \
		"./pbihb_randomise.sh $data $scripts groups basic cueYesNoDiff full";


	############################################################################################
	# MODEL DESIGN

	# Set off randomise for group diff + model design: positive prediction
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_predPos \
		"./pbihb_randomise.sh $data $scripts groups model predictPos_min full";

	# Set off randomise for group diff + model design: negative prediction
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_predNeg \
		"./pbihb_randomise.sh $data $scripts groups model predictNeg_min full";

	# Set off randomise for group diff + model design: average prediction
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_predAvg \
		"./pbihb_randomise.sh $data $scripts groups model predAvg full";

	# Set off randomise for group diff + model design: prediction interaction positive
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_predIntPos \
		"./pbihb_randomise.sh $data $scripts groups model predIntPos full";

	# Set off randomise for group diff + model design: prediction interaction negative
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_predIntNeg \
		"./pbihb_randomise.sh $data $scripts groups model predIntNeg full";

	# Set off randomise for group diff + model design: positive error
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_errorPos \
		"./pbihb_randomise.sh $data $scripts groups model errorPos full";

	# Set off randomise for group diff + model design: negative error
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_errorNeg \
		"./pbihb_randomise.sh $data $scripts groups model errorNeg full";

	# Set off randomise for group diff + model design: average error
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_errorAvg \
		"./pbihb_randomise.sh $data $scripts groups model errorAvg full";

	# Set off randomise for group diff + model design: error interaction positive
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_errorIntPos \
		"./pbihb_randomise.sh $data $scripts groups model errorIntPos full";

	# Set off randomise for group diff + model design: error interaction negative
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_errorIntNeg \
		"./pbihb_randomise.sh $data $scripts groups model errorIntNeg full";

	# Set off randomise for group diff + model design (excluded participants): positive prediction
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_predPos \
		"./pbihb_randomise.sh $data $scripts groupsEx model predictPos_min full";

	# Set off randomise for group diff + model design (excluded participants): negative prediction
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_predNeg \
		"./pbihb_randomise.sh $data $scripts groupsEx model predictNeg_min full";

	# Set off randomise for group diff + model design (excluded participants): average prediction
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_predAvg \
		"./pbihb_randomise.sh $data $scripts groupsEx model predAvg full";

	# Set off randomise for group diff + model design (excluded participants): prediction interaction positive
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_predIntPos \
		"./pbihb_randomise.sh $data $scripts groupsEx model predIntPos full";

	# Set off randomise for group diff + model design (excluded participants): prediction interaction negative
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_predIntNeg \
		"./pbihb_randomise.sh $data $scripts groupsEx model predIntNeg full";

	# Set off randomise for group diff + model design (excluded participants): positive error
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_errorPos \
		"./pbihb_randomise.sh $data $scripts groupsEx model errorPos full";

	# Set off randomise for group diff + model design (excluded participants): negative error
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_errorNeg \
		"./pbihb_randomise.sh $data $scripts groupsEx model errorNeg full";

	# Set off randomise for group diff + model design (excluded participants): average error
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_errorAvg \
		"./pbihb_randomise.sh $data $scripts groupsEx model errorAvg full";

	# Set off randomise for group diff + model design (excluded participants): error interaction positive
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_errorIntPos \
		"./pbihb_randomise.sh $data $scripts groupsEx model errorIntPos full";

	# Set off randomise for group diff + model design (excluded participants): error interaction negative
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupDiff_model_errorIntNeg \
		"./pbihb_randomise.sh $data $scripts groupsEx model errorIntNeg full";

	# Set off randomise for group regress + model design: positive prediction
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupRegress_model_predPos \
		"./pbihb_randomise.sh $data $scripts regress model predictPos_min full";

	# Set off randomise for group regress + model design: negative prediction
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupRegress_model_predNeg \
		"./pbihb_randomise.sh $data $scripts regress model predictNeg_min full";

	# Set off randomise for group regress + model design: average prediction
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupRegress_model_predAvg \
		"./pbihb_randomise.sh $data $scripts regress model predAvg full";

	# Set off randomise for group regress + model design: prediction interaction positive
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupRegress_model_predIntPos \
		"./pbihb_randomise.sh $data $scripts regress model predIntPos full";

	# Set off randomise for group regress + model design: prediction interaction negative
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupRegress_model_predIntNeg \
		"./pbihb_randomise.sh $data $scripts regress model predIntNeg full";

	# Set off randomise for group regress + model design: positive error
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupRegress_model_errorPos \
		"./pbihb_randomise.sh $data $scripts regress model errorPos full";

	# Set off randomise for group regress + model design: negative error
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupRegress_model_errorNeg \
		"./pbihb_randomise.sh $data $scripts regress model errorNeg full";

	# Set off randomise for group regress + model design: average error
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupRegress_model_errorAvg \
		"./pbihb_randomise.sh $data $scripts regress model errorAvg full";

	# Set off randomise for group regress + model design: error interaction positive
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupRegress_model_errorIntPos \
		"./pbihb_randomise.sh $data $scripts regress model errorIntPos full";

	# Set off randomise for group regress + model design: error interaction negative
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupRegress_model_errorIntNeg \
		"./pbihb_randomise.sh $data $scripts regress model errorIntNeg full";

	# Set off randomise for group explore + model design: positive prediction
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupExplore_model_predPos \
		"./pbihb_randomise.sh $data $scripts explore model predictPos_min full";

	# Set off randomise for group explore + model design: negative prediction
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupExplore_model_predNeg \
		"./pbihb_randomise.sh $data $scripts explore model predictNeg_min full";

	# Set off randomise for group explore + model design: average prediction
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupExplore_model_predAvg \
		"./pbihb_randomise.sh $data $scripts explore model predAvg full";

	# Set off randomise for group explore + model design: prediction interaction positive
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupExplore_model_predIntPos \
		"./pbihb_randomise.sh $data $scripts explore model predIntPos full";

	# Set off randomise for group explore + model design: prediction interaction negative
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupExplore_model_predIntNeg \
		"./pbihb_randomise.sh $data $scripts explore model predIntNeg full";

	# Set off randomise for group explore + model design: positive error
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupExplore_model_errorPos \
		"./pbihb_randomise.sh $data $scripts explore model errorPos full";

	# Set off randomise for group explore + model design: negative error
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupExplore_model_errorNeg \
		"./pbihb_randomise.sh $data $scripts explore model errorNeg full";

	# Set off randomise for group explore + model design: average error
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupExplore_model_errorAvg \
		"./pbihb_randomise.sh $data $scripts explore model errorAvg full";

	# Set off randomise for group explore + model design: error interaction positive
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupExplore_model_errorIntPos \
		"./pbihb_randomise.sh $data $scripts explore model errorIntPos full";

	# Set off randomise for group explore + model design: error interaction negative
	bsub -W 24:00 -R "rusage[mem=50000]" -o pbihb_euler_logFiles/PBIHB_randomise_groupExplore_model_errorIntNeg \
		"./pbihb_randomise.sh $data $scripts explore model errorIntNeg full";

}

# Invoke the function and pass arguments
eulerRandomise $1 $2
