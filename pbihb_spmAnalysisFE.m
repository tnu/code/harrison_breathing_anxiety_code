%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% BREATHING SCANNER TASK: FIXED EFFECTS SPM ANALYSIS %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 27/05/2020
% -------------------------------------------------------------------------
% TO RUN:   analysis    = pbihb_spmAnalysisFE(options, roi, contrast)
% INPUTS:   options     = PPID-specific matrix output from pbihb_setOptions
%           roi         = seed: 'aIns' or 'PAG'
%           contrast    = 'predict', 'error' or 'both'
% OUTPUTS:  analysis    = Structure with spm batch specifications and 
%                         results added
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script runs the middle level fixed effects analysis for the 
% functional connectivity of the 7 Tesla functional task data collected in 
% the PBIHB study (in spm), using the following:
%   1) Model specification from the GLM designed in the specified 
%      functional connectivity lower level analyses
%      higher levels
%   2) Model estimation of this GLM
%   3) Setting contrasts of interest
%   4) Estimation and output of thresholded results
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function analysis = pbihb_spmAnalysisFE(options, roi, contrast)

%% SPECIFY INFO AND LOAD GLMS

% Add options
analysis.options = options;

% Parse out contrasts
if strcmp(contrast, 'both')
    contrast1 = 'predict';
    contrast2 = 'error';
else
    contrast1 = contrast;
end

% Specify sub-contrasts
analysis.contrast1Pos = [contrast1, 'Pos'];
analysis.contrast1Neg = [contrast1, 'Neg'];

% Specify additional information
analysis.name = ['FE_', roi, '_', contrast1];
analysis.design1Pos = load([analysis.options.saveNames.glmModelMatrixFC, roi, '_', analysis.contrast1Pos, '.mat']);
analysis.design1Neg = load([analysis.options.saveNames.glmModelMatrixFC, roi, '_', analysis.contrast1Neg, '.mat']);
analysis.SPM1pos = fullfile([analysis.options.paths.analysis_directories.modelFC, roi, '_', analysis.contrast1Pos], 'SPM.mat');
analysis.SPM1neg = fullfile([analysis.options.paths.analysis_directories.modelFC, roi, '_', analysis.contrast1Neg], 'SPM.mat');

% Check if running both predictions and errors at once and add information
if strcmp(contrast, 'both')
    % Specify sub-contrasts
    analysis.contrast2Pos = [contrast2, 'Pos'];
    analysis.contrast2Neg = [contrast2, 'Neg'];
    % Specify additional information
    analysis.name = [analysis.name, '_', contrast2];
    analysis.design2Pos = load([analysis.options.saveNames.glmModelMatrixFC, roi, '_', analysis.contrast2Pos, '.mat']);
    analysis.design2Neg = load([analysis.options.saveNames.glmModelMatrixFC, roi, '_', analysis.contrast2Neg, '.mat']);
    analysis.SPM2pos = fullfile([analysis.options.paths.analysis_directories.modelFC, roi, '_', analysis.contrast2Pos], 'SPM.mat');
    analysis.SPM2neg = fullfile([analysis.options.paths.analysis_directories.modelFC, roi, '_', analysis.contrast2Neg], 'SPM.mat');
end

% Specify save names and locations
saveFolder = [analysis.options.paths.analysis_directories.modelFC, analysis.name];
saveName = fullfile(saveFolder, ['PBIHB_', options.PPID, '_analysis_modelFC_', analysis.name]);

% Check directory existance and make if required or delete old SPM
if ~exist(saveFolder, 'dir')
   mkdir(saveFolder)
elseif isfile(fullfile(saveFolder, 'SPM.mat'))
    delete(fullfile(saveFolder, 'SPM.mat'));
end

% Clear matlabbatch variables
matlabbatch = {};


%% 1) RUN MODEL SPECIFICATION

matlabbatch{1}.spm.stats.mfx.ffx.dir = cellstr(saveFolder);
matlabbatch{1}.spm.stats.mfx.ffx.spmmat{1,1} = analysis.SPM1pos;
matlabbatch{1}.spm.stats.mfx.ffx.spmmat{2,1} = analysis.SPM1neg;
if strcmp(contrast, 'both')
    matlabbatch{1}.spm.stats.mfx.ffx.spmmat{3,1} = analysis.SPM2pos;
    matlabbatch{1}.spm.stats.mfx.ffx.spmmat{4,1} = analysis.SPM2neg;
end


%% 2) RUN MODEL ESTIMATION

matlabbatch{2}.spm.stats.fmri_est.spmmat(1) = cfg_dep('FFX Specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;


%% 3) SET CONTRASTS OF INTEREST AND EXPORT RESULTS

matlabbatch{3}.spm.stats.con.spmmat(1) = cfg_dep('FFX Specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));

% Create a filler for weights if necessary
if strcmp(contrast, 'both')
    postFill = zeros(1, (length(analysis.design2Pos.extraContrasts.weights) + length(analysis.design2Neg.extraContrasts.weights)));
    preFill = zeros(1, (length(analysis.design1Pos.extraContrasts.weights) + length(analysis.design1Neg.extraContrasts.weights)));
else
    postFill = [];
end

% Create contrast for average
matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = [contrast1, '_average'];
matlabbatch{3}.spm.stats.con.consess{1}.tcon.weights = [analysis.design1Pos.extraContrasts.weights, analysis.design1Neg.extraContrasts.weights, postFill];
matlabbatch{3}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
analysis.design.finalMatrixOrder{1} = matlabbatch{3}.spm.stats.con.consess{1}.tcon.name;

% Create contrast for difference
matlabbatch{3}.spm.stats.con.consess{2}.tcon.name = [contrast1, '_PosNegDiff'];
matlabbatch{3}.spm.stats.con.consess{2}.tcon.weights = [analysis.design1Pos.extraContrasts.weights, -analysis.design1Neg.extraContrasts.weights, postFill];
matlabbatch{3}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
analysis.design.finalMatrixOrder{2} = matlabbatch{3}.spm.stats.con.consess{2}.tcon.name;

% Add second condition contrasts (if using)
if strcmp(contrast, 'both')
    % Create contrast for average
    matlabbatch{3}.spm.stats.con.consess{3}.tcon.name = [contrast2, '_average'];
    matlabbatch{3}.spm.stats.con.consess{3}.tcon.weights = [preFill, analysis.design2Pos.extraContrasts.weights, analysis.design2Neg.extraContrasts.weights];
    matlabbatch{3}.spm.stats.con.consess{3}.tcon.sessrep = 'none';
    analysis.design.finalMatrixOrder{3} = matlabbatch{3}.spm.stats.con.consess{3}.tcon.name;
    
    % Create contrast for difference
    matlabbatch{3}.spm.stats.con.consess{4}.tcon.name = [contrast2, '_PosNegDiff'];
    matlabbatch{3}.spm.stats.con.consess{4}.tcon.weights = [preFill, analysis.design2Pos.extraContrasts.weights, -analysis.design2Neg.extraContrasts.weights];
    matlabbatch{3}.spm.stats.con.consess{4}.tcon.sessrep = 'none';
    analysis.design.finalMatrixOrder{4} = matlabbatch{3}.spm.stats.con.consess{4}.tcon.name;

    % Create contrast for overall difference between conditions
    matlabbatch{3}.spm.stats.con.consess{5}.tcon.name = [contrast1, '_', contrast2, '_diff'];
    matlabbatch{3}.spm.stats.con.consess{5}.tcon.weights = [analysis.design1Pos.extraContrasts.weights, analysis.design1Neg.extraContrasts.weights, -analysis.design2Pos.extraContrasts.weights, -analysis.design2Neg.extraContrasts.weights];
    matlabbatch{3}.spm.stats.con.consess{5}.tcon.sessrep = 'none';
    analysis.design.finalMatrixOrder{5} = matlabbatch{3}.spm.stats.con.consess{5}.tcon.name;
    
end

matlabbatch{3}.spm.stats.con.delete = 0;


%% 4) ESTIMATE AND OUTPUT THRESHOLDED RESULTS

matlabbatch{4}.spm.stats.results.spmmat(1) = cfg_dep('FFX Specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{4}.spm.stats.results.conspec.titlestr = '';
matlabbatch{4}.spm.stats.results.conspec.contrasts = Inf;
matlabbatch{4}.spm.stats.results.conspec.threshdesc = 'FWE';
matlabbatch{4}.spm.stats.results.conspec.thresh = 0.05;
matlabbatch{4}.spm.stats.results.conspec.extent = 0;
matlabbatch{4}.spm.stats.results.conspec.conjunction = 1;
matlabbatch{4}.spm.stats.results.conspec.mask.none = 1;
matlabbatch{4}.spm.stats.results.units = 1;
matlabbatch{4}.spm.stats.results.export{1}.ps = true;
matlabbatch{4}.spm.stats.results.export{2}.tspm.basename = 'thresh';


%% SAVE THE SPECS AND RUN THE JOBS

analysis.matlabbatch = matlabbatch;
save(saveName, 'analysis');

spm_jobman('run', matlabbatch);


%% REMOVE ALL BETA FILES TO SAVE SPACE

% Remove all beta files to save space
disp('Deleting all beta analysis files...');
delete(fullfile(saveFolder, 'beta*.nii'));
disp('... Finished!');


%% RETURN TO ANALYSIS DIRECTORY

cd(analysis.options.paths.analysis_scripts);


end