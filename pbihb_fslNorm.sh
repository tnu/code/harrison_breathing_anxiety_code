############################################################################################

# PBIHB: REGISTRATION OF EPI IMAGE TO STANDARD SPACE

############################################################################################

# DESCRIPTION:
# This function runs an FSL EPI image normalisation to standard space for the PBIHB study.
# Requires a full registration run prior.
# Usage: ./fslNorm $subj $data $scripts $type
#	subj = variable containing PPID
#	data = path to main data folder
#	scripts = path to main scripts folder
#       type = task or rest

############################################################################################
############################################################################################


# Define the function
fslNorm(){

	# Define local variables from inputs
	local subj=$1
	local data=$2
	local scripts=$3
	local type=$4
	
	# make sure filename supplied as command line arg else die
	[ $# -eq 0 ] && { echo "Usage: $0 subj data scripts type"; exit 1; }
	
	echo ---------------------------------------
	echo FSL EPI IMAGE NORMALISATION
	echo ---------------------------------------
	
	# Run applywarp for normalisation of EPI from registration folder
	echo Normalising EPI image...
	applywarp -i ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_preproc_FSL_${type}.feat/filtered_func_data.nii.gz \
		-o ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_${type}_std.nii.gz \
		-r $FSLDIR/data/standard/MNI152_T1_1mm_brain.nii.gz \
		-w ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_preproc_FSL_${type}.feat/reg/example_func2standard_warp.nii.gz

	echo ---------------------------------------
	echo FSL EPI IMAGE NORMALISATION FINISHED!
	echo ---------------------------------------

}

# Invoke the function and pass arguments
fslNorm $1 $2 $3 $4
