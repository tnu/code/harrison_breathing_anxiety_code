%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% BREATHING SCANNER TASK: GROUP TASK ANALYSES %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 10/04/2020
% -------------------------------------------------------------------------
% TO RUN:   pbihb_taskGroupEuler
% INPUTS:   location    = 'local' or 'euler'
% OUTPUTS:  All specified group fMRI analyses
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script runs the group analysis steps on Euler for the 7 Tesla 
% functional data collected in the PBIHB study.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_taskGroup(location)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set all options for preprocessing with specific location
pbihb_setOptions('all', location);

% Initialise SPM and PhysIO
pbihb_initSPM;

% Create group GLMs
pbihb_createGlmGroup(location);

% Run group analysis for basic design
pbihb_spmAnalysisGroup(location, 'groups', 'basic')

% Run group analyses for model design
pbihb_checkModelCorr(location);
pbihb_spmAnalysisGroup(location, 'groups', 'model')
pbihb_spmAnalysisGroup(location, 'groupsEx', 'model')
pbihb_spmAnalysisGroup(location, 'regress', 'model')
pbihb_spmAnalysisGroup(location, 'explore', 'model')

% % Run sanity check group analyses
% pbihb_spmAnalysisGroup(location, 'groups', 'basicSPM')
% pbihb_spmAnalysisGroup(location, 'groups', 'modelPred')
% pbihb_spmAnalysisGroup(location, 'groups', 'modelErr')
% pbihb_spmAnalysisGroup(location, 'groups', 'modelFactors')

% Run group analyses for FC analyses
pbihb_spmAnalysisGroup(location, 'groups', 'FC', 'aIns', 'both');
pbihb_spmAnalysisGroup(location, 'groupsEx', 'FC', 'aIns', 'both');
pbihb_spmAnalysisGroup(location, 'groups', 'FC', 'PAG', 'error');
pbihb_spmAnalysisGroup(location, 'groupsEx', 'FC', 'PAG', 'error');


end