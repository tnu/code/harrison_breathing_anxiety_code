%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% BREATHING SCANNER TASK: CREATING FUNCTIONAL CONNECTIVITY GLM %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 22/04/2020
% -------------------------------------------------------------------------
% TO RUN:   glmModelFC  = pbihb_createGlmModelFC(options)
% INPUTS:   options     = PPID-specific matrix output from pbihb_setOptions
%           roi         = 'aIns' or 'PAG'
%           contrast    = 'predictPos', 'predictNeg', 'errorPos' or
%                         'errorNeg'
% OUTPUTS:  modelFC     = Structure with glm matrix for functional
%                         connectivity analysis
%           figures     = Summary figures are generated to display the
%                         correlation matrices for key model trajectories 
%                         against motion traces
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script takes the model-based task design matrix for input into SPM,
% and substitutes specified regressors for specified timeseries for
% functional connectivity analyses. All other regressors in the GLM are
% manually convolved and this structure does not use the automatic SPM
% functions for creating GLMs.
% N.B. This script requires outputs from the pbihb_createGlmModel function
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function modelFC = pbihb_createGlmModelFC(options, roi, contrast)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY OPTIONS AND LOAD REQUIRED DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load the model-based design
modelGLM = load(options.saveNames.glmModelMatrix);

% Save the specs
modelFC.roi = roi;
modelFC.contrast = contrast;

% Add the '_min' prefix for predictions
if contains(contrast, 'predict')
    add = '_min';
else
    add = [];
end

% Load the roi timeseries
modelFC.roi_timeseries = load(fullfile(options.paths.timeseries_directory, ...
    ['VOI_', options.PPID, '_', roi, '_', contrast, add, '_1.mat']));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% UPDATE THE GLM: SUBSTITUTE THE ROI TIMESERIES FOR CONTRAST OF INTEREST
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copy the original GLM to new structure
modelFC.R = modelGLM.R;
modelFC.names = modelGLM.names;

% Find the index for the contrast to replace
idx = find(strcmp(modelGLM.names, contrast));
if isempty(idx)
    error('Specified contrast not found!')
end

% Re-name the contrast to the ROI
modelFC.names{idx} = [roi, '_', contrast];

% Calculate the regressor and contrast index
conIdx = (idx - 1) * 3 + 1;

% Pull out the original regressor
modelFC.origRegressor = modelFC.R(:,conIdx);

% Replace the appropriate matrix column with the ROI timeseries
modelFC.R(:,conIdx) = modelFC.roi_timeseries.Y;

% Remove the temporal and dispersion derivatives of the original regressor
modelFC.R = [modelFC.R(:,1:conIdx), modelFC.R(:,(conIdx+3):end)];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY THE FUNCTIONAL CONNECTIVITY CONTRAST FOR ANALYSIS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Add contrast name
modelFC.extraContrasts.names = [roi, '_', contrast];

% Add the contrast weight
modelFC.extraContrasts.weights = zeros(size(modelFC.R,2),1)';
modelFC.extraContrasts.weights(conIdx) = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE THE FUNCTIONAL CONNECTIVITY GLM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify the save name
saveName = [options.saveNames.glmModelMatrixFC, roi, '_', contrast, '.mat'];

% Save the structure
save(saveName, '-struct', 'modelFC');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLOT THE ORIGINAL REGRESSOR AND ROI TIMESERIES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Make the figure
figure
set(gcf, 'Position', [0 0 1500 300]);
plot(modelFC.origRegressor, 'Color', [0.8 0.8 0.8], 'LineWidth', 2);
hold on
yyaxis right
plot(modelFC.roi_timeseries.Y, 'k');
titletext = [modelFC.roi, ' ROI / ', modelFC.contrast, ' regressor'];
title(titletext, 'FontSize', 20);
xlabel('TR', 'FontSize', 20);
saveName = [options.saveNames.glmModelFCFig, modelFC.roi, '_', modelFC.contrast];
print(saveName, '-dtiff');

end