############################################################################################

# PBIHB: RUN FIX STEPS

############################################################################################

# DESCRIPTION:
# This function runs FIX on the ICA's for denoising in the PBIHB study
# Usage: ./runFIX $subj $data $scripts $type
#	subj = variable containing PPID
#	data = path to main data folder
#	scripts = path to main scripts folder

############################################################################################
############################################################################################
# Installation:

# Install R packages
# $ module load new gcc/4.8.2 r/3.3.0
# $ R
#   -> source("pbihb_installFixDependencies.r")
#   -> quit()

# Copy key files into `fix/` folder
# $ cp call_fsl.m fix/
# $ cp call_matlab.sh fix/

# NOTES
# This is because FSL requires OpenBLAS, but it is installed in a nonstandard place, i.e.
# $ module purge
# $ module load new fsl/6.0.1
# $ ldd $(which fslhd)
# We therefore monkey patch `call_fsl.m` to include the location of OpenBLAS:
# $ diff call_fsl.m $FSLDIR/etc/matlab/call_fsl.m
# And have to add the current path to `call_matlab.sh` to make it persist...

############################################################################################

# Define the function
runFIX(){

	# Define local variables from inputs
	local subj=$1
	local data=$2
	local scripts=$3
	local type=$4
	
	# make sure filename supplied as command line arg else die
	[ $# -eq 0 ] && { echo "Usage: $0 subj data scripts type"; exit 1; }
	
	echo ---------------------------------------
	echo FIX CLASSIFICATION
	echo ---------------------------------------
	
	# Run component extraction (FIX step 1)
#	echo Running component extraction...
#	${scripts}/fix/fix -f ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_preproc_FSL_${type}.feat

	# Run classification (FIX step 2)
	echo Running component classification with PBIHB_training_rest.RData...
	${scripts}/fix/fix -c ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_preproc_FSL_${type}.feat ${scripts}/fix_PBIHB_trainingFiles/PBIHB_training_taskFirst20.RData 15

	echo ---------------------------------------
	echo FIX CLASSIFICATION FINISHED!
	echo ---------------------------------------

}

# Invoke the function and pass arguments
runFIX $1 $2 $3 $4
