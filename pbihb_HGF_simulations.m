%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% BREATHING SCANNER TASK: HYPOTHESIS GENERATION %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 28/05/2019
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script runs HGF simulations to determine the effect of omega 2 and 
% kappa 2 on HGF variables such as accuracy, learning rate, precision 
% estimates and prediction errors at each level. This information can then
% be used to inform hypotheses as to how anxiety may influence parameters.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load the 80-trial sequence
sequence = load('pbihb_cue_seq.mat');

% Specify the baseline parameter inputs
parameter_inputs = [NaN 0 1 NaN 1 1 NaN 0 0 1 1 NaN -3 -6];

% Specify parameter changes (high to low)
omega2_inputs = [-1 -3 -5];
kappa2_inputs = [1.9 1 0.1];

% Simulate and plot with omega 2 changes
for a = 1:length(omega2_inputs)
    parameter_inputs_om2(a,:) = parameter_inputs;
    parameter_inputs_om2(a,13) = omega2_inputs(a);
    % Simulate
    sim_om2(a) = tapas_simModel(sequence.pairings,...
                 'tapas_hgf_binary',...
                 parameter_inputs_om2(a,1:14),...
                 'tapas_unitsq_sgm',...
                 5);
    % Score for accuracy
    om2scores(a,1) = 0;
    for n = 1:length(sim_om2(a).u)
        if (sim_om2(a).u(n,1) == 1 && sim_om2(a).y(n,1) == 1) || (sim_om2(a).u(n,1) == 0 && sim_om2(a).y(n,1) == 0)
            om2scores(a,1) = om2scores(a,1) + 1;
        end
    end
    om2scores(a,1) = om2scores(a,1) / (length(sim_om2(a).u)) * 100;
    % Plot
    tapas_hgf_binary_plotTraj(sim_om2(a))
end

% Simulate and plot with kappa 2 changes
for a = 1:length(kappa2_inputs)
    parameter_inputs_ka2(a,:) = parameter_inputs;
    parameter_inputs_ka2(a,11) = kappa2_inputs(a);
    % Simulate
    sim_ka2(a) = tapas_simModel(sequence.pairings,...
                 'tapas_hgf_binary',...
                 parameter_inputs_ka2(a,1:14),...
                 'tapas_unitsq_sgm',...
                 5);
    % Score for accuracy
    ka2scores(a,1) = 0;
    for n = 1:length(sim_ka2(a).u)
        if (sim_ka2(a).u(n,1) == 1 && sim_ka2(a).y(n,1) == 1) || (sim_ka2(a).u(n,1) == 0 && sim_ka2(a).y(n,1) == 0)
            ka2scores(a,1) = ka2scores(a,1) + 1;
        end
    end
    ka2scores(a,1) = ka2scores(a,1) / (length(sim_ka2(a).u)) * 100;
    % Plot
    tapas_hgf_binary_plotTraj(sim_ka2(a))
end

% Specify value
value = {'High'; 'Medium'; 'Low'};

% Calculate average learning rate (order = high med low)
om2learningRate = [(mean((sim_om2(1).traj.wt(:,1)))); (mean((sim_om2(2).traj.wt(:,1)))); (mean((sim_om2(3).traj.wt(:,1))))];
ka2learningRate = [(mean((sim_ka2(1).traj.wt(:,1)))); (mean((sim_ka2(2).traj.wt(:,1)))); (mean((sim_ka2(3).traj.wt(:,1))))];

% Calculate average precisions (order = high med low)
om2precisionsLevel2 = [(1/(mean(sim_om2(1).traj.sa(:,2)))); (1/(mean(sim_om2(2).traj.sa(:,2)))); (1/(mean(sim_om2(3).traj.sa(:,2))))];
om2precisionsLevel3 = [(1/(mean(sim_om2(1).traj.sa(:,3)))); (1/(mean(sim_om2(2).traj.sa(:,3)))); (1/(mean(sim_om2(3).traj.sa(:,3))))];
ka2precisionsLevel2 = [(1/(mean(sim_ka2(1).traj.sa(:,2)))); (1/(mean(sim_ka2(2).traj.sa(:,2)))); (1/(mean(sim_ka2(3).traj.sa(:,2))))];
ka2precisionsLevel3 = [(1/(mean(sim_ka2(1).traj.sa(:,3)))); (1/(mean(sim_ka2(2).traj.sa(:,3)))); (1/(mean(sim_ka2(3).traj.sa(:,3))))];

% Calculate average prediction errors (order = high med low)
om2errorsLevel2 = [(mean((sim_om2(1).traj.psi(:,2)))); (mean((sim_om2(2).traj.psi(:,2)))); (mean((sim_om2(3).traj.psi(:,2))))];
om2errorsLevel3 = [(mean((sim_om2(1).traj.psi(:,3)))); (mean((sim_om2(2).traj.psi(:,3)))); (mean((sim_om2(3).traj.psi(:,3))))];
ka2errorsLevel2 = [(mean((sim_ka2(1).traj.psi(:,2)))); (mean((sim_ka2(2).traj.psi(:,2)))); (mean((sim_ka2(3).traj.psi(:,2))))];
ka2errorsLevel3 = [(mean((sim_ka2(1).traj.psi(:,3)))); (mean((sim_ka2(2).traj.psi(:,3)))); (mean((sim_ka2(3).traj.psi(:,3))))];

% Create a results table
resultsTable = table(value, om2scores, ka2scores, om2learningRate, ka2learningRate, om2precisionsLevel2, ka2precisionsLevel2, om2errorsLevel2, ka2errorsLevel2, om2precisionsLevel3, ka2precisionsLevel3, om2errorsLevel3, ka2errorsLevel3);



