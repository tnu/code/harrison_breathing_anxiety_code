###########################################################################################

# PBIHB: GET <AXIMUM SINGLE SUBJECT CONTRAST ESTIMATES FOR INSULA ROIS

############################################################################################

# DESCRIPTION:
# This function pulls out the maximum single-subject contrast estimates from lower level
# contrasts of interest in the anterior insula: positive/negative predictions and errors.
# Usage: ./getMaxInsula $data $subj
#	data = path to main data folder
#	subj = list of PPIDs to analyse

############################################################################################
############################################################################################

# Define the function
getMaxInsula(){

	# Define local variables from inputs
	local data=$1
	local subj=("$@")

	# Set filenames for collated contrast estimates
	dataPathTotal=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groups_model
	dataInsPosPred_outTotal=${dataPathTotal}/VOI_total_aIns_predictPos_min_mean.txt
	dataInsNegPred_outTotal=${dataPathTotal}/VOI_total_aIns_predictNeg_min_mean.txt
	dataInsPosErr_outTotal=${dataPathTotal}/VOI_total_aIns_errorPos_mean.txt
	dataInsNegErr_outTotal=${dataPathTotal}/VOI_total_aIns_errorNeg_mean.txt

	# Check if collated files exist and delete if necessary
	if [ -f "$dataInsPosPred_outTotal" ]; then
		rm $dataInsPosPred_outTotal
	fi
	if [ -f "$dataInsNegPred_outTotal" ]; then
		rm $dataInsNegPred_outTotal
	fi
	if [ -f "$dataInsPosErr_outTotal" ]; then
		rm $dataInsPosErr_outTotal
	fi
	if [ -f "$dataInsNegErr_outTotal" ]; then
		rm $dataInsNegErr_outTotal
	fi

	# Loop through each PPID (minus data variable)
	numSubj=${#subj[@]}
	
	for ((a=1; a<=${numSubj}-1; a++)); do

		# Specify PPID
		singleSubj="${subj[a]}"

		# Set data paths
		dataPath=${data}/TNU_PBIHB_${singleSubj}/PBIHB_${singleSubj}_analysis/PBIHB_${singleSubj}_model
		dataPath_mask=${data}/TNU_PBIHB_${singleSubj}/PBIHB_${singleSubj}_glm/PBIHB_${singleSubj}_timeseries

		# Specify required data for positive predictions
		dataInsPosPred=${dataPath}/con_0002.nii.gz
		dataInsPosPred_mask=${dataPath_mask}/VOI_${singleSubj}_aIns_predictPos_min_mask.nii.gz
		dataInsPosPred_out=${dataPath_mask}/VOI_${singleSubj}_aIns_predictPos_min_mean.txt

		# Specify required data for negative predictions
		dataInsNegPred=${dataPath}/con_0003.nii.gz
		dataInsNegPred_mask=${dataPath_mask}/VOI_${singleSubj}_aIns_predictNeg_min_mask.nii.gz
		dataInsNegPred_out=${dataPath_mask}/VOI_${singleSubj}_aIns_predictNeg_min_mean.txt

		# Specify required data for positive errors
		dataInsPosErr=${dataPath}/con_0006.nii.gz
		dataInsPosErr_mask=${dataPath_mask}/VOI_${singleSubj}_aIns_errorPos_mask.nii.gz
		dataInsPosErr_out=${dataPath_mask}/VOI_${singleSubj}_aIns_errorPos_mean.txt

		# Specify required data for negative errors
		dataInsNegErr=${dataPath}/con_0007.nii.gz
		dataInsNegErr_mask=${dataPath_mask}/VOI_${singleSubj}_aIns_errorNeg_mask.nii.gz
		dataInsNegErr_out=${dataPath_mask}/VOI_${singleSubj}_aIns_errorNeg_mean.txt

		# Pull out contrast estimate for positive predictions
		fslstats ${dataInsPosPred} -k ${dataInsPosPred_mask} -n -M > ${dataInsPosPred_out}
		echo $singleSubj `fslstats ${dataInsPosPred} -k ${dataInsPosPred_mask} -n -M` >> ${dataInsPosPred_outTotal}

		# Pull out contrast estimate for negative predictions
		fslstats ${dataInsNegPred} -k ${dataInsNegPred_mask} -n -M > ${dataInsNegPred_out}
		echo $singleSubj `fslstats ${dataInsNegPred} -k ${dataInsNegPred_mask} -n -M` >> ${dataInsNegPred_outTotal}

		# Pull out contrast estimate for positive errors
		fslstats ${dataInsPosErr} -k ${dataInsPosErr_mask} -n -M > ${dataInsPosErr_out}
		echo $singleSubj `fslstats ${dataInsPosErr} -k ${dataInsPosErr_mask} -n -M` >> ${dataInsPosErr_outTotal}

		# Pull out contrast estimate for negative errors
		fslstats ${dataInsNegErr} -k ${dataInsNegErr_mask} -n -M > ${dataInsNegErr_out}
		echo $singleSubj `fslstats ${dataInsNegErr} -k ${dataInsNegErr_mask} -n -M` >> ${dataInsNegErr_outTotal}


	# End PPID loop
	done

}

# Invoke the function and pass arguments
getMaxInsula $*
