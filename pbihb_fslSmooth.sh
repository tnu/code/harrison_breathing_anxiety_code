############################################################################################

# PBIHB: SMOOTHING OF EPI IMAGE IN STANDARD SPACE

############################################################################################

# DESCRIPTION:
# This function runs smoothing in FSL on a normalised EPI (in image standard space) for the 
# PBIHB study. Requires image to be normalised using pbihb_fslNorm prior.
# Usage: ./fslSmooth $subj $data $scripts $type
#	subj = variable containing PPID
#	data = path to main data folder
#	scripts = path to main scripts folder
#       type = task or rest
#	FWHM = smoothing kernel e.g. 3

############################################################################################
############################################################################################


# Define the function
fslSmooth(){

	# Define local variables from inputs
	local subj=$1
	local data=$2
	local scripts=$3
	local type=$4
	local FWHM=$5
	
	# make sure filename supplied as command line arg else die
	[ $# -eq 0 ] && { echo "Usage: $0 subj data scripts type FWHM"; exit 1; }
	
	echo ---------------------------------------
	echo FSL EPI IMAGE SMOOTHING
	echo ---------------------------------------
	
	# Convert FWHM into sigma
	sig=$(echo "$FWHM / 2.3548" | bc -l)

	# Run smoothing on normalisation EPI
	echo Smoothing normalised EPI image...
	fslmaths ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_${type}_std.nii.gz \
		-s $sig \
		${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/sPBIHB_${subj}_${type}_std.nii.gz
	echo Smoothing finished!

	# Remove unsmoothed normalised task image to save space
	echo Removing unsmoothed normalised image to save space...
	rm ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_${type}_std.nii.gz
	echo Image deleted: ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_${type}_std.nii.gz

	echo ---------------------------------------
	echo FSL EPI IMAGE SMOOTHING FINISHED!
	echo ---------------------------------------

}

# Invoke the function and pass arguments
fslSmooth $1 $2 $3 $4 $5
