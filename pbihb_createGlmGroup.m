%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% BREATHING SCANNER TASK: GROUP GLM SETUP %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 10/04/2020
% -------------------------------------------------------------------------
% TO RUN:   glmGroup    = pbihb_createGlmGroup(location)
% INPUTS:   location    = 'local' or 'euler'
% OUTPUTS:  glmGroup    = Structure with GLM specifications added
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script creates the group GLMs for the 7 Tesla functional task data
% collected in the PBIHB study, including the following:
%   1) Group design: Whole sample mean + group difference regressor
%   2) Regression design: Whole sample mean + breathing anxiety regressor
%   3) Exploratory design: Whole sample mean + trait anxiety + depression +
%      gender regressors
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function glmGroup = pbihb_createGlmGroup(location)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Secify PPIDs and options for whole group
glmGroup.options = pbihb_setOptions('all', location);

% Check directory existance and make if required
if ~exist(glmGroup.options.paths.group_directory, 'dir')
    mkdir(glmGroup.options.paths.group_directory);
end
if ~exist(glmGroup.options.paths.group_directory_imaging, 'dir')
    mkdir(glmGroup.options.paths.group_directory_imaging);
end
if ~exist(glmGroup.options.paths.group_directory_imagingGroupGLMs, 'dir')
   mkdir(glmGroup.options.paths.group_directory_imagingGroupGLMs)
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD BEHAVIOURAL DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set paths and load breathing anxiety scores for all PPIDs
for a = 1:length(glmGroup.options.PPIDs.total)
    setupTemp = pbihb_setOptions(glmGroup.options.PPIDs.total{a}, location);
    rawBehaviourData = load(fullfile(setupTemp.paths.task_directory, setupTemp.names.taskData));
    glmGroup.scores.breathAnxiety(a) = rawBehaviourData.data.rate_answer(end);
end

% Load the model fit results
load(glmGroup.options.saveNames.modelFits);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD AND SCORE QUESTIONNAIRE DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load and score questionnaires
glmGroup.scores.quest = pbihb_questScore(location);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE GROUP GLM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Add regressor names to information
groupDiffGlmInfo.names = {'GroupMean' 'GroupDiff'};

% Create the GLM
groupDiffGlm = ones(length(glmGroup.options.PPIDs.total),1);
groupDiffGlm(glmGroup.options.PPIDs.low.idx,2) = -1;
groupDiffGlm(glmGroup.options.PPIDs.mod.idx,2) = 1;

% Check that group column doesn't have a mean
groupDiffGlm(:,2) = groupDiffGlm(:,2) - mean(groupDiffGlm(:,2));

% Add GLM values to info
groupDiffGlmInfo.values = groupDiffGlm;

% Save the outputs for the full sample
fileID = fopen(glmGroup.options.saveNames.groupDiffGLM, 'w');
fprintf(fileID,'%-5.2f %-5.2f\n', groupDiffGlm');
fclose(fileID);
command = sprintf('Text2Vest %s %s', glmGroup.options.saveNames.groupDiffGLM, glmGroup.options.saveNames.groupDiffGLMrd);
status = system(command);

% Create sub-sample GLM without excluded subjects
groupDiffGlmEx = groupDiffGlm;
groupDiffGlmEx(models.est.goodness.excludeIdx,:) = [];

% Save the outputs for the sub-sample
fileID = fopen(glmGroup.options.saveNames.groupDiffGLMex, 'w');
fprintf(fileID,'%-5.2f %-5.2f\n', groupDiffGlmEx');
fclose(fileID);
command = sprintf('Text2Vest %s %s', glmGroup.options.saveNames.groupDiffGLMex, glmGroup.options.saveNames.groupDiffGLMrdEx);
status = system(command);

% Add GLM values to info
groupDiffGlmInfo.valuesEx = groupDiffGlmEx;
groupDiffGlmInfo.idxEx = models.est.goodness.excludeIdx;

% Save the info
save(glmGroup.options.saveNames.groupDiffGLMinfo, '-struct', 'groupDiffGlmInfo');
glmGroup.groupDiff = groupDiffGlmInfo;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE REGRESSION GLM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Add regressor names to information
groupRegressGlmInfo.names = {'GroupMean' 'BreathAnx'};

% Create the GLM
groupRegressGlm = ones(length(glmGroup.options.PPIDs.total),1);
groupRegressGlm(:,2) = glmGroup.scores.breathAnxiety;

% Remove mean anxiety value
groupRegressGlm(:,2) = groupRegressGlm(:,2) - mean(groupRegressGlm(:,2));

% Add GLM values to info
groupRegressGlmInfo.values = groupRegressGlm;

% Save the outputs
fileID = fopen(glmGroup.options.saveNames.groupRegressGLM, 'w');
fprintf(fileID,'%-5.2f %-5.2f\n', groupRegressGlm');
fclose(fileID);
command = sprintf('Text2Vest %s %s', glmGroup.options.saveNames.groupRegressGLM, glmGroup.options.saveNames.groupRegressGLMrd);
status = system(command);
save(glmGroup.options.saveNames.groupRegressGLMinfo, '-struct', 'groupRegressGlmInfo');
glmGroup.groupRegress = groupRegressGlmInfo;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE EXPLORATORY GLM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Add regressor names to information
nameAnx = 'staiT';
nameDep = 'cesd';
groupExploreGlmInfo.names = [{'GroupMean'}, nameAnx, nameDep, {'gender'}];

% Find values
idxAnx = find(contains(glmGroup.scores.quest.names.total, nameAnx));
idxDep = find(contains(glmGroup.scores.quest.names.total, nameDep));
groupExploreGlmInfo.valuesRaw = [glmGroup.scores.quest.finalScores.total(:,idxAnx), glmGroup.scores.quest.finalScores.total(:,idxDep), glmGroup.scores.quest.data.extra.raw.gender];

% Remove means
values = groupExploreGlmInfo.valuesRaw - mean(groupExploreGlmInfo.valuesRaw);

% Create the GLM
groupExploreGlm = [ones(length(glmGroup.options.PPIDs.total),1), values];

% Add GLM values to info
groupExploreGlmInfo.values = groupExploreGlm;

% Save the outputs
fileID = fopen(glmGroup.options.saveNames.groupExploreGLM, 'w');
fprintf(fileID,'%-5.2f %-5.2f %-5.2f %-5.2f\n', groupExploreGlm');
fclose(fileID);
command = sprintf('Text2Vest %s %s', glmGroup.options.saveNames.groupExploreGLM, glmGroup.options.saveNames.groupExploreGLMrd);
status = system(command);
save(glmGroup.options.saveNames.groupExploreGLMinfo, '-struct', 'groupExploreGlmInfo');
glmGroup.groupExplore = groupExploreGlmInfo;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE TOTAL GROUP GLM INFO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save(glmGroup.options.saveNames.groupTotalGLMinfo, 'glmGroup');

end