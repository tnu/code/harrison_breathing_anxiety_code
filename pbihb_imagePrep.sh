############################################################################################

# PBIHB: IMAGE PREPARATION

############################################################################################

# DESCRIPTION:
# This function runs the image preparation in FSL for the PBIHB study
# Usage: ./imagePrep $subj $data $scripts
#	subj = variable containing PPID
#	data = path to main data folder
#	scripts = path to main scripts folder

############################################################################################
############################################################################################


# Define the function
imagePrep(){

	# Define local variables from inputs
	local subj=$1
	local data=$2
	local scripts=$3
	
	# make sure filename supplied as command line arg else die
	[ $# -eq 0 ] && { echo "Usage: $0 subj data scripts"; exit 1; }
	
	echo ---------------------------------------
	echo FSL IMAGE PREPARATION
	echo ---------------------------------------
	
	# Bias-correct structural image
	echo Bias-correcting structural image...
	fast -t 1 -n 3 -H 0.1 -I 10 -l 20.0 --nopve -B -b \
		-o ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_skullStrip/PBIHB_${subj}_struct_biasCorr.nii.gz \
		${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_struct_orig.nii.gz
	
	# Copy bias-corrected image to images directory
	echo Copying bias-corrected structural to images folder...
	cp ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_skullStrip/PBIHB_${subj}_struct_biasCorr_restore.nii.gz \
		${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_struct.nii.gz

	# Remove skull
	echo Running initial brain extraction...
	if [ "${subj}" = "0018" ] || [ "${subj}" = "1032" ] ; then
		betf=0.5
		betg=-0.5
	else
		betf=0.1
		betg=0
	fi
	bet ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_struct.nii.gz \
		${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_struct_brain.nii.gz \
		-f ${betf} -g ${betg}

	# Create affine transformation to standard space
	echo Calculating affine registration to standard space...
	flirt -in ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_struct_brain.nii.gz \
		-ref $FSLDIR/data/standard/MNI152_T1_1mm_brain.nii.gz \
		-out ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_skullStrip/PBIHB_${subj}_highres2standardAff.nii.gz \
		-omat ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_skullStrip/PBIHB_${subj}_highres2standardAff.mat \
		-cost corratio -dof 12 -searchrx -90 90 -searchry -90 90 -searchrz -90 90 -interp trilinear

	# Invert transformation
	echo Inverting affine registration to standard space...
	convert_xfm -inverse -omat ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_skullStrip/PBIHB_${subj}_standard2highresAff.mat \
		${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_skullStrip/PBIHB_${subj}_highres2standardAff.mat

	# Transform dilated brain mask to structural space
	echo Transforming brain mask from standard space...
	flirt -ref ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_struct.nii.gz \
		-in ${scripts}/pbihb_FSL_templates/MNI152_T1_1mm_brain_mask_dil_ero.nii.gz \
		-out ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_skullStrip/PBIHB_${subj}_standard2highresMask.nii.gz  \
		-applyxfm \
		-init ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_skullStrip/PBIHB_${subj}_standard2highresAff.mat \
		-interp trilinear

	# Skull strip T1 using mask
	echo Applying brain mask from standard space...
	fslmaths ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_struct.nii.gz \
		-mas ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_skullStrip/PBIHB_${subj}_standard2highresMask.nii.gz \
		${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_struct_brain.nii.gz

	# Take only first volume of any whole-brain images
	if [ -f "${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_task_wholebrain.nii.gz" ]; then
		fslroi ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_task_wholebrain.nii.gz \
		 ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_task_wholebrain.nii.gz 0 1
	fi
	if [ -f "${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_rest_wholebrain.nii.gz" ]; then
		fslroi ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_rest_wholebrain.nii.gz \
		 ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_rest_wholebrain.nii.gz 0 1
	fi

	echo ---------------------------------------
	echo FSL IMAGE PREPARATION FINISHED!
	echo ---------------------------------------

}

# Invoke the function and pass arguments
imagePrep $1 $2 $3
