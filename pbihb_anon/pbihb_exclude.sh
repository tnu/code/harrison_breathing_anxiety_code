############################################################################################

# PBIHB: REMOVE PARTICIPANTS WHO DID NOT CONSENT FOR DATA SHARING

############################################################################################

# DESCRIPTION:
# This function excludes participants who did not consent for their data to be shared beyond
# the PBIHB study
# Usage: ./exclude $data $scripts $subj
#	data = path to main data folder
#	scripts = path to main scripts folder
#	subj = list of PPIDs to analyse

############################################################################################
############################################################################################


# Define the function
exclude(){

	# Define local variables from inputs
	local data=$1
	local scripts=$2
	local subj=("$@")
	
	echo ---------------------------------------
	echo REMOVING RELEVANT PPIDS FOR SHARING
	echo ---------------------------------------
	
	# Loop through each PPID (minus data variable)
	numSubj=${#subj[@]}
	count=0
	for ((a=2; a<=${numSubj}-1; a++)); do
		# Specify PPID
		singleSubj="${subj[a]}"
		if [ "${count}" = "0" ] ; then
			subjMatlab=$singleSubj;
		else
			subjMatlab="${subjMatlab[@]}, $singleSubj";
		fi
		# Print to screen
		echo Removing PPID $singleSubj files...
		# Remove relevant folders and files
		#rm -r ${data}/fmri/TNU_PBIHB_${singleSubj}
		#rm -r ${data}/fmri/TNU_PBIHB_noiseFiles/PBIHB_${singleSubj}_icaNoiseFile_task.txt
		#rm -r ${data}/behavior/TNU_PBIHB_${singleSubj}
		count=$((count+1));
	done

	# Remove the information from the anonymised questionnaire data (via Matlab)
	echo Removing questionnaire information...
	matlab -singleCompThread -nosplash -nodesktop -r "addpath('${scripts}/pbihb_anon/'); \
		pbihb_excludeQuest('$data', ${subjMatlab[@]}) ; exit" ;

	echo ... FINISHED!
	echo ---------------------------------------

}

# Invoke the function and pass arguments
exclude $*
