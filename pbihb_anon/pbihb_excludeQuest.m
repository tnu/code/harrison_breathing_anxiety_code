%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% PBIHB: EXCLUDE PPIDS FROM QUESTIONNAIRES FILES %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 06/09/2020
% -------------------------------------------------------------------------
% TO RUN:    pbihb_excludeQuest(data, subjects)
% INPUTS:    data       = Path to main data folder
%            varargin   = List of PPIDs (anonymised) to exclude
% OUTPUTS:   Questionnaire data files with PPIDs removed
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script loads the questionnaire data files and removes anonymised 
% PPIDs for those participants who did not consent to data sharing beyond
% the PBIHB study.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_excludeQuest(data, varargin)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD THE DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Print message to screen
disp('---------------------------------------');
disp('REMOVING PPIDS FROM QUESTIONNAIRES');
disp('---------------------------------------');
disp('Loading data...');

% Specify file names
fileNames.idx = fullfile(data, 'questionnaires', 'PBIHB_questionnaire_indexing.xlsx');
fileNames.prescreen1 = fullfile(data, 'questionnaires', 'results-survey976467.csv');
fileNames.prescreen2 = fullfile(data, 'questionnaires', 'results-survey757339.csv');
fileNames.session = fullfile(data, 'questionnaires', 'results-survey951191.csv');

% Load the data
tables.idx = readtable(fileNames.idx);
tables.prescreen1 = readtable(fileNames.prescreen1);
tables.prescreen2 = readtable(fileNames.prescreen2);
tables.session = readtable(fileNames.session);

% Delete the old index file
delete(fileNames.idx);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY THE PPIDs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

PPID.exclude = pad(string(varargin),4,'left','0');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FIND AND REMOVE THE PPIDS FROM THE QUESTIONNAIRES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Print message to screen
disp('Removing excluded PPIDs...');

% Find and remove PPID rows in tables
for a = 1:length(PPID.exclude)
    % Find PPID in index table
    idxPPID(a) = find(contains(tables.idx.PPID, PPID.exclude{a}));
    % Remove row in prescreening table
    if tables.idx.PRE_V(a) == 1
        excludePre = find(ismember(tables.prescreen1.x_id, tables.idx.PRE_NUM(idxPPID(a))));
        tables.prescreen1(excludePre,:) = [];
    elseif tables.idx.PRE_V(a) == 2
        excludePre = find(ismember(str2double(tables.prescreen2.x__id_), tables.idx.PRE_NUM(idxPPID(a))));
        tables.prescreen2(excludePre,:) = [];
    end
    % Remove row in session table 
    excludeSess = find(ismember(str2double(tables.session.x__id_), tables.idx.SESS_NUM(idxPPID(a))));
    tables.session(excludeSess,:) = [];
    % Remove row from index table
    tables.idx(idxPPID(a),:) = [];
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE THE NEW SHEETS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Print message to screen
disp('Saving new files...');

% Save out new session sheet
writetable(tables.idx, fileNames.idx);
writetable(tables.prescreen1, fileNames.prescreen1);
writetable(tables.prescreen2, fileNames.prescreen2);
writetable(tables.session, fileNames.session);


end