############################################################################################

# PBIHB: RENAME FOLDERS AND FILES FOR BIDS FORMATTING

############################################################################################

# DESCRIPTION:
# This function renames the folders and files for the PBIHB study into BIDS format
# Usage: ./rename $dataIn $dataOut $subj
#	dataIn = path to main input data folder
#	dataOut = path to main output data folder
#	subj = participant identifier (PPID)

############################################################################################
############################################################################################


# Define the function
renameBids(){

	# Define local variables from inputs
	local dataIn=$1
	local dataOut=$2
	local subj=$3

	echo ----------------------------------------------
	echo RE-NAMING PPID ${subj} TO BIDS
	echo ----------------------------------------------

	echo Creating folder structure...
	mkdir ${dataOut}/sub-${subj}
	mkdir ${dataOut}/sub-${subj}/anat
	mkdir ${dataOut}/sub-${subj}/func
	mkdir ${dataOut}/sub-${subj}/beh
	
	if [ -f "${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_struct_orig.nii.gz" ]; then
		echo Copying anatomical images...
		cp ${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_struct_orig.nii.gz \
			${dataOut}/sub-${subj}/anat/sub-${subj}_T1w.nii.gz
		if [ -f "${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_task_wholebrain.nii.gz" ]; then
			cp ${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_task_wholebrain.nii.gz \
				${dataOut}/sub-${subj}/anat/sub-${subj}_T2starw.nii.gz
		else
			cp ${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_rest_wholebrain.nii.gz \
				${dataOut}/sub-${subj}/anat/sub-${subj}_T2starw.nii.gz
		fi
	fi

	if [ -f "${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_task.nii.gz" ]; then
		echo Copying task image...
		cp ${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_task.nii.gz \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_bold.nii.gz
	fi

	if [ -f "${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_physiology/PBIHB_${subj}_task_preproc_data.txt" ]; then
		echo Copying and zipping task physiology...
		cp ${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_physiology/PBIHB_${subj}_task_preproc_data.txt \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_recording-physiology_physio_mid.tsv
		awk -v OFS='\t' '{ printf "%.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f\n", $1, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12 }' \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_recording-physiology_physio_mid.tsv >\
			${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_recording-physiology_physio.tsv
		gzip ${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_recording-physiology_physio.tsv
		rm ${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_recording-physiology_physio_mid.tsv
	fi

	if [ -f "${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_physiology/PBIHB_${subj}_physTaskValues.mat" ]; then
		echo Copying task physiology summary for use in events file...
		cp ${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_physiology/PBIHB_${subj}_physTaskValues.mat \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_physTaskValues.mat
	fi

	if [ -f "${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_task/PBIHB_${subj}_task_behavior.mat" ]; then
		echo Copying task behaviour...
		cp ${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_task/PBIHB_${subj}_task_behavior.mat \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_events.mat
		echo Transforming learning task behaviour into tsv file in MATLAB...
		matlab -nosplash -nodesktop -r "pbihb_anonBids('$subj', '$dataOut', 'BLT'); exit"
	fi

	if [ -f "${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_rest.nii.gz" ]; then
		echo Copying rest image...
		cp ${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_rest.nii.gz \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_bold.nii.gz
	fi

	if [ -f "${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_physiology/PBIHB_${subj}_rest_preproc_data.txt" ]; then
		echo Copying and zipping rest physiology...
		cp ${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_physiology/PBIHB_${subj}_rest_preproc_data.txt \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_recording-physiology_physio_mid.tsv
		awk -v OFS='\t' '{ printf "%.4f %.4f %.4f %.4f %.4f %.4f\n", $1, $4, $7, $10, $11, $12 }' \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_recording-physiology_physio_mid.tsv >\
			${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_recording-physiology_physio.tsv
		gzip ${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_recording-physiology_physio.tsv
		rm ${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_recording-physiology_physio_mid.tsv
	fi

	if [ -f "${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_scanner_logs/PBIHB_${subj}_scanner_task.log" ]; then
		echo Copying and zipping task scanner logs...
		cp ${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_scanner_logs/PBIHB_${subj}_scanner_task.log \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_recording-scanner_physio.log
		awk -v OFS='\t' '{ print $5, $6, $7, $8, $9 }' \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_recording-scanner_physio.log >\
			${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_recording-scanner_physio.tsv
		sed -i '' 1d ${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_recording-scanner_physio.tsv
		gzip ${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_recording-scanner_physio.tsv
		rm ${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_recording-scanner_physio.log
	fi

	if [ -f "${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_scanner_logs/PBIHB_${subj}_scanner_rest.log" ]; then
		echo Copying and zipping rest scanner logs...
		cp ${dataIn}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_scanner_logs/PBIHB_${subj}_scanner_rest.log \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_recording-scanner_physio.log
		awk -v OFS='\t' '{ print $5, $6, $7, $8, $9 }' \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_recording-scanner_physio.log >\
			${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_recording-scanner_physio.tsv
		sed -i '' 1d ${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_recording-scanner_physio.tsv
		gzip ${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_recording-scanner_physio.tsv
		rm ${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_recording-scanner_physio.log
	fi

	if [ -f "${dataIn}/fmri/TNU_PBIHB_noiseFiles/PBIHB_${subj}_icaNoiseFile_task.txt" ]; then
		echo Copying ICA task noise file...
		cp ${dataIn}/fmri/TNU_PBIHB_noiseFiles/PBIHB_${subj}_icaNoiseFile_task.txt \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_desc-ica_noise_mid.tsv
		awk -v OFS='\t' '{ print $0 }' \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_desc-ica_noise_mid.tsv >\
			${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_desc-ica_noise.tsv
		rm ${dataOut}/sub-${subj}/func/sub-${subj}_task-learning_desc-ica_noise_mid.tsv
	fi

	if [ -f "${dataIn}/fmri/TNU_PBIHB_noiseFiles/PBIHB_${subj}_icaNoiseFile_rest.txt" ]; then
		echo Copying ICA rest noise file...
		cp ${dataIn}/fmri/TNU_PBIHB_noiseFiles/PBIHB_${subj}_icaNoiseFile_rest.txt \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_desc-ica_noise_mid.tsv
		awk -v OFS='\t' '{ print $0 }' \
			${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_desc-ica_noise_mid.tsv >\
			${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_desc-ica_noise.tsv
		rm ${dataOut}/sub-${subj}/func/sub-${subj}_task-rest_desc-ica_noise_mid.tsv
	fi

	if [ -f "${dataIn}/behavior/TNU_PBIHB_${subj}/behavior/filter_task_results_${subj}.mat" ]; then
		echo Copying FDT data...
		cp ${dataIn}/behavior/TNU_PBIHB_${subj}/behavior/filter_task_results_${subj}.mat \
			${dataOut}/sub-${subj}/beh/sub-${subj}_task-detection_beh.mat
		echo Transforming FDT task behaviour into tsv file in MATLAB...
		matlab -nosplash -nodesktop -r "pbihb_anonBids('$subj', '$dataOut', 'FDT'); exit"
	fi
	
	echo ... BIDS FORMATTING FINISHED!
	echo ----------------------------------------------

}

# Invoke the function and pass arguments
renameBids $1 $2 $3
