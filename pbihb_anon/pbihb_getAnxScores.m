%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%% PBIHB: GATHER TASK ANXIETY SCORES %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 20/07/2021
% -------------------------------------------------------------------------
% TO RUN:   scores      = pbihb_getAnxScores(location)
% INPUTS:   location    = 'local' or 'euler'
% OUTPUTS:  scores      = Structure with anxiety scores and PPIDs 
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script gathers all of the task anxiety scores for the breathing 
% learning task (BLT) for the 7 Tesla functional data collected in the
% PBIHB study.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function scores = pbihb_getAnxScores(location)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD AND CODE BEHAVIOURAL DATA FROM ALL PARTICIPANTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Secify PPIDs and options for whole group
scores.options = pbihb_setOptions('all', location);

% Set paths and load data for all PPIDs
for a = 1:length(scores.options.PPIDs.total)
    setupTemp = pbihb_setOptions(scores.options.PPIDs.total{a}, location);
    scores.rawData{a} = load(fullfile(setupTemp.paths.task_directory, setupTemp.names.taskData));
    scores.anx(a) = scores.rawData{a}.data.rate_answer(81);
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE TABLE OF PPIDS AND ANXIETY SCORES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

T = table(scores.options.PPIDs.total', scores.anx');


end