############################################################################################

# ANONYMISATION SCRIPT FOR MINIMALLY-PREPROCESSED DATA FOR PBIHB STUDY (LOCAL)

############################################################################################

# DESCRIPTION:
# This is the anonymisation script to change the PPIDs for the minimally-preprocessed data
# from the PBIHB study. This script is set up to run on a local (Mac) machine.

############################################################################################

# INSTRUCTIONS:
# 1) Set the paths
# 2) Create a new PPID list and enter into the script
# 3) Run the script
# 4) DELETE the new PPID list from the script to break the key and fully anonymise the data

############################################################################################
############################################################################################

# ADJUST THIS SECTION

# Set the paths
dataIn=/Volumes/TNU_data/PBIHB_data_local/PBIHB_setup
dataOut=/Volumes/TNU_data/PBIHB_data_local/PBIHB_anon
scripts=/Users/faullol/Documents/just_breathe/zurich/gitTNU/pbihb_scanner_analysis

# Create a new PPID list (example old list provided - do not start PPIDs with 0 or 1)
subjNew=(0019 0002 0003 0034 0005 0039 0007 0032 0031 0010 0011 0012 0013 0014 0015 \
0016 0017 0018 0033 0020 0021 0022 0037 0038 0042 0026 0027 0028 0029 0041 \
1041 1024 1042 1004 1005 1017 1032 1025 1006 1012 1036 1010 1002 1031 1015 \
1028 1009 1016 1019 1020 1021 1034 1037 1018 1003 1038 1027 1043 1029 1030 \
0001 0009 0008 0035 1013 1014 1008 1011 \
0004 0023 0024 0025 0030 0040 1007 1001 1033 1035 1039 1040)

# Specify PPIDs who do not consent to data sharing for removal
exclude=(5122 5113)

############################################################################################
############################################################################################

# --> DO NOT CHANGE BELOW HERE 

# Set original PPIDs to analyse
subjOld=(0019 0002 0003 0034 0005 0039 0007 0032 0031 0010 0011 0012 0013 0014 0015 \
0016 0017 0018 0033 0020 0021 0022 0037 0038 0042 0026 0027 0028 0029 0041 \
1041 1024 1042 1004 1005 1017 1032 1025 1006 1012 1036 1010 1002 1031 1015 \
1028 1009 1016 1019 1020 1021 1034 1037 1018 1003 1038 1027 1043 1029 1030 \
0001 0009 0008 0035 1013 1014 1008 1011 \
0004 0023 0024 0025 0030 0040 1007 1001 1033 1035 1039 1040)

############################################################################################
############################################################################################

# RUN ANONYMISATIONS

# Copy dataset to new location
cp -r $dataIn $dataOut

# Set each participant anonymisation running
count=0
subjOldMatlab=();
subjNewMatlab=();
for old in ${subjOld[@]}; do
	new=${subjNew[$count]};
	if [ "${count}" = "0" ] ; then
		subjOldMatlab=$old;
		subjNewMatlab=$new;
	else
		subjOldMatlab="${subjOldMatlab[@]}, $old";
		subjNewMatlab="${subjNewMatlab[@]}, $new";
	fi
	${scripts}/pbihb_anon/pbihb_rename.sh $dataOut $old $new ;
	${scripts}/pbihb_anon/pbihb_deface.sh $new $dataOut $scripts ;
	matlab -singleCompThread -nosplash -nodesktop -r "addpath('${scripts}/pbihb_anon/'); \
		pbihb_anonMatlab('$new', '$dataOut') ; exit" ;
	count=$((count+1));
done

# Set questionnaire anonymisation script running
matlab -singleCompThread -nosplash -nodesktop -r "addpath('${scripts}/pbihb_anon/'); \
	pbihb_anonQuest('$dataOut', ${subjOldMatlab[@]}, ${subjNewMatlab[@]}) ; exit" ;

# Remove all unused PPIDs and folders
rm -r $dataOut/fmri/TNU_PBIHB_0* ; rm -r $dataOut/fmri/TNU_PBIHB_1* ; rm -r $dataOut/fmri/TNU_PBIHB_9* ;
rm -r $dataOut/behavior/TNU_PBIHB_0* ; rm -r $dataOut/behavior/TNU_PBIHB_1* ; m -r $dataOut/behavior/TNU_PBIHB_9* ;
rm -r $dataOut/questionnaires/TNU_PBIHB_*

# Exclude PPIDs not consented for sharing
#${scripts}/pbihb_anon/pbihb_exclude.sh $dataOut $scripts $exclude


############################################################################################

# ANONYMISATION PROCEDURES:
# - Folder structure:
#	- Change all path names in folder structure
#	- Remove all unused PPID folders
# - fmri folder
#	- MRI scans:
#		- Change file names
#		- De-face structural scans
# 	- Scanner logs:
#		- Change file names
#		- Remove text info at the top of file
# 	- Physiology text files:
#		- Change file names
# 	- BLT behavioural task files:
#		- Change file names
#		- Open file in Matlab, change PPID and path names (check for others)
#		- Check for any additional task files and remove
#	- ICA noise files:
#		- Change file names
# - behaviour folder
#	- BLT training files:
#		- Change files names
#		- Open file in Matlab, change PPID and path names (check for others)
#		- Check for any additional training files and remove
#	- FDT results:
#		- Change files names
#		- Open file in Matlab, change PPID and path names (check for others)
#		- Check for any additional task and image files and remove
#	- MIP results:
#		- Delete files
# - questionnaires folder
#	- Single subject folders:
#		- Delete all single subject folders
#	- Limesurvey results files
#		- Open with Matlab and remove all date stamps plus medical information
#		- Remove all MRI prescreening information - just keep questionnaires
#	- Limesurvey indexing file
#		- Open with Matlab and change PPIDs

