############################################################################################

# PBIHB: RENAME FOLDERS AND FILES FOR ANONYMISATION

############################################################################################

# DESCRIPTION:
# This function renames the folders and files for the PBIHB study
# Usage: ./rename $data $PPID_old $PPID_new
#	data = path to main data folder
#	PPID_old = original PPID
#	PPID_new = new PPID

############################################################################################
############################################################################################


# Define the function
rename(){

	# Define local variables from inputs
	local dataOut=$1
	local old=$2
	local new=$3

	echo ---------------------------------------
	echo RE-NAMING PPID ${old} TO ${new}
	echo ---------------------------------------

	echo Changing path structures...
	mv ${dataOut}/fmri/TNU_PBIHB_${old} ${dataOut}/fmri/TNU_PBIHB_${new}
	mv ${dataOut}/behavior/TNU_PBIHB_${old} ${dataOut}/behavior/TNU_PBIHB_${new}
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${old}_analysis ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_analysis
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_analysis/PBIHB_${old}_basic ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_analysis/PBIHB_${new}_basic
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${old}_glm ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_glm
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${old}_images ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${old}_physiology ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_physiology
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${old}_preprocessing ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_preprocessing
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_preprocessing/PBIHB_${old}_physio ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_preprocessing/PBIHB_${new}_physio
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_preprocessing/PBIHB_${old}_skullStrip ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_preprocessing/PBIHB_${new}_skullStrip
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${old}_scanner_logs ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_scanner_logs
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${old}_task ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_task

	echo Anonymising MRI image names...
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${old}_task.nii.gz ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${new}_task.nii.gz
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${old}_task.nii.gz ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${new}_task.nii.gz
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${old}_struct_orig.nii.gz ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${new}_struct_orig.nii.gz
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${old}_struct_orig.nii.gz \
		${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${new}_struct_orig.nii.gz
	if [ -f "${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${old}_task_wholebrain.nii.gz" ]; then
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${old}_task_wholebrain.nii.gz \
			${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${new}_task_wholebrain.nii.gz
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${old}_task_wholebrain.nii.gz \
			${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${new}_task_wholebrain.nii.gz
	fi
	if [ -f "${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${old}_task_fieldmap.nii.gz" ]; then
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${old}_task_fieldmap.nii.gz ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${new}_task_fieldmap.nii.gz
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${old}_task_fieldmap_mag.nii.gz \
			${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${new}_task_fieldmap_mag.nii.gz
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${old}_task_fieldmap.nii.gz \
			${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${new}_task_fieldmap.nii.gz
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${old}_task_fieldmap_mag.nii.gz \
			${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${new}_task_fieldmap_mag.nii.gz
	fi
	if [ -f "${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${old}_rest.nii.gz" ]; then
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${old}_rest.nii.gz ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${new}_rest.nii.gz
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${old}_rest.nii.gz ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${new}_rest.nii.gz

	fi
	if [ -f "${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${old}_rest_wholebrain.nii.gz" ]; then
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${old}_rest_wholebrain.nii.gz \
			${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${new}_rest_wholebrain.nii.gz
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${old}_rest_wholebrain.nii.gz \
			${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${new}_rest_wholebrain.nii.gz
	fi
	if [ -f "${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${old}_rest_fieldmap.nii.gz" ]; then
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${old}_rest_fieldmap.nii.gz ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${new}_rest_fieldmap.nii.gz
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${old}_rest_fieldmap_mag.nii.gz \
			${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/PBIHB_${new}_rest_fieldmap_mag.nii.gz
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${old}_rest_fieldmap.nii.gz \
			${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${new}_rest_fieldmap.nii.gz
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${old}_rest_fieldmap_mag.nii.gz \
			${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_images/originals/PBIHB_${new}_rest_fieldmap_mag.nii.gz
	fi

	echo Anonymising scanner logs...
	if [ -f "${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_scanner_logs/PBIHB_${old}_scanner_task.log" ]; then
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_scanner_logs/PBIHB_${old}_scanner_task.log \
			${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_scanner_logs/PBIHB_${new}_scanner_task.log
		sed -i '' -e 1,4d ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_scanner_logs/PBIHB_${new}_scanner_task.log
	fi
	if [ -f "${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_scanner_logs/PBIHB_${old}_scanner_rest.log" ]; then
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_scanner_logs/PBIHB_${old}_scanner_rest.log \
			${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_scanner_logs/PBIHB_${new}_scanner_rest.log
		sed -i '' -e 1,4d ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_scanner_logs/PBIHB_${new}_scanner_rest.log
	fi

	echo Anonymising physiology files...
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_physiology/PBIHB_${old}_task_preproc_data.txt \
		${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_physiology/PBIHB_${new}_task_preproc_data.txt
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_physiology/PBIHB_${old}_task_preproc_comments.txt \
		${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_physiology/PBIHB_${new}_task_preproc_comments.txt
	if [ -f "${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_physiology/PBIHB_${old}_rest_preproc_data.txt" ]; then
		mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_physiology/PBIHB_${old}_rest_preproc_data.txt \
			${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_physiology/PBIHB_${new}_rest_preproc_data.txt
	fi

	echo Anonymising BLT results file name...
	mv ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_task/PBIHB_${old}_task_behavior.mat \
		${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_task/PBIHB_${new}_task_behavior.mat
	if [ -f "${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_task/*orig.mat" ]; then
		rm ${dataOut}/fmri/TNU_PBIHB_${new}/PBIHB_${new}_task/*orig.mat
	fi

	echo Anonymising FDT results file name...
	mv ${dataOut}/behavior/TNU_PBIHB_${new}/behavior/filter_task_results_${old}.mat \
		${dataOut}/behavior/TNU_PBIHB_${new}/behavior/filter_task_results_${new}.mat
	if [ -f "${dataOut}/behavior/TNU_PBIHB_${new}/behavior/*orig*.mat" ]; then
		rm ${dataOut}/behavior/TNU_PBIHB_${new}/behavior/*orig*.mat
	fi

	echo Removing unused behavioural files...
	rm ${dataOut}/behavior/TNU_PBIHB_${new}/behavior/*train*.mat
	rm ${dataOut}/behavior/TNU_PBIHB_${new}/behavior/*.pdf
	if [ -f "${dataOut}/behavior/TNU_PBIHB_${new}/behavior/*.tif" ]; then
		rm ${dataOut}/behavior/TNU_PBIHB_${new}/behavior/*.tif
	fi

	echo Renaming ICA noise file...
	mv ${dataOut}/fmri/TNU_PBIHB_noiseFiles/PBIHB_${old}_icaNoiseFile_task.txt ${dataOut}/fmri/TNU_PBIHB_noiseFiles/PBIHB_${new}_icaNoiseFile_task.txt
	echo ... FINISHED!

}

# Invoke the function and pass arguments
rename $1 $2 $3
