%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% PBIHB: ANONYMISE QUESTIONNAIRE FILES %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 06/09/2020
% -------------------------------------------------------------------------
% TO RUN:    pbihb_anonQuest(data)
% INPUTS:    data       = Path to main data folder
%            varargin   = List of old and then new PPIDs
% OUTPUTS:   Questionnaire data files with internal PPIDs changed
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script loads the questionnaire data files and re-names PPIDs to
% anonymised ones within the index file. It also removes all MRI
% prescreening information and BPQ health data.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_anonQuest(dataOut, varargin)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD THE DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Print message to screen
disp('---------------------------------------');
disp('ANONYMISING MATLAB QUESTIONNAIRES');
disp('---------------------------------------');
disp('Loading data...');

% Specify file names
fileNames.idx = fullfile(dataOut, 'questionnaires', 'PBIHB_questionnaire_indexing.xlsx');
fileNames.prescreen1 = fullfile(dataOut, 'questionnaires', 'results-survey976467.csv');
fileNames.prescreen2 = fullfile(dataOut, 'questionnaires', 'results-survey757339.csv');
fileNames.session = fullfile(dataOut, 'questionnaires', 'results-survey951191.csv');

% Load the data
tables.idx = readtable(fileNames.idx);
tables.prescreen1 = readtable(fileNames.prescreen1);
tables.prescreen2 = readtable(fileNames.prescreen2);
tables.session = readtable(fileNames.session);

% Delete the old index file
delete(fileNames.idx);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SEPARATE THE PPIDs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

PPID.total = pad(string(varargin),4,'left','0');
PPID.old = PPID.total(1:(length(PPID.total)/2));
PPID.new = PPID.total((length(PPID.total)/2 + 1):end);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REPLACE THE PPIDS IN THE INDEX TABLE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Replace PPIDs in index table
for a = 1:length(PPID.old)
    fprintf('Replacing PPID %s...\n', PPID.old{a});
    idxPPID = find(contains(tables.idx.PPID, PPID.old{a}));
    tables.idx.PPID{idxPPID} = PPID.new{a};
end

% Remove unused PPIDs from index table
toDelete = find(~contains(tables.idx.PPID, PPID.new));
tables.idx(toDelete,:) = [];

% Save out the updated sheet
writetable(tables.idx, fileNames.idx);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADJUST THE PRESCREENING FILES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find PPIDs in each prescreening table
preIdx1 = find(tables.idx.PRE_V == 1);
preIdx2 = find(tables.idx.PRE_V == 2);

% Find unusable rows in each prescreening table
limesurveyIdsEx1 = find(~ismember(tables.prescreen1.x_id, tables.idx.PRE_NUM(preIdx1)));
limesurveyIdsEx2 = find(~ismember(str2double(tables.prescreen2.x__id_), tables.idx.PRE_NUM(preIdx2)));

% Remove unused PPIDs from prescreening tables
tables.prescreen1(limesurveyIdsEx1,:) = [];
tables.prescreen2(limesurveyIdsEx2,:) = [];

% Remove all MRI and identifying information from prescreening tables
idxPS1anx = find(contains(tables.prescreen1.Properties.VariableNames, "StaitAllQuestions_"), 20);
idxPS2anx = find(contains(tables.prescreen2.Properties.VariableNames, "StaitAllQuestions_"), 20);
idxPS1dep = find(contains(tables.prescreen1.Properties.VariableNames, "CESD"), 20);
idxPS2dep = find(contains(tables.prescreen2.Properties.VariableNames, "CESD"), 20);
idxPS1keep = [1, idxPS1anx, idxPS1dep];
idxPS2keep = [1, idxPS2anx, idxPS2dep];
tables.prescreen1 = tables.prescreen1(:,idxPS1keep);
tables.prescreen2 = tables.prescreen2(:,idxPS2keep);

% Save out new prescreening sheets
writetable(tables.prescreen1, fileNames.prescreen1);
writetable(tables.prescreen2, fileNames.prescreen2);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADJUST THE SESSION FILE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find unusable rows in the session table
limesurveyIdsExSess = find(~ismember(str2double(tables.session.x__id_), tables.idx.SESS_NUM));

% Remove unused PPIDs from session table
tables.session(limesurveyIdsExSess,:) = [];

% Remove all date information from session file
idxSessQuestDate = find(contains(tables.session.Properties.VariableNames, "date"));
tables.session(:,idxSessQuestDate) = [];

% Remove all comments from session file
idxSessQuestCom = [find(contains(tables.session.Properties.VariableNames, "comment")), find(contains(tables.session.Properties.VariableNames, "BPQ21")), find(contains(tables.session.Properties.VariableNames, "BPQ22"))];
tables.session(:,idxSessQuestCom) = [];

% Save out new session sheet
writetable(tables.session, fileNames.session);

% Print message to screen
disp('... FINISHED!');
disp('---------------------------------------');

end