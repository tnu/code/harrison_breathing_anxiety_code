%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% PBIHB: BIDS FORMAT BEHAVIOURAL FILES %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 14/07/2021
% -------------------------------------------------------------------------
% TO RUN:    pbihb_bidsMatlab(PPID, data, type)
% INPUTS:    PPID       = Participant PPID
%            data       = Path to main data folder
%            type       = 'BLT' or 'FDT'
% OUTPUTS:   BLT and FDT data files in bids format
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script loads the BLT and FDT matrices and replaces the internal PPID
% and removes any paths containing the original PPID (for anonymisation
% purposes).
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_anonBids(PPID, dataLocation, type)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET THE OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Print message to screen
fprintf('BIDS formatting %s MATLAB matrix for %s...\n', PPID, type);
disp('Loading data...');

% Set required paths
BLTfileIn = fullfile(dataLocation, ['sub-', PPID], 'func', ['sub-', PPID, '_task-learning_events.mat']);
BLTPhysFileIn = fullfile(dataLocation, ['sub-', PPID], 'func', ['sub-', PPID, '_task-learning_physTaskValues.mat']);
BLTfileOut = fullfile(dataLocation, ['sub-', PPID], 'func', ['sub-', PPID, '_task-learning_events.txt']);
BLTfileOutTsv = fullfile(dataLocation, ['sub-', PPID], 'func', ['sub-', PPID, '_task-learning_events.tsv']);
FDTfileIn = fullfile(dataLocation, ['sub-', PPID], 'beh', ['sub-', PPID, '_task-detection_beh.mat']);
FDTfileOut = fullfile(dataLocation, ['sub-', PPID], 'beh', ['sub-', PPID, '_task-detection_beh.txt']);
FDTfileOutTsv = fullfile(dataLocation, ['sub-', PPID], 'beh', ['sub-', PPID, '_task-detection_beh.tsv']);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET UP EVENTS FILE IF BLT OPTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(type, 'BLT')
    
    % Variables: trial_number cue_onset cue_duration cue_type
    % response_prediction response_time contingency contingency_prediction
    % stim_onset_visual stim_duration_visual stim_onset_breathing 
    % stim_duration_breathing stim_type rate_onset rate_duration rating

    % Load files
    load(BLTfileIn);
    if isfile(BLTPhysFileIn)
        physioVals = load(BLTPhysFileIn);
    end

    % Define trial_number variable
    trial_number = (1:length(data.events.cue_pred_on))';

    % Define cue_onset variable
    cue_onset = round(data.events.cue_pred_on', 2);

    % Define cue_duration variable
    cue_duration = round((data.events.cue_pred_off - data.events.cue_pred_on)', 2);

    % Create cue_type variables for cue (NaN for stimulus and rating)
    cue_type = params.cue;

    % Create response_type variables for cue (NaN for stimulus and rating)
    response_prediction = data.pred_answer';

    % Create response_time variables for cue (NaN for stimulus and rating)
    response_time = data.pred_rt';

    % Define contingency variable
    contingency = params.pairings;

    % Define contingency_prediction variable
    for a = 1:length(data.events.cue_pred_on)
        if params.cue(a) == 1
            responseContingencies(a) = data.pred_answer(a);
        elseif params.cue(a) == 2
            responseContingencies(a) = 1 - data.pred_answer(a);
        end
    end
    contingency_prediction = round(responseContingencies', 2);

    % Define stim_onset_visual variable (presentation of the visual
    % cue for the stimulus presence
    stim_onset_visual = round(data.events.stim_on', 2);

    % Define stim_duration_visual variable (presentation of the visual
    % cue for the stimulus presence
    stim_duration_visual = round((data.events.stim_off - data.events.stim_on)', 2);
    
    % Define stim_onset_breathing and stim_duration_breathing variables 
    % (onset and duration of the breathing stimuli)
    if exist('physioVals', 'var')
        stim_onset_breathing = round(physioVals.physValues.values.inspiratoryPressure.allStimuli.onsetTime, 2);
        stim_duration_breathing = round(physioVals.physValues.values.inspiratoryPressure.allStimuli.duration, 2);
    else
        stim_onset_breathing(1:length(data.events.cue_pred_on),1) = NaN;
        stim_duration_breathing(1:length(data.events.cue_pred_on),1) = NaN;
    end

    % Define stim_type variable
    stim_type = params.resist;

    % Define rate_onset variable
    rate_onset = round(data.events.rating_on(1:length(data.events.cue_pred_on))', 2);

    % Define rate_duration variable
    rate_duration = round((data.events.rating_off(1:length(data.events.cue_pred_on)) - data.events.rating_on(1:length(data.events.cue_pred_on)))', 2);

    % Define rating variable
    rating = round(data.rate_answer(1:length(data.events.cue_pred_on))', 2);

    % Create output table
    T = table(trial_number, cue_onset, cue_duration, cue_type, response_prediction, response_time, contingency, contingency_prediction,...
        stim_onset_visual, stim_duration_visual, stim_onset_breathing, stim_duration_breathing, stim_type, rate_onset, rate_duration, rating);

    % Write to file
    writetable(T, BLTfileOut, 'Delimiter', 'tab');

    % Change to .tsv file and delete originals
    movefile(BLTfileOut, BLTfileOutTsv);
    delete(BLTfileIn);
    if isfile(BLTPhysFileIn)
        delete(BLTPhysFileIn);
    end

    % Print message to screen
    disp('... Finished MATLAB conversion!');
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET UP EVENTS FILE IF FDT OPTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(type, 'FDT')
    
    % Load FDT file
    load(FDTfileIn);
    
    % Variables: trial_number stim_type stim_intensity response correct confidence
    
    % Define trial_number variable
    trial_number = (1:results.trials)';
    
    % Define stim_type variable
    stim_type = results.thresholdTrials.filters';
    
    % Define stim_intensity variable
    stim_intensity(1:results.trials,1) = results.thresholdTrials.filterNum;
    
    % Define response variable
    response = results.thresholdTrials.response';
    
    % Define correct variable
    correct = results.thresholdTrials.score';
    
    % Define confidence variable
    confidence = results.thresholdTrials.confidence';
    
    % Create output table
    T = table(trial_number, stim_type, stim_intensity, response, correct, confidence);
    
    % Write to file
    writetable(T, FDTfileOut, 'Delimiter', 'tab');

    % Change to .tsv file and delete original
    movefile(FDTfileOut, FDTfileOutTsv);
    delete(FDTfileIn);

    % Print message to screen
    disp('... Finished MATLAB conversion!');
end


end