%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% PBIHB: ANONYMISE BEHAVIOURAL FILES %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 06/09/2020
% -------------------------------------------------------------------------
% TO RUN:    pbihb_anonMatlab(PPIDnew, data)
% INPUTS:    PPIDnew    = Anonymised PPID
%            data       = Path to main data folder
% OUTPUTS:   BLT and FDT data files with internal PPIDs and paths changed
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script loads the BLT and FDT matrices and replaces the internal PPID
% and removes any paths containing the original PPID (for anonymisation
% purposes).
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_anonMatlab(PPIDnew, data)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET THE OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Print message to screen
disp('---------------------------------------');
fprintf('ANONYMISING %s MATLAB MATRICES\n', PPIDnew);
disp('---------------------------------------');
disp('Loading data...');

% Set required paths
BLTfile = fullfile(data, ['fmri/TNU_PBIHB_', PPIDnew], ['PBIHB_', PPIDnew, '_task'], ['PBIHB_', PPIDnew, '_task_behavior.mat']);
FDTfile = fullfile(data, ['behavior/TNU_PBIHB_', PPIDnew], 'behavior', ['filter_task_results_', PPIDnew, '.mat']);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CHANGE PPID AND INTERNAL PATHS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Print message to screen
disp('Changing PPIDs within matrices...');

% Change BLT matrix
if isfile(BLTfile)
    load(BLTfile);
    params.PPID = PPIDnew;
    params.path.datafile = '';
    save(BLTfile, 'data', 'params');
end

% Change FDT matrix
if isfile(FDTfile)
    load(FDTfile);
    results.PPID = PPIDnew;
    save(FDTfile, 'results');
end

% Print message to screen
disp('... FINISHED!');
disp('---------------------------------------');

end