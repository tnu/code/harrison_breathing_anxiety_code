############################################################################################

# PBIHB: DE-FACE STRUCTURAL IMAGE

############################################################################################

# DESCRIPTION:
# This function runs the de-facing procedure in FSL for the PBIHB study
# Usage: ./deface $subj $data $scripts
#	subj = variable containing PPID
#	data = path to main data folder
#	scripts = path to main scripts folder

############################################################################################
############################################################################################


# Define the function
deface(){

	# Define local variables from inputs
	local subj=$1
	local data=$2
	local scripts=$3
	
	# make sure filename supplied as command line arg else die
	[ $# -eq 0 ] && { echo "Usage: $0 subj data scripts"; exit 1; }
	
	echo ---------------------------------------
	echo DE-FACING ${subj} STRUCTURAL IMAGE
	echo ---------------------------------------
	
	# Create temporary de-face directory
	mkdir ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface
	
	# Bias-correct structural image
	echo Bias-correcting structural image...
	fast -t 1 -n 3 -H 0.1 -I 10 -l 20.0 --nopve -B -b \
		-o ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_struct_biasCorr.nii.gz \
		${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_struct_orig.nii.gz
	
	# Re-name bias-corrected image
	mv ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_struct_biasCorr_restore.nii.gz \
		${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_struct.nii.gz

	# Remove any unnecessary structures from the T1 image
	robustfov -i ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_struct.nii.gz

	# Remove skull
	echo Running initial brain extraction...
	if [ "${subj}" = "5105" ] || [ "${subj}" = "5108" ] || [ "${subj}" = "5223" ] ; then
		betf=0.5
		betg=-0.5
	else
		betf=0.1
		betg=0
	fi
	bet ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_struct.nii.gz \
		${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_struct_brain.nii.gz \
		-f ${betf} -g ${betg}

	# Create affine transformation to standard space
	echo Calculating affine registration to standard space...
	flirt -in ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_struct_brain.nii.gz \
		-ref $FSLDIR/data/standard/MNI152_T1_1mm_brain.nii.gz \
		-out ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_highres2standardAff.nii.gz \
		-omat ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_highres2standardAff.mat \
		-cost corratio -dof 12 -searchrx -90 90 -searchry -90 90 -searchrz -90 90 -interp trilinear

	# Invert transformation
	echo Inverting affine registration to standard space...
	convert_xfm -inverse -omat ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_standard2highresAff.mat \
		${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_highres2standardAff.mat

	# Transform de-face brain mask to structural space
	echo Transforming de-face mask from standard space...
	flirt -interp trilinear -setbackground 0 \
		-in ${scripts}/pbihb_anon/MNI152_T1_1mm_deface_mask.nii.nii.gz \
		-ref ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_struct.nii.gz \
		-applyxfm -init ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_standard2highresAff.mat \
		-out ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_standard2highresDefaceMask.nii.gz
		

	# De-face T1 using mask
	echo Applying de-face mask from standard space...
	fslmaths ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_struct.nii.gz \
		-mas ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface/PBIHB_${subj}_standard2highresDefaceMask.nii.gz \
		${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_struct_orig.nii.gz
	cp ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_struct_orig.nii.gz \
		${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/originals/PBIHB_${subj}_struct_orig.nii.gz

	# Remove temporary files
	echo Removing temporary files...
#	rm -r ${data}/fmri/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_deface

	echo ... FINISHED!
	echo ---------------------------------------

}

# Invoke the function and pass arguments
deface $1 $2 $3
