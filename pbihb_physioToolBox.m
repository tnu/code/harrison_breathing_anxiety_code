%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% BREATHING SCANNER TASK: PHYSIO TOOLBOX %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 06/02/2019
% -------------------------------------------------------------------------
% TO RUN:   [physValues, physTraces] = pbihb_physioToolBox(options, type, physValues, physTraces)
% INPUTS:   options     = PPID-specific matrix output from pbihb_setOptions
%           type        = Specify 'task' or 'rest' scans
%           physValues  = Structure with physiological traces from the 
%                         learning task + summary, or [] if rest
%           physTraces  = Structure with physiological values from the
%                         learning task, or [] if rest
% OUTPUTS:  physValues  = Structure with input plus summary values added
%           physTraces  = Structure with input plus PhysIO output added
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script runs the PhysIO toolbox for the 7 Tesla functional data
% collected in the PBIHB study. A log file is generated from the external
% physiological file information (from LabChart data), with input for the
% cardiac data from the scanner logfile if this exists. This logfile is
% downsampled if the LabChart data is 10000 Hz or above. If the cardiac
% data exists, it is added to the task information in task PhysIO analyses,
% and both respiratory and cardiac data (if available) is additionally 
% analysed for mean and variability in the rest PhysIO analysis. The
% physiology output figures need to be visually inspected to see if
% post-hoc peak detection is required.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [physValues, physTraces] = pbihb_physioToolBox(options, type, physValues, physTraces)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE RESPIRATORY BELT TO NEW PHYSIO LOG FILE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load in log file
if strcmp(type, 'task') == 1
    if isfile(fullfile(options.paths.scanner_logs, options.names.scannerLogs.task))
        fileID = fopen(fullfile(options.paths.scanner_logs, options.names.scannerLogs.task));
    end
    vols = options.taskScan.vols;
elseif strcmp(type, 'rest') == 1
    if isfile(fullfile(options.paths.scanner_logs, options.names.scannerLogs.rest))
        fileID = fopen(fullfile(options.paths.scanner_logs, options.names.scannerLogs.rest));
    end
    vols = options.restScan.vols;
else
    disp('EXITING: No scan type set');
    return
end

% Pull out cardiac data if scanner log available
if exist('fileID', 'var')
    physioData.originalLog = textscan(fileID, '%n %n %n %n %n %n %n %n %n %n', 'CommentStyle', '#');
    physioData.cardiacSource = 'Scanner';
    fclose(fileID);
    for a = 1:length(physioData.originalLog)
        physioData.originalLogData(:,a) = physioData.originalLog{a}(:,1);
    end
    % Find the last gradient signal for lining up breathing trace
    endIndex = find(physioData.originalLogData(:,8), 1, 'last');
    startIndex = endIndex - round(options.taskScan.tr * (vols) * 500);
    % Replace any NaNs in data with previous number (in case of out of range)
    for c = 1:(size(physioData.originalLogData,2))
        % Find first non-NaN row
        indxFirst = find(~isnan(physioData.originalLogData(:,c)),1,'first');
        if ~isempty(indxFirst) == 1
            % Find all NaN rows
            indxNaN = find(isnan(physioData.originalLogData(:,c)));
            % Find NaN rows beyond first non-NaN
            indx = indxNaN(indxNaN > indxFirst);
            % For each of these, copy previous value
            for r = (indx(:))'
                physioData.originalLogData(r,c) = physioData.originalLogData(r-1,c);
            end
        end
    end
else
    physioData.cardiacSource = 'None';
end

if strcmp(type, 'task') == 1
    if isempty(physTraces) == 1
        if isfile(options.saveNames.physTaskTraces) == 1
            load(options.saveNames.physTaskTraces);
        else
            error('No physTraces datafile provided or found... Exiting')
        end   
    end
    if isempty(physValues) == 1
        if isfile(options.saveNames.physTaskValues) == 1
            load(options.saveNames.physTaskValues);
        else
            error('No physValues datafile provided or found... Exiting')
        end        
    end
    physTraces.type = 'task';
    physValues.type = 'task';  
    % Pull out breathing belt trace, demean flip and scale up
    physioData.newLogData(:,2) = -(physTraces.physiology.breathing.rawTrace - mean(physTraces.physiology.breathing.rawTrace)) * 100;
    % Pull out scanner triggers from task preprocessing
    physioData.newLogData(:,3) = physTraces.physiology.MRItrigs > 0.5;
else
    if strcmp(type, 'rest') == 1
        labChart.data = dlmread(fullfile(options.paths.physiology_directory, options.names.labChartData.rest));
    elseif strcmp(type, 'task') == 1
        labChart.data = dlmread(fullfile(options.paths.physiology_directory, options.names.labChartData.task));
    end
    physiology.time = labChart.data(:,1);
    physiology.MRItrigs.trigs = labChart.data(:,7);
    physiology.breathing.rawTrace = labChart.data(:,4);
    physiology.breathingTrigs.peakTrigs = labChart.data(:,10);
    physiology.breathing.rate.rawTrace = labChart.data(:,11);
    physiology.breathing.depth.rawTrace = labChart.data(:,12);
    if strcmp(type, 'rest') == 1 && isfile(fullfile(options.paths.physiology_directory, options.names.labChartComments.rest))
        fileIDcomments = fopen(fullfile(options.paths.physiology_directory, options.names.labChartComments.rest));
        physiology.comments = textscan(fileIDcomments, '%n %s');
        % Insert start and finish indices from comments
        physiology.MRItrigs.startTime = physiology.comments{1};
        physiology.MRItrigs.startIndex = find(physiology.time == physiology.MRItrigs.startTime);
        physiology.MRItrigs.finishIndex = round(physiology.MRItrigs.startIndex + options.taskScan.tr * vols * options.physiology.samplingRate); % Start + TR x volumes x sampling rate
        % Manually insert a trigger every TR
        physiology.MRItrigs.trigs = zeros([length(physiology.MRItrigs.trigs) 1]);
        physiology.MRItrigs.trigs(physiology.MRItrigs.startIndex:round(options.taskScan.tr * options.physiology.samplingRate):physiology.MRItrigs.finishIndex) = 1;
        physiology.MRItrigs.trigIndex = find(physiology.MRItrigs.trigs == 1);
    else % Use triggers from scan data
        physiology.MRItrigs.trigIndex = find(physiology.MRItrigs.trigs == 1);
        physiology.MRItrigs.startIndex = physiology.MRItrigs.trigIndex(1);
        physiology.MRItrigs.finishIndex = physiology.MRItrigs.trigIndex(end) + options.taskScan.tr * options.physiology.samplingRate; % Add extra TR to final trigger index for the end of the scan
    end
    physTraces.type = 'rest';
    physValues.type = 'rest';    
    physTraces.physiology.breathing.rawTrace = physiology.breathing.rawTrace(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
    physTraces.physiology.MRItrigs = physiology.MRItrigs.trigs(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
    physTraces.timeInSeconds = (1:(physiology.MRItrigs.finishIndex - physiology.MRItrigs.startIndex + 1))' / options.physiology.samplingRate;
    % Pull out breathing belt trace, demean flip and scale up
    physioData.newLogData(:,2) = -(physTraces.physiology.breathing.rawTrace - nanmean(physTraces.physiology.breathing.rawTrace)) * 100;
    % Pull out scanner triggers from rest data
    physioData.newLogData(:,3) = physTraces.physiology.MRItrigs > 0.5;
end

% Pull out cardiac trace and upsample from scanner log
if exist('fileID', 'var') == 1
    physioData.upsampled.cardiacTrace = interp1(((1:length(physioData.originalLogData(startIndex:endIndex,5))) / 500), physioData.originalLogData(startIndex:endIndex,5)', ((1:(length(physioData.newLogData(:,1)))) / options.physiology.samplingRate), 'linear'); % Interpolate between points
    physioData.upsampled.breathingTrace = interp1(((1:length(physioData.originalLogData(startIndex:endIndex,6))) / 500), physioData.originalLogData(startIndex:endIndex,6)', ((1:(length(physioData.newLogData(:,2)))) / options.physiology.samplingRate), 'linear'); % Interpolate between points
    physioData.newLogData(:,1) = physioData.upsampled.cardiacTrace;
end

% Convolve the scan trigger trace
convolution = ones(1, (options.physiology.samplingRate / 10));
convolution = conv(physioData.newLogData(:,3)', convolution);
physioData.newLogData(:,3) = convolution(1:length(physioData.newLogData));

% Downsample the physiology data for PhysIO if >= 10000 Hz
if options.physiology.samplingRate >= 10000
    physioData.newLogData = downsample(physioData.newLogData, (options.physiology.samplingRate / 100));
end

% Replace any NaNs in cardiac trace due to upsampling
if isnan(physioData.newLogData(1,1)) == 1
    physioData.newLogData(1,1) = physioData.newLogData(2,1);
end

% Replace any NaNs in data with previous number
for c = 1:(size(physioData.newLogData,2))
    % Find first non-NaN row
    indxFirst = find(~isnan(physioData.newLogData(:,c)),1,'first');
    if ~isempty(indxFirst) == 1
        % Find all NaN rows
        indxNaN = find(isnan(physioData.newLogData(:,c)));
        % Find NaN rows beyond first non-NaN
        indx = indxNaN(indxNaN > indxFirst);
        % For each of these, copy previous value
        for r = (indx(:))'
            physioData.newLogData(r,c) = physioData.newLogData(r-1,c);
        end
    end
end

% Pad the beginning
physioData.newLogData = [zeros(10,3); physioData.newLogData];

% Replace cardiac trace with NaN if does not exist
if strcmp(physioData.cardiacSource, 'None') == 1
    physioData.newLogData(:,1) = NaN;
end

% Print new log file for 'bids' input to PhysIO
if strcmp(type, 'task') == 1
    fileID = fopen((fullfile(options.paths.scanner_logs, options.names.scannerLogsNew.task)), 'w');
    fprintf(fileID,'%.0f %.0f %.0f\n', physioData.newLogData');
    fclose(fileID);
elseif strcmp(type, 'rest') == 1
    fileID = fopen((fullfile(options.paths.scanner_logs, options.names.scannerLogsNew.rest)), 'w');
    fprintf(fileID,'%.0f %.0f %.0f\n', physioData.newLogData');
    fclose(fileID);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN PHYSIO TOOLBOX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

physio = tapas_physio_new();

physio.save_dir = options.paths.physio_directory;
physio.log_files.vendor = 'bids';
if strcmp(type, 'task') == 1    
    physio.log_files.cardiac = cellstr(fullfile(options.paths.scanner_logs, options.names.scannerLogsNew.task));
    physio.log_files.respiration = cellstr(fullfile(options.paths.scanner_logs, options.names.scannerLogsNew.task));
elseif strcmp(type, 'rest') == 1
    physio.log_files.cardiac = cellstr(fullfile(options.paths.scanner_logs, options.names.scannerLogsNew.rest));
    physio.log_files.respiration = cellstr(fullfile(options.paths.scanner_logs, options.names.scannerLogsNew.rest));
end
physio.log_files.scan_timing = {''};
if options.physiology.samplingRate >= 10000
    physio.log_files.sampling_interval = 1 / (options.physiology.samplingRate / 100);
else
    physio.log_files.sampling_interval = 1 / options.physiology.samplingRate;
end
physio.log_files.relative_start_acquisition = 0;
physio.log_files.align_scan = 'first';
physio.scan_timing.sqpar.Nslices = 1; % Only giving volume triggers
physio.scan_timing.sqpar.NslicesPerBeat = [];
physio.scan_timing.sqpar.TR = options.taskScan.tr;
physio.scan_timing.sqpar.Ndummies = 0;
physio.scan_timing.sqpar.Nscans = vols;
physio.scan_timing.sqpar.Nprep = [];
physio.scan_timing.sqpar.time_slice_to_slice = [];
physio.scan_timing.sqpar.onset_slice = 1; % Trigger is at the beginning of the volume
physio.scan_timing.sync.method = 'scan_timing_log';
physio.preproc.cardiac.modality = 'PPU';
physio.preproc.cardiac.initial_cpulse_select.method = 'auto_matched';
physio.preproc.cardiac.initial_cpulse_select.file = 'initial_cpulse_kRpeakfile.mat';
physio.preproc.cardiac.initial_cpulse_select.min = 0.4;
physio.preproc.cardiac.initial_cpulse_select.max_heart_rate_bpm = 120;
physio.preproc.cardiac.initial_cpulse_select.kRpeak = [];
physio.preproc.cardiac.posthoc_cpulse_select.file = '';
physio.preproc.cardiac.posthoc_cpulse_select.percentile = 80;
physio.preproc.cardiac.posthoc_cpulse_select.upper_thresh = 60;
physio.preproc.cardiac.posthoc_cpulse_select.lower_thresh = 60;
physio.model.orthogonalise = 'none';
physio.model.censor_unreliable_recording_intervals = false;
if strcmp(type, 'task') == 1
    physio.model.movement.file_realignment_parameters = {options.saveNames.motion.task_txt};
    physio.verbose.fig_output_file = options.saveNames.physioFigOutput.task;
    physio.model.output_physio = options.saveNames.physio.task;
    physio.model.output_multiple_regressors = options.saveNames.physioRegressors.task;
elseif strcmp(type, 'rest') == 1
    physio.model.movement.file_realignment_parameters = {options.saveNames.motion.rest_txt};
    physio.verbose.fig_output_file = options.saveNames.physioFigOutput.rest;
    physio.model.output_physio = options.saveNames.physio.rest;
    physio.model.output_multiple_regressors = options.saveNames.physioRegressors.rest;
end
physio.model.R = [];
physio.model.retroicor.include = true;
physio.model.retroicor.order.c = 3;
physio.model.retroicor.order.r = 4;
physio.model.retroicor.order.cr = 1;
physio.model.rvt.include = true;
physio.model.rvt.method = 'hilbert';
physio.model.rvt.delays = [-5 0 5 10];
physio.model.hrv.include = false;
physio.model.hrv.delays = 0;
physio.model.noise_rois.include = false;
physio.model.noise_rois.fmri_files = {};
physio.model.noise_rois.roi_files = {};
physio.model.noise_rois.thresholds = 0.9;
physio.model.noise_rois.n_voxel_crop = 0;
physio.model.noise_rois.n_components = 0;
physio.model.noise_rois.force_coregister = 1;
physio.model.movement.include = true;
physio.model.movement.order = 12;
physio.model.movement.censoring_method = 'none';
physio.model.other.include = false;
physio.model.other.input_multiple_regressors = '';
physio.verbose.level = 2;
physio.verbose.process_log = cell(0, 1);
physio.verbose.fig_handles = zeros(0, 1);
physio.verbose.use_tabs = false;
physio.ons_secs.c_scaling = 1;
physio.ons_secs.r_scaling = 1;

physio = tapas_physio_main_create_regressors(physio);

% Add to traces matrix
physTraces.physioData = physioData;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADD EXTRA DATA TO PHYSIOLOGY INFORMATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(physioData.cardiacSource, 'Scanner') == 1
    % Add cardiac trace (interpolate to upsample)
    timeInSeconds = physTraces.timeInSeconds;
    cardiac.rawTrace = physioData.upsampled.cardiacTrace;
    % Calculate heart rate and add trace
    cardiac.peakTime = physio.ons_secs.cpulse - physio.ons_secs.t(1);
    cardiac.peakTime = cardiac.peakTime(cardiac.peakTime >= 0);
    cardiac.peakIndex = round(cardiac.peakTime * options.physiology.samplingRate);
    cardiac.peakTrigs = zeros(length(timeInSeconds), 1);
    cardiac.peakTrigs(cardiac.peakIndex) = 1;
    for a = 2:length(cardiac.peakTime)
        cardiac.peakRate(a,1) = 1 / (cardiac.peakTime(a) - cardiac.peakTime(a - 1)) * 60;
    end
    cardiac.heartRateTrace = interp1(cardiac.peakTime, cardiac.peakRate, timeInSeconds, 'linear');

    % Calculate average heart rate values for each period in the task
    if strcmp(type, 'task') == 1 && exist('physTraces', 'var') == 1
        for n = 1:length(physTraces.physiology.indices.itiOnsetIndex)
            physValues.values.heartRate.iti.heartRate(n,1) = mean(cardiac.heartRateTrace(physTraces.physiology.indices.itiOnsetIndex(n):(physTraces.physiology.indices.itiOnsetIndex(n) + (physValues.values.behaviour.params.dur.iti(n) * options.physiology.samplingRate))));
        end
        for n = 1:length(physValues.values.inspiratoryPressure.noStimulus.onsetTime)
            if timeInSeconds(end) - physValues.values.inspiratoryPressure.noStimulus.onsetTime(n) > 0
                physValues.values.heartRate.noStimulus.heartRate(n,1) = mean(cardiac.heartRateTrace(physTraces.physiology.indices.noStimulusOnsetIndex(n):(physTraces.physiology.indices.noStimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.noStimulus.duration(n) * options.physiology.samplingRate))));
            end
        end
        for n = 1:length(physValues.values.inspiratoryPressure.stimulus.onsetTime)
            physValues.values.heartRate.stimulus.heartRate(n,1) = mean(cardiac.heartRateTrace(physTraces.physiology.indices.stimulusOnsetIndex(n):(physTraces.physiology.indices.stimulusOnsetIndex(n) + round(physValues.values.inspiratoryPressure.stimulus.duration(n) * options.physiology.samplingRate))));
        end
        physValues.summary.iti.absolute.heartRate = mean(physValues.values.heartRate.iti.heartRate);
        physValues.summary.noStimulus.absolute.heartRate = mean(physValues.values.heartRate.noStimulus.heartRate);
        physValues.summary.stimulus.absolute.heartRate = mean(physValues.values.heartRate.stimulus.heartRate);
    end
    
    % Add information to matrix
    physTraces.physiology.cardiac = cardiac;
end

% Add RVT values
physTraces.physiology.breathing.rvtValuesPerVolume = physio.ons_secs.rvt;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CALCULATE HR AND BREATHING VARIABILITY FOR REST DATA 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(type, 'rest') == 1
    
    if strcmp(physioData.cardiacSource, 'Scanner') == 1
        % CARDIAC
        % Calculate average and SD resting HR
        physValues.summary.cardiac.heartRate = mean(cardiac.peakRate);
        physValues.summary.cardiac.heartRateSTD = std(cardiac.peakRate);
        % Calculate RR interval for heartbeats
        for a = 1:(length(cardiac.peakTime) - 1)
            physTraces.physiology.cardiac.peakIntervals(a) = cardiac.peakTime(a + 1) - cardiac.peakTime(a);
        end
        % Calculate successive differences between heartbeats (squared) for RMSSD HR variability metric
        for a = 1:(length(physTraces.physiology.cardiac.peakIntervals) - 1)
            physTraces.physiology.cardiac.RMSSDvalues(a) = (physTraces.physiology.cardiac.peakIntervals(a) + physTraces.physiology.cardiac.peakIntervals(a + 1))^2;
        end
        % Calculate HR variability via RMSSD
        physValues.summary.cardiac.variability.RMSSD = sqrt(mean(physTraces.physiology.cardiac.RMSSDvalues));
        % Calculate HR variability via SDNN method
        physValues.summary.cardiac.variability.SDNN = std(physTraces.physiology.cardiac.peakIntervals);
    end
    
    % BREATHING
    % Find and interpolate between breathing rate values during scan time
    physTraces.physiology.breathing.rate.rawTrace = physiology.breathing.rate.rawTrace(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
    physTraces.physiology.breathing.depth.rawTrace = physiology.breathing.depth.rawTrace(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
    physTraces.physiology.breathing.peakTrigs = physiology.breathingTrigs.peakTrigs(physiology.MRItrigs.startIndex:physiology.MRItrigs.finishIndex);
    physTraces.physiology.breathing.peakIndex = find(physTraces.physiology.breathing.peakTrigs == 1);
    physTraces.physiology.breathing.rate.peakValues = physTraces.physiology.breathing.rate.rawTrace(physTraces.physiology.breathing.peakIndex); % Breathing rate value at maximal inspiration point
    physTraces.physiology.breathing.rate.peakTime = physTraces.timeInSeconds(physTraces.physiology.breathing.peakIndex); % Time of breathing rate value
    physTraces.physiology.breathing.rate.peakTrace = interp1(physTraces.physiology.breathing.rate.peakTime, physTraces.physiology.breathing.rate.peakValues, physTraces.timeInSeconds, 'linear'); % Interpolate between peaks
    physTraces.physiology.breathing.depth.peakValues = physTraces.physiology.breathing.depth.rawTrace(physTraces.physiology.breathing.peakIndex); % Breathing rate value at maximal inspiration point
    physTraces.physiology.breathing.depth.peakTrace = interp1(physTraces.physiology.breathing.rate.peakTime, physTraces.physiology.breathing.depth.peakValues, physTraces.timeInSeconds, 'linear'); % Interpolate between peaks
    % Calculate average and SD resting breathing rate and depth
    physValues.summary.breathing.rate = mean(physTraces.physiology.breathing.rate.peakValues);
    physValues.summary.breathing.rateSTD = std(physTraces.physiology.breathing.rate.peakValues);
    physValues.summary.breathing.rateCV = std(physTraces.physiology.breathing.rate.peakValues) / mean(physTraces.physiology.breathing.rate.peakValues);
    physValues.summary.breathing.depthCV = std(physTraces.physiology.breathing.depth.peakValues) / mean(physTraces.physiology.breathing.depth.peakValues);
    % Calculate PP interval for breaths
    for a = 1:(length(physTraces.physiology.breathing.rate.peakTime) - 1)
        physTraces.physiology.breathing.peakIntervals(a) = physTraces.physiology.breathing.rate.peakTime(a + 1) - physTraces.physiology.breathing.rate.peakTime(a);
    end
    % Calculate successive differences between breaths (squared) for RMSSD breathing variability metric
    for a = 1:(length(physTraces.physiology.breathing.peakIntervals) - 1)
        physTraces.physiology.breathing.RMSSDvalues(a) = (physTraces.physiology.breathing.peakIntervals(a) + physTraces.physiology.breathing.peakIntervals(a + 1))^2;
    end
     % Calculate breathing variability via RMSSD
    physValues.summary.breathing.variability.RMSSD = sqrt(mean(physTraces.physiology.breathing.RMSSDvalues));
    % Calculate breathing variability via SDNN method
    physValues.summary.breathing.variability.SDNN = std(physTraces.physiology.breathing.peakIntervals);
    % Calculate number of sighs based on breathing depth
    physValues.summary.breathing.sighs.cutOffRatio = 2;
    physTraces.physiology.breathing.sighs.ratioToMeanBreathingDepths = physTraces.physiology.breathing.depth.peakValues / mean(physTraces.physiology.breathing.depth.peakValues);
    physValues.summary.breathing.sighs.numberOfSighs = sum(physTraces.physiology.breathing.sighs.ratioToMeanBreathingDepths >= physValues.summary.breathing.sighs.cutOffRatio);   
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE PREPROCESSING MATRIX WITH PHYSIO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Add / update options to structures
physTraces.options = options;
physValues.options = options;

% Save physiology outputs and physio structure
if strcmp(type, 'task') == 1
    save(options.saveNames.physTaskTraces, 'physTraces', '-v7.3', '-nocompression');
    save(options.saveNames.physTaskValues, 'physValues');
    save(options.saveNames.physio.task, 'physio');
elseif strcmp(type, 'rest') == 1
    rest.physTraces = physTraces;
    rest.physValues = physValues;
    save(options.saveNames.physRest, 'rest', '-v7.3', '-nocompression');
    save(options.saveNames.physio.rest, 'physio');
end

end