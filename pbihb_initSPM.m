%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% BREATHING SCANNER TASK: INITIALISE SPM & PHYSIO %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 26/03/2020
% -------------------------------------------------------------------------
% TO RUN:   pbihb_initSPM
% -------------------------------------------------------------------------
% DESCRIPTION:
% This function initialises SPM and PhysIO for the PBIHB study.
% pbihb_setOptions function MUST have been run first to add necessary paths
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_initSPM()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INITIALISE SPM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

spm('defaults', 'fmri');
spm_jobman('initcfg');
% spm_get_defaults('cmdline', true);
% https://en.wikibooks.org/wiki/SPM/Faster_SPM
spm_get_defaults('stats.maxmem', 4 * 2^30); % 4 GB
spm_get_defaults('stats.resmem', true);
fprintf('SPM initialised.\n');
fprintf('Location: %s\n', which('spm'));
fprintf('Version:  %s\n', spm('version'));
fprintf('\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INITIALISE PHYSIO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tapas_physio_init();
fprintf('PhysIO initialised.\n');
fprintf('Location: %s\n', which('tapas_physio_version'));
fprintf('Version:  %s\n', tapas_physio_version());
fprintf('\n');

end