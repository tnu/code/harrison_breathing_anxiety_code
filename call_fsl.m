function [status,output] = call_fsl(cmd)
% [status, output] = call_fsl(cmd)
% 
% Wrapper around calls to FSL binaries
% clears LD_LIBRARY_PATH and ensures
% the FSL envrionment variables have been
% set up
% Debian/Ubuntu users should uncomment as
% indicated

fsldir=getenv('FSLDIR');

% Debian/Ubuntu - uncomment the following
%fsllibdir=sprintf('%s/%s', fsldir, 'bin');

if ismac
  dylibpath=getenv('DYLD_LIBRARY_PATH');
  setenv('DYLD_LIBRARY_PATH');
else
  ldlibpath=getenv('LD_LIBRARY_PATH');
  setenv('LD_LIBRARY_PATH', '/cluster/apps/openblas/0.2.13_seq/x86_64/gcc_4.8.2/lib');
  % Debian/Ubuntu - uncomment the following
  % setenv('LD_LIBRARY_PATH',fsllibdir);
end

disp('DEBUG: Using monkey-patched version of `call_fsl()`.')
[status,output] = system('/bin/sh -c ''ls /cluster/apps/openblas/0.2.13_seq/x86_64/gcc_4.8.2/lib''');
disp(output)
[status,output] = system('/bin/sh -c ''echo $LD_LIBRARY_PATH''');
disp(output)
disp('DEBUG: Tests complete.')

command = sprintf('/bin/sh -c ''. %s/etc/fslconf/fsl.sh; %s''', fsldir, cmd);
[status,output] = system(command);

if ismac
  setenv('DYLD_LIBRARY_PATH', dylibpath);
else
    setenv('LD_LIBRARY_PATH', ldlibpath);
end

if status
    error('FSL call (%s) failed, %s', command, output)
else
    disp('DEBUG: Command run.')
end
