%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% BREATHING SCANNER TASK: EPI REGISTRATION %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 02/07/2019
% -------------------------------------------------------------------------
% TO RUN:               pbihb_fslReg(preproc, type)
% INPUTS:   preproc     = PPID-specific matrix output from pbihb_setOptions
%                         Contains a preproc.options structure
%           type        = 'task' or 'rest'
% OUTPUTS:  image       = EPI image that has been registered to standard  
%                         space using previously-calculated FSL warp
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script registers a preprocessed EPI scan to standard space using
% FSL's applywarp function. An FSL preprocessing .feat folder that contains 
% both a filtered_func.nii.gz image and a reg directory is required.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_fslReg(preproc, type)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WRITE AND RUN A BASH SCRIPT TO REGISTER EPI TO STANDARD SPACE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify images
if strcmp(type, 'task') == 1
    origEPI = fullfile(preproc.options.saveNames.fslPreprocessing.task.directory, 'filtered_func_data.nii.gz');
    EPI = fullfile(preproc.options.paths.image_directory, [preproc.options.names.images.task_EPI_std, '.gz']);
    warp = fullfile(preproc.options.saveNames.fslPreprocessing.task.directory, '/reg/example_func2standard_warp.nii.gz');
elseif strcmp(type, 'rest') == 1
    origEPI = fullfile(preproc.options.saveNames.fslPreprocessing.rest.directory, 'filtered_func_data.nii.gz');
    EPI = fullfile(preproc.options.paths.image_directory, [preproc.options.names.images.rest_EPI_std, '.gz']);
    warp = fullfile(preproc.options.saveNames.fslPreprocessing.rest.directory, '/reg/example_func2standard_warp.nii.gz');
end

if ~isfile(EPI)
    % Open a file for bash script
    fileID = fopen('bashScript.sh','w');

    % Add bash command
    fprintf(fileID, '#!/bin/bash\n\n');

    % Add an applywarp command
    fprintf(fileID, ['applywarp -i ', origEPI, ' -o ', EPI, ' -r $FSLDIR/data/standard/MNI152_T1_1mm_brain.nii.gz -w ', warp, ' \n\n']);

    % Close the file
    fclose(fileID);

    % Run the bash script
    ! chmod 777 bashScript.sh
    disp('Registering EPI to standard space...');
    ! ./bashScript.sh
    disp('...EPI registration finished!');

    % Remove the bash script
    ! rm bashScript.sh
else
    disp('SKIPPING EPI REGISTRATION: Standard space EPI scan already exists');
end

end