%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%% BREATHING SCANNER TASK: GROUP SPM ANALYSIS %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 04/04/2020
% -------------------------------------------------------------------------
% TO RUN:   analysis    = pbihb_spmAnalysisGroup(location, higherL, lowerL, varargin)
% INPUTS:   location    = 'local' or 'euler'
%           higherL     = 'groups', 'groupsEx', 'regress', 'explore'
%           lowerL      = 'basic', 'basicSPM', 'model', 'modelPred', 
%                         'modelErr', 'modelFactors' or 'FC'
%           varargin    = If 'lowerL' is 'FC', need to specify: 
%                           1) The seed: 'aIns' or 'PAG'
%                           2) The middle level contrast: 'predict', 
%                              'error' or 'both'
%                           OR
%                           2) The lower level contrast: 'predictPos',
%                              'predictNeg', 'errorPos' or 'errorNeg'
% OUTPUTS:  analysis    = Structure with spm batch specifications and 
%                         results added
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script runs the group analysis for the 7 Tesla functional task data
% collected in the PBIHB study (in spm), using the following:
%   1) Model specification from the GLM designed in the specified lower and
%      higher levels
%   2) Model estimation of this GLM
%   3) Setting contrasts of interest
%   4) Estimation and output of thresholded results
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function analysis = pbihb_spmAnalysisGroup(location, higherL, lowerL, varargin)

%% LOAD GLMS

% Secify PPIDs and options for whole group
analysis.options = pbihb_setOptions('all', location);

% Load higher level GLM
if strcmp(higherL, 'groups') == 1
    analysis.nameHigher = 'groups';
    analysis.designHigher = load(analysis.options.saveNames.groupDiffGLM);
    analysis.designHigherLocation = analysis.options.saveNames.groupDiffGLM;
    analysis.designHigherInfo = load(analysis.options.saveNames.groupDiffGLMinfo);
    analysis.designConOut = analysis.options.saveNames.groupDiffGLMcon;
    analysis.designConOutRd = analysis.options.saveNames.groupDiffGLMconRd;
elseif strcmp(higherL, 'groupsEx') == 1
    analysis.nameHigher = 'groupsEx';
    analysis.designHigher = load(analysis.options.saveNames.groupDiffGLMex);
    analysis.designHigherLocation = analysis.options.saveNames.groupDiffGLMex;
    analysis.designHigherInfo = load(analysis.options.saveNames.groupDiffGLMinfo);
    analysis.designConOut = analysis.options.saveNames.groupDiffGLMconEx;
    analysis.designConOutRd = analysis.options.saveNames.groupDiffGLMconRdEx;
elseif strcmp(higherL, 'regress') == 1
    analysis.nameHigher = 'regress';
    analysis.designHigher = load(analysis.options.saveNames.groupRegressGLM);
    analysis.designHigherLocation = analysis.options.saveNames.groupRegressGLM;
    analysis.designHigherInfo = load(analysis.options.saveNames.groupRegressGLMinfo);
    analysis.designConOut = analysis.options.saveNames.groupRegressGLMcon;
    analysis.designConOutRd = analysis.options.saveNames.groupRegressGLMconRd;
elseif strcmp(higherL, 'explore') == 1
    analysis.nameHigher = 'explore';
    analysis.designHigher = load(analysis.options.saveNames.groupExploreGLM);
    analysis.designHigherLocation = analysis.options.saveNames.groupExploreGLM;
    analysis.designHigherInfo = load(analysis.options.saveNames.groupExploreGLMinfo);
    analysis.designConOut = analysis.options.saveNames.groupExploreGLMcon;
    analysis.designConOutRd = analysis.options.saveNames.groupExploreGLMconRd;
else
    disp('Higher level GLM type invalid... Exiting');
    return
end

% Load example lower level GLM
example = pbihb_setOptions(analysis.options.PPIDs.total{1}, analysis.options.paths.location);
analysis.designLowerEx.PPID = analysis.options.PPIDs.total{1};
if strcmp(lowerL, 'basic') == 1
    analysis.nameLower = 'basic';
    analysis.lowerEx = load(example.saveNames.analysis.basic);
    totalContrasts = length(analysis.lowerEx.analysis.design.names) + length(analysis.lowerEx.analysis.design.extraContrasts.names);
elseif strcmp(lowerL, 'basicSPM') == 1
    analysis.nameLower = 'basicSPM';
    analysis.lowerEx = load(example.saveNames.analysis.basicSpm);
    totalContrasts = length(analysis.lowerEx.analysis.design.names) + analysis.lowerEx.analysis.design.numMod + length(analysis.lowerEx.analysis.design.extraContrasts.names);
elseif strcmp(lowerL, 'model') == 1
    analysis.nameLower = 'model';
    analysis.lowerEx = load(example.saveNames.analysis.model);
    totalContrasts = length(analysis.lowerEx.analysis.design.names) + length(analysis.lowerEx.analysis.design.extraContrasts.names);
elseif strcmp(lowerL, 'modelPred') == 1
    analysis.nameLower = 'modelPred';
    analysis.lowerEx = load(example.saveNames.analysis.modelPred);
    totalContrasts = length(analysis.lowerEx.analysis.design.names) + length(analysis.lowerEx.analysis.design.extraContrasts.names);
elseif strcmp(lowerL, 'modelErr') == 1
    analysis.nameLower = 'modelErr';
    analysis.lowerEx = load(example.saveNames.analysis.modelErr);
    totalContrasts = length(analysis.lowerEx.analysis.design.names) + length(analysis.lowerEx.analysis.design.extraContrasts.names);
elseif strcmp(lowerL, 'modelFactors') == 1
    analysis.nameLower = 'modelFactors';
    analysis.lowerEx = load(example.saveNames.analysis.modelFactors);
    totalContrasts = length(analysis.lowerEx.analysis.design.names);
elseif strcmp(lowerL, 'FC') == 1
    roi = varargin{1};
    if strcmp(varargin{2}, 'both') || strcmp(varargin{2}, 'predict') || strcmp(varargin{2}, 'error')
        if strcmp(varargin{2}, 'both')
            contrast = 'predict_error';
        else
            contrast = varargin{2};
        end
        analysis.nameLower = ['FC_FE_', roi, '_', contrast];
        lowerExSaveFolder = [example.paths.analysis_directories.modelFC, 'FE_', roi, '_', contrast];
        lowerExSaveName = fullfile(lowerExSaveFolder, ['PBIHB_', example.PPID, '_analysis_modelFC_FE_', roi, '_', contrast, '.mat']);
    else
        contrast = varargin{2};
        analysis.nameLower = ['FC_', roi, '_', contrast];
        lowerExSaveFolder = [example.paths.analysis_directories.modelFC, roi, '_', contrast];
        lowerExSaveName = fullfile(lowerExSaveFolder, ['PBIHB_', example.PPID, '_analysis_modelFC_', roi, '_', contrast, '.mat']);
    end
    analysis.lowerEx = load(lowerExSaveName);
    totalContrasts = length(analysis.lowerEx.analysis.design.finalMatrixOrder);
else
    disp('Lower level GLM type invalid... Exiting');
    return
end

% Specify save names and locations
saveName = ['PBIHB_', analysis.nameHigher, '_', analysis.nameLower];
saveFolder = fullfile(analysis.options.paths.group_directory_imaging, saveName);

% Check directory existance and make if required
if ~exist(saveFolder, 'dir')
   mkdir(saveFolder);
end


%% LOOP ANALYSIS OVER CONTRASTS FROM LOWER LEVEL

for conNum = 1:totalContrasts
    
    % Specify name of lower level contrast
    analysis.setup{conNum}.name = analysis.lowerEx.analysis.design.finalMatrixOrder{conNum};
    
    % Clear matlabbatch variables
    matlabbatch = {};
    

    %% 1) RUN MODEL SPECIFICATION
    
    saveSubFolder = fullfile(saveFolder, ['lowerLevel_con', num2str(conNum, '%04.f'), '_', analysis.setup{conNum}.name]);
    if ~exist(saveSubFolder, 'dir')
        mkdir(saveSubFolder)
    elseif isfile(fullfile(saveSubFolder, 'SPM.mat'))
        delete(fullfile(saveSubFolder, '*'));
    end
    matlabbatch{1}.spm.stats.factorial_design.dir = cellstr(saveSubFolder);
    for PPIDidx = 1:length(analysis.options.PPIDs.total)
        setupTemp = pbihb_setOptions(analysis.options.PPIDs.total{PPIDidx}, analysis.options.paths.location);
        if strcmp(lowerL, 'basic') == 1
            path = setupTemp.paths.analysis_directories.basic;
        elseif strcmp(lowerL, 'basicSPM') == 1
            path = setupTemp.paths.analysis_directories.basicSpm;
        elseif strcmp(lowerL, 'model') == 1
            path = setupTemp.paths.analysis_directories.model;
        elseif strcmp(lowerL, 'modelPred') == 1
            path = setupTemp.paths.analysis_directories.modelPred;
        elseif strcmp(lowerL, 'modelErr') == 1
            path = setupTemp.paths.analysis_directories.modelErr;
        elseif strcmp(lowerL, 'modelFactors') == 1
            path = setupTemp.paths.analysis_directories.modelFactors;
        elseif strcmp(lowerL, 'FC') == 1
            if strcmp(varargin{2}, 'both') || strcmp(varargin{2}, 'predict') || strcmp(varargin{2}, 'error')
                path = [setupTemp.paths.analysis_directories.modelFC, 'FE_', roi, '_', contrast];
            else
                path = [setupTemp.paths.analysis_directories.modelFC, roi, '_', contrast];
            end
        end
        matlabbatch{1}.spm.stats.factorial_design.des.mreg.scans{PPIDidx,1} = fullfile(path, ['con_', num2str(conNum, '%04.f'), '.nii']);
    end
    if strcmp(higherL, 'groupsEx') == 1 % Remove excluded participants if required
        matlabbatch{1}.spm.stats.factorial_design.des.mreg.scans(analysis.designHigherInfo.idxEx) = [];
    end
    matlabbatch{1}.spm.stats.factorial_design.des.mreg.mcov = struct('c', {}, 'cname', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.des.mreg.incint = 0;
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = cellstr(analysis.designHigherLocation);
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 5;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    if strcmp(lowerL, 'FC') == 1
        matlabbatch{1}.spm.stats.factorial_design.masking.em = cellstr(analysis.options.masks.standardGM);
    else
        matlabbatch{1}.spm.stats.factorial_design.masking.em = cellstr(analysis.options.masks.standard);
    end
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;


    %% 2) RUN MODEL ESTIMATION

    matlabbatch{2}.spm.stats.fmri_est.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
    matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
    matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;


    %% 3) SET CONTRASTS OF INTEREST AND EXPORT RESULTS

    matlabbatch{3}.spm.stats.con.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
    weights = zeros(1,size(analysis.designHigher,2));

    % Add an F-test over group mean (first contrast in all designs)
    matlabbatch{3}.spm.stats.con.consess{1}.fcon.name = [analysis.designHigherInfo.names{1}, '_fTest'];
    matlabbatch{3}.spm.stats.con.consess{1}.fcon.weights = weights;
    matlabbatch{3}.spm.stats.con.consess{1}.fcon.weights(1) = 1;
    matlabbatch{3}.spm.stats.con.consess{1}.fcon.sessrep = 'none';

    % Add a positive and negative contrast for every other regressor in HL
    conCount = 1;
    for regressNum = 1:length(analysis.designHigherInfo.names)
        % Add the positive T-contrast
        conCount = conCount + 1;
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.name = [analysis.designHigherInfo.names{regressNum}, '_pos'];
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights = weights;
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights(regressNum) = 1;
        designCon((conCount-1),:) = matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights;
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.sessrep = 'none';
        % Add the negative T-contrast
        conCount = conCount + 1;
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.name = [analysis.designHigherInfo.names{regressNum}, '_neg'];
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights = weights;
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights(regressNum) = -1;
        designCon((conCount-1),:) = matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights;
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.sessrep = 'none';
    end
    matlabbatch{3}.spm.stats.con.delete = 0;
    
    
    %% SAVE THE SPECS AND RUN THE JOBS

    % Add the batch to the matrix for safe keeping and save the files
    analysis.batches{conNum}.matlabbatch = matlabbatch;
    save(fullfile(saveSubFolder, [saveName, '.mat']), 'analysis');
    
    % Unzip the required lower level images
    disp('Unzipping all lower level files...');
    for toUnZip = 1:length(matlabbatch{1}.spm.stats.factorial_design.des.mreg.scans)
        system(['gunzip ', matlabbatch{1}.spm.stats.factorial_design.des.mreg.scans{toUnZip,1}]);
    end
    
    % Run the analysis
    fprintf('\nRunning HL analysis for contrast %d of %d...', conNum, totalContrasts);
    spm_jobman('run', matlabbatch);
    
    
    %% RUN CLUSTER CORRECTION AND OUTPUT THRESHOLDED RESULTS

    % Run for every contrast specified
    for export = 1:length(analysis.batches{conNum}.matlabbatch{3}.spm.stats.con.consess)
         
        % Clear batch variable
        matlabbatch = {};
        clear global pbihb_cluster_threshold
        
        % First pass, where significance is the cluster-forming threshold
        matlabbatch{1}.spm.stats.results.spmmat = {fullfile(saveSubFolder, 'SPM.mat')};
        matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
        matlabbatch{1}.spm.stats.results.conspec.contrasts = export;
        matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
        matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
        matlabbatch{1}.spm.stats.results.conspec.extent = 0;
        matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
        matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
        matlabbatch{1}.spm.stats.results.units = 1;
        if export == 1
            matlabbatch{1}.spm.stats.results.export{1}.tspm.basename = ['thresh_', analysis.batches{conNum}.matlabbatch{3}.spm.stats.con.consess{export}.fcon.name, '_noFWE'];
        else
            matlabbatch{1}.spm.stats.results.export{1}.tspm.basename = ['thresh_', analysis.batches{conNum}.matlabbatch{3}.spm.stats.con.consess{export}.tcon.name, '_noFWE'];
        end

        % Save the mini-job for safe keeping
        analysis.batches{conNum}.matlabbatch{end+1} = matlabbatch;
        
        % Run the mini-job
        spm_jobman('run', matlabbatch);
        
        % F*** you SPM
        global pbihb_cluster_threshold;
        if isempty(pbihb_cluster_threshold)
            continue
        end
        pbihb_cluster_threshold
        pbihb_cluster_threshold(3)
        round(pbihb_cluster_threshold(3))
        pbihb_cluster_threshold(3) - round(pbihb_cluster_threshold(3))
        
        % Save the extent thresholds
        analysis.conExtentThresholds{export} = pbihb_cluster_threshold;
        
        % Second pass, which sets the cluster-extent for FWE (p < 0.05)
        matlabbatch{1}.spm.stats.results.conspec.extent = round(pbihb_cluster_threshold(3));  % See spm_list.m
        if export == 1
            matlabbatch{1}.spm.stats.results.export{1}.tspm.basename = ['thresh_', analysis.batches{conNum}.matlabbatch{3}.spm.stats.con.consess{export}.fcon.name, '_clusterFWE'];
        else
            matlabbatch{1}.spm.stats.results.export{1}.tspm.basename = ['thresh_', analysis.batches{conNum}.matlabbatch{3}.spm.stats.con.consess{export}.tcon.name, '_clusterFWE'];
        end
        matlabbatch{1}.spm.stats.results.export{2}.ps = true;
        matlabbatch{1}.spm.stats.results.export{3}.csv = true;
        
        % Save the mini-job for safe keeping
        analysis.batches{conNum}.matlabbatch{end+1} = matlabbatch;
        
        % Re-run the mini-job with cluster correction
         spm_jobman('run', matlabbatch);
        
    end
    
    % Save the files
    save(fullfile(saveSubFolder, [saveName, '.mat']), 'analysis');
    
    % CLUSTER CORRECTION PROCEDURE NOTES:
    % See e.g. https://www.jiscmail.ac.uk/cgi-bin/webadmin?A2=ind02&L=SPM&P=R93190
    % Start SPM
    % Open the results and contrast of interest
    % Set the significance level (this becomes the cluster-forming threshold): 0.001 unc
    % Read the bottom line of the table, and find e.g. `FWEc: 197`. This is the cluster-extent threshold (p < 0.05). 
    % Change the significance level: keep the original, but now add the FWEc value to the extent box
    % Double check smallest cluster still has pFWE-corr < 0.05
    % Save the thresholded SPM from the dropdown menu
    
    
    %% EXPORT THE NECESSARY FILES FOR FURTHER NON-PARAMETRIC ANALYSES
    
    % Export the contrast setup for randomise analyses
    fileID = fopen(analysis.designConOut, 'w');
    if strcmp(higherL, 'groups') == 1 || strcmp(higherL, 'groupsEx') == 1 || strcmp(higherL, 'regress') == 1
        fprintf(fileID,'%-5.2f %-5.2f\n', designCon');
    elseif strcmp(higherL, 'explore') == 1
        fprintf(fileID,'%-5.2f %-5.2f %-5.2f %-5.2f\n', designCon');
    end
    fclose(fileID);
    command1 = sprintf('Text2Vest %s %s', analysis.designConOut, analysis.designConOutRd);
    status = system(command1);
    
    % Add all contrasts together in 4D file for randomise analyses
    out4D = fullfile(saveSubFolder, ['lowerLevel_con', num2str(conNum, '%04.f'), '_', analysis.setup{conNum}.name, '_4Dfile.nii.gz']);
    files = join(analysis.batches{conNum}.matlabbatch{1}.spm.stats.factorial_design.des.mreg.scans);
    command2 = sprintf('fslmerge -t %s %s', out4D, char(files));
    disp('Concatenating contrast files into 4D image...');
    status = system(command2);

    
    %% CLEAN UP FILES

    % Re-zip all lower levels
    disp('Re-zipping all lower level files...');
    for toZip = 1:length(analysis.batches{conNum}.matlabbatch{1}.spm.stats.factorial_design.des.mreg.scans)
        system(['gzip ', analysis.batches{conNum}.matlabbatch{1}.spm.stats.factorial_design.des.mreg.scans{toZip,1}]);
    end
    
    % Zip all analysis files
    disp('Zipping all created analysis files...');
    system(['gzip ', [saveSubFolder, '/*.nii']]);
    disp('... Finished!');

    
    %% RETURN TO ANALYSIS DIRECTORY
    
    cd(analysis.options.paths.analysis_scripts);
    
    
end

end