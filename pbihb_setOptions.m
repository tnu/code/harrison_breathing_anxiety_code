%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% PBIHB BREATHING SCANNER TASK: SET UP OPTIONS %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 06/02/2019
% -------------------------------------------------------------------------
% TO RUN:   options     = pbihb_setOptions(PPID, varargin)
% INPUTS:   PPID        = PPID to analyse (4 digit number in string format)
%                         or 'all' to set options for group analyses
%           varargin    = Optional location 'local' or 'euler'
%                         Default is 'local' if not specified
% OUTPUTS:  options     = Structure with all setup options specified
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script sets the required options, paths and file names for the
% imaging analysis in the PBIHB study. Any changes should be made here, and
% will influence all other analysis scripts for this study.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function options = pbihb_setOptions(PPID, varargin)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY NECESSARY PATHS FOR LOCATION OF THE ANALYSIS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify roots
if strcmp(varargin, 'euler') == 1
    options.paths.location = 'euler';
    options.paths.root = '/cluster/work/tnu/ofaull/PBIHB_anon_rep'; %'/cluster/home/ofaull/studies/PBIHB'; --> TO CHANGE FOR CODE REVIEW
    options.paths.analysis_scripts = '/cluster/home/ofaull/PBIHB/pbihb_scanner_analysis/'; % --> TO CHANGE FOR CODE REVIEW
else % SHOULD NOT NEED TO CHANGE BELOW HERE FOR CODE REVIEW ON EULER
    options.paths.location = 'local';
    options.paths.root = '/Volumes/TNU_data/PBIHB_data_local/PBIHB_anon';
%     options.paths.root = '/Users/faullol/Desktop/PBIHB_working_anon';
    options.paths.analysis_scripts = '/Users/faullol/Documents/just_breathe/zurich/gitTNU/pbihb_scanner_analysis/';
    options.paths.root_local_raw = '/Volumes/TNU_data/PBIHB_data_local/PBIHB_raw/fmri/raw/';
    options.paths.root_local_setup = '/Volumes/TNU_data/PBIHB_data_local/PBIHB_setup/fmri/';
end
options.paths.root_fmri = fullfile(options.paths.root, 'fmri');
options.paths.root_behavior = fullfile(options.paths.root, 'behavior');
options.paths.root_quest = fullfile(options.paths.root, 'questionnaires');

% Specify paths to code
options.paths.spm_directory = fullfile(options.paths.analysis_scripts, 'SPM12/');
options.paths.physIO_directory = fullfile(options.paths.analysis_scripts, 'PhysIO/code/');
options.paths.hgf_directory = fullfile(options.paths.analysis_scripts, 'TAPAS/HGF/');
options.paths.fsl_templates = fullfile(options.paths.analysis_scripts, 'pbihb_FSL_templates');
options.paths.smoothingFunc = fullfile(options.paths.analysis_scripts, 'smoothing');
options.paths.prepModels = fullfile(options.paths.analysis_scripts, 'pbihb_prepModels');
options.paths.imagingMasks = fullfile(options.paths.analysis_scripts, 'pbihb_masks');
options.paths.HMetad = fullfile(options.paths.analysis_scripts, 'HMeta-d/');
options.paths.matjagsEuler = fullfile(options.paths.analysis_scripts, 'HMeta-d-installEuler/');
options.paths.figureCode = fullfile(options.paths.analysis_scripts, 'pbihb_figures');
options.paths.brainNet = fullfile(options.paths.figureCode, 'BrainNetViewer_20191031');

% Add paths for necessary code
addpath(options.paths.spm_directory);
addpath(options.paths.physIO_directory);
addpath(options.paths.hgf_directory);
addpath(options.paths.analysis_scripts);
addpath(options.paths.fsl_templates);
addpath(options.paths.smoothingFunc);
addpath(options.paths.prepModels);
addpath(options.paths.figureCode);
addpath(options.paths.brainNet);
addpath(genpath(options.paths.HMetad));

% Add location of questionnaires
options.names.questionnaires.prescreen1 = fullfile(options.paths.root_quest, 'results-survey976467.csv');
options.names.questionnaires.prescreen2 = fullfile(options.paths.root_quest, 'results-survey757339.csv');
options.names.questionnaires.session = fullfile(options.paths.root_quest, 'results-survey951191.csv');
options.names.questionnaires.idx = fullfile(options.paths.root_quest, 'PBIHB_questionnaire_indexing.xlsx');

% Specify save directories for group fMRI task results
options.paths.group_directory = fullfile(options.paths.root_fmri, 'PBIHB_BLT_results');
options.paths.group_directory_models = fullfile(options.paths.group_directory, 'PBIHB_models');
options.paths.group_directory_groupComp = fullfile(options.paths.group_directory, 'PBIHB_groupComp');
options.paths.group_directory_imaging = fullfile(options.paths.group_directory, 'PBIHB_imaging');
options.paths.group_directory_imagingGroupGLMs = fullfile(options.paths.group_directory_imaging, 'PBIHB_groupGLMs');

% Specify save names for FDT
options.paths.fdt.results = fullfile(options.paths.root_behavior, 'PBIHB_FDT_results');

% Specify directory for original ICA noise files
options.paths.noise_directory = fullfile(options.paths.root_fmri, 'TNU_PBIHB_noiseFiles');

% Specify required masks
options.masks.standard = fullfile(options.paths.imagingMasks, 'MNI152_T1_1mm_brain_mask.nii');
options.masks.standardGM = fullfile(options.paths.imagingMasks, 'avg152T1_gray_1mm_mask.nii');
options.masks.aIns = fullfile(options.paths.imagingMasks, 'PBIHB_aInsBN_mask.nii');
options.masks.aInsL = fullfile(options.paths.imagingMasks, 'PBIHB_aInsBN_mask_left.nii');
options.masks.aInsR = fullfile(options.paths.imagingMasks, 'PBIHB_aInsBN_mask_right.nii');
options.masks.PAG = fullfile(options.paths.imagingMasks, 'PBIHB_PAG_mask.nii');
options.masks.PAGL = fullfile(options.paths.imagingMasks, 'PBIHB_PAG_mask_left.nii');
options.masks.PAGR = fullfile(options.paths.imagingMasks, 'PBIHB_PAG_mask_right.nii');

% Specify sphere sizes for timeseries extraction
options.masks.sphereSize.aIns = 4;
options.masks.sphereSize.PAG = 2;

% Specify necessary save names for the fMRI task analyses
options.saveNames.pilotData = fullfile(options.paths.group_directory_models, 'PBIHB_pilotData.mat');
options.saveNames.modelFits = fullfile(options.paths.group_directory_models, 'PBIHB_modelFits.mat');
options.saveNames.modelSimSummary = fullfile(options.paths.group_directory_models, 'PBIHB_modelSimSummary.mat');
options.saveNames.modelSimFigCM = fullfile(options.paths.group_directory_models, 'PBIHB_modelSimFigCM');
options.saveNames.modelSimFigParamPrc = fullfile(options.paths.group_directory_models, 'PBIHB_modelSimFigParamPrc');
options.saveNames.modelSimFigParamObs = fullfile(options.paths.group_directory_models, 'PBIHB_modelSimFigParamObs');
options.saveNames.groupComp = fullfile(options.paths.group_directory_groupComp, 'PBIHB_groupComp.mat');
options.saveNames.pcaResults = fullfile(options.paths.group_directory_groupComp, 'PBIHB_pcaResults.mat');
options.saveNames.glmModelCorr = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_modelGlmCorr.mat');
options.saveNames.glmModelRvalues = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_modelGlmCorr_Rvalues');
options.saveNames.glmModelPvalues = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_modelGlmCorr_Pvalues');
options.saveNames.glmModelRvaluesPredErr = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_modelGlmCorr_RvaluesPredErr');
options.saveNames.glmModelPvaluesPredErr = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_modelGlmCorr_PvaluesPredErr');
options.saveNames.glmBasicRvalues = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_basicGlmCorr_Rvalues');
options.saveNames.glmBasicPvalues = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_basicGlmCorr_Pvalues');
options.saveNames.glmBasicRvaluesPredErr = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_basicGlmCorr_RvaluesPredErr');
options.saveNames.glmBasicPvaluesPredErr = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_basicGlmCorr_PvaluesPredErr');
options.saveNames.groupTotalGLMinfo = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupTotalModelInfo.mat');
options.saveNames.groupDiffGLM = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupDiffModel.txt');
options.saveNames.groupDiffGLMex = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupDiffModelEx.txt');
options.saveNames.groupDiffGLMrd = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupDiffModel.mat');
options.saveNames.groupDiffGLMrdEx = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupDiffModelEx.mat');
options.saveNames.groupDiffGLMinfo = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupDiffModelInfo.mat');
options.saveNames.groupDiffGLMcon = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupDiffModel_con.txt');
options.saveNames.groupDiffGLMconRd = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupDiffModel_con.con');
options.saveNames.groupDiffGLMconEx = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupDiffModel_conEx.txt');
options.saveNames.groupDiffGLMconRdEx = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupDiffModel_conEx.con');
options.saveNames.groupRegressGLM = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupRegressModel.txt');
options.saveNames.groupRegressGLMrd = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupRegressModel.mat');
options.saveNames.groupRegressGLMinfo = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupRegressModelInfo.mat');
options.saveNames.groupRegressGLMcon = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupRegressModel_con.txt');
options.saveNames.groupRegressGLMconRd = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupRegressModel_con.con');
options.saveNames.groupExploreGLM = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupExploreModel.txt');
options.saveNames.groupExploreGLMrd = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupExploreModel.mat');
options.saveNames.groupExploreGLMinfo = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupExploreModelInfo.mat');
options.saveNames.groupExploreGLMcon = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupExploreModel_con.txt');
options.saveNames.groupExploreGLMconRd = fullfile(options.paths.group_directory_imagingGroupGLMs, 'PBIHB_groupExploreModel_con.con');

% Specify the save names for the extracted aIns activity
options.saveNames.aIns.posPred = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_model', 'VOI_total_aIns_predictPos_min_mean.txt');
options.saveNames.aIns.negPred = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_model', 'VOI_total_aIns_predictNeg_min_mean.txt');
options.saveNames.aIns.posErr = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_model', 'VOI_total_aIns_errorPos_mean.txt');
options.saveNames.aIns.negErr = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_model', 'VOI_total_aIns_errorNeg_mean.txt');

% Specify save name for FDT and questionnaire analysis
options.saveNames.fdt = fullfile(options.paths.fdt.results, 'PBIHB_FDT.mat');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY REQUIRED VALUES FOR WHOLE GROUP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(PPID, 'all') == 1
    
    % Specify all PPIDs
    options.PPIDs.total = {'5105' '5104' '5112' '5127' '5119' '5126' ...
    '5107' '5125' '5123' '5103' '5124' '5121' '5115' '5110' '5117' ...
    '5129' '5120' '5113' '5130' '5102' '5111' '5114' '5128' '5106' ...
    '5118' '5122' '5109' '5108' '5116' '5101' '5201' '5203' '5217' ...
    '5210' '5208' '5213' '5222' '5212' '5221' '5209' '5228' '5215' ...
    '5214' '5206' '5223' '5202' '5219' '5227' '5207' '5230' '5204' ...
    '5216' '5218' '5220' '5226' '5205' '5225' '5229' '5211' '5224'};

    % Specify indices for low and moderate groups
    options.PPIDs.low.idx = 1:30;
    options.PPIDs.mod.idx = 31:60;
    
    % Specify all pilot PPIDs
    options.PPIDs.pilots = {'5303' '5304' '5301' '5302' '5403' '5404' ...
        '5402', '5401'};
    
    % Specify specs for simulations
    options.sims.numGroups = 10;
    options.sims.numSubs = 60;
    options.sims.ze = [1 5 10];
    
    % Exit here if group analysis
    return;
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY REQUIRED VALUES FOR SINGLE SUBJECT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify necessary values
options.PPID = num2str(PPID);
options.taskScan.tr = 2.3; % Insert TR value here
options.taskScan.slices = 32; % Insert number of slices
options.taskScan.sliceMiddle = 16; % Insert middle slice
options.taskScan.vols = 860; % Insert number of task EPI volumes in here
options.restScan.vols = 250; % Insert number of rest EPI volumes here
options.taskScan.hpf = 100; % Specify high pass filter to use
options.physiology.samplingRate = 10000; % Specify sampling rate of physiology file in Hz
options.taskScan.logFileSampling = 50; % Specify sampling rate of scanner log file in Hz


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY EXCEPTION VALUES FOR SUBJECTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify general exceptions
if options.PPID == '9997'
    options.taskScan.vols = 950;
    options.physiology.samplingRate = 1000;
elseif options.PPID == '9996'
    options.physiology.samplingRate = 1000;
elseif options.PPID == '0009'
    options.taskScan.tr = 2.315;
end

% Specify bet exceptions
if strcmp(options.PPID,'0018') == 1 || strcmp(options.PPID,'1032')
    options.taskScan.bet.f_val = 0.5;
    options.taskScan.bet.g_val = -0.5;
else % Standard
    options.taskScan.bet.f_val = 0.1;
    options.taskScan.bet.g_val = 0;
end

% Specify timing exceptions
if options.PPID == '0021'
    options.taskScan.resistTimings = 'valves';
else  % Standard
    options.taskScan.resistTimings = 'inhalation';
end
  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY NECESSARY FILE NAMES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify required paths
options.paths.image_directory = fullfile(options.paths.root_fmri, ['TNU_PBIHB_', options.PPID], ['PBIHB_', options.PPID, '_images']);
options.paths.physiology_directory = fullfile(options.paths.root_fmri, ['TNU_PBIHB_', options.PPID], ['PBIHB_', options.PPID, '_physiology']);
options.paths.task_directory = fullfile(options.paths.root_fmri, ['TNU_PBIHB_', options.PPID], ['PBIHB_', options.PPID, '_task']);
options.paths.scanner_logs = fullfile(options.paths.root_fmri, ['TNU_PBIHB_', options.PPID], ['PBIHB_', options.PPID, '_scanner_logs']);
options.paths.preprocessing_directory = fullfile(options.paths.root_fmri, ['TNU_PBIHB_', options.PPID], ['PBIHB_', options.PPID, '_preprocessing']);
options.paths.physio_directory = fullfile(options.paths.preprocessing_directory, ['PBIHB_', options.PPID, '_physio']);
options.paths.glm_directory = fullfile(options.paths.root_fmri, ['TNU_PBIHB_', options.PPID], ['PBIHB_', options.PPID, '_glm']);
options.paths.timeseries_directory = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_timeseries']);
options.paths.analysis_directories.root = fullfile(options.paths.root_fmri, ['TNU_PBIHB_', options.PPID], ['PBIHB_', options.PPID, '_analysis']);
options.paths.analysis_directories.basic = fullfile(options.paths.analysis_directories.root, ['PBIHB_', options.PPID, '_basic']);
options.paths.analysis_directories.basicSpm = fullfile(options.paths.analysis_directories.root, ['PBIHB_', options.PPID, '_basicSPM']);
options.paths.analysis_directories.model = fullfile(options.paths.analysis_directories.root, ['PBIHB_', options.PPID, '_model']);
options.paths.analysis_directories.modelFC = fullfile(options.paths.analysis_directories.root, ['PBIHB_', options.PPID, '_modelFC_']);
options.paths.analysis_directories.modelPred = fullfile(options.paths.analysis_directories.root, ['PBIHB_', options.PPID, '_modelPred']);
options.paths.analysis_directories.modelErr = fullfile(options.paths.analysis_directories.root, ['PBIHB_', options.PPID, '_modelErr']);
options.paths.analysis_directories.modelFactors = fullfile(options.paths.analysis_directories.root, ['PBIHB_', options.PPID, '_modelFactors']);
options.paths.bash_directory = fullfile(options.paths.preprocessing_directory, ['PBIHB_', options.PPID, '_bashScripts']);
options.paths.skullStrip_directory = fullfile(options.paths.preprocessing_directory, ['PBIHB_', options.PPID, '_skullStrip']);

% Specify images
options.names.images.task_EPI = ['PBIHB_', options.PPID, '_task.nii']; % Generic name structure of the limited FOV functional scan, without volume specified
options.names.images.task_EPI_std = ['PBIHB_', options.PPID, '_task_std.nii']; % Name for registered EPI image
options.names.images.task_EPI_stdSm = ['sPBIHB_', options.PPID, '_task_std.nii']; % Name for registered EPI image
options.names.images.task_wbEPI = ['PBIHB_', options.PPID, '_task_wholebrain.nii']; % Whole-brain EPI, matched to functional EPI (partial FOV)
options.names.images.task_fieldmap = ['PBIHB_', options.PPID, '_task_fieldmap.nii']; % Fieldmap image provided by scanner
options.names.images.task_fieldmap_mag = ['PBIHB_', options.PPID, '_task_fieldmap_mag.nii']; % Fieldmap magnitude image provided by scanner
options.names.images.rest_EPI = ['PBIHB_', options.PPID, '_rest.nii']; % Generic name structure of the limited FOV functional scan, without volume specified
options.names.images.rest_EPI_std = ['PBIHB_', options.PPID, '_rest_std.nii']; % Name for registered EPI image
options.names.images.rest_wbEPI = ['PBIHB_', options.PPID, '_rest_wholebrain.nii']; % Whole-brain EPI, matched to functional EPI (partial FOV)
options.names.images.rest_fieldmap = ['PBIHB_', options.PPID, '_rest_fieldmap.nii']; % Fieldmap image provided by scanner
options.names.images.rest_fieldmap_mag = ['PBIHB_', options.PPID, '_rest_fieldmap_mag.nii']; % Fieldmap magnitude image provided by scanner
options.names.images.struct_orig = ['PBIHB_', options.PPID, '_struct_orig.nii']; % Structural T1 scan
options.names.images.struct = ['PBIHB_', options.PPID, '_struct.nii.gz']; % Structural T1 scan
options.names.images.struct_brain = ['PBIHB_', options.PPID, '_struct_brain.nii.gz']; % Structural T1 scan

% Specify physiology data files
options.names.labChartData.task = ['PBIHB_', options.PPID, '_task_preproc_data.txt'];
options.names.labChartComments.task = ['PBIHB_', options.PPID, '_task_preproc_comments.txt'];
options.names.labChartData.rest = ['PBIHB_', options.PPID, '_rest_preproc_data.txt'];
options.names.labChartComments.rest = ['PBIHB_', options.PPID, '_rest_preproc_comments.txt'];

% Specify behavioural task data file
options.names.taskData = ['PBIHB_', options.PPID, '_task_behavior.mat'];

% Specify scanner log files
options.names.scannerLogs.task = ['PBIHB_', options.PPID, '_scanner_task.log'];
options.names.scannerLogs.rest = ['PBIHB_', options.PPID, '_scanner_rest.log'];

% Specify new scanner log files
options.names.scannerLogsNew.task = ['PBIHB_', options.PPID, '_scanner_task.tsv'];
options.names.scannerLogsNew.rest = ['PBIHB_', options.PPID, '_scanner_rest.tsv'];

% Specify the templates for FSL preprocessing
options.names.fslPreprocessing.task = fullfile(options.paths.fsl_templates, 'pbihb_FSLpreproc_template_task.fsf');
options.names.fslPreprocessing.rest = fullfile(options.paths.fsl_templates, 'pbihb_FSLpreproc_template_rest.fsf');
options.names.fslPreprocessing.stdMask = fullfile(options.paths.fsl_templates, 'MNI152_T1_1mm_brain_mask_dil_ero.nii.gz');

% Specify the weighting mask for registration
options.names.fslWeightingMask = fullfile(options.paths.fsl_templates, 'PBIHB_weighting_mask.nii.gz');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY NECESSARY SAVE NAMES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify skull strip registration save names
options.saveNames.fslPreprocessing.struct.bashScript = fullfile(options.paths.bash_directory, ['PBIHB_', options.PPID, '_skullStrip.sh']);
options.saveNames.fslPreprocessing.struct.biasCorrected1 = fullfile(options.paths.skullStrip_directory, ['PBIHB_', options.PPID, '_struct_biasCorr.nii.gz']);
options.saveNames.fslPreprocessing.struct.biasCorrected2 = fullfile(options.paths.skullStrip_directory, ['PBIHB_', options.PPID, '_struct_biasCorr_restore.nii.gz']);
options.saveNames.fslPreprocessing.struct.highres2standardImageAff = fullfile(options.paths.skullStrip_directory, ['PBIHB_', options.PPID, '_highres2standardAff.nii.gz']);
options.saveNames.fslPreprocessing.struct.highres2standardMatrixAff = fullfile(options.paths.skullStrip_directory, ['PBIHB_', options.PPID, '_highres2standardAff.mat']);
options.saveNames.fslPreprocessing.struct.standard2highresMatrixAff = fullfile(options.paths.skullStrip_directory, ['PBIHB_', options.PPID, '_standard2highresAff.mat']);
options.saveNames.fslPreprocessing.struct.standard2highresMaskAff = fullfile(options.paths.skullStrip_directory, ['PBIHB_', options.PPID, '_standard2highresMask.nii.gz']);

% Specify (created) FSL preprocessing files
options.saveNames.fslPreprocessing.task.bashScript = fullfile(options.paths.bash_directory, ['PBIHB_', options.PPID, '_setFslPreprocessingBash_task.sh']);
options.saveNames.fslPreprocessing.rest.bashScript = fullfile(options.paths.bash_directory, ['PBIHB_', options.PPID, '_setFslPreprocessingBash_rest.sh']);
options.saveNames.fslPreprocessing.task.design = fullfile(options.paths.bash_directory, ['PBIHB_', options.PPID, '_preproc_FSL_task.fsf']);
options.saveNames.fslPreprocessing.rest.design = fullfile(options.paths.bash_directory, ['PBIHB_', options.PPID, '_preproc_FSL_rest.fsf']);
options.saveNames.fslPreprocessing.task.directory = fullfile(options.paths.preprocessing_directory, ['PBIHB_', options.PPID, '_preproc_FSL_task.feat']);
options.saveNames.fslPreprocessing.rest.directory = fullfile(options.paths.preprocessing_directory, ['PBIHB_', options.PPID, '_preproc_FSL_rest.feat']);

% Specify (created) motion parameter files
options.saveNames.motion.task_orig = fullfile(options.saveNames.fslPreprocessing.task.directory, 'mc', 'prefiltered_func_data_mcf.par');
options.saveNames.motion.task_txt = fullfile(options.saveNames.fslPreprocessing.task.directory, 'mc', 'prefiltered_func_data_mcf.txt');
options.saveNames.motion.rest_orig = fullfile(options.saveNames.fslPreprocessing.rest.directory, 'mc', 'prefiltered_func_data_mcf.par');
options.saveNames.motion.rest_txt = fullfile(options.saveNames.fslPreprocessing.rest.directory, 'mc', 'prefiltered_func_data_mcf.txt');

% Specify (created) physiology figure files
options.saveNames.physioFigOutput.task = fullfile(options.paths.physio_directory, ['PBIHB_', options.PPID, '_physio_output_task.png']);
options.saveNames.physioFigOutput.rest = fullfile(options.paths.physio_directory, ['PBIHB_', options.PPID, '_physio_output_rest.png']);

% Specify (created) physIO file
options.saveNames.physio.task = fullfile(options.paths.preprocessing_directory, ['PBIHB_', options.PPID, '_physio'], ['PBIHB_', options.PPID, '_physio_task.mat']);
options.saveNames.physio.rest = fullfile(options.paths.preprocessing_directory, ['PBIHB_', options.PPID, '_physio'], ['PBIHB_', options.PPID, '_physio_rest.mat']);

% Specify (created) physio.mat files
options.saveNames.physioRegressors.task = fullfile(options.paths.physio_directory, ['PBIHB_', options.PPID, '_physio_regressors_task.txt']);
options.saveNames.physioRegressors.rest = fullfile(options.paths.physio_directory, ['PBIHB_', options.PPID, '_physio_regressors_rest.txt']);

% Specify (created) ICA noise files
options.saveNames.fslPreprocessing.task.noiseFile = fullfile(options.saveNames.fslPreprocessing.task.directory, ['PBIHB_', options.PPID, '_icaNoiseFile_task.txt']);
options.saveNames.fslPreprocessing.rest.noiseFile = fullfile(options.saveNames.fslPreprocessing.rest.directory, ['PBIHB_', options.PPID, '_icaNoiseFile_rest.txt']);

% Specify original ICA noise files
options.saveNames.fslPreprocessing.task.noiseFile_orig = fullfile(options.paths.noise_directory, ['PBIHB_', options.PPID, '_icaNoiseFile_task.txt']);
options.saveNames.fslPreprocessing.rest.noiseFile_orig = fullfile(options.paths.noise_directory, ['PBIHB_', options.PPID, '_icaNoiseFile_rest.txt']);

% Specify save names for specific output files
options.saveNames.physTaskTraces = fullfile(options.paths.physiology_directory, ['PBIHB_', options.PPID, '_physTaskTraces.mat']);
options.saveNames.physTaskValues = fullfile(options.paths.physiology_directory, ['PBIHB_', options.PPID, '_physTaskValues.mat']);
options.saveNames.physRest = fullfile(options.paths.physiology_directory, ['PBIHB_', options.PPID, '_physRest.mat']);
options.saveNames.correlations = fullfile(options.paths.physiology_directory, ['PBIHB_', options.PPID, '_physiology_correlations']);
options.saveNames.motionVsPressure = fullfile(options.paths.physiology_directory, ['PBIHB_', options.PPID, '_motionVsPressure_traces']);
options.saveNames.glmBasicInfo = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_basicInfo.mat']);
options.saveNames.glmBasicSpmMatrix = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_basicSpmMatrix.mat']);
options.saveNames.glmBasicMatrix = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_basicMatrix.mat']);
options.saveNames.glmBasicFig = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_basic_figure']);
options.saveNames.glmModelInfo = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_modelInfo.mat']);
options.saveNames.glmModelMatrix = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_modelMatrix.mat']);
options.saveNames.glmModelMatrixFC = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_modelFCMatrix_']);
options.saveNames.glmModelPredMatrix = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_modelPredMatrix.mat']);
options.saveNames.glmModelErrMatrix = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_modelErrMatrix.mat']);
options.saveNames.glmModelFig = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_model_figure']);
options.saveNames.glmModelFCFig = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_FCtimeseries_figure_']);
options.saveNames.glmModelPredFig = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_modelPred_figure']);
options.saveNames.glmModelErrFig = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_modelErr_figure']);
options.saveNames.glmModelFactorsMatrix = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_modelFactors.mat']);
options.saveNames.glmModelFactorsFig = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_modelFactors_figure']);
options.saveNames.glmNoise = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_noise.txt']);
options.saveNames.glmNoiseInfo = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_glm_noiseInfo.mat']);
options.saveNames.models.RW = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_fit_RW']);
options.saveNames.models.correlations = fullfile(options.paths.glm_directory, ['PBIHB_', options.PPID, '_correlations']);
options.saveNames.analysis.basic = fullfile(options.paths.analysis_directories.basic, ['PBIHB_', options.PPID, '_analysis_basic.mat']);
options.saveNames.analysis.basicSpm = fullfile(options.paths.analysis_directories.basicSpm, ['PBIHB_', options.PPID, '_analysis_basicSPM.mat']);
options.saveNames.analysis.model = fullfile(options.paths.analysis_directories.model, ['PBIHB_', options.PPID, '_analysis_model.mat']);
options.saveNames.analysis.modelPred = fullfile(options.paths.analysis_directories.modelPred, ['PBIHB_', options.PPID, '_analysis_modelPred.mat']);
options.saveNames.analysis.modelErr = fullfile(options.paths.analysis_directories.modelErr, ['PBIHB_', options.PPID, '_analysis_modelErr.mat']);
options.saveNames.analysis.modelFactors = fullfile(options.paths.analysis_directories.modelFactors, ['PBIHB_', options.PPID, '_analysis_modelFactors.mat']);

% Specify save names for timeseries extraction information
options.saveNames.timeSeries = fullfile(options.paths.timeseries_directory, ['PBIHB_', options.PPID, '_timeseriesExInfo.mat']);


end