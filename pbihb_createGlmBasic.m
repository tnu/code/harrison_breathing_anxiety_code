%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% BREATHING SCANNER TASK: CREATING BASIC GLM %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 06/02/2019
% -------------------------------------------------------------------------
% TO RUN:   glmBasic    = pbihb_createGlmBasic(options, physValues)
% INPUTS:   options     = PPID-specific matrix output from pbihb_setOptions
%           physValues  = Structure with physiological traces from the
%                         learning task, from pbihb_physiology function
% OUTPUTS:  glmBasic    = Structure with glm matrices added
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script creates a basic task design matrix for input into SPM. This
% design includes:
%   1) A regressor for the presentation of each of the cues (block)
%   2) A regressor for when the breathing resistance was applied (block)
%   3) A regressor for when the breathing resistance wasn't applied (block)
%   4) A noise regressor for when the ratings were being made (block)
%   5) A noise regressor for when the prediction occured (event)
%   6) A modulatory regressor for (2) specifying surprise
%   7) A modulatory regressor for (2) using the demeaned rating scores
%   8) A modulatory regressor for (3) specifying surprise
%   9) A modulatory regressor for (3) using the demeaned rating scores
%   10+) A noise matrix containing the end-tidal CO2 values (per volume)
%      that are convolved with the SPM basis functions, plus (if specified)
%      the physio noise matrix and ICA noise components
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function glmBasic = pbihb_createGlmBasic(options, physValues)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY OPTIONS AND LOAD REQUIRED DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

glmBasic.options = options;
behaviour = load(fullfile(options.paths.task_directory, options.names.taskData));

% Check for existance of physValues variable
if isempty(physValues) == 1
    if isfile(options.saveNames.physTaskValues) == 1
        load(options.saveNames.physTaskValues);
    else
        error('No physValues datafile provided or found... Exiting')
    end        
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY INFO FOR CUES AND STIMULI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify onsets and durations for the presentation of all cues
glmBasic.cueTotal.onsets = behaviour.data.events.cue_pred_on;
glmBasic.cueTotal.durations = behaviour.data.events.cue_pred_off - behaviour.data.events.cue_pred_on;

% Specify onsets and durations for all stimulus periods together
glmBasic.stimuliTotal.onsets = physValues.values.inspiratoryPressure.allStimuli.onsetTime';
glmBasic.stimuliTotal.durations = physValues.values.inspiratoryPressure.allStimuli.duration';

% Specify onsets, durations and values for stimuli when resistance was applied
glmBasic.stimulus.onsets = physValues.values.inspiratoryPressure.stimulus.onsetTime';
glmBasic.stimulus.durations = physValues.values.inspiratoryPressure.stimulus.duration';
glmBasic.stimulus.values.avgPressure = physValues.values.inspiratoryPressure.stimulus.avgPressure.values';
glmBasic.stimulus.values.maxPressure = physValues.values.inspiratoryPressure.stimulus.maxPressure.values';

% Specify onsets and durations for stimuli when no resistance was applied
glmBasic.noStimulus.onsets = physValues.values.inspiratoryPressure.noStimulus.onsetTime';
glmBasic.noStimulus.durations = physValues.values.inspiratoryPressure.noStimulus.duration';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY INFO FOR NOISE VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify onsets and durations for noise of rating (sliding bar)
glmBasic.noiseRating.onsets = behaviour.data.events.rating_on;
glmBasic.noiseRating.durations = behaviour.data.events.rating_off - behaviour.data.events.rating_on;

% Specify onsets and durations for noise of prediction (button press)
glmBasic.noisePredict.onsets = behaviour.data.events.cue_pred_on(~isnan(behaviour.data.pred_rt)) + behaviour.data.pred_rt(~isnan(behaviour.data.pred_rt));
glmBasic.noisePredict.durations = 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY INFO FOR RATINGS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify onsets, durations and values for ratings applied to stimuli (all)
glmBasic.ratingTotal.onsets = physValues.values.inspiratoryPressure.allStimuli.onsetTime;
glmBasic.ratingTotal.durations = physValues.values.inspiratoryPressure.allStimuli.duration;
glmBasic.ratingTotal.values.raw = behaviour.data.rate_answer(1:80);
glmBasic.ratingTotal.values.demeaned = glmBasic.ratingTotal.values.raw - mean(glmBasic.ratingTotal.values.raw);
glmBasic.ratingTotal.values.zscored = zscore(glmBasic.ratingTotal.values.raw);

% Specify onsets, durations and values for ratings applied to resistance stimuli
glmBasic.ratingStimulus.onsets = physValues.values.inspiratoryPressure.stimulus.onsetTime;
glmBasic.ratingStimulus.durations = physValues.values.inspiratoryPressure.stimulus.duration;
glmBasic.ratingStimulus.values.raw = behaviour.data.rate_answer(behaviour.params.resist == 1);
glmBasic.ratingStimulus.values.demeaned = glmBasic.ratingStimulus.values.raw - nanmean(glmBasic.ratingStimulus.values.raw);
glmBasic.ratingStimulus.values.demeaned(isnan(glmBasic.ratingStimulus.values.demeaned)) = 0;
glmBasic.ratingStimulus.values.zscored = zscore(glmBasic.ratingStimulus.values.raw);

% Specify onsets, durations and values for ratings applied to no resistance stimuli
glmBasic.ratingNoStimulus.onsets = physValues.values.inspiratoryPressure.noStimulus.onsetTime;
glmBasic.ratingNoStimulus.durations = physValues.values.inspiratoryPressure.noStimulus.duration;
glmBasic.ratingNoStimulus.values.raw = behaviour.data.rate_answer(behaviour.params.resist == 0);
glmBasic.ratingNoStimulus.values.demeaned = glmBasic.ratingNoStimulus.values.raw - nanmean(glmBasic.ratingNoStimulus.values.raw);
glmBasic.ratingNoStimulus.values.demeaned(isnan(glmBasic.ratingNoStimulus.values.demeaned)) = 0;
glmBasic.ratingNoStimulus.values.zscored = zscore(glmBasic.ratingNoStimulus.values.raw);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY INFO FOR SURPRISE REGRESSORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nStim = 0;
nNoStim = 0;
for a = 1:length(behaviour.data.pred_answer)
    if behaviour.params.resist(a) == 1
        nStim = nStim + 1;
        if behaviour.data.pred_answer(a) == 0
            glmBasic.surprise.stimulus.values(nStim) = 1;
        else
            glmBasic.surprise.stimulus.values(nStim) = 0;
        end
    else
        nNoStim = nNoStim + 1;
        if behaviour.data.pred_answer(a) == 0
            glmBasic.surprise.noStimulus.values(nNoStim) = 0;
        else
            glmBasic.surprise.noStimulus.values(nNoStim) = 1;
        end        
    end
end

% Demean surprise regressors in case needed
glmBasic.surprise.stimulus.valuesDemeaned = glmBasic.surprise.stimulus.values - mean(glmBasic.surprise.stimulus.values);
glmBasic.surprise.noStimulus.valuesDemeaned = glmBasic.surprise.noStimulus.values - mean(glmBasic.surprise.noStimulus.values);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE DESIGN MATRIX FOR BASIC TASK DESIGN -- SPM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

basicSPM.names = {'cueYes', 'cueNo', 'resistanceBlock', 'noResistanceBlock', 'noiseRating', 'noisePredict'}; % Assign names in order for GLM
basicSPM.extraContrasts.names = {'avgCue', 'cueYesNoDiff', 'surpriseAvg', 'surpriseDiff'};
basicSPM.extraContrasts.weights{1} = [1 0 0 1 0 0]; % Remember to account for derivatives and modulators here
basicSPM.extraContrasts.weights{2} = [1 0 0 -1 0 0]; % Remember to account for derivatives and modulators here
basicSPM.extraContrasts.weights{3} = [0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 0 0]; % Remember to account for derivatives and modulators here
basicSPM.extraContrasts.weights{4} = [0 0 0 0 0 0 0 0 0 -1 0 0 0 0 0 1 0 0]; % Remember to account for derivatives and modulators here
basicSPM.onsets = {glmBasic.cueTotal.onsets(behaviour.data.pred_answer == 1), glmBasic.cueTotal.onsets(behaviour.data.pred_answer == 0), glmBasic.stimulus.onsets, glmBasic.noStimulus.onsets, glmBasic.noiseRating.onsets, glmBasic.noisePredict.onsets};
basicSPM.durations = {glmBasic.cueTotal.durations(behaviour.data.pred_answer == 1), glmBasic.cueTotal.durations(behaviour.data.pred_answer == 0), glmBasic.stimulus.durations, glmBasic.noStimulus.durations, glmBasic.noiseRating.durations, glmBasic.noisePredict.durations};
basicSPM.numMod = 2;
basicSPM.pmod(1).name = [];
basicSPM.pmod(2).name = [];
basicSPM.pmod(3).name{1} = 'resistanceSurprise';
basicSPM.pmod(3).param{1} = glmBasic.surprise.stimulus.values;
basicSPM.pmod(3).poly{1} = 1;
basicSPM.pmod(4).name{1} = 'noResistanceSurprise';
basicSPM.pmod(4).param{1} = glmBasic.surprise.noStimulus.values;
basicSPM.pmod(4).poly{1} = 1;
% if nansum(glmBasic.ratingStimulus.values.demeaned) ~= 0
%     basicSPM.pmod(3).name{2} = 'resistanceRating';
%     basicSPM.pmod(3).param{2} = glmBasic.ratingStimulus.values.demeaned;
%     basicSPM.pmod(3).poly{2} = 1;
%     basicSPM.numMod = basicSPM.numMod + 1;
% end
% if nansum(glmBasic.ratingNoStimulus.values.demeaned) ~= 0
%     basicSPM.pmod(4).name{2} = 'noResistanceRating';
%     basicSPM.pmod(4).param{2} = glmBasic.ratingNoStimulus.values.demeaned;
%     basicSPM.pmod(4).poly{2} = 1;
%     basicSPM.numMod = basicSPM.numMod + 1;
% end
basicSPM.pmod(5).name = [];
basicSPM.pmod(6).name = [];

glmBasic.designs.basicSPM = basicSPM;
save(glmBasic.options.saveNames.glmBasicSpmMatrix, '-struct', 'basicSPM');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE MANUAL TASK TRACES (PSEUDO SAMPLING = 1000 Hz)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create base task regressors
glmBasic.convolution.baseRegressor = zeros(1,(round(glmBasic.options.taskScan.tr * glmBasic.options.taskScan.vols) * 1000))';
glmBasic.convolution.cue.yes.toConvolve.u = glmBasic.convolution.baseRegressor;
glmBasic.convolution.cue.no.toConvolve.u = glmBasic.convolution.baseRegressor;
glmBasic.convolution.stim.stimulus.toConvolve.u = glmBasic.convolution.baseRegressor;
glmBasic.convolution.stim.noStimulus.toConvolve.u = glmBasic.convolution.baseRegressor;
glmBasic.convolution.stim.stimSurp.toConvolve.u = glmBasic.convolution.baseRegressor;
glmBasic.convolution.stim.noStimSurp.toConvolve.u = glmBasic.convolution.baseRegressor;
glmBasic.convolution.noise.predict.toConvolve.u = glmBasic.convolution.baseRegressor;
glmBasic.convolution.noise.rating.toConvolve.u = glmBasic.convolution.baseRegressor;

% Add in values for cue yes
cueYes.onsets = glmBasic.cueTotal.onsets(behaviour.data.pred_answer == 1);
cueYes.durations = glmBasic.cueTotal.durations(behaviour.data.pred_answer == 1);
for a = 1:length(cueYes.onsets)
    b = round(cueYes.onsets(a) * 1000);
    c = b + round(cueYes.durations(a) * 1000);
    glmBasic.convolution.cue.yes.toConvolve.u(b:c) = 1;
end

% Add in values for cue no
cueNo.onsets = glmBasic.cueTotal.onsets(behaviour.data.pred_answer == 0);
cueNo.durations = glmBasic.cueTotal.durations(behaviour.data.pred_answer == 0);
for a = 1:length(cueNo.onsets)
    b = round(cueNo.onsets(a) * 1000);
    c = b + round(cueNo.durations(a) * 1000);
    glmBasic.convolution.cue.no.toConvolve.u(b:c) = 1;
end

% Add in values for resistance (stimulus)
for a = 1:length(glmBasic.stimulus.onsets)
    b = round(glmBasic.stimulus.onsets(a) * 1000);
    c = b + round(glmBasic.stimulus.durations(a) * 1000);
    glmBasic.convolution.stim.stimulus.toConvolve.u(b:c) = 1;
end

% Add in values for no resistance (no stimulus)
for a = 1:length(glmBasic.noStimulus.onsets)
    b = round(glmBasic.noStimulus.onsets(a) * 1000);
    c = b + round(glmBasic.noStimulus.durations(a) * 1000);
    glmBasic.convolution.stim.noStimulus.toConvolve.u(b:c) = 1;
end

% Add in values for resistance (stimulus) surprise
for a = 1:length(glmBasic.stimulus.onsets)
    b = round(glmBasic.stimulus.onsets(a) * 1000);
    c = b + (1 * 500);
    glmBasic.convolution.stim.stimSurp.toConvolve.u(b:c) = glmBasic.surprise.stimulus.values(a);
end

% Add in values for no resistance (no stimulus) surprise
for a = 1:length(glmBasic.noStimulus.onsets)
    b = round(glmBasic.noStimulus.onsets(a) * 1000);
    c = b + (1 * 500);
    glmBasic.convolution.stim.noStimSurp.toConvolve.u(b:c) = glmBasic.surprise.noStimulus.values(a);
end

% Add in values for button press prediction noise
for a = 1:length(glmBasic.noisePredict.onsets)
    b = round(glmBasic.noisePredict.onsets(a) * 1000);
    c = b + (1 * 500);
    glmBasic.convolution.noise.predict.toConvolve.u(b:c,1) = 1;
end

% Add in values for rating noise
for a = 1:length(glmBasic.noiseRating.onsets)
    b = round(glmBasic.noiseRating.onsets(a) * 1000);
    c = b + round(glmBasic.noiseRating.durations(a) * 1000);
    glmBasic.convolution.noise.rating.toConvolve.u(b:c,1) = 1;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CONVOLVE TASK TRACES WITH STANDARD HRF AND DERIVATIVES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get basis functions for convolution
glmBasic.convolution.basisFunctions.task.T = 1;
glmBasic.convolution.basisFunctions.task.dt = 0.001; % Specify sampling rate
glmBasic.convolution.basisFunctions.task.name = 'hrf (with time and dispersion derivatives)';
glmBasic.convolution.basisFunctions.task.length = 32.2;
glmBasic.convolution.basisFunctions.task.order = 3;
[glmBasic.convolution.basisFunctions.task] = spm_get_bf(glmBasic.convolution.basisFunctions.task);

% Convolve cue yes regressor with basis functions
glmBasic.convolution.cue.yes.toConvolve.name = {'cueYes'};
glmBasic.convolution.cue.yes.convolved = spm_Volterra(glmBasic.convolution.cue.yes.toConvolve, glmBasic.convolution.basisFunctions.task.bf);
glmBasic.convolution.cue.yes.convolvedTR = downsample(glmBasic.convolution.cue.yes.convolved, round(glmBasic.options.taskScan.tr * 1000), round(glmBasic.options.taskScan.tr * 1000 / 2)); 
glmBasic.convolution.cue.yes.convolvedTR = glmBasic.convolution.cue.yes.convolvedTR(1:glmBasic.options.taskScan.vols,:);

% Convolve cue no regressor with basis functions
glmBasic.convolution.cue.no.toConvolve.name = {'cueNo'};
glmBasic.convolution.cue.no.convolved = spm_Volterra(glmBasic.convolution.cue.no.toConvolve, glmBasic.convolution.basisFunctions.task.bf);
glmBasic.convolution.cue.no.convolvedTR = downsample(glmBasic.convolution.cue.no.convolved, round(glmBasic.options.taskScan.tr * 1000), round(glmBasic.options.taskScan.tr * 1000 / 2)); 
glmBasic.convolution.cue.no.convolvedTR = glmBasic.convolution.cue.no.convolvedTR(1:glmBasic.options.taskScan.vols,:);

% Convolve stimulus (resistance) regressor with basis functions
glmBasic.convolution.stim.stimulus.toConvolve.name = {'resist'};
glmBasic.convolution.stim.stimulus.convolved = spm_Volterra(glmBasic.convolution.stim.stimulus.toConvolve, glmBasic.convolution.basisFunctions.task.bf);
glmBasic.convolution.stim.stimulus.convolvedTR = downsample(glmBasic.convolution.stim.stimulus.convolved, round(glmBasic.options.taskScan.tr * 1000), round(glmBasic.options.taskScan.tr * 1000 / 2)); 
glmBasic.convolution.stim.stimulus.convolvedTR = glmBasic.convolution.stim.stimulus.convolvedTR(1:glmBasic.options.taskScan.vols,:);

% Convolve no stimulus (no resistance) regressor with basis functions
glmBasic.convolution.stim.noStimulus.toConvolve.name = {'noResist'};
glmBasic.convolution.stim.noStimulus.convolved = spm_Volterra(glmBasic.convolution.stim.noStimulus.toConvolve, glmBasic.convolution.basisFunctions.task.bf);
glmBasic.convolution.stim.noStimulus.convolvedTR = downsample(glmBasic.convolution.stim.noStimulus.convolved, round(glmBasic.options.taskScan.tr * 1000), round(glmBasic.options.taskScan.tr * 1000 / 2)); 
glmBasic.convolution.stim.noStimulus.convolvedTR = glmBasic.convolution.stim.noStimulus.convolvedTR(1:glmBasic.options.taskScan.vols,:);

% Convolve stimulus (resistance) surprise regressor with basis functions
glmBasic.convolution.stim.stimSurp.toConvolve.name = {'resistSurp'};
glmBasic.convolution.stim.stimSurp.convolved = spm_Volterra(glmBasic.convolution.stim.stimSurp.toConvolve, glmBasic.convolution.basisFunctions.task.bf);
glmBasic.convolution.stim.stimSurp.convolvedTR = downsample(glmBasic.convolution.stim.stimSurp.convolved, round(glmBasic.options.taskScan.tr * 1000), round(glmBasic.options.taskScan.tr * 1000 / 2)); 
glmBasic.convolution.stim.stimSurp.convolvedTR = glmBasic.convolution.stim.stimSurp.convolvedTR(1:glmBasic.options.taskScan.vols,:);

% Convolve no stimulus (no resistance) surprise regressor with basis functions
glmBasic.convolution.stim.noStimSurp.toConvolve.name = {'noResistSurp'};
glmBasic.convolution.stim.noStimSurp.convolved = spm_Volterra(glmBasic.convolution.stim.noStimSurp.toConvolve, glmBasic.convolution.basisFunctions.task.bf);
glmBasic.convolution.stim.noStimSurp.convolvedTR = downsample(glmBasic.convolution.stim.noStimSurp.convolved, round(glmBasic.options.taskScan.tr * 1000), round(glmBasic.options.taskScan.tr * 1000 / 2)); 
glmBasic.convolution.stim.noStimSurp.convolvedTR = glmBasic.convolution.stim.noStimSurp.convolvedTR(1:glmBasic.options.taskScan.vols,:);

% Convolve prediction noise regressor with basis functions
glmBasic.convolution.noise.predict.toConvolve.name = {'noisePredict'}; 
glmBasic.convolution.noise.predict.convolved = spm_Volterra(glmBasic.convolution.noise.predict.toConvolve, glmBasic.convolution.basisFunctions.task.bf);
glmBasic.convolution.noise.predict.convolvedTR = downsample(glmBasic.convolution.noise.predict.convolved, round(glmBasic.options.taskScan.tr * 1000), round(glmBasic.options.taskScan.tr * 1000 / 2)); 
glmBasic.convolution.noise.predict.convolvedTR = glmBasic.convolution.noise.predict.convolvedTR(1:glmBasic.options.taskScan.vols,:);

% Convolve rating noise regressor with basis functions
glmBasic.convolution.noise.rating.toConvolve.name = {'noiseRating'}; 
glmBasic.convolution.noise.rating.convolved = spm_Volterra(glmBasic.convolution.noise.rating.toConvolve, glmBasic.convolution.basisFunctions.task.bf);
glmBasic.convolution.noise.rating.convolvedTR = downsample(glmBasic.convolution.noise.rating.convolved, round(glmBasic.options.taskScan.tr * 1000), round(glmBasic.options.taskScan.tr * 1000 / 2)); 
glmBasic.convolution.noise.rating.convolvedTR = glmBasic.convolution.noise.rating.convolvedTR(1:glmBasic.options.taskScan.vols,:);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD NOISE MATRIX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isfile(glmBasic.options.saveNames.glmNoise)
    noise = load(glmBasic.options.saveNames.glmNoise);
else
    disp('No noise matrix found... No noise regressors added to model GLM')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE DESIGN MATRIX FOR BASIC TASK DESIGN -- SPM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

basic.R = [glmBasic.convolution.cue.yes.convolvedTR glmBasic.convolution.cue.no.convolvedTR glmBasic.convolution.stim.stimulus.convolvedTR glmBasic.convolution.stim.stimSurp.convolvedTR glmBasic.convolution.stim.noStimulus.convolvedTR glmBasic.convolution.stim.noStimSurp.convolvedTR glmBasic.convolution.noise.rating.convolvedTR glmBasic.convolution.noise.predict.convolvedTR noise];
basic.names = {'cueYes', 'cueNo', 'resistanceBlock', 'resistanceSurprise', 'noResistanceBlock', 'noResistanceSurprise', 'noiseRating', 'noisePredict'}; % Assign names in order for GLM
basic.extraContrasts.names = {'avgCue', 'cueYesNoDiff', 'surpriseAvg', 'surpriseDiffPos', 'surpriseDiffNeg'};
basic.extraContrasts.weights{1} = [1 0 0 1 0 0]; % Remember to account for derivatives and modulators here
basic.extraContrasts.weights{2} = [1 0 0 -1 0 0]; % Remember to account for derivatives and modulators here
basic.extraContrasts.weights{3} = [0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 0 0]; % Remember to account for derivatives and modulators here
basic.extraContrasts.weights{4} = [0 0 0 0 0 0 0 0 0 -1 0 0 0 0 0 1 0 0]; % Remember to account for derivatives and modulators here
basic.extraContrasts.weights{5} = [0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 -1 0 0]; % Remember to account for derivatives and modulators here
glmBasic.designs.basic = basic;
save(glmBasic.options.saveNames.glmBasicMatrix, '-struct', 'basic');

% Output Figure (not including noise)
figure()
    set(gcf, 'Position', [0 0 1000 400]);
    ticks = 1:(length(basic.names) * glmBasic.convolution.basisFunctions.task.order);
    a = 0;
    for x = 1:length(basic.names)
        a = a + 1;
        xlabels{a} = basic.names{x};
        for b = 2:glmBasic.convolution.basisFunctions.task.order
            xlabels{a+b} = [];
            a = a + 1;
        end
    end
    subplot(1,2,1)
    imagesc(zscore(basic.R(:,1:(length(basic.names)*glmBasic.convolution.basisFunctions.task.order))), 3*[-1 1]);
    colormap('gray');
    colorbar();
    axis square
    title(['Basic GLM: PPID ', glmBasic.options.PPID], 'FontSize', 20);
    set(gca, 'XTick', ticks, 'XTickLabels', xlabels);
    xtickangle(45);
    subplot(1,2,2)
    imagesc(corrcoef(basic.R(:,1:(length(basic.names)*glmBasic.convolution.basisFunctions.task.order))), [-1 1]);
    colormap('gray');
    colorbar();
    axis square
    title('Correlations', 'FontSize', 20);
    set(gca, 'XTick', ticks, 'XTickLabels', xlabels);
    xtickangle(45);
    set(gca, 'YTick', ticks, 'YTickLabels', xlabels);
    print(glmBasic.options.saveNames.glmBasicFig, '-dtiff');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE DESIGN MATRIX FOR BASIC TASK DESIGN
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save(glmBasic.options.saveNames.glmBasicInfo, 'glmBasic');

end