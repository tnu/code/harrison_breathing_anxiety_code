%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% ANALYSIS OF THE PBIHB FDT TASK %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 30/03/2020
% -------------------------------------------------------------------------
% TO RUN:                 pbihb_FDT_analysis(location)
% INPUTS:   location    = 'local' or 'euler'
% OUTPUTS:              = FDT results from PBIHB study
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script analyses the FDT data and questionnaires collected as part of
% the PBIHB study, according to the specified analysis plan.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function fdt = pbihb_FDT_analysis(location)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set the options
fdt.options = pbihb_setOptions('all', location);

% Note how many confidence ratings were used
fdt.options.settings.confidenceRatings = 10;

% Copy matjags.m required for running HMeta-d on Euler
if strcmp(location, 'euler')
    movefile(fullfile(fdt.options.paths.HMetad, 'Matlab', 'matjags.m'), fullfile(fdt.options.paths.HMetad, 'Matlab', 'matjags_orig.m'))
    copyfile(fullfile(fdt.options.paths.matjagsEuler, 'matjags.m'), fullfile(fdt.options.paths.HMetad, 'Matlab', 'matjags.m'));
elseif isfile(fullfile(fdt.options.paths.HMetad, 'Matlab', 'matjags_orig.m'))
    movefile(fullfile(fdt.options.paths.HMetad, 'Matlab', 'matjags_orig.m'), fullfile(fdt.options.paths.HMetad, 'Matlab', 'matjags.m'))
end

% Check directory existance and make if required
if ~exist(fdt.options.paths.fdt.results, 'dir')
    mkdir(fdt.options.paths.fdt.results);
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD FDT AND QUESTIONNAIRE DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load the FDT data
for a = 1:length(fdt.options.PPIDs.total)
    fdt.data{a} = load(fullfile(fdt.options.paths.root_behavior, ['TNU_PBIHB_', fdt.options.PPIDs.total{a}], 'behavior', ['filter_task_results_', fdt.options.PPIDs.total{a}, '.mat']));
end

% Load and score questionnaires
fdt.quest = pbihb_questScore(location);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN TRIALS2COUNTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Run script for each PPID
for a = 1:length(fdt.data)
    stimID = fdt.data{a}.results.thresholdTrials.filters;
    response = fdt.data{a}.results.thresholdTrials.response;
    rating = fdt.data{a}.results.thresholdTrials.confidence;
    [fdt.trials2counts.total.nR_S1{a}, fdt.trials2counts.total.nR_S2{a}] = trials2counts(stimID, response, rating, fdt.options.settings.confidenceRatings, 0);
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FIT HMETA-D MODEL FOR HIGH AND LOW GROUPS TOGETHER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify parameters
fdt.options.settings.mcmc_params = fit_meta_d_params;
fdt.options.settings.mcmc_params.estimate_dprime = 0;

% Fit the anxiety group models
fdt.modelFits.total = fit_meta_d_mcmc_group(fdt.trials2counts.total.nR_S1, fdt.trials2counts.total.nR_S2, fdt.options.settings.mcmc_params, @normcdf, @norminv, 'PBIHB_FDT');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PULL OUT FDT PARAMETERS FROM MODEL FITS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

low = 0;
mod = 0;
fdt.params.names = {'filterNum', 'c1', 'avgConfidence', 'Mratio'};
fdt.params.extra.names = {'accuracyTotal', 'd1'};
for a = 1:length(fdt.options.PPIDs.total)
    fdt.params.values.total(a,1) = fdt.data{a}.results.thresholdTrials.filterNum;
    fdt.params.values.total(a,2) = fdt.modelFits.total.c1(a);
    fdt.params.values.total(a,3) = mean(fdt.data{a}.results.thresholdTrials.confidence);
    fdt.params.values.total(a,4) = fdt.modelFits.total.Mratio(a);
    fdt.params.extra.values.total(a,1) = fdt.data{a}.results.thresholdTrials.accuracyTotal;
    fdt.params.extra.values.total(a,2) = fdt.modelFits.total.d1(a);
    if ismember(a, fdt.options.PPIDs.low.idx) == 1
        low = low + 1;
        fdt.params.values.low(low,1) = fdt.params.values.total(a,1);
        fdt.params.values.low(low,2) = fdt.params.values.total(a,2);
        fdt.params.values.low(low,3) = fdt.params.values.total(a,3);
        fdt.params.values.low(low,4) = fdt.params.values.total(a,4);
        fdt.params.extra.values.low(low,1) = fdt.params.extra.values.total(a,1);
        fdt.params.extra.values.low(low,2) = fdt.params.extra.values.total(a,2);
    elseif ismember(a, fdt.options.PPIDs.mod.idx) == 1
        mod = mod + 1;
        fdt.params.values.mod(mod,1) = fdt.params.values.total(a,1);
        fdt.params.values.mod(mod,2) = fdt.params.values.total(a,2);
        fdt.params.values.mod(mod,3) = fdt.params.values.total(a,3);
        fdt.params.values.mod(mod,4) = fdt.params.values.total(a,4);
        fdt.params.extra.values.mod(mod,1) = fdt.params.extra.values.total(a,1);
        fdt.params.extra.values.mod(mod,2) = fdt.params.extra.values.total(a,2);
    end
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CALCULATE FDT SUMMARY MEASURES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate n's
nTot = length(fdt.options.PPIDs.total);
nLow = length(fdt.options.PPIDs.low.idx);
nMod = length(fdt.options.PPIDs.mod.idx);

% Calculate group-wise means, standard errors, medians and iqr for main parameters
for a = 1:length(fdt.params.names)
    fdt.params.summary{a}.totalMean = mean(fdt.params.values.total(:,a));
    fdt.params.summary{a}.totalStdE = std(fdt.params.values.total(:,a)) / sqrt(nTot);
    fdt.params.summary{a}.totalMed = median(fdt.params.values.total(:,a));
    fdt.params.summary{a}.totalIqr = iqr(fdt.params.values.total(:,a));
    fdt.params.summary{a}.lowMean = mean(fdt.params.values.low(:,a));
    fdt.params.summary{a}.lowStdE = std(fdt.params.values.low(:,a)) / sqrt(nLow);
    fdt.params.summary{a}.lowMed = median(fdt.params.values.low(:,a));
    fdt.params.summary{a}.lowIqr = iqr(fdt.params.values.low(:,a));
    fdt.params.summary{a}.modMean = mean(fdt.params.values.mod(:,a));
    fdt.params.summary{a}.modStdE = std(fdt.params.values.mod(:,a)) / sqrt(nMod);
    fdt.params.summary{a}.modMed = median(fdt.params.values.mod(:,a));
    fdt.params.summary{a}.modIqr = iqr(fdt.params.values.mod(:,a));
end

% Add fits for group-wise means and standard errors for Mratio
idx = length(fdt.params.names);
fdt.params.summary{idx}.totalMratio_mu = exp(fdt.modelFits.total.mu_logMratio);
fdt.params.summary{idx}.totalMratio_stdE = std(fdt.modelFits.total.mcmc.samples.mu_logMratio(:));

% Calculate group-wise means, standard errors, medians and iqr for extra parameters
for a = 1:length(fdt.params.extra.names)
    fdt.params.extra.summary{a}.totalMean = mean(fdt.params.extra.values.total(:,a));
    fdt.params.extra.summary{a}.totalStdE = std(fdt.params.extra.values.total(:,a)) / sqrt(nTot);
    fdt.params.extra.summary{a}.totalMed = median(fdt.params.extra.values.total(:,a));
    fdt.params.extra.summary{a}.totalIqr = iqr(fdt.params.extra.values.total(:,a));
    fdt.params.extra.summary{a}.lowMean = mean(fdt.params.extra.values.low(:,a));
    fdt.params.extra.summary{a}.lowStdE = std(fdt.params.extra.values.low(:,a)) / sqrt(nLow);
    fdt.params.extra.summary{a}.lowMed = median(fdt.params.extra.values.low(:,a));
    fdt.params.extra.summary{a}.lowIqr = iqr(fdt.params.extra.values.low(:,a));
    fdt.params.extra.summary{a}.modMean = mean(fdt.params.extra.values.mod(:,a));
    fdt.params.extra.summary{a}.modStdE = std(fdt.params.extra.values.mod(:,a)) / sqrt(nMod);
    fdt.params.extra.summary{a}.modMed = median(fdt.params.extra.values.mod(:,a));
    fdt.params.extra.summary{a}.modIqr = iqr(fdt.params.extra.values.mod(:,a));
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMPARE GROUP SCORES FOR FDT PARAMETERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set alphas and tails
fdt.params.comp.groupComp{1}.alpha = 0.013; % Filter number
fdt.params.comp.groupComp{1}.tail = 'left';
fdt.params.comp.groupComp{2}.alpha = 0.013; % C1
fdt.params.comp.groupComp{2}.tail = 'right';
fdt.params.comp.groupComp{3}.alpha = 0.013; % Average confidence
fdt.params.comp.groupComp{3}.tail = 'right';
fdt.params.comp.groupComp{4}.alpha = 0.013; % Mratio
fdt.params.comp.groupComp{4}.tail = 'right';
fdt.params.compExtra.groupComp{1}.alpha = 0.05; % Accuracy
fdt.params.compExtra.groupComp{1}.tail = 'both';
fdt.params.compExtra.groupComp{2}.alpha = 0.05; % d1
fdt.params.compExtra.groupComp{2}.tail = 'both';

% Compare groups for main parameters
for a = 1:length(fdt.params.names)
    fdt.params.comp.adtests{a}.low = adtest(fdt.params.values.low(:,a));
    fdt.params.comp.adtests{a}.mod = adtest(fdt.params.values.mod(:,a));
    if fdt.params.comp.adtests{a}.low == 1 || fdt.params.comp.adtests{a}.mod == 1
        fdt.params.comp.groupComp{a}.test = 'Wcxn_rankSum';
        [fdt.params.comp.groupComp{a}.p, fdt.params.comp.groupComp{a}.h, fdt.params.comp.groupComp{a}.stats] = ranksum(fdt.params.values.low(:,a), fdt.params.values.mod(:,a), 'alpha', fdt.params.comp.groupComp{a}.alpha, 'tail', fdt.params.comp.groupComp{a}.tail);
    else
        fdt.params.comp.groupComp{a}.test = 'tTest2';
        [fdt.params.comp.groupComp{a}.h, fdt.params.comp.groupComp{a}.p, fdt.params.comp.groupComp{a}.ci, fdt.params.comp.groupComp{a}.stats] = ttest2(fdt.params.values.low(:,a), fdt.params.values.mod(:,a), 'Alpha', fdt.params.comp.groupComp{a}.alpha, 'Tail', fdt.params.comp.groupComp{a}.tail);
    end
end

% Compare groups for extra parameters
for a = 1:length(fdt.params.extra.names)
    fdt.params.compExtra.adtests{a}.low = adtest(fdt.params.extra.values.low(:,a));
    fdt.params.compExtra.adtests{a}.mod = adtest(fdt.params.extra.values.mod(:,a));
    if fdt.params.compExtra.adtests{a}.low == 1 || fdt.params.compExtra.adtests{a}.mod == 1
        fdt.params.compExtra.groupComp{a}.test = 'Wcxn_rankSum';
        [fdt.params.compExtra.groupComp{a}.p, fdt.params.compExtra.groupComp{a}.h, fdt.params.compExtra.groupComp{a}.stats] = ranksum(fdt.params.extra.values.low(:,a), fdt.params.extra.values.mod(:,a), 'alpha', fdt.params.compExtra.groupComp{a}.alpha, 'tail', fdt.params.compExtra.groupComp{a}.tail);
    else
        fdt.params.compExtra.groupComp{a}.test = 'tTest2';
        [fdt.params.compExtra.groupComp{a}.h, fdt.params.compExtra.groupComp{a}.p, fdt.params.compExtra.groupComp{a}.ci, fdt.params.compExtra.groupComp{a}.stats] = ttest2(fdt.params.extra.values.low(:,a), fdt.params.extra.values.mod(:,a), 'Alpha', fdt.params.compExtra.groupComp{a}.alpha, 'Tail', fdt.params.compExtra.groupComp{a}.tail);
    end
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CALCULATE QUESTIONNAIRE SUMMARY MEASURES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate group-wise means, standard errors, medians and iqr for total scores
for a = 1:length(fdt.quest.names.total)
    fdt.quest.summary.total{a}.totalMean = mean(fdt.quest.finalScores.total(:,a));
    fdt.quest.summary.total{a}.totalStdE = std(fdt.quest.finalScores.total(:,a)) / sqrt(nTot);
    fdt.quest.summary.total{a}.totalMed = median(fdt.quest.finalScores.total(:,a));
    fdt.quest.summary.total{a}.totalIqr = iqr(fdt.quest.finalScores.total(:,a));
    fdt.quest.summary.total{a}.lowMean = mean(fdt.quest.finalScores.total(fdt.options.PPIDs.low.idx,a));
    fdt.quest.summary.total{a}.lowStdE = std(fdt.quest.finalScores.total(fdt.options.PPIDs.low.idx,a)) / sqrt(nLow);
    fdt.quest.summary.total{a}.lowMed = median(fdt.quest.finalScores.total(fdt.options.PPIDs.low.idx,a));
    fdt.quest.summary.total{a}.lowIqr = iqr(fdt.quest.finalScores.total(fdt.options.PPIDs.low.idx,a));
    fdt.quest.summary.total{a}.modMean = mean(fdt.quest.finalScores.total(fdt.options.PPIDs.mod.idx,a));
    fdt.quest.summary.total{a}.modStdE = std(fdt.quest.finalScores.total(fdt.options.PPIDs.mod.idx,a)) / sqrt(nMod);
    fdt.quest.summary.total{a}.modMed = median(fdt.quest.finalScores.total(fdt.options.PPIDs.mod.idx,a));
    fdt.quest.summary.total{a}.modIqr = iqr(fdt.quest.finalScores.total(fdt.options.PPIDs.mod.idx,a));
end

% Calculate group-wise means, standard errors, medians and iqr for sub-scores
for a = 1:length(fdt.quest.names.subScales)
    fdt.quest.summary.subScales{a}.totalMean = mean(fdt.quest.finalScores.subScales(:,a));
    fdt.quest.summary.subScales{a}.totalStdE = std(fdt.quest.finalScores.subScales(:,a)) / sqrt(nTot);
    fdt.quest.summary.subScales{a}.totalMed = median(fdt.quest.finalScores.subScales(:,a));
    fdt.quest.summary.subScales{a}.totalIqr = iqr(fdt.quest.finalScores.subScales(:,a));
    fdt.quest.summary.subScales{a}.lowMean = mean(fdt.quest.finalScores.subScales(fdt.options.PPIDs.low.idx,a));
    fdt.quest.summary.subScales{a}.lowStdE = std(fdt.quest.finalScores.subScales(fdt.options.PPIDs.low.idx,a)) / sqrt(nLow);
    fdt.quest.summary.subScales{a}.lowMed = median(fdt.quest.finalScores.subScales(fdt.options.PPIDs.low.idx,a));
    fdt.quest.summary.subScales{a}.lowIqr = iqr(fdt.quest.finalScores.subScales(fdt.options.PPIDs.low.idx,a));
    fdt.quest.summary.subScales{a}.modMean = mean(fdt.quest.finalScores.subScales(fdt.options.PPIDs.mod.idx,a));
    fdt.quest.summary.subScales{a}.modStdE = std(fdt.quest.finalScores.subScales(fdt.options.PPIDs.mod.idx,a)) / sqrt(nMod);
    fdt.quest.summary.subScales{a}.modMed = median(fdt.quest.finalScores.subScales(fdt.options.PPIDs.mod.idx,a));
    fdt.quest.summary.subScales{a}.modIqr = iqr(fdt.quest.finalScores.subScales(fdt.options.PPIDs.mod.idx,a));
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMPARE GROUP SCORES FOR QUESTIONNAIRES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set alphas and tails
fdt.quest.comp.alpha = 0.05;
fdt.quest.comp.tail = 'both';

% Compare total scores across groups
for a = 1:length(fdt.quest.names.total)
    fdt.quest.comp.total.adtests{a}.low = adtest(fdt.quest.finalScores.total(fdt.options.PPIDs.low.idx,a));
    fdt.quest.comp.total.adtests{a}.mod = adtest(fdt.quest.finalScores.total(fdt.options.PPIDs.mod.idx,a));
    if fdt.quest.comp.total.adtests{a}.low == 1 || fdt.quest.comp.total.adtests{a}.mod == 1
        fdt.quest.comp.total.groupComp{a}.test = 'Wcxn_rankSum';
        [fdt.quest.comp.total.groupComp{a}.p, fdt.quest.comp.total.groupComp{a}.h, fdt.quest.comp.total.groupComp{a}.stats] = ranksum(fdt.quest.finalScores.total(fdt.options.PPIDs.low.idx,a), fdt.quest.finalScores.total(fdt.options.PPIDs.mod.idx,a), 'alpha', fdt.quest.comp.alpha, 'tail', fdt.quest.comp.tail);
    else
        fdt.quest.comp.total.groupComp{a}.test = 'tTest2';
        [fdt.quest.comp.total.groupComp{a}.h, fdt.quest.comp.total.groupComp{a}.p, fdt.quest.comp.total.groupComp{a}.ci, fdt.quest.comp.total.groupComp{a}.stats] = ttest2(fdt.quest.finalScores.total(fdt.options.PPIDs.low.idx,a), fdt.quest.finalScores.total(fdt.options.PPIDs.mod.idx,a), 'Alpha', fdt.quest.comp.alpha, 'Tail', fdt.quest.comp.tail);
    end
end

% Compare sub-scores across groups
for a = 1:length(fdt.quest.names.subScales)
    fdt.quest.comp.subScales.adtests{a}.low = adtest(fdt.quest.finalScores.subScales(fdt.options.PPIDs.low.idx,a));
    fdt.quest.comp.subScales.adtests{a}.mod = adtest(fdt.quest.finalScores.subScales(fdt.options.PPIDs.mod.idx,a));
    if fdt.quest.comp.subScales.adtests{a}.low == 1 || fdt.quest.comp.subScales.adtests{a}.mod == 1
        fdt.quest.comp.subScales.groupComp{a}.test = 'Wcxn_rankSum';
        [fdt.quest.comp.subScales.groupComp{a}.p, fdt.quest.comp.subScales.groupComp{a}.h, fdt.quest.comp.subScales.groupComp{a}.stats] = ranksum(fdt.quest.finalScores.subScales(fdt.options.PPIDs.low.idx,a), fdt.quest.finalScores.subScales(fdt.options.PPIDs.mod.idx,a), 'alpha', fdt.quest.comp.alpha, 'tail', fdt.quest.comp.tail);
    else
        fdt.quest.comp.subScales.groupComp{a}.test = 'tTest2';
        [fdt.quest.comp.subScales.groupComp{a}.h, fdt.quest.comp.subScales.groupComp{a}.p, fdt.quest.comp.subScales.groupComp{a}.ci, fdt.quest.comp.subScales.groupComp{a}.stats] = ttest2(fdt.quest.finalScores.subScales(fdt.options.PPIDs.low.idx,a), fdt.quest.finalScores.subScales(fdt.options.PPIDs.mod.idx,a), 'Alpha', fdt.quest.comp.alpha, 'Tail', fdt.quest.comp.tail);
    end
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN FULL CORRELATION MATRIX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Flip and find any necessary questionnaires from total scores
fdt.quest.finalScores.toFlipTotal = {'maia', 'cdrisc', 'panasP', 'gss'};
fdt.quest.finalScores.totalZ = zscore(fdt.quest.finalScores.total);
fdt.quest.finalScores.totalZFlipped = fdt.quest.finalScores.totalZ;
idxFlip = find(contains(fdt.quest.names.total, fdt.quest.finalScores.toFlipTotal));
fdt.quest.finalScores.totalZFlipped(:,idxFlip) = -fdt.quest.finalScores.totalZFlipped(:,idxFlip);
fdt.gender.zscored = zscore(fdt.quest.data.extra.raw.gender);

% Flip any necessary questionnaires from sub-scales
fdt.quest.finalScores.toFlipSubscales = {'maiaNoticing', 'maiaNotDistract', 'maiaNotWorrying', ...
    'maiaAttnReg', 'maiaEmotAware', 'maiaSelfReg', 'maiaBodyList', 'maiaTrust'};
fdt.quest.finalScores.subScalesZ = zscore(fdt.quest.finalScores.subScales);
fdt.quest.finalScores.subScalesZFlipped = fdt.quest.finalScores.subScalesZ;
idxFlipSub = find(contains(fdt.quest.names.subScales, fdt.quest.finalScores.toFlipSubscales));
fdt.quest.finalScores.subScalesZFlipped(:,idxFlipSub) = -fdt.quest.finalScores.subScalesZFlipped(:,idxFlipSub);

% Flip any necessary FDT parameters
fdt.params.values.totalFlipped = fdt.params.values.total;
fdt.params.values.totalFlipped(:,2:4) = -fdt.params.values.totalFlipped(:,2:4);

% Create a matrix of all values to use
fdt.corrTotal.names = {fdt.params.names{:}, 'logMratio', 'MaxInsp', fdt.quest.names.total{:}, fdt.quest.names.subScales{:}, 'gender'};
fdt.corrTotal.valuesZ = [zscore(fdt.params.values.totalFlipped), -zscore(log(fdt.params.values.total(:,4))), -zscore(fdt.quest.data.extra.raw.maxInsp)', fdt.quest.finalScores.totalZFlipped, fdt.quest.finalScores.subScalesZFlipped, -fdt.gender.zscored];

% Calculate correlation matrix
[fdt.corrTotal.R, fdt.corrTotal.p] = corrcoef(fdt.corrTotal.valuesZ);

% Create a clustergram
clustergramFDT = clustergram(fdt.corrTotal.R, 'RowLabels', fdt.corrTotal.names, 'ColumnLabels', fdt.corrTotal.names, 'ColumnLabelsRotate', 70, 'Linkage', 'complete', 'Dendrogram', 3, 'Colormap', redbluecmap, 'DisplayRange', 3);


% %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % FIT HMETA-D MODEL FOR REGRESSIONS
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% % Find required covariates
% idx_staiS = find(contains(fdt.quest.names.total, 'staiS'));
% idx_cesd = find(contains(fdt.quest.names.total, 'cesd'));
% fdt.covMatrix.SA = fdt.quest.finalScores.totalZ(:,idx_staiS);
% fdt.gender.intStaiSZ = zscore(fdt.quest.finalScores.total(:,idx_staiS) .* fdt.quest.data.extra.raw.gender);
% fdt.covMatrix.SAD = [fdt.quest.finalScores.totalZ(:,idx_staiS), fdt.quest.finalScores.totalZ(:,idx_cesd)];
% fdt.covMatrix.SAG = [fdt.quest.finalScores.totalZ(:,idx_staiS), fdt.gender.zscored];
% fdt.covMatrix.SAGI = [fdt.quest.finalScores.totalZ(:,idx_staiS), fdt.gender.zscored, fdt.gender.intStaiSZ];
% 
% % Fit the whole-group models
% fdt.modelFits.total_regressSA = fit_meta_d_mcmc_regression(fdt.trials2counts.total.nR_S1, fdt.trials2counts.total.nR_S2, fdt.covMatrix.SA', fdt.options.settings.mcmc_params, @normcdf, @norminv, 'PBIHB_FDT');
% fdt.modelFits.total_regressSAD = fit_meta_d_mcmc_regression(fdt.trials2counts.total.nR_S1, fdt.trials2counts.total.nR_S2, fdt.covMatrix.SAD', fdt.options.settings.mcmc_params, @normcdf, @norminv, 'PBIHB_FDT');
% fdt.modelFits.total_regressSAG = fit_meta_d_mcmc_regression(fdt.trials2counts.total.nR_S1, fdt.trials2counts.total.nR_S2, fdt.covMatrix.SAG', fdt.options.settings.mcmc_params, @normcdf, @norminv, 'PBIHB_FDT');
% fdt.modelFits.total_regressSAGI = fit_meta_d_mcmc_regression(fdt.trials2counts.total.nR_S1, fdt.trials2counts.total.nR_S2, fdt.covMatrix.SAGI', fdt.options.settings.mcmc_params, @normcdf, @norminv, 'PBIHB_FDT');
% 
% % Pull out the regression values: SA model
% fdt.params.regress.SA{4}.coefficients = [fdt.modelFits.total_regressSA.mu_logMratio fdt.modelFits.total_regressSA.mu_beta1];
% fdt.params.regress.SA{4}.HDIinterval = [0 0.987];
% fdt.params.regress.SA{4}.HDIintervalUnc = [0 0.95];
% fdt.params.regress.SA{4}.HDIbeta1_inner = calc_HDI(fdt.modelFits.total_regressSA.mcmc.samples.mu_beta1(:), fdt.params.regress.SA{4}.HDIinterval(2));
% fdt.params.regress.SA{4}.HDIbeta1_outer = calc_HDI(fdt.modelFits.total_regressSA.mcmc.samples.mu_beta1(:), 0.9999);
% fdt.params.regress.SA{4}.HDIbeta1 = [fdt.params.regress.SA{4}.HDIbeta1_outer(1) fdt.params.regress.SA{4}.HDIbeta1_inner(2)];
% fdt.params.regress.SA{4}.HDIbeta1Unc_inner = calc_HDI(fdt.modelFits.total_regressSA.mcmc.samples.mu_beta1(:), fdt.params.regress.SA{4}.HDIintervalUnc(2));
% fdt.params.regress.SA{4}.HDIbeta1Unc = [fdt.params.regress.SA{4}.HDIbeta1_outer(1) fdt.params.regress.SA{4}.HDIbeta1Unc_inner(2)];
% 
% % Pull out the regression values: SAD model
% fdt.params.regress.SAD{4}.coefficients = [fdt.modelFits.total_regressSAD.mu_logMratio fdt.modelFits.total_regressSAD.mu_beta1 fdt.modelFits.total_regressSAD.mu_beta2];
% fdt.params.regress.SAD{4}.HDIinterval = [0 0.987];
% fdt.params.regress.SAD{4}.HDIintervalUnc = [0 0.95];
% fdt.params.regress.SAD{4}.HDIbeta1_inner = calc_HDI(fdt.modelFits.total_regressSAD.mcmc.samples.mu_beta1(:), fdt.params.regress.SAD{4}.HDIinterval(2));
% fdt.params.regress.SAD{4}.HDIbeta1_outer = calc_HDI(fdt.modelFits.total_regressSAD.mcmc.samples.mu_beta1(:), 0.9999);
% fdt.params.regress.SAD{4}.HDIbeta1 = [fdt.params.regress.SAD{4}.HDIbeta1_outer(1) fdt.params.regress.SAD{4}.HDIbeta1_inner(2)];
% fdt.params.regress.SAD{4}.HDIbeta1Unc_inner = calc_HDI(fdt.modelFits.total_regressSAD.mcmc.samples.mu_beta1(:), fdt.params.regress.SAD{4}.HDIintervalUnc(2));
% fdt.params.regress.SAD{4}.HDIbeta1Unc = [fdt.params.regress.SAD{4}.HDIbeta1_outer(1) fdt.params.regress.SAD{4}.HDIbeta1Unc_inner(2)];
% fdt.params.regress.SAD{4}.HDIbeta2_inner = calc_HDI(fdt.modelFits.total_regressSAD.mcmc.samples.mu_beta2(:), fdt.params.regress.SAD{4}.HDIinterval(2));
% fdt.params.regress.SAD{4}.HDIbeta2_outer = calc_HDI(fdt.modelFits.total_regressSAD.mcmc.samples.mu_beta2(:), 0.9999);
% fdt.params.regress.SAD{4}.HDIbeta2 = [fdt.params.regress.SAD{4}.HDIbeta2_outer(1) fdt.params.regress.SAD{4}.HDIbeta2_inner(2)];
% fdt.params.regress.SAD{4}.HDIbeta2Unc_inner = calc_HDI(fdt.modelFits.total_regressSAD.mcmc.samples.mu_beta2(:), fdt.params.regress.SAD{4}.HDIintervalUnc(2));
% fdt.params.regress.SAD{4}.HDIbeta2Unc = [fdt.params.regress.SAD{4}.HDIbeta2_outer(1) fdt.params.regress.SAD{4}.HDIbeta2Unc_inner(2)];
% fdt.params.regress.SAD{4}.HDIbetaDiffUnc = calc_HDI((fdt.modelFits.total_regressSAD.mcmc.samples.mu_beta1(:) - fdt.modelFits.total_regressSAD.mcmc.samples.mu_beta2(:)));
% fdt.params.regress.SAD{4}.HDIbetaAvg_inner = calc_HDI((fdt.modelFits.total_regressSAD.mcmc.samples.mu_beta1(:) + fdt.modelFits.total_regressSAD.mcmc.samples.mu_beta2(:))/2, fdt.params.regress.SAD{4}.HDIinterval(2));
% fdt.params.regress.SAD{4}.HDIbetaAvg_outer = calc_HDI((fdt.modelFits.total_regressSAD.mcmc.samples.mu_beta1(:) + fdt.modelFits.total_regressSAD.mcmc.samples.mu_beta2(:))/2, 0.9999);
% fdt.params.regress.SAD{4}.HDIbetaAvg = [fdt.params.regress.SAD{4}.HDIbetaAvg_outer(1) fdt.params.regress.SAD{4}.HDIbetaAvg_inner(2)];
% fdt.params.regress.SAD{4}.HDIbetaAvgUnc_inner = calc_HDI((fdt.modelFits.total_regressSAD.mcmc.samples.mu_beta1(:) + fdt.modelFits.total_regressSAD.mcmc.samples.mu_beta2(:))/2, fdt.params.regress.SAD{4}.HDIintervalUnc(2));
% fdt.params.regress.SAD{4}.HDIbetaAvgUnc = [fdt.params.regress.SAD{4}.HDIbetaAvg_outer(1) fdt.params.regress.SAD{4}.HDIbetaAvgUnc_inner(2)];
% 
% % Pull out the regression values: SAG model
% fdt.params.regress.SAG{4}.coefficients = [fdt.modelFits.total_regressSAG.mu_logMratio fdt.modelFits.total_regressSAG.mu_beta1 fdt.modelFits.total_regressSAG.mu_beta2];
% fdt.params.regress.SAG{4}.HDIinterval1 = [0 0.987];
% fdt.params.regress.SAG{4}.HDIinterval1Unc = [0 0.95];
% fdt.params.regress.SAG{4}.HDIinterval2 = [0.025 0.975];
% fdt.params.regress.SAG{4}.HDIbeta1_inner = calc_HDI(fdt.modelFits.total_regressSAG.mcmc.samples.mu_beta1(:), fdt.params.regress.SAG{4}.HDIinterval(2));
% fdt.params.regress.SAG{4}.HDIbeta1_outer = calc_HDI(fdt.modelFits.total_regressSAG.mcmc.samples.mu_beta1(:), 0.9999);
% fdt.params.regress.SAG{4}.HDIbeta1 = [fdt.params.regress.SAG{4}.HDIbeta1_outer(1) fdt.params.regress.SAG{4}.HDIbeta1_inner(2)];
% fdt.params.regress.SAG{4}.HDIbeta1Unc_inner = calc_HDI(fdt.modelFits.total_regressSAG.mcmc.samples.mu_beta1(:), fdt.params.regress.SAG{4}.HDIintervalUnc(2));
% fdt.params.regress.SAG{4}.HDIbeta2Unc = [fdt.params.regress.SAG{4}.HDIbeta2_outer(1) fdt.params.regress.SAG{4}.HDIbeta2Unc_inner(2)];
% fdt.params.regress.SAG{4}.HDIbeta2 = calc_HDI(fdt.modelFits.total_regressSAG.mcmc.samples.mu_beta2(:));
% 
% % Pull out the regression values: SAGI model
% fdt.params.regress.SAGI{4}.coefficients = [fdt.modelFits.total_regressSAGI.mu_logMratio fdt.modelFits.total_regressSAGI.mu_beta1 fdt.modelFits.total_regressSAGI.mu_beta2 fdt.modelFits.total_regressSAGI.mu_beta3];
% fdt.params.regress.SAGI{4}.HDIinterval1 = [0 0.987];
% fdt.params.regress.SAGI{4}.HDIinterval1Unc = [0 0.95];
% fdt.params.regress.SAGI{4}.HDIinterval2 = [0.025 0.975];
% fdt.params.regress.SAGI{4}.HDIinterval3 = [0.025 0.975];
% fdt.params.regress.SAGI{4}.HDIbeta1_inner = calc_HDI(fdt.modelFits.total_regressSAGI.mcmc.samples.mu_beta1(:), fdt.params.regress.SAGI{4}.HDIinterval(2));
% fdt.params.regress.SAGI{4}.HDIbeta1_outer = calc_HDI(fdt.modelFits.total_regressSAGI.mcmc.samples.mu_beta1(:), 0.9999);
% fdt.params.regress.SAGI{4}.HDIbeta1 = [fdt.params.regress.SAGI{4}.HDIbeta1_outer(1) fdt.params.regress.SAGI{4}.HDIbeta1_inner(2)];
% fdt.params.regress.SAGI{4}.HDIbeta1Unc_inner = calc_HDI(fdt.modelFits.total_regressSAGI.mcmc.samples.mu_beta1(:), fdt.params.regress.SAGI{4}.HDIintervalUnc(2));
% fdt.params.regress.SAGI{4}.HDIbeta2Unc = [fdt.params.regress.SAGI{4}.HDIbeta2_outer(1) fdt.params.regress.SAGI{4}.HDIbeta2Unc_inner(2)];
% fdt.params.regress.SAGI{4}.HDIbeta2 = calc_HDI(fdt.modelFits.total_regressSAGI.mcmc.samples.mu_beta2(:));
% fdt.params.regress.SAGI{4}.HDIbeta3 = calc_HDI(fdt.modelFits.total_regressSAGI.mcmc.samples.mu_beta3(:));
% 
% 
% %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % FIT OTHER REGRESSION MODELS FOR FDT PARAMS
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% % Fit normal non-hierarchical regressions: filterNum, c1 and avgConfidence
% for a = 1:(length(fdt.params.names) - 1)
%     fdt.params.regress.SA{a}.fit = fitlm(fdt.covMatrix.SA, fdt.params.values.total(:,a));
%     fdt.params.regress.SAD{a}.fit = fitlm(fdt.covMatrix.SAD, fdt.params.values.total(:,a));
%     fdt.params.regress.SAG{a}.fit = fitlm(fdt.covMatrix.SAG, fdt.params.values.total(:,a));
%     fdt.params.regress.SAGI{a}.fit = fitlm(fdt.covMatrix.SAGI, fdt.params.values.total(:,a));
%     fdt.params.regress.SA{a}.coefficients = fdt.params.regress.SA{a}.fit.Coefficients.Estimate(:);
%     fdt.params.regress.SA{a}.pvalues = fdt.params.regress.SA{a}.fit.Coefficients.pValue(:);
%     fdt.params.regress.SAD{a}.coefficients = fdt.params.regress.SAD{a}.fit.Coefficients.Estimate(:);
%     fdt.params.regress.SAD{a}.pvalues = fdt.params.regress.SAD{a}.fit.Coefficients.pValue(:);
%     [fdt.params.regress.SAD{a}.coeffDiffP, fdt.params.regress.SAD{a}.coeffDiffF] = coefTest(fdt.params.regress.SAD{a}.fit,[0 1 -1]);
%     [fdt.params.regress.SAD{a}.coeffBothP, fdt.params.regress.SAD{a}.coeffBothF] = coefTest(fdt.params.regress.SAD{a}.fit,[0 1 1]);
%     fdt.params.regress.SAG{a}.coefficients = fdt.params.regress.SAG{a}.fit.Coefficients.Estimate(:);
%     fdt.params.regress.SAG{a}.pvalues = fdt.params.regress.SAG{a}.fit.Coefficients.pValue(:);
%     fdt.params.regress.SAGI{a}.coefficients = fdt.params.regress.SAGI{a}.fit.Coefficients.Estimate(:);
%     fdt.params.regress.SAGI{a}.pvalues = fdt.params.regress.SAGI{a}.fit.Coefficients.pValue(:);
% end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE RESULTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save(fdt.options.saveNames.fdt, 'fdt');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE FIGURES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Make figures for both FDT and questionnaire results
pbihb_figuresMatlab_barPlotsFDT(fdt.options);


end