%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% PBIHB BREATHING SCANNER TASK: PREPROCESSING & BASIC ANALYSES %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 06/02/2019
% -------------------------------------------------------------------------
% DESCRIPTION:
% This is the master script for the preprocessing and basic analyses for
% the fMRI session of the PBIHB study. PPIDs to analyse need to be
% specified, and the functions can be run either locally or on euler with
% the appropriate calls.
%   The script can be halted between preprocessing and analysis to allow
% for manual data quality checks in the first run. Preprocessing is 
% conducted in FSL, to both allow for accurate registration of the 7T
% functional scans using boundary-based registration, and to run ICA
% decomposition for noise component identification. Single subject GLM
% analysis is conducted in SPM. Additional noise components from ICA
% decomposition need to be identified and can be added as part of the noise
% matrix for single subject analyses.
%   Both a basic analysis (no model) and a standard Rescoral Wagner
% analysis are specified and analysed here. Final analysis may include an
% optimised version of the Rescorla Wagner (or alternative) model.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1) SET UP FOLDERS LOCALLY AND CHECK PHYSIOLOGY FILE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 1.1) Specify PPID to analyse
PPID = '9996';

% 1.2) Set all options for preprocessing (first instance on local computer = default)
options = pbihb_setOptions(PPID, 'local');

% 1.3) Run directory setup function
% --> Only run once, must be within a 'local' preprocessing analysis
pbihb_setUpFolders(options);

% 1.4) Run physiology processing in test mode to check breathing peak detection
pbihb_runPhysiology(preproc);
% NB: Check printed message to validate stimulus detections 


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --> 2) RUN SINGLE SUBJECT ANALYSIS ON EULER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% INSTRUCTIONS: Single subject analysis can be run in two separate steps
% (preprocessing and analysis) or together. However, there are some key
% quality control steps that require manual intervention. Therefore, it is
% recommended to run preprocessing followed by manual checks prior to 
% analysis in the first instance. Both options are given below.

% MANUAL CHECKS REQUIRED FROM PREPROCESSING:
%     - Registration and normalistaion from EPI to structural and standard
%     - PhysIO peak detection
%     - Extent of motion correlation with inspiratory resistances
%     - Detection of noise components from ICA (but can be run without)

% BEFOREHAND: Sync folders with euler (setup and working directories)
! rsync -avuz /Volumes/TNU_liv_data/PBIHB_data_local/PBIHB_setup/fmri/ ofaull@euler.ethz.ch:/cluster/work/tnu/ofaull/PBIHB_setup
% bsub rsync -avuz /cluster/work/tnu/ofaull/PBIHB_setup/ /cluster/work/tnu/ofaull/PBIHB

% 2A) PREPROCESSING:
% --> Run the following command in euler for preprocessing one subject
% bsub -W 10:00 -R "rusage[mem=20000]" -o preproc_PBIHB_{PPID}_euler_task matlab -singleCompThread -r "pbihb_preprocEuler('{PPID}', 'task')"
% --> OR Run the following command in euler for preprocessing multiple subjects (from the main study folder)
% for a in '{PPID}'; do cd TNU_PBIHB_${a}/PBIHB_${a}_preprocessing ; bsub -W 10:00 -R "rusage[mem=20000]" -o preproc_PBIHB_${a}_euler_task matlab -singleCompThread -r "pbihb_preprocEuler('${a}', 'task')" ; cd ../../ ; done

% 2B) ANALYSIS:
% --> Run the following command in euler for analysis of one subject
% bsub -W 10:00 -R "rusage[mem=50000]" -o analysis_PBIHB_{PPID}_euler_task matlab -singleCompThread -r "pbihb_analysisEuler('{PPID}', [3 3 3])"
% --> OR Run the following command in euler for analysis multiple subjects (from the main study folder)
% for a in '{PPID}'; do cd TNU_PBIHB_${a}/PBIHB_${a}_analysis ; bsub -W 10:00 -R "rusage[mem=50000]" -o analysis_PBIHB_${a}_euler_task matlab -singleCompThread -r "pbihb_analysisEuler('${a}', [3 3 3])" ; cd ../../ ; done

% OR 2) PREPROCESSING + ANALYSIS:
% --> Run the following command in euler for preprocessing and analysis of one subject
% bsub -W 10:00 -R "rusage[mem=20000]" -o preproc_PBIHB_{PPID}_euler_task; bsub -W 10:00 -R "rusage[mem=50000]" -o analysis_PBIHB_{PPID}_euler_task matlab -singleCompThread -r "pbihb_analysisEuler('{PPID}', [3 3 3])"
% --> OR Run the following command in euler for analysis multiple subjects (from the main study folder)
% for a in '{PPID}'; do cd TNU_PBIHB_${a}/PBIHB_${a}_preprocessing ; bsub -J jobPreproc_${a} -W 10:00 -R "rusage[mem=20000]" -o preproc_PBIHB_${a}_euler_task matlab -singleCompThread -r "pbihb_preprocEuler('${a}', 'task')" ; cd ../PBIHB_${a}_analysis/ ; bsub -w "done(jobPreproc_${a})" -W 10:00 -R "rusage[mem=50000]" -o analysis_PBIHB_${a}_euler_task matlab -singleCompThread -r "pbihb_analysisEuler('${a}', [3 3 3])" ; cd ../../ ; done


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OR --> 2) RUN SINGLE SUBJECT ANALYSIS LOCALLY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 2A) SINGLE SUBJECT PREPROCESSING

% 2A.1) Specify PPID to analyse
PPID = '0001';
    
% 2A.2) Set all options for preprocessing with specific location
preproc = pbihb_setOptions(PPID, [], 'local');

% 2A.3) Run image preprocessing in fsl
pbihb_fslPreprocessing(preproc, 'task', 'run');
% pbihb_fslPreprocessing(preproc, 'task', 'design');
% NB: Including 'run' instead of 'design' will run FSL from matlab and disable matlab command line until complete
% NB: If 'design' is specified, only the FSL design is created and will need to manually set off FSL outside matlab (and wait for completion before following steps are run)
% NB: NEED TO MANUALLY IDENTIFY NOISE COMPONENTS FROM ICA OUTPUT

% 2A.4) Run preprocessing of physiology data from task scan
if isfile(preproc.options.saveNames.motion.task_txt)
    preproc = pbihb_runPhysiology(preproc, 'full');
    preproc = pbihb_runPhysioToolBox(preproc, 'task');
end


%% 2B) SINGLE SUBJECT ANALYSIS

% 2B.1) Specify PPID to analyse
PPID = '0001';

% 2B.2) Set all options for preprocessing with specific location
preproc = pbihb_setOptions(PPID, [], 'local');

% 2B.3) Check for and load existing preprocessing file
if isfile(preproc.options.saveNames.preproc)
    load(preproc.options.saveNames.preproc)
end
preproc = pbihb_setOptions(PPID, preproc, 'local'); % Set to 'local'

% 2B.4) Register preprocessed task scan to standard space
pbihb_fslReg(preproc, 'task')

% 2B.5) Create GLM for basic analysis, plus additional noise matrices
if isfile(preproc.options.saveNames.fslPreprocessing.task.noiseFile)
    glm = pbihb_createGlmBasic(preproc, 'physio', 'ica');
else
    glm = pbihb_createGlmBasic(preproc, 'physio');
end

% 2B.6) Create a GLM for a Rescorla Wagner analysis
glm = pbihb_createGlmRW(preproc, glm);

% 2B.7) Run SPM basic single subject analysis with specified smoothing
pbihb_spmAnalysis(preproc, glm, [3 3 3], 'basic');

% 2B.8) Run SPM basic single subject analysis with specified smoothing
pbihb_spmAnalysis(preproc, glm, [3 3 3], 'RW');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --> 3) RUN REST SINGLE SUBJECT ANALYSIS ON EULER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 3A) PREPROCESSING:
% --> Run the following command in euler for preprocessing one subject
% bsub -W 10:00 -R "rusage[mem=20000]" -o preproc_PBIHB_{PPID}_euler_rest matlab -singleCompThread -r "pbihb_preprocEuler('{PPID}', 'rest')"
% --> OR Run the following command in euler for preprocessing multiple subjects (from the main study folder)
% for a in '{PPID}'; do cd TNU_PBIHB_${a}/PBIHB_${a}_preprocessing ; bsub -W 10:00 -R "rusage[mem=20000]" -o preproc_PBIHB_${a}_euler_rest matlab -singleCompThread -r "pbihb_preprocEuler('${a}', 'rest')" ; cd ../../ ; done


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OR --> 3) RUN REST SINGLE SUBJECT ANALYSIS LOCALLY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 3A) SINGLE SUBJECT REST PREPROCESSING

% 3A.1) Specify PPID to analyse
PPID = '0001';
    
% 3A.2) Set all options for preprocessing with specific location
preproc = pbihb_setOptions(PPID, [], 'local');

% 3A.3) Check for existing preprocessing.mat file
if isfile(preproc.options.saveNames.preproc)
    load(preproc.options.saveNames.preproc)
end
preproc = pbihb_setOptions(PPID, preproc, 'local'); % Set to 'local'

% 3A.4) Run preprocessing in fsl --> NEED TO MANUALLY IDENTIFY NOISE COMPONENTS FROM ICA OUTPUT
pbihb_fslPreprocessing(preproc, 'rest');

% 3A.5) Run PhysIO for rest, swap breathing belt if specified
preproc = pbihb_runPhysioToolBox(preproc, 'rest');

% --> Regress out noise for preprocessed rest data?


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXTRA: USEFUL COMMANDS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sync folders from local to euler (with deletion)
! rsync -avuz --delete /Volumes/TNU_liv_data/PBIHB_data_local/PBIHB_setup/ ofaull@euler.ethz.ch:/cluster/work/tnu/ofaull/PBIHB_setup/

% Sync folders from euler to local
! rsync -avuz ofaull@euler.ethz.ch:/cluster/work/tnu/ofaull/PBIHB/fmri /Volumes/TNU_liv_data/PBIHB_data_local/PBIHB_working/fmri

% Sync scripts from local to euler
! rsync -avuz --exclude '.git*' /Users/faullol/Documents/just_breathe/zurich/gitTNU/pbihb_scanner_analysis/ ofaull@euler.ethz.ch:/cluster/home/ofaull/PBIHB/pbihb_scanner_analysis


