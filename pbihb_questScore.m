%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% QUESTIONNAIRE SCORING FOR PBIHB %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 30/03/2020
% -------------------------------------------------------------------------
% TO RUN:   questScores = pbihb_questScore(location)
% INPUTS:   location    = 'local' or 'euler'
% OUTPUTS:              = Scored questionnaires
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script loads and scores a subset of the questionnaires from the 
% PBIHB study: Trait anxiety, depression (ces-d) and gender (from the BPQ)
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function quest = pbihb_questScore(location)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD QUESTIONNAIRE DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set options
options = pbihb_setOptions('all', location);

% Load the data
quest.data.tables.prescreen1 = readtable(options.names.questionnaires.prescreen1);
optsPS2 = detectImportOptions(options.names.questionnaires.prescreen2);
optsPS2.VariableTypes = repmat({'char'}, size(optsPS2.VariableTypes));
quest.data.tables.prescreen2 = readtable(options.names.questionnaires.prescreen2, optsPS2);
optsS = detectImportOptions(options.names.questionnaires.session);
optsS.VariableTypes = repmat({'char'}, size(optsS.VariableTypes));
quest.data.tables.sess = readtable(options.names.questionnaires.session, optsS);
quest.data.tables.idx = readtable(options.names.questionnaires.idx);

% Pull out participant values for each questionnaire
quest.names.total = {'staiT', 'cesd', 'staiS', 'asi', 'gad7', 'bpq', 'maia', 'pcsR', 'pvqR', 'cdrisc', 'panasP', 'panasN', 'gss', 'fss'};
for a = 1:length(options.PPIDs.total)
    idxPPID = find(contains(quest.data.tables.idx.PPID, options.PPIDs.total{a})); 
    if quest.data.tables.idx.PRE_V(idxPPID) == 1
        idxPreID = find(ismember(quest.data.tables.prescreen1.x_id, quest.data.tables.idx.PRE_NUM(idxPPID)),1);
        quest.data.raw{1}(a,:) = quest.data.tables.prescreen1{idxPreID,find(contains(quest.data.tables.prescreen1.Properties.VariableNames, "StaitAllQuestions_"))};
        quest.data.raw{2}(a,:) = quest.data.tables.prescreen1{idxPreID,find(contains(quest.data.tables.prescreen1.Properties.VariableNames, "CESD"))};
    elseif quest.data.tables.idx.PRE_V(idxPPID) == 2
        idxPreID = find(contains(quest.data.tables.prescreen2.x__id_, num2str(quest.data.tables.idx.PRE_NUM(idxPPID))),1);
        quest.data.raw{1}(a,:) = str2double(quest.data.tables.prescreen2{idxPreID,find(contains(quest.data.tables.prescreen2.Properties.VariableNames, "StaitAllQuestions_"))});
        quest.data.raw{2}(a,:) = str2double(quest.data.tables.prescreen2{idxPreID,find(contains(quest.data.tables.prescreen2.Properties.VariableNames, "CESD"))});
    end
    idxSess = find(contains(quest.data.tables.sess.x__id_, num2str(quest.data.tables.idx.SESS_NUM(idxPPID))),1);
    quest.data.raw{3}(a,:) = str2double(quest.data.tables.sess{idxSess,find(contains(quest.data.tables.sess.Properties.VariableNames, "StaitAllQuestionsS_"))});
    quest.data.raw{4}(a,:) = str2double(quest.data.tables.sess{idxSess,find(contains(quest.data.tables.sess.Properties.VariableNames, "ASI"))});
    quest.data.raw{5}(a,:) = str2double(quest.data.tables.sess{idxSess,find(contains(quest.data.tables.sess.Properties.VariableNames, "GADMatrix"))});
    quest.data.raw{6}(a,:) = str2double(quest.data.tables.sess{idxSess,find(contains(quest.data.tables.sess.Properties.VariableNames, "BPQ"))});
    quest.data.raw{7}(a,:) = str2double(quest.data.tables.sess{idxSess,find(contains(quest.data.tables.sess.Properties.VariableNames, "MAIA"))});
    quest.data.raw{8}(a,:) = str2double(quest.data.tables.sess{idxSess,find(contains(quest.data.tables.sess.Properties.VariableNames, "PCS"))});
    quest.data.raw{9}(a,:) = str2double(quest.data.tables.sess{idxSess,find(contains(quest.data.tables.sess.Properties.VariableNames, "PVQ"))});
    quest.data.raw{10}(a,:) = str2double(quest.data.tables.sess{idxSess,find(contains(quest.data.tables.sess.Properties.VariableNames, "CDRISC"))});
    quest.data.raw{11}(a,:) = str2double(quest.data.tables.sess{idxSess,find(contains(quest.data.tables.sess.Properties.VariableNames, "PANAS"))});
    quest.data.raw{12}(a,:) = str2double(quest.data.tables.sess{idxSess,find(contains(quest.data.tables.sess.Properties.VariableNames, "PANAS"))});
    quest.data.raw{13}(a,:) = str2double(quest.data.tables.sess{idxSess,find(contains(quest.data.tables.sess.Properties.VariableNames, "GES"))});
    quest.data.raw{14}(a,:) = str2double(quest.data.tables.sess{idxSess,find(contains(quest.data.tables.sess.Properties.VariableNames, "FSS"))});
    quest.data.extra.raw.maxInsp(a) = quest.data.tables.idx.MAX_INSP(idxPPID);
    quest.data.extra.raw.gender(a,1) = str2double(quest.data.tables.sess{idxSess,find(contains(quest.data.tables.sess.Properties.VariableNames, "BPQ8"))});
    quest.data.extra.raw.age(a,1) = str2double(quest.data.tables.sess{idxSess,find(contains(quest.data.tables.sess.Properties.VariableNames, "BPQ7"))});
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCORE QUESTIONNAIRE DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify the length of each questionnaire
quest.data.scoring.length(1) = 20; % Trait anxiety
quest.data.scoring.length(2) = 20; % Cesd-d
quest.data.scoring.length(3) = 20; % State anxiety
quest.data.scoring.length(4) = 18; % ASI
quest.data.scoring.length(5) = 7; % GAD-7
quest.data.scoring.length(6) = 122; % BPQ
quest.data.scoring.length(7) = 32; % MAIA
quest.data.scoring.length(8) = 13; % PCS-R
quest.data.scoring.length(9) = 16; % PVQ-R
quest.data.scoring.length(10) = 25; % CD-RISC
quest.data.scoring.length(11) = 10; % PANAS+
quest.data.scoring.length(12) = 10; % PANAS-
quest.data.scoring.length(13) = 10; % GSS
quest.data.scoring.length(14) = 9; % FSS

% Code any reverse scoring for each of the questionnaires
quest.data.scoring.reverseIdx{1} = [1 6 7 10 13 16 19]; % Trait anxiety
quest.data.scoring.reverseNum(1) = 5;
quest.data.scoring.reverseIdx{2} = [4 8 12 16]; % Ces-d
quest.data.scoring.reverseNum(2) = 4;
quest.data.scoring.reverseIdx{3} = [1 2 5 8 10 11 15 16 19 20]; % State anxiety
quest.data.scoring.reverseNum(3) = 5;
quest.data.scoring.reverseIdx{7} = [5 6 7 8 9]; % Maia
quest.data.scoring.reverseNum(7) = 5;
quest.data.scoring.reverseIdx{9} = [8 16]; % Pvq-R
quest.data.scoring.reverseNum(9) = 5;
quest.data.scoring.adjust{11} = [1 3 4 6 10 11 13 15 17 18]; % PANAS-Pos
quest.data.scoring.adjust{12} = [2 5 7 8 9 12 14 16 19 20]; % PANAS-Neg

% Adjust scores for length, reversals and adjustments for each of the questionnaires
quest.data.trans = quest.data.raw;
for a = 1:length(quest.names.total)
    % Reverse if necessary
    try
        if ~isempty(quest.data.scoring.reverseIdx{a})
            quest.data.trans{a}(:,quest.data.scoring.reverseIdx{a}) = quest.data.scoring.reverseNum(a) - quest.data.trans{a}(:,quest.data.scoring.reverseIdx{a});
        end
    catch % If reverse field does not exist
    end
    % Adjust if necessary
    try
        if ~isempty(quest.data.scoring.adjust{a})
            quest.data.trans{a} = quest.data.trans{a}(:,quest.data.scoring.adjust{a});
        end
    catch % If adjust field does not exist
    end
    % Make sure length is correct
    quest.data.trans{a} = quest.data.trans{a}(:,1:quest.data.scoring.length(a));
end

% Calculate all total scores for each of the questionnaires
for a = 1:length(quest.names.total)
    quest.finalScores.total(:,a) = sum(quest.data.trans{a},2);
end

% Code any subscales
quest.names.subScales = {'asiSocial', 'asiCognitive', 'asiSomatic', ...
    'bpqAware', 'bpqStressResp', 'bpqANS', 'bpqStressStyle1', 'bpqStressStyle2', 'bpqHealth', ...
    'maiaNoticing', 'maiaNotDistract', 'maiaNotWorrying', 'maiaAttnReg', ...
    'maiaEmotAware', 'maiaSelfReg', 'maiaBodyList', 'maiaTrust', ...
    'pcsRHelpless', 'pcsRMag', 'pcsRRumin', ...
    'pvqRSymptoms', 'pvqChanges'};
quest.scoring.subScales{4}{1} = [1 6 9 11 13 17]; % ASI: social
quest.scoring.subScales{4}{2} = [2 5 10 14 16 18]; % ASI: cognitive
quest.scoring.subScales{4}{3} = [3 4 7 8 12 15]; % ASI: somatic
quest.scoring.subScales{6}{1} = 1:45; % BPQ: awareness
quest.scoring.subScales{6}{2} = 46:55; % BPQ: Stress response
quest.scoring.subScales{6}{3} = 56:82; % BPQ: ANS reactivity
quest.scoring.subScales{6}{4} = 83:90; % BPQ: Stress style 1
quest.scoring.subScales{6}{5} = 91:94; % BPQ: Stress style 2
quest.scoring.subScales{6}{6} = 95:122; % BPQ: Health
quest.scoring.subScales{7}{1} = 1:4; % MAIA: Noticing
quest.scoring.subScales{7}{2} = 5:7; % MAIA: Not distracting
quest.scoring.subScales{7}{3} = 8:10; % MAIA: Not worrying
quest.scoring.subScales{7}{4} = 11:17; % MAIA: Attention regulation
quest.scoring.subScales{7}{5} = 18:22; % MAIA: Emotional awareness
quest.scoring.subScales{7}{6} = 23:26; % MAIA: Self regulation
quest.scoring.subScales{7}{7} = 27:29; % MAIA: Body listening
quest.scoring.subScales{7}{8} = 30:32; % MAIA: Trusting
quest.scoring.subScales{8}{1} = [1:5 12]; % PCS-R: Helplessness
quest.scoring.subScales{8}{2} = [6 7 13]; % PCS-R: Magnitude
quest.scoring.subScales{8}{3} = 8:11; % PCS-R: Rumination
quest.scoring.subScales{9}{1} = [1 6 7 8 10 12 13 14 15 16]; % PVQ-R: Symptoms
quest.scoring.subScales{9}{2} = [2 3 4 5 9 11]; % PVQ-R: Change is symptoms

% Calculate all sub-scores for any of the necessary questionnaires
count = 0;
for a = 1:length(quest.names.subScales)
    try
        if ~isempty(quest.scoring.subScales{a})
            for subs = 1:length(quest.scoring.subScales{a})
                count = count + 1;
                quest.finalScores.subScales(:,count) = sum(quest.data.trans{a}(:,quest.scoring.subScales{a}{subs}),2);
            end
        end
    catch % If subscales field does not exist
    end
end


end