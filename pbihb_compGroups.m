%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% BREATHING SCANNER TASK: GROUP COMPARISONS (NON-fMRI) %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 14/04/2020
% -------------------------------------------------------------------------
% TO RUN:   comp        = pbihb_compGroups(location)
% INPUTS:   location    = 'local' or 'euler'
% OUTPUTS:  All specified group comparisons and regressions (non-fMRI)
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script runs the group comparisons and regressions for the non-
% imaging data collected in the PBIHB study. Results include group 
% comparisons for learning model parameters, scores of breathing-related 
% intensity and anxiety, reaction times, physiological parameters and
% motion parameters. Regressions are conducted between learning rate and
% breathing-related anxiety, and an exploratory regression between learning
% rate / breathing-related anxiety / breathing-related intensity and trait
% anxiety, depression and gender (specified in analysis plan).
% NB: Model fits must be completed (using pbihb_fitModels.m) as well as 
% physiological analysis (using pbihb_physiology.m) and FSL preprocessing
% (pbihb_fslPreproc.sh) before running this analysis.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function comp = pbihb_compGroups(location)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Secify options
comp.options = pbihb_setOptions('all', location);

% Check save directory existance and make if required
if ~exist(comp.options.paths.group_directory_groupComp, 'dir')
   mkdir(comp.options.paths.group_directory_groupComp)
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load models
if isfile(comp.options.saveNames.modelFits)
    load(comp.options.saveNames.modelFits);
else
    error('Warning: Model fits not run... Exiting');
end

% Load physiological and motion data for all participants
for a = 1:length(comp.options.PPIDs.total)
    setupTemp = pbihb_setOptions(comp.options.PPIDs.total{a}, location);
    phys.rawData{a} = load(setupTemp.saveNames.physTaskValues);
    motion.relative.rawData{a} = load(fullfile(setupTemp.saveNames.fslPreprocessing.task.directory, 'mc', 'prefiltered_func_data_mcf_rel.rms'));
    motion.absolute.rawData{a} = load(fullfile(setupTemp.saveNames.fslPreprocessing.task.directory, 'mc', 'prefiltered_func_data_mcf_abs.rms'));
end

% Load and score questionnaires
comp.quest = pbihb_questScore(location);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DISPLAY GROUP TRAIT ANXIETY SCORES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find trait anxiety scores
nameAnx = 'staiT';
idxAnx = find(contains(comp.quest.names.total, nameAnx));
comp.quest.staiT.lowMean = mean(comp.quest.finalScores.total(comp.options.PPIDs.low.idx,idxAnx));
comp.quest.staiT.lowStd = std(comp.quest.finalScores.total(comp.options.PPIDs.low.idx,idxAnx));
comp.quest.staiT.modMean = mean(comp.quest.finalScores.total(comp.options.PPIDs.mod.idx,idxAnx));
comp.quest.staiT.modStd = std(comp.quest.finalScores.total(comp.options.PPIDs.mod.idx,idxAnx));

% Print group trait anxiety scores to screen
fprintf('\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('\nGROUP TRAIT ANXIETY SUMMARIES:\n');
fprintf('Low anxiety scores = %1.2f ± %1.2f \n', comp.quest.staiT.lowMean, comp.quest.staiT.lowStd);
fprintf('Mod anxiety scores = %1.2f ± %1.2f \n\n', comp.quest.staiT.modMean, comp.quest.staiT.modStd);
disp('-------------------------------------------------------------------------------------------');
fprintf('\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DISPLAY BMS RESULTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Print model comparison results to screen
fprintf('\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('\nMODEL COMPARISON RESULTS:\n');
fprintf('Whole group XPs for models (%s : %s : %s) = %1.2f %1.2f %1.2f \n', models.est.names{1}, models.est.names{2}, models.est.names{3}, models.est.comp.total.xp(1), models.est.comp.total.xp(2), models.est.comp.total.xp(3))
fprintf('Whole group PXPs for models (%s : %s : %s) = %1.2f %1.2f %1.2f \n', models.est.names{1}, models.est.names{2}, models.est.names{3}, models.est.comp.total.pxp(1), models.est.comp.total.pxp(2), models.est.comp.total.pxp(3))
fprintf('Low group XPs for models (%s : %s : %s) = %1.2f %1.2f %1.2f \n', models.est.names{1}, models.est.names{2}, models.est.names{3}, models.est.comp.low.xp(1), models.est.comp.low.xp(2), models.est.comp.low.xp(3))
fprintf('Low group PXPs for models (%s : %s : %s) = %1.2f %1.2f %1.2f \n', models.est.names{1}, models.est.names{2}, models.est.names{3}, models.est.comp.low.pxp(1), models.est.comp.low.pxp(2), models.est.comp.low.pxp(3))
fprintf('Moderate group XPs for models (%s : %s : %s) = %1.2f %1.2f %1.2f \n', models.est.names{1}, models.est.names{2}, models.est.names{3}, models.est.comp.mod.xp(1), models.est.comp.mod.xp(2), models.est.comp.mod.xp(3))
fprintf('Moderate group PXPs for models (%s : %s : %s) = %1.2f %1.2f %1.2f \n\n', models.est.names{1}, models.est.names{2}, models.est.names{3}, models.est.comp.mod.pxp(1), models.est.comp.mod.pxp(2), models.est.comp.mod.pxp(3))
disp('-------------------------------------------------------------------------------------------');
fprintf('\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN GROUP COMPARISON ANALYSES FOR MODEL PARAMETERS (ALL PARTICIPANTS)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check PPID order for model data
if ~isempty(setdiff(comp.options.PPIDs.total, models.options.PPIDs.total))
    error('PPIDs from model fits do not match options... Exiting');
end

% Pull out all fitted parameters
idx = models.est.comp.total.winner.idx;
comp.models.name = models.est.comp.total.winner.name;
for a = 1:length(models.options.PPIDs.total)
    if strcmp(comp.models.name, 'Rescorla-Wagner')
        comp.models.values.prcModelValues(a) = models.est.fits{idx}{a}.p_prc.al;
        comp.models.values.learningRates(a) = comp.models.values.prcModelValues(a);
    else
        if strcmp(comp.models.name, '2-level HGF')
            comp.models.values.prcModelValues(a) = models.est.fits{idx}{a}.p_prc.om(2);
        elseif strcmp(comp.models.name, '3-level HGF')
            comp.models.values.prcModelValues(a) = models.est.fits{idx}{a}.p_prc.ka(2);
        end
        comp.models.values.learningRates(a) = mean(models.est.fits{idx}{a}.traj.sa(:,2));
    end
    comp.models.values.obsModelValues(a) = models.est.fits{idx}{a}.p_obs.ze;
end

% Calculate mean, std, median and inter-quartile range for:
% Model PRC parameter: Total
comp.models.groupValues.totPrcMean = mean(comp.models.values.prcModelValues);
comp.models.groupValues.totPrcStd = std(comp.models.values.prcModelValues);
comp.models.groupValues.totPrcMed = median(comp.models.values.prcModelValues);
comp.models.groupValues.totPrcIqr = iqr(comp.models.values.prcModelValues);
% Model PRC parameter: Low group
comp.models.groupValues.lowPrcMean = mean(comp.models.values.prcModelValues(models.options.PPIDs.low.idx));
comp.models.groupValues.lowPrcStd = std(comp.models.values.prcModelValues(models.options.PPIDs.low.idx));
comp.models.groupValues.lowPrcMed = median(comp.models.values.prcModelValues(models.options.PPIDs.low.idx));
comp.models.groupValues.lowPrcIqr = iqr(comp.models.values.prcModelValues(models.options.PPIDs.low.idx));
% Model PRC parameter: Mod group
comp.models.groupValues.modPrcMean = mean(comp.models.values.prcModelValues(models.options.PPIDs.mod.idx));
comp.models.groupValues.modPrcStd = std(comp.models.values.prcModelValues(models.options.PPIDs.mod.idx));
comp.models.groupValues.modPrcMed = median(comp.models.values.prcModelValues(models.options.PPIDs.mod.idx));
comp.models.groupValues.modPrcIqr = iqr(comp.models.values.prcModelValues(models.options.PPIDs.mod.idx));
% Model learning rate: Total
comp.models.groupValues.totLearnMean = mean(comp.models.values.learningRates);
comp.models.groupValues.totLearnStd = std(comp.models.values.learningRates);
comp.models.groupValues.totLearnMed = median(comp.models.values.learningRates);
comp.models.groupValues.totLearnIqr = iqr(comp.models.values.learningRates);
% Model learning rate: Low group
comp.models.groupValues.lowLearnMean = mean(comp.models.values.learningRates(models.options.PPIDs.low.idx));
comp.models.groupValues.lowLearnStd = std(comp.models.values.learningRates(models.options.PPIDs.low.idx));
comp.models.groupValues.lowLearnMed = median(comp.models.values.learningRates(models.options.PPIDs.low.idx));
comp.models.groupValues.lowLearnIqr = iqr(comp.models.values.learningRates(models.options.PPIDs.low.idx));
% Model learning rate: Mod group
comp.models.groupValues.modLearnMean = mean(comp.models.values.learningRates(models.options.PPIDs.mod.idx));
comp.models.groupValues.modLearnStd = std(comp.models.values.learningRates(models.options.PPIDs.mod.idx));
comp.models.groupValues.modLearnMed = median(comp.models.values.learningRates(models.options.PPIDs.mod.idx));
comp.models.groupValues.modLearnIqr = iqr(comp.models.values.learningRates(models.options.PPIDs.mod.idx));
% Model OBS parameter: Total
comp.models.groupValues.totObsMean = mean(comp.models.values.obsModelValues);
comp.models.groupValues.totObsStd = std(comp.models.values.obsModelValues);
comp.models.groupValues.totObsMed = median(comp.models.values.obsModelValues);
comp.models.groupValues.totObsIqr = iqr(comp.models.values.obsModelValues);
% Model OBS parameter: Low group
comp.models.groupValues.lowObsMean = mean(comp.models.values.obsModelValues(models.options.PPIDs.low.idx));
comp.models.groupValues.lowObsStd = std(comp.models.values.obsModelValues(models.options.PPIDs.low.idx));
comp.models.groupValues.lowObsMed = median(comp.models.values.obsModelValues(models.options.PPIDs.low.idx));
comp.models.groupValues.lowObsIqr = iqr(comp.models.values.obsModelValues(models.options.PPIDs.low.idx));
% Model OBS parameter: Mod group
comp.models.groupValues.modObsMean = mean(comp.models.values.obsModelValues(models.options.PPIDs.mod.idx));
comp.models.groupValues.modObsStd = std(comp.models.values.obsModelValues(models.options.PPIDs.mod.idx));
comp.models.groupValues.modObsMed = median(comp.models.values.obsModelValues(models.options.PPIDs.mod.idx));
comp.models.groupValues.modObsIqr = iqr(comp.models.values.obsModelValues(models.options.PPIDs.mod.idx));

% Compare model parameters between groups
comp.models.groupComp.alphaCorrected = 0.01;
comp.models.groupComp.tails = 'two';
comp.models.adtests.prcModelValues.low = adtest(comp.models.values.prcModelValues(models.options.PPIDs.low.idx));
comp.models.adtests.prcModelValues.mod = adtest(comp.models.values.prcModelValues(models.options.PPIDs.mod.idx));
if comp.models.adtests.prcModelValues.low == 1 || comp.models.adtests.prcModelValues.mod == 1
    comp.models.groupComp.prcModelValues.test = 'Wcxn_rankSum';
    [comp.models.groupComp.prcModelValues.p, comp.models.groupComp.prcModelValues.h, comp.models.groupComp.prcModelValues.stats] = ranksum(comp.models.values.prcModelValues(models.options.PPIDs.low.idx), comp.models.values.prcModelValues(models.options.PPIDs.mod.idx), 'alpha', comp.models.groupComp.alphaCorrected);
else
    comp.models.groupComp.prcModelValues.test = 'tTest2';
    [comp.models.groupComp.prcModelValues.h, comp.models.groupComp.prcModelValues.p, comp.models.groupComp.prcModelValues.ci, comp.models.groupComp.prcModelValues.stats] = ttest2(comp.models.values.prcModelValues(models.options.PPIDs.low.idx), comp.models.values.prcModelValues(models.options.PPIDs.mod.idx), 'Alpha', comp.models.groupComp.alphaCorrected);
end
comp.models.adtests.obsModelValues.low = adtest(comp.models.values.obsModelValues(models.options.PPIDs.low.idx));
comp.models.adtests.obsModelValues.mod = adtest(comp.models.values.obsModelValues(models.options.PPIDs.mod.idx));
if comp.models.adtests.obsModelValues.low == 1 || comp.models.adtests.obsModelValues.mod == 1
    comp.models.groupComp.obsModelValues.test = 'Wcxn_rankSum';
    [comp.models.groupComp.obsModelValues.p, comp.models.groupComp.obsModelValues.h, comp.models.groupComp.obsModelValues.stats] = ranksum(comp.models.values.obsModelValues(models.options.PPIDs.low.idx), comp.models.values.obsModelValues(models.options.PPIDs.mod.idx), 'alpha', comp.models.groupComp.alphaCorrected);
else
    comp.models.groupComp.obsModelValues.test = 'tTest2';
    [comp.models.groupComp.obsModelValues.h, comp.models.groupComp.obsModelValues.p, comp.models.groupComp.obsModelValues.ci, comp.models.groupComp.obsModelValues.stats] = ttest2(comp.models.values.obsModelValues(models.options.PPIDs.low.idx), comp.models.values.obsModelValues(models.options.PPIDs.mod.idx), 'Alpha', comp.models.groupComp.alphaCorrected);
end
comp.models.adtests.learningRates.low = adtest(comp.models.values.learningRates(models.options.PPIDs.low.idx));
comp.models.adtests.learningRates.mod = adtest(comp.models.values.learningRates(models.options.PPIDs.mod.idx));
if comp.models.adtests.learningRates.low == 1 || comp.models.adtests.learningRates.mod == 1
    comp.models.groupComp.learningRates.test = 'Wcxn_rankSum';
    [comp.models.groupComp.learningRates.p, comp.models.groupComp.learningRates.h, comp.models.groupComp.learningRates.stats] = ranksum(comp.models.values.learningRates(models.options.PPIDs.low.idx), comp.models.values.learningRates(models.options.PPIDs.mod.idx), 'alpha', comp.models.groupComp.alphaCorrected);
else
    comp.models.groupComp.learningRates.test = 'tTest2';
    [comp.models.groupComp.learningRates.h, comp.models.groupComp.learningRates.p, comp.models.groupComp.learningRates.ci, comp.models.groupComp.learningRates.stats] = ttest2(comp.models.values.learningRates(models.options.PPIDs.low.idx), comp.models.values.learningRates(models.options.PPIDs.mod.idx), 'Alpha', comp.models.groupComp.alphaCorrected);
end

% Print parameter comparison results to screen
fprintf('\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('MODEL PARAMETER COMPARISON RESULTS (MODEL: %s, sig: %1.3f, tails: %s):\n', models.est.comp.total.winner.name, comp.models.groupComp.alphaCorrected, comp.models.groupComp.tails);
fprintf('Whole group perceptual model param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValues.totPrcMean, comp.models.groupValues.totPrcStd, comp.models.groupValues.totPrcMed, comp.models.groupValues.totPrcIqr);
fprintf('Low group perceptual model param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValues.lowPrcMean, comp.models.groupValues.lowPrcStd, comp.models.groupValues.lowPrcMed, comp.models.groupValues.lowPrcIqr);
fprintf('Mod group perceptual model param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValues.modPrcMean, comp.models.groupValues.modPrcStd, comp.models.groupValues.modPrcMed, comp.models.groupValues.modPrcIqr);
fprintf('Low vs. mod perceptual model param: %s p = %1.3f\n', comp.models.groupComp.prcModelValues.test, comp.models.groupComp.prcModelValues.p);
fprintf('Whole group observation model param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValues.totObsMean, comp.models.groupValues.totObsStd, comp.models.groupValues.totObsMed, comp.models.groupValues.totObsIqr);
fprintf('Low group observation model param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValues.lowObsMean, comp.models.groupValues.lowObsStd, comp.models.groupValues.lowObsMed, comp.models.groupValues.lowObsIqr);
fprintf('Mod group observation model param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValues.modObsMean, comp.models.groupValues.modObsStd, comp.models.groupValues.modObsMed, comp.models.groupValues.modObsIqr);
fprintf('Low vs. mod observation model param: %s p = %1.3f\n', comp.models.groupComp.obsModelValues.test, comp.models.groupComp.obsModelValues.p);
if strcmp(models.est.comp.total.winner.name, '2-level HGF') || strcmp(models.est.comp.total.winner.name, '3-level HGF')
    fprintf('Whole group learning rate param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValues.totLearnMean, comp.models.groupValues.totLearnStd, comp.models.groupValues.totLearnMed, comp.models.groupValues.totLearnIqr);
    fprintf('Low group learning rate param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValues.lowLearnMean, comp.models.groupValues.lowLearnStd, comp.models.groupValues.lowLearnMed, comp.models.groupValues.lowLearnIqr);
    fprintf('Mod group learning rate param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValues.modLearnMean, comp.models.groupValues.modLearnStd, comp.models.groupValues.modLearnMed, comp.models.groupValues.modLearnIqr);
    fprintf('Low vs. mod learning rate param: %s p = %1.3f\n', comp.models.groupComp.learningRates.test, comp.models.groupComp.learningRates.p);
end
disp('-------------------------------------------------------------------------------------------');
fprintf('\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN GROUP COMPARISON ANALYSES FOR MODEL PARAMETERS (EXCLUDING PARTICIPANTS)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate mean, std, median and inter-quartile range for:
% Model PRC parameter: Total
comp.models.groupValuesEx.totPrcMean = mean(comp.models.values.prcModelValues(models.est.goodness.totalEx));
comp.models.groupValuesEx.totPrcStd = std(comp.models.values.prcModelValues(models.est.goodness.totalEx));
comp.models.groupValuesEx.totPrcMed = median(comp.models.values.prcModelValues(models.est.goodness.totalEx));
comp.models.groupValuesEx.totPrcIqr = iqr(comp.models.values.prcModelValues(models.est.goodness.totalEx));
% Model PRC parameter: Low group
comp.models.groupValuesEx.lowPrcMean = mean(comp.models.values.prcModelValues(models.est.goodness.lowEx));
comp.models.groupValuesEx.lowPrcStd = std(comp.models.values.prcModelValues(models.est.goodness.lowEx));
comp.models.groupValuesEx.lowPrcMed = median(comp.models.values.prcModelValues(models.est.goodness.lowEx));
comp.models.groupValuesEx.lowPrcIqr = iqr(comp.models.values.prcModelValues(models.est.goodness.lowEx));
% Model PRC parameter: Mod group
comp.models.groupValuesEx.modPrcMean = mean(comp.models.values.prcModelValues(models.est.goodness.modEx));
comp.models.groupValuesEx.modPrcStd = std(comp.models.values.prcModelValues(models.est.goodness.modEx));
comp.models.groupValuesEx.modPrcMed = median(comp.models.values.prcModelValues(models.est.goodness.modEx));
comp.models.groupValuesEx.modPrcIqr = iqr(comp.models.values.prcModelValues(models.est.goodness.modEx));
% Model learning rate: Total
comp.models.groupValuesEx.totLearnMean = mean(comp.models.values.learningRates(models.est.goodness.totalEx));
comp.models.groupValuesEx.totLearnStd = std(comp.models.values.learningRates(models.est.goodness.totalEx));
comp.models.groupValuesEx.totLearnMed = median(comp.models.values.learningRates(models.est.goodness.totalEx));
comp.models.groupValuesEx.totLearnIqr = iqr(comp.models.values.learningRates(models.est.goodness.totalEx));
% Model learning rate: Low group
comp.models.groupValuesEx.lowLearnMean = mean(comp.models.values.learningRates(models.est.goodness.lowEx));
comp.models.groupValuesEx.lowLearnStd = std(comp.models.values.learningRates(models.est.goodness.lowEx));
comp.models.groupValuesEx.lowLearnMed = median(comp.models.values.learningRates(models.est.goodness.lowEx));
comp.models.groupValuesEx.lowLearnIqr = iqr(comp.models.values.learningRates(models.est.goodness.lowEx));
% Model learning rate: Mod group
comp.models.groupValuesEx.modLearnMean = mean(comp.models.values.learningRates(models.est.goodness.modEx));
comp.models.groupValuesEx.modLearnStd = std(comp.models.values.learningRates(models.est.goodness.modEx));
comp.models.groupValuesEx.modLearnMed = median(comp.models.values.learningRates(models.est.goodness.modEx));
comp.models.groupValuesEx.modLearnIqr = iqr(comp.models.values.learningRates(models.est.goodness.modEx));
% Model OBS parameter: Total
comp.models.groupValuesEx.totObsMean = mean(comp.models.values.obsModelValues(models.est.goodness.totalEx));
comp.models.groupValuesEx.totObsStd = std(comp.models.values.obsModelValues(models.est.goodness.totalEx));
comp.models.groupValuesEx.totObsMed = median(comp.models.values.obsModelValues(models.est.goodness.totalEx));
comp.models.groupValuesEx.totObsIqr = iqr(comp.models.values.obsModelValues(models.est.goodness.totalEx));
% Model OBS parameter: Low group
comp.models.groupValuesEx.lowObsMean = mean(comp.models.values.obsModelValues(models.est.goodness.lowEx));
comp.models.groupValuesEx.lowObsStd = std(comp.models.values.obsModelValues(models.est.goodness.lowEx));
comp.models.groupValuesEx.lowObsMed = median(comp.models.values.obsModelValues(models.est.goodness.lowEx));
comp.models.groupValuesEx.lowObsIqr = iqr(comp.models.values.obsModelValues(models.est.goodness.lowEx));
% Model OBS parameter: Mod group
comp.models.groupValuesEx.modObsMean = mean(comp.models.values.obsModelValues(models.est.goodness.modEx));
comp.models.groupValuesEx.modObsStd = std(comp.models.values.obsModelValues(models.est.goodness.modEx));
comp.models.groupValuesEx.modObsMed = median(comp.models.values.obsModelValues(models.est.goodness.modEx));
comp.models.groupValuesEx.modObsIqr = iqr(comp.models.values.obsModelValues(models.est.goodness.modEx));

% Compare model parameters between groups
comp.models.groupCompEx.alphaCorrected = 0.01;
comp.models.groupCompEx.tails = 'two';
comp.models.adtests.prcModelValues.lowEx = adtest(comp.models.values.prcModelValues(models.est.goodness.lowEx));
comp.models.adtests.prcModelValues.modEx = adtest(comp.models.values.prcModelValues(models.est.goodness.modEx));
if comp.models.adtests.prcModelValues.lowEx == 1 || comp.models.adtests.prcModelValues.modEx == 1
    comp.models.groupCompEx.prcModelValues.test = 'Wcxn_rankSum';
    [comp.models.groupCompEx.prcModelValues.p, comp.models.groupCompEx.prcModelValues.h, comp.models.groupCompEx.prcModelValues.stats] = ranksum(comp.models.values.prcModelValues(models.est.goodness.lowEx), comp.models.values.prcModelValues(models.est.goodness.modEx), 'alpha', comp.models.groupCompEx.alphaCorrected);
else
    comp.models.groupCompEx.prcModelValues.test = 'tTest2';
    [comp.models.groupCompEx.prcModelValues.h, comp.models.groupCompEx.prcModelValues.p, comp.models.groupCompEx.prcModelValues.ci, comp.models.groupCompEx.prcModelValues.stats] = ttest2(comp.models.values.prcModelValues(models.est.goodness.lowEx), comp.models.values.prcModelValues(models.est.goodness.modEx), 'Alpha', comp.models.groupCompEx.alphaCorrected);
end
comp.models.adtests.obsModelValues.lowEx = adtest(comp.models.values.obsModelValues(models.est.goodness.lowEx));
comp.models.adtests.obsModelValues.modEx = adtest(comp.models.values.obsModelValues(models.est.goodness.modEx));
if comp.models.adtests.obsModelValues.lowEx == 1 || comp.models.adtests.obsModelValues.modEx == 1
    comp.models.groupCompEx.obsModelValues.test = 'Wcxn_rankSum';
    [comp.models.groupCompEx.obsModelValues.p, comp.models.groupCompEx.obsModelValues.h, comp.models.groupCompEx.obsModelValues.stats] = ranksum(comp.models.values.obsModelValues(models.est.goodness.lowEx), comp.models.values.obsModelValues(models.est.goodness.modEx), 'alpha', comp.models.groupCompEx.alphaCorrected);
else
    comp.models.groupCompEx.obsModelValues.test = 'tTest2';
    [comp.models.groupCompEx.obsModelValues.h, comp.models.groupCompEx.obsModelValues.p, comp.models.groupCompEx.obsModelValues.ci, comp.models.groupCompEx.obsModelValues.stats] = ttest2(comp.models.values.obsModelValues(models.est.goodness.lowEx), comp.models.values.obsModelValues(models.est.goodness.modEx), 'Alpha', comp.models.groupCompEx.alphaCorrected);
end
comp.models.adtests.learningRates.lowEx = adtest(comp.models.values.learningRates(models.est.goodness.lowEx));
comp.models.adtests.learningRates.modEx = adtest(comp.models.values.learningRates(models.est.goodness.modEx));
if comp.models.adtests.learningRates.lowEx == 1 || comp.models.adtests.learningRates.modEx == 1
    comp.models.groupCompEx.learningRates.test = 'Wcxn_rankSum';
    [comp.models.groupCompEx.learningRates.p, comp.models.groupCompEx.learningRates.h, comp.models.groupCompEx.learningRates.stats] = ranksum(comp.models.values.learningRates(models.est.goodness.lowEx), comp.models.values.learningRates(models.est.goodness.modEx), 'alpha', comp.models.groupCompEx.alphaCorrected);
else
    comp.models.groupCompEx.learningRates.test = 'tTest2';
    [comp.models.groupCompEx.learningRates.h, comp.models.groupCompEx.learningRates.p, comp.models.groupCompEx.learningRates.ci, comp.models.groupCompEx.learningRates.stats] = ttest2(comp.models.values.learningRates(models.est.goodness.lowEx), comp.models.values.learningRates(models.est.goodness.modEx), 'Alpha', comp.models.groupCompEx.alphaCorrected);
end

% Print parameter comparison results to screen
fprintf('\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('MODEL PARAMETER COMPARISON RESULTS (EXCLUDED PPIDS) (MODEL: %s, sig: %1.3f, tails: %s):\n', models.est.comp.total.winner.name, comp.models.groupCompEx.alphaCorrected, comp.models.groupCompEx.tails);
fprintf('Whole group perceptual model param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValuesEx.totPrcMean, comp.models.groupValuesEx.totPrcStd, comp.models.groupValuesEx.totPrcMed, comp.models.groupValuesEx.totPrcIqr);
fprintf('Low group perceptual model param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValuesEx.lowPrcMean, comp.models.groupValuesEx.lowPrcStd, comp.models.groupValuesEx.lowPrcMed, comp.models.groupValuesEx.lowPrcIqr);
fprintf('Mod group perceptual model param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValuesEx.modPrcMean, comp.models.groupValuesEx.modPrcStd, comp.models.groupValuesEx.modPrcMed, comp.models.groupValuesEx.modPrcIqr);
fprintf('Low vs. mod perceptual model param: %s p = %1.3f\n', comp.models.groupCompEx.prcModelValues.test, comp.models.groupCompEx.prcModelValues.p);
fprintf('Whole group observation model param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValuesEx.totObsMean, comp.models.groupValuesEx.totObsStd, comp.models.groupValuesEx.totObsMed, comp.models.groupValuesEx.totObsIqr);
fprintf('Low group observation model param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValuesEx.lowObsMean, comp.models.groupValuesEx.lowObsStd, comp.models.groupValuesEx.lowObsMed, comp.models.groupValuesEx.lowObsIqr);
fprintf('Mod group observation model param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValuesEx.modObsMean, comp.models.groupValuesEx.modObsStd, comp.models.groupValuesEx.modObsMed, comp.models.groupValuesEx.modObsIqr);
fprintf('Low vs. mod observation model param: %s p = %1.3f\n', comp.models.groupCompEx.obsModelValues.test, comp.models.groupCompEx.obsModelValues.p);
if strcmp(models.est.comp.total.winner.name, '2-level HGF') || strcmp(models.est.comp.total.winner.name, '3-level HGF')
    fprintf('Whole group learning rate param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValuesEx.totLearnMean, comp.models.groupValuesEx.totLearnStd, comp.models.groupValuesEx.totLearnMed, comp.models.groupValuesEx.totLearnIqr);
    fprintf('Low group learning rate param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValuesEx.lowLearnMean, comp.models.groupValuesEx.lowLearnStd, comp.models.groupValuesEx.lowLearnMed, comp.models.groupValuesEx.lowLearnIqr);
    fprintf('Mod group learning rate param: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.models.groupValuesEx.modLearnMean, comp.models.groupValuesEx.modLearnStd, comp.models.groupValuesEx.modLearnMed, comp.models.groupValuesEx.modLearnIqr);
    fprintf('Low vs. mod learning rate param: %s p = %1.3f\n', comp.models.groupCompEx.learningRates.test, comp.models.groupCompEx.learningRates.p);
end
disp('-------------------------------------------------------------------------------------------');
fprintf('\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN GROUP COMPARISON ANALYSES FOR INTENSITY AND ANXIETY SCORES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pull out anxiety and intensity scores
for a = 1:length(models.options.PPIDs.total)
    comp.scores.values.anxiety(a) = models.rawData{a}.data.rate_answer(end);
    comp.scores.values.intensity(a) = nanmean(models.rawData{a}.data.rate_answer(models.rawData{a}.params.resist == 1));
end

% Calculate mean, std, median and inter-quartile range for:
% Anxiety scores: Total
comp.scores.groupValues.totAnxMean = mean(comp.scores.values.anxiety);
comp.scores.groupValues.totAnxStd = std(comp.scores.values.anxiety);
comp.scores.groupValues.totAnxMed = median(comp.scores.values.anxiety);
comp.scores.groupValues.totAnxIqr = iqr(comp.scores.values.anxiety);
% Anxiety scores: Low group
comp.scores.groupValues.lowAnxMean = mean(comp.scores.values.anxiety(models.options.PPIDs.low.idx));
comp.scores.groupValues.lowAnxStd = std(comp.scores.values.anxiety(models.options.PPIDs.low.idx));
comp.scores.groupValues.lowAnxMed = median(comp.scores.values.anxiety(models.options.PPIDs.low.idx));
comp.scores.groupValues.lowAnxIqr = iqr(comp.scores.values.anxiety(models.options.PPIDs.low.idx));
% Anxiety scores: Mod group
comp.scores.groupValues.modAnxMean = mean(comp.scores.values.anxiety(models.options.PPIDs.mod.idx));
comp.scores.groupValues.modAnxStd = std(comp.scores.values.anxiety(models.options.PPIDs.mod.idx));
comp.scores.groupValues.modAnxMed = median(comp.scores.values.anxiety(models.options.PPIDs.mod.idx));
comp.scores.groupValues.modAnxIqr = iqr(comp.scores.values.anxiety(models.options.PPIDs.mod.idx));
% Intensity scores: Total
comp.scores.groupValues.totIntMean = mean(comp.scores.values.intensity);
comp.scores.groupValues.totIntStd = std(comp.scores.values.intensity);
comp.scores.groupValues.totIntMed = median(comp.scores.values.intensity);
comp.scores.groupValues.totIntIqr = iqr(comp.scores.values.intensity);
% Intensity scores: Low group
comp.scores.groupValues.lowIntMean = mean(comp.scores.values.intensity(models.options.PPIDs.low.idx));
comp.scores.groupValues.lowIntStd = std(comp.scores.values.intensity(models.options.PPIDs.low.idx));
comp.scores.groupValues.lowIntMed = median(comp.scores.values.intensity(models.options.PPIDs.low.idx));
comp.scores.groupValues.lowIntIqr = iqr(comp.scores.values.intensity(models.options.PPIDs.low.idx));
% Intensity scores: Mod group
comp.scores.groupValues.modIntMean = mean(comp.scores.values.intensity(models.options.PPIDs.mod.idx));
comp.scores.groupValues.modIntStd = std(comp.scores.values.intensity(models.options.PPIDs.mod.idx));
comp.scores.groupValues.modIntMed = median(comp.scores.values.intensity(models.options.PPIDs.mod.idx));
comp.scores.groupValues.modIntIqr = iqr(comp.scores.values.intensity(models.options.PPIDs.mod.idx));

% Compare scores between groups
comp.scores.groupComp.alphaCorrected = 0.01;
comp.scores.groupComp.tails = 'two';
comp.scores.adtests.anxiety.low = adtest(comp.scores.values.anxiety(models.options.PPIDs.low.idx));
comp.scores.adtests.anxiety.mod = adtest(comp.scores.values.anxiety(models.options.PPIDs.mod.idx));
if comp.scores.adtests.anxiety.low == 1 || comp.scores.adtests.anxiety.mod == 1
    comp.scores.groupComp.anxiety.test = 'Wcxn_rankSum';
    [comp.scores.groupComp.anxiety.p, comp.scores.groupComp.anxiety.h, comp.scores.groupComp.anxiety.stats] = ranksum(comp.scores.values.anxiety(models.options.PPIDs.low.idx), comp.scores.values.anxiety(models.options.PPIDs.mod.idx), 'alpha', comp.scores.groupComp.alphaCorrected);
else
    comp.scores.groupComp.anxiety.test = 'tTest2';
    [comp.scores.groupComp.anxiety.h, comp.scores.groupComp.anxiety.p, comp.scores.groupComp.anxiety.ci, comp.scores.groupComp.anxiety.stats] = ttest2(comp.scores.values.anxiety(models.options.PPIDs.low.idx), comp.scores.values.anxiety(models.options.PPIDs.mod.idx), 'Alpha', comp.scores.groupComp.alphaCorrected);
end
comp.scores.adtests.intensity.low = adtest(comp.scores.values.anxiety(models.options.PPIDs.low.idx));
comp.scores.adtests.intensity.mod = adtest(comp.scores.values.anxiety(models.options.PPIDs.mod.idx));
if comp.scores.adtests.intensity.low == 1 || comp.scores.adtests.intensity.mod == 1
    comp.scores.groupComp.intensity.test = 'Wcxn_rankSum';
    [comp.scores.groupComp.intensity.p, comp.scores.groupComp.intensity.h, comp.scores.groupComp.intensity.stats] = ranksum(comp.scores.values.intensity(models.options.PPIDs.low.idx), comp.scores.values.intensity(models.options.PPIDs.mod.idx), 'alpha', comp.scores.groupComp.alphaCorrected);
else
    comp.scores.groupComp.intensity.test = 'tTest2';
    [comp.scores.groupComp.intensity.h, comp.scores.groupComp.intensity.p, comp.scores.groupComp.intensity.ci, comp.scores.groupComp.intensity.stats] = ttest2(comp.scores.values.intensity(models.options.PPIDs.low.idx), comp.scores.values.intensity(models.options.PPIDs.mod.idx), 'Alpha', comp.scores.groupComp.alphaCorrected);
end

% Print score comparison results to screen
fprintf('\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('SCORE COMPARISON RESULTS (sig: %1.3f, tails: %s):\n', comp.scores.groupComp.alphaCorrected, comp.scores.groupComp.tails);
fprintf('Whole group anxiety score: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.scores.groupValues.totAnxMean, comp.scores.groupValues.totAnxStd, comp.scores.groupValues.totAnxMed, comp.scores.groupValues.totAnxIqr);
fprintf('Low group anxiety score: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.scores.groupValues.lowAnxMean, comp.scores.groupValues.lowAnxStd, comp.scores.groupValues.lowAnxMed, comp.scores.groupValues.lowAnxIqr);
fprintf('Mod group anxiety score: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.scores.groupValues.modAnxMean, comp.scores.groupValues.modAnxStd, comp.scores.groupValues.modAnxMed, comp.scores.groupValues.modAnxIqr);
fprintf('Low vs. mod anxiety score: %s p = %1.3f\n', comp.scores.groupComp.anxiety.test, comp.scores.groupComp.anxiety.p); 
fprintf('Whole group intensity score: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.scores.groupValues.totIntMean, comp.scores.groupValues.totIntStd, comp.scores.groupValues.totIntMed, comp.scores.groupValues.totIntIqr);
fprintf('Low group intensity score: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.scores.groupValues.lowIntMean, comp.scores.groupValues.lowIntStd, comp.scores.groupValues.lowIntMed, comp.scores.groupValues.lowIntIqr);
fprintf('Mod group intensity score: mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.scores.groupValues.modIntMean, comp.scores.groupValues.modIntStd, comp.scores.groupValues.modIntMed, comp.scores.groupValues.modIntIqr);
fprintf('Low vs. mod intensity score: %s p = %1.3f\n', comp.scores.groupComp.intensity.test, comp.scores.groupComp.intensity.p); 
disp('-------------------------------------------------------------------------------------------');
fprintf('\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN REGRESSION ANALYSES FOR MODEL PARAMETERS vs. ANXIETY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Add analysis options
comp.models.regressAnx.alphaCorrected = 0.01;
comp.models.regressAnx.tails = 'two';

% Fit regression model
comp.models.regressAnx.fit = fitlm(comp.scores.values.anxiety, comp.models.values.learningRates);

% Pull out parameters
comp.models.regressAnx.coeff = comp.models.regressAnx.fit.Coefficients.Estimate(2);
comp.models.regressAnx.p = comp.models.regressAnx.fit.Coefficients.pValue(2);

% Print results to screen
fprintf('\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('MODEL LEARNING RATE vs. ANXIETY REGRESSION (sig: %1.3f, tails: %s):\n', comp.models.regressAnx.alphaCorrected, comp.models.regressAnx.tails);
fprintf('Regression coefficient = %1.3f (p = %1.3f)\n', comp.models.regressAnx.coeff, comp.models.regressAnx.p)
disp('-------------------------------------------------------------------------------------------');
fprintf('\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN EXPLORATORY ANALYSES AGAINST ANXIETY, DEPRESSION AND GENDER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Add analysis options
comp.models.regressExplore.alphaCorrected = 0.017;
comp.models.regressExplore.tails = 'two';
comp.scores.regressExplore.alphaCorrected = 0.017;
comp.scores.regressExplore.tails = 'two';

% Find and de-mean scores
nameAnx = 'staiT';
nameDep = 'cesd';
idxAnx = find(contains(comp.quest.names.total, nameAnx));
idxDep = find(contains(comp.quest.names.total, nameDep));
comp.scores.regressExplore.valuesRaw = [comp.quest.finalScores.total(:,idxAnx), comp.quest.finalScores.total(:,idxDep), comp.quest.data.extra.raw.gender];
comp.scores.regressExplore.valuesDemeaned = comp.scores.regressExplore.valuesRaw - mean(comp.scores.regressExplore.valuesRaw);

% Fit regression model to learning rates
comp.models.regressExplore.fit = fitlm(comp.scores.regressExplore.valuesDemeaned, comp.models.values.learningRates);

% Fit regression model to anxiety and intensity scores
comp.scores.regressExplore.anxiety.fit = fitlm(comp.scores.regressExplore.valuesDemeaned, comp.scores.values.anxiety);
comp.scores.regressExplore.intensity.fit = fitlm(comp.scores.regressExplore.valuesDemeaned, comp.scores.values.intensity);

% Pull out parameters
comp.models.regressExplore.coeff = comp.models.regressExplore.fit.Coefficients.Estimate;
comp.models.regressExplore.p = comp.models.regressExplore.fit.Coefficients.pValue;
comp.scores.regressExplore.anxiety.coeff = comp.scores.regressExplore.anxiety.fit.Coefficients.Estimate;
comp.scores.regressExplore.anxiety.p = comp.scores.regressExplore.anxiety.fit.Coefficients.pValue;
comp.scores.regressExplore.intensity.coeff = comp.scores.regressExplore.intensity.fit.Coefficients.Estimate;
comp.scores.regressExplore.intensity.p = comp.scores.regressExplore.intensity.fit.Coefficients.pValue;

% Print results to screen
fprintf('\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('MODEL LEARNING RATE EXPLORATORY REGRESSION (sig: %1.3f, tails: %s):\n', comp.models.regressExplore.alphaCorrected, comp.models.regressExplore.tails);
fprintf('Regression coefficient trait anxiety = %1.3f (p = %1.3f)\n', comp.models.regressExplore.coeff(2), comp.models.regressExplore.p(2));
fprintf('Regression coefficient depression = %1.3f (p = %1.3f)\n', comp.models.regressExplore.coeff(3), comp.models.regressExplore.p(3));
fprintf('Regression coefficient gender = %1.3f (p = %1.3f)\n', comp.models.regressExplore.coeff(4), comp.models.regressExplore.p(4));
disp('-------------------------------------------------------------------------------------------');
fprintf('\n\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('ANXIETY SCORE EXPLORATORY REGRESSION (sig: %1.3f, tails: %s):\n', comp.scores.regressExplore.alphaCorrected, comp.scores.regressExplore.tails);
fprintf('Regression coefficient trait anxiety = %1.3f (p = %1.3f)\n', comp.scores.regressExplore.anxiety.coeff(2), comp.scores.regressExplore.anxiety.p(2));
fprintf('Regression coefficient depression = %1.3f (p = %1.3f)\n', comp.scores.regressExplore.anxiety.coeff(3), comp.scores.regressExplore.anxiety.p(3));
fprintf('Regression coefficient gender = %1.3f (p = %1.3f)\n', comp.scores.regressExplore.anxiety.coeff(4), comp.scores.regressExplore.anxiety.p(4));
disp('-------------------------------------------------------------------------------------------');
fprintf('\n\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('INTENSITY SCORE EXPLORATORY REGRESSION (sig: %1.3f, tails: %s):\n', comp.scores.regressExplore.alphaCorrected, comp.scores.regressExplore.tails);
fprintf('Regression coefficient trait anxiety = %1.3f (p = %1.3f)\n', comp.scores.regressExplore.intensity.coeff(2), comp.scores.regressExplore.intensity.p(2));
fprintf('Regression coefficient depression = %1.3f (p = %1.3f)\n', comp.scores.regressExplore.intensity.coeff(3), comp.scores.regressExplore.intensity.p(3));
fprintf('Regression coefficient gender = %1.3f (p = %1.3f)\n', comp.scores.regressExplore.intensity.coeff(4), comp.scores.regressExplore.intensity.p(4));
disp('-------------------------------------------------------------------------------------------');
fprintf('\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN EXPLORATORY ANALYSIS FOR REACTION TIMES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pull out reaction times
for a = 1:length(models.options.PPIDs.total)
    comp.RTs.values(a) = nanmean(models.rawData{a}.data.pred_rt);
end

% Calculate mean, std, median and inter-quartile range for:
% Reaction times: Total
comp.RTs.groupValues.totMean = mean(comp.RTs.values);
comp.RTs.groupValues.totStd = std(comp.RTs.values);
comp.RTs.groupValues.totMed = median(comp.RTs.values);
comp.RTs.groupValues.totIqr = iqr(comp.RTs.values);
% Reaction times: Low group
comp.RTs.groupValues.lowMean = mean(comp.RTs.values(models.options.PPIDs.low.idx));
comp.RTs.groupValues.lowStd = std(comp.RTs.values(models.options.PPIDs.low.idx));
comp.RTs.groupValues.lowMed = median(comp.RTs.values(models.options.PPIDs.low.idx));
comp.RTs.groupValues.lowIqr = iqr(comp.RTs.values(models.options.PPIDs.low.idx));
% Reaction times: Mod group
comp.RTs.groupValues.modMean = mean(comp.RTs.values(models.options.PPIDs.mod.idx));
comp.RTs.groupValues.modStd = std(comp.RTs.values(models.options.PPIDs.mod.idx));
comp.RTs.groupValues.modMed = median(comp.RTs.values(models.options.PPIDs.mod.idx));
comp.RTs.groupValues.modIqr = iqr(comp.RTs.values(models.options.PPIDs.mod.idx));

% Compare reaction times between groups
comp.RTs.groupComp.alpha = 0.05;
comp.RTs.groupComp.tails = 'two';
comp.RTs.adtest.low = adtest(comp.RTs.values(models.options.PPIDs.low.idx));
comp.RTs.adtest.mod = adtest(comp.RTs.values(models.options.PPIDs.mod.idx));
if comp.RTs.adtest.low == 1 || comp.RTs.adtest.mod == 1
    comp.RTs.groupComp.test = 'Wcxn_rankSum';
    [comp.RTs.groupComp.p, comp.RTs.groupComp.h, comp.RTs.groupComp.stats] = ranksum(comp.RTs.values(models.options.PPIDs.low.idx), comp.RTs.values(models.options.PPIDs.mod.idx), 'alpha', comp.RTs.groupComp.alpha);
else
    comp.RTs.groupComp.test = 'tTest2';
    [comp.RTs.groupComp.h, comp.RTs.groupComp.p, comp.RTs.groupComp.ci, comp.RTs.groupComp.stats] = ttest2(comp.RTs.values(models.options.PPIDs.low.idx), comp.RTs.values(models.options.PPIDs.mod.idx), 'Alpha', comp.RTs.groupComp.alpha);
end

% Print reaction time comparison results to screen
fprintf('\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('REACTION TIME COMPARISON RESULTS (sig: %1.3f, tails: %s):\n', comp.RTs.groupComp.alpha, comp.RTs.groupComp.tails);
fprintf('Whole group RT (s): mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.RTs.groupValues.totMean, comp.RTs.groupValues.totStd, comp.RTs.groupValues.totMed, comp.RTs.groupValues.totIqr);
fprintf('Low group RT (s): mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.RTs.groupValues.lowMean, comp.RTs.groupValues.lowStd, comp.RTs.groupValues.lowMed, comp.RTs.groupValues.lowIqr);
fprintf('Mod group RT (s): mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.RTs.groupValues.modMean, comp.RTs.groupValues.modStd, comp.RTs.groupValues.modMed, comp.RTs.groupValues.modIqr);
fprintf('Low vs. mod RT: %s p = %1.3f\n', comp.RTs.groupComp.test, comp.RTs.groupComp.p); 
disp('-------------------------------------------------------------------------------------------');
fprintf('\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN EXPLORATORY ANALYSIS FOR PREDICTION DECISIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pull out decisions
for a = 1:length(models.options.PPIDs.total)
    comp.decisions.values(a) = nanmean(models.rawData{a}.data.pred_answer);
end

% Calculate mean, std, median and inter-quartile range for:
% Decisions: Total
comp.decisions.groupValues.totMean = mean(comp.decisions.values);
comp.decisions.groupValues.totStd = std(comp.decisions.values);
comp.decisions.groupValues.totMed = median(comp.decisions.values);
comp.decisions.groupValues.totIqr = iqr(comp.decisions.values);
% Decisions: Low group
comp.decisions.groupValues.lowMean = mean(comp.decisions.values(models.options.PPIDs.low.idx));
comp.decisions.groupValues.lowStd = std(comp.decisions.values(models.options.PPIDs.low.idx));
comp.decisions.groupValues.lowMed = median(comp.decisions.values(models.options.PPIDs.low.idx));
comp.decisions.groupValues.lowIqr = iqr(comp.decisions.values(models.options.PPIDs.low.idx));
% Decisions: Mod group
comp.decisions.groupValues.modMean = mean(comp.decisions.values(models.options.PPIDs.mod.idx));
comp.decisions.groupValues.modStd = std(comp.decisions.values(models.options.PPIDs.mod.idx));
comp.decisions.groupValues.modMed = median(comp.decisions.values(models.options.PPIDs.mod.idx));
comp.decisions.groupValues.modIqr = iqr(comp.decisions.values(models.options.PPIDs.mod.idx));

% Compare decision between groups
comp.decisions.groupComp.alpha = 0.05;
comp.decisions.groupComp.tails = 'two';
comp.decisions.adtest.low = adtest(comp.decisions.values(models.options.PPIDs.low.idx));
comp.decisions.adtest.mod = adtest(comp.decisions.values(models.options.PPIDs.mod.idx));
if comp.decisions.adtest.low == 1 || comp.decisions.adtest.mod == 1
    comp.decisions.groupComp.test = 'Wcxn_rankSum';
    [comp.decisions.groupComp.p, comp.decisions.groupComp.h, comp.decisions.groupComp.stats] = ranksum(comp.decisions.values(models.options.PPIDs.low.idx), comp.decisions.values(models.options.PPIDs.mod.idx), 'alpha', comp.decisions.groupComp.alpha);
else
    comp.decisions.groupComp.test = 'tTest2';
    [comp.decisions.groupComp.h, comp.decisions.groupComp.p, comp.decisions.groupComp.ci, comp.decisions.groupComp.stats] = ttest2(comp.decisions.values(models.options.PPIDs.low.idx), comp.decisions.values(models.options.PPIDs.mod.idx), 'Alpha', comp.decisions.groupComp.alpha);
end

% Print decision comparison results to screen
fprintf('\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('PREDICTION DECISION COMPARISON RESULTS (sig: %1.3f, tails: %s):\n', comp.decisions.groupComp.alpha, comp.decisions.groupComp.tails);
fprintf('Whole group decisions (s): mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.decisions.groupValues.totMean, comp.decisions.groupValues.totStd, comp.decisions.groupValues.totMed, comp.decisions.groupValues.totIqr);
fprintf('Low group decisions (s): mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.decisions.groupValues.lowMean, comp.decisions.groupValues.lowStd, comp.decisions.groupValues.lowMed, comp.decisions.groupValues.lowIqr);
fprintf('Mod group decisions (s): mean ± std = %1.2f ± %1.2f; med ± iqr = %1.2f ± %1.2f\n', comp.decisions.groupValues.modMean, comp.decisions.groupValues.modStd, comp.decisions.groupValues.modMed, comp.decisions.groupValues.modIqr);
fprintf('Low vs. mod decisions: %s p = %1.3f\n', comp.decisions.groupComp.test, comp.decisions.groupComp.p); 
disp('-------------------------------------------------------------------------------------------');
fprintf('\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN EXPLORATORY ANALYSES FOR PHYSIOLOGY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pull out all physiological values
comp.phys.names = {'avgPressure', 'maxPressure', 'avgBreathingRate', 'avgBreathingDepth', 'heartRate'};
for a = 1:length(comp.options.PPIDs.total)
    comp.phys.values.stimulus(a,1) = phys.rawData{a}.physValues.summary.stimulus.absolute.avgPressureMinusRest;
    comp.phys.values.noStimulus(a,1) = phys.rawData{a}.physValues.summary.noStimulus.absolute.avgPressureMinusRest;
    comp.phys.values.stimulus(a,2) = phys.rawData{a}.physValues.summary.stimulus.absolute.maxPressureMinusRest;
    comp.phys.values.noStimulus(a,2) = phys.rawData{a}.physValues.summary.noStimulus.absolute.maxPressureMinusRest;
    comp.phys.values.stimulus(a,3) = phys.rawData{a}.physValues.summary.stimulus.absolute.avgBreathingRate;
    comp.phys.values.noStimulus(a,3) = phys.rawData{a}.physValues.summary.noStimulus.absolute.avgBreathingRate;
    comp.phys.values.stimulus(a,4) = phys.rawData{a}.physValues.summary.stimulus.relative.avgBreathingDepth;
    comp.phys.values.noStimulus(a,4) = phys.rawData{a}.physValues.summary.noStimulus.relative.avgBreathingDepth;
    if isfield(phys.rawData{a}.physValues.summary.stimulus.absolute, 'heartRate')
        comp.phys.values.stimulus(a,5) = phys.rawData{a}.physValues.summary.stimulus.absolute.heartRate;
        comp.phys.values.noStimulus(a,5) = phys.rawData{a}.physValues.summary.noStimulus.absolute.heartRate;
    else
        comp.phys.values.stimulus(a,5) = NaN;
        comp.phys.values.noStimulus(a,5) = NaN;
    end
    comp.phys.values.sighs(a) = phys.rawData{a}.physValues.summary.sighs.sighTotal;
    comp.phys.values.debs(a) = phys.rawData{a}.physValues.summary.noStimulus.debsTotal;
end

% Calculate mean, std, median and inter-quartile range for:
% Stimulus periods: Total
comp.phys.groupValues.stimulus.totMean = nanmean(comp.phys.values.stimulus);
comp.phys.groupValues.stimulus.totStd = nanstd(comp.phys.values.stimulus);
comp.phys.groupValues.stimulus.totMed = nanmedian(comp.phys.values.stimulus);
comp.phys.groupValues.stimulus.totIqr = iqr(comp.phys.values.stimulus);
% Stimulus periods: Low group
comp.phys.groupValues.stimulus.lowMean = nanmean(comp.phys.values.stimulus(comp.options.PPIDs.low.idx,:));
comp.phys.groupValues.stimulus.lowStd = nanstd(comp.phys.values.stimulus(comp.options.PPIDs.low.idx,:));
comp.phys.groupValues.stimulus.lowMed = nanmedian(comp.phys.values.stimulus(comp.options.PPIDs.low.idx,:));
comp.phys.groupValues.stimulus.lowIqr = iqr(comp.phys.values.stimulus(comp.options.PPIDs.low.idx,:));
% Stimulus periods: Mod group
comp.phys.groupValues.stimulus.modMean = nanmean(comp.phys.values.stimulus(comp.options.PPIDs.mod.idx,:));
comp.phys.groupValues.stimulus.modStd = nanstd(comp.phys.values.stimulus(comp.options.PPIDs.mod.idx,:));
comp.phys.groupValues.stimulus.modMed = nanmedian(comp.phys.values.stimulus(comp.options.PPIDs.mod.idx,:));
comp.phys.groupValues.stimulus.modIqr = iqr(comp.phys.values.stimulus(comp.options.PPIDs.mod.idx,:));
% No-stimulus periods: Total
comp.phys.groupValues.noStimulus.totMean = nanmean(comp.phys.values.noStimulus);
comp.phys.groupValues.noStimulus.totStd = nanstd(comp.phys.values.noStimulus);
comp.phys.groupValues.noStimulus.totMed = nanmedian(comp.phys.values.noStimulus);
comp.phys.groupValues.noStimulus.totIqr = iqr(comp.phys.values.noStimulus);
% No-stimulus periods: Low group
comp.phys.groupValues.noStimulus.lowMean = nanmean(comp.phys.values.noStimulus(comp.options.PPIDs.low.idx,:));
comp.phys.groupValues.noStimulus.lowStd = nanstd(comp.phys.values.noStimulus(comp.options.PPIDs.low.idx,:));
comp.phys.groupValues.noStimulus.lowMed = nanmedian(comp.phys.values.noStimulus(comp.options.PPIDs.low.idx,:));
comp.phys.groupValues.noStimulus.lowIqr = iqr(comp.phys.values.noStimulus(comp.options.PPIDs.low.idx,:));
% No-stimulus periods: Mod group
comp.phys.groupValues.noStimulus.modMean = nanmean(comp.phys.values.noStimulus(comp.options.PPIDs.mod.idx,:));
comp.phys.groupValues.noStimulus.modStd = nanstd(comp.phys.values.noStimulus(comp.options.PPIDs.mod.idx,:));
comp.phys.groupValues.noStimulus.modMed = nanmedian(comp.phys.values.noStimulus(comp.options.PPIDs.mod.idx,:));
comp.phys.groupValues.noStimulus.modIqr = iqr(comp.phys.values.noStimulus(comp.options.PPIDs.mod.idx,:));
% Sighs: Total
comp.phys.groupValues.sighs.totMean = mean(comp.phys.values.sighs);
comp.phys.groupValues.sighs.totStd = std(comp.phys.values.sighs);
comp.phys.groupValues.sighs.totMed = median(comp.phys.values.sighs);
comp.phys.groupValues.sighs.totIqr = iqr(comp.phys.values.sighs);
% Sighs: Low group
comp.phys.groupValues.sighs.lowMean = mean(comp.phys.values.sighs(comp.options.PPIDs.low.idx));
comp.phys.groupValues.sighs.lowStd = std(comp.phys.values.sighs(comp.options.PPIDs.low.idx));
comp.phys.groupValues.sighs.lowMed = median(comp.phys.values.sighs(comp.options.PPIDs.low.idx));
comp.phys.groupValues.sighs.lowIqr = iqr(comp.phys.values.sighs(comp.options.PPIDs.low.idx));
% Sighs: Mod group
comp.phys.groupValues.sighs.modMean = mean(comp.phys.values.sighs(comp.options.PPIDs.mod.idx));
comp.phys.groupValues.sighs.modStd = std(comp.phys.values.sighs(comp.options.PPIDs.mod.idx));
comp.phys.groupValues.sighs.modMed = median(comp.phys.values.sighs(comp.options.PPIDs.mod.idx));
comp.phys.groupValues.sighs.modIqr = iqr(comp.phys.values.sighs(comp.options.PPIDs.mod.idx));
% Debs: Total
comp.phys.groupValues.debs.totMean = mean(comp.phys.values.debs);
comp.phys.groupValues.debs.totStd = std(comp.phys.values.debs);
comp.phys.groupValues.debs.totMed = median(comp.phys.values.debs);
comp.phys.groupValues.debs.totIqr = iqr(comp.phys.values.debs);
% Debs: Low group
comp.phys.groupValues.debs.lowMean = mean(comp.phys.values.debs(comp.options.PPIDs.low.idx));
comp.phys.groupValues.debs.lowStd = std(comp.phys.values.debs(comp.options.PPIDs.low.idx));
comp.phys.groupValues.debs.lowMed = median(comp.phys.values.debs(comp.options.PPIDs.low.idx));
comp.phys.groupValues.debs.lowIqr = iqr(comp.phys.values.debs(comp.options.PPIDs.low.idx));
% Debs: Mod group
comp.phys.groupValues.debs.modMean = mean(comp.phys.values.debs(comp.options.PPIDs.mod.idx));
comp.phys.groupValues.debs.modStd = std(comp.phys.values.debs(comp.options.PPIDs.mod.idx));
comp.phys.groupValues.debs.modMed = median(comp.phys.values.debs(comp.options.PPIDs.mod.idx));
comp.phys.groupValues.debs.modIqr = iqr(comp.phys.values.debs(comp.options.PPIDs.mod.idx));

% Compare physiological values between groups
comp.phys.groupComp.alpha = 0.05;
comp.phys.groupComp.tails = 'two';
for a = 1:length(comp.phys.names)
    comp.phys.adtest.stimulus.low(a) = adtest(comp.phys.values.stimulus(models.options.PPIDs.low.idx,a));
    comp.phys.adtest.stimulus.mod(a) = adtest(comp.phys.values.stimulus(models.options.PPIDs.mod.idx,a));
    if comp.phys.adtest.stimulus.low(a) == 1 || comp.phys.adtest.stimulus.mod(a) == 1
        comp.phys.groupComp.stimulus{a}.test = 'Wcxn_rankSum';
        [comp.phys.groupComp.stimulus{a}.p, comp.phys.groupComp.stimulus{a}.h, comp.phys.groupComp.stimulus{a}.stats] = ranksum(comp.phys.values.stimulus(models.options.PPIDs.low.idx,a), comp.phys.values.stimulus(models.options.PPIDs.mod.idx,a));
    else
        comp.phys.groupComp.stimulus{a}.test = 'tTest2';
        [comp.phys.groupComp.stimulus{a}.h, comp.phys.groupComp.stimulus{a}.p, comp.phys.groupComp.stimulus{a}.ci, comp.phys.groupComp.stimulus{a}.stats] = ttest2(comp.phys.values.stimulus(models.options.PPIDs.low.idx,a), comp.phys.values.stimulus(models.options.PPIDs.mod.idx,a));
    end
    comp.phys.adtest.noStimulus.low(a) = adtest(comp.phys.values.noStimulus(models.options.PPIDs.low.idx,a));
    comp.phys.adtest.noStimulus.mod(a) = adtest(comp.phys.values.noStimulus(models.options.PPIDs.mod.idx,a));
    if comp.phys.adtest.noStimulus.low(a) == 1 || comp.phys.adtest.noStimulus.mod(a) == 1
        comp.phys.groupComp.noStimulus{a}.test = 'Wcxn_rankSum';
        [comp.phys.groupComp.noStimulus{a}.p, comp.phys.groupComp.noStimulus{a}.h, comp.phys.groupComp.noStimulus{a}.stats] = ranksum(comp.phys.values.noStimulus(models.options.PPIDs.low.idx,a), comp.phys.values.noStimulus(models.options.PPIDs.mod.idx,a));
    else
        comp.phys.groupComp.noStimulus{a}.test = 'tTest2';
        [comp.phys.groupComp.noStimulus{a}.h, comp.phys.groupComp.noStimulus{a}.p, comp.phys.groupComp.noStimulus{a}.ci, comp.phys.groupComp.noStimulus{a}.stats] = ttest2(comp.phys.values.noStimulus(models.options.PPIDs.low.idx,a), comp.phys.values.noStimulus(models.options.PPIDs.mod.idx,a));
    end
end

% Compare sighs and debs between groups
comp.phys.adtest.sighs.low = adtest(comp.phys.values.sighs(models.options.PPIDs.low.idx));
comp.phys.adtest.sighs.mod = adtest(comp.phys.values.sighs(models.options.PPIDs.mod.idx));
if comp.phys.adtest.sighs.low == 1 || comp.phys.adtest.sighs.mod == 1
    comp.phys.groupComp.sighs.test = 'Wcxn_rankSum';
    [comp.phys.groupComp.sighs.p, comp.phys.groupComp.sighs.h, comp.phys.groupComp.sighs.stats] = ranksum(comp.phys.values.sighs(models.options.PPIDs.low.idx), comp.phys.values.sighs(models.options.PPIDs.mod.idx));
else
    comp.phys.groupComp.sighs.test = 'tTest2';
    [comp.phys.groupComp.sighs.h, comp.phys.groupComp.sighs.p, comp.phys.groupComp.sighs.ci, comp.phys.groupComp.sighs.stats] = ttest2(comp.phys.values.sighs(models.options.PPIDs.low.idx), comp.phys.values.sighs(models.options.PPIDs.mod.idx));
end
comp.phys.adtest.debs.low = adtest(comp.phys.values.debs(models.options.PPIDs.low.idx));
comp.phys.adtest.debs.mod = adtest(comp.phys.values.debs(models.options.PPIDs.mod.idx));
if comp.phys.adtest.debs.low == 1 || comp.phys.adtest.debs.mod == 1
    comp.phys.groupComp.debs.test = 'Wcxn_rankSum';
    [comp.phys.groupComp.debs.p, comp.phys.groupComp.debs.h, comp.phys.groupComp.debs.stats] = ranksum(comp.phys.values.debs(models.options.PPIDs.low.idx), comp.phys.values.debs(models.options.PPIDs.mod.idx));
else
    comp.phys.groupComp.debs.test = 'tTest2';
    [comp.phys.groupComp.debs.h, comp.phys.groupComp.debs.p, comp.phys.groupComp.debs.ci, comp.phys.groupComp.debs.stats] = ttest2(comp.phys.values.debs(models.options.PPIDs.low.idx), comp.phys.values.debs(models.options.PPIDs.mod.idx));
end

% Create summary tables for mean and std
test_stimulus = {comp.phys.groupComp.stimulus{1}.test, comp.phys.groupComp.stimulus{2}.test, comp.phys.groupComp.stimulus{3}.test, comp.phys.groupComp.stimulus{4}.test, comp.phys.groupComp.stimulus{5}.test};
pValues_stimulus = [comp.phys.groupComp.stimulus{1}.p, comp.phys.groupComp.stimulus{2}.p, comp.phys.groupComp.stimulus{3}.p, comp.phys.groupComp.stimulus{4}.p, comp.phys.groupComp.stimulus{5}.p];
test_noStimulus = {comp.phys.groupComp.noStimulus{1}.test, comp.phys.groupComp.noStimulus{2}.test, comp.phys.groupComp.noStimulus{3}.test, comp.phys.groupComp.noStimulus{4}.test, comp.phys.groupComp.noStimulus{5}.test};
pValues_noStimulus = [comp.phys.groupComp.noStimulus{1}.p, comp.phys.groupComp.noStimulus{2}.p, comp.phys.groupComp.noStimulus{3}.p, comp.phys.groupComp.noStimulus{4}.p, comp.phys.groupComp.noStimulus{5}.p];
test_sighsDebs = {comp.phys.groupComp.sighs.test, comp.phys.groupComp.debs.test};
pValues_sighsDebs = [comp.phys.groupComp.sighs.p, comp.phys.groupComp.debs.p];
VarNames = {'groupMean', 'groupStd', 'lowMean', 'lowStd', 'modMean', 'modStd', 'Test', 'Pvalue'};
comp.phys.tables.mean.stimulus = table(comp.phys.groupValues.stimulus.totMean', comp.phys.groupValues.stimulus.totStd', comp.phys.groupValues.stimulus.lowMean', comp.phys.groupValues.stimulus.lowStd', comp.phys.groupValues.stimulus.modMean', comp.phys.groupValues.stimulus.modStd', test_stimulus', pValues_stimulus', 'VariableNames', VarNames, 'RowNames', comp.phys.names);
comp.phys.tables.mean.noStimulus = table(comp.phys.groupValues.noStimulus.totMean', comp.phys.groupValues.noStimulus.totStd', comp.phys.groupValues.noStimulus.lowMean', comp.phys.groupValues.noStimulus.lowStd', comp.phys.groupValues.noStimulus.modMean', comp.phys.groupValues.noStimulus.modStd', test_noStimulus', pValues_noStimulus', 'VariableNames', VarNames, 'RowNames', comp.phys.names);
comp.phys.tables.mean.sighsAndDebs = table([comp.phys.groupValues.sighs.totMean, comp.phys.groupValues.debs.totMean]', [comp.phys.groupValues.sighs.totStd, comp.phys.groupValues.debs.totStd]', [comp.phys.groupValues.sighs.lowMean, comp.phys.groupValues.debs.lowMean]', [comp.phys.groupValues.sighs.lowStd', comp.phys.groupValues.debs.lowStd]', [comp.phys.groupValues.sighs.modMean, comp.phys.groupValues.debs.modMean]', ...
    [comp.phys.groupValues.sighs.modStd, comp.phys.groupValues.debs.modStd]', test_sighsDebs', pValues_sighsDebs', 'VariableNames', VarNames, 'RowNames', {'Sighs', 'DEBS'});

% Create summary tables for median and iqr
VarNames = {'groupMedian', 'groupIqr', 'lowMedian', 'lowIqr', 'modMedian', 'modIqr', 'Test', 'Pvalue'};
comp.phys.tables.median.stimulus = table(comp.phys.groupValues.stimulus.totMed', comp.phys.groupValues.stimulus.totIqr', comp.phys.groupValues.stimulus.lowMed', comp.phys.groupValues.stimulus.lowIqr', comp.phys.groupValues.stimulus.modMed', comp.phys.groupValues.stimulus.modIqr', test_stimulus', pValues_stimulus', 'VariableNames', VarNames, 'RowNames', comp.phys.names);
comp.phys.tables.median.noStimulus = table(comp.phys.groupValues.noStimulus.totMed', comp.phys.groupValues.noStimulus.totIqr', comp.phys.groupValues.noStimulus.lowMed', comp.phys.groupValues.noStimulus.lowIqr', comp.phys.groupValues.noStimulus.modMed', comp.phys.groupValues.noStimulus.modIqr', test_noStimulus', pValues_noStimulus', 'VariableNames', VarNames, 'RowNames', comp.phys.names);
comp.phys.tables.median.sighsAndDebs = table([comp.phys.groupValues.sighs.totMed, comp.phys.groupValues.debs.totMed]', [comp.phys.groupValues.sighs.totIqr, comp.phys.groupValues.debs.totIqr]', [comp.phys.groupValues.sighs.lowMed, comp.phys.groupValues.debs.lowMed]', [comp.phys.groupValues.sighs.lowIqr', comp.phys.groupValues.debs.lowIqr]', [comp.phys.groupValues.sighs.modMed, comp.phys.groupValues.debs.modMed]', ...
    [comp.phys.groupValues.sighs.modIqr, comp.phys.groupValues.debs.modIqr]', test_sighsDebs', pValues_sighsDebs', 'VariableNames', VarNames, 'RowNames', {'Sighs', 'DEBS'});

% Print physiology results to screen: Mean ± std
fprintf('\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('PHYSIOLOGICAL COMPARISON RESULTS: MEAN ± STD (sig: %1.3f, tails: %s):\n', comp.phys.groupComp.alpha, comp.phys.groupComp.tails);
disp('-------------------------------------------------------------------------------------------');
fprintf('\nRESISTANCE PERIODS:\n');
comp.phys.tables.mean.stimulus
disp('-------------------------------------------------------------------------------------------');
fprintf('\nNO RESISTANCE PERIODS:\n');
comp.phys.tables.mean.noStimulus
disp('-------------------------------------------------------------------------------------------');
fprintf('\nSIGHS AND DEEP BREATHS:\n');
comp.phys.tables.mean.sighsAndDebs
disp('-------------------------------------------------------------------------------------------');
fprintf('\n');

% Print physiology results to screen: Median ± iqr
fprintf('\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('PHYSIOLOGICAL COMPARISON RESULTS: MEDIAN ± IQR (sig: %1.3f, tails: %s):\n', comp.phys.groupComp.alpha, comp.phys.groupComp.tails);
disp('-------------------------------------------------------------------------------------------');
fprintf('\nRESISTANCE PERIODS:\n');
comp.phys.tables.median.stimulus
disp('-------------------------------------------------------------------------------------------');
fprintf('\nNO RESISTANCE PERIODS:\n');
comp.phys.tables.median.noStimulus
disp('-------------------------------------------------------------------------------------------');
fprintf('\nSIGHS AND DEEP BREATHS:\n');
comp.phys.tables.median.sighsAndDebs
disp('-------------------------------------------------------------------------------------------');
fprintf('\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN CHECK OF MOTION PARAMETERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pull out all physiological values
for a = 1:length(comp.options.PPIDs.total)
    comp.motion.values.relative(a) = mean(motion.relative.rawData{a});
    comp.motion.values.absolute(a) = mean(motion.absolute.rawData{a});
end

% Calculate mean, std, median and inter-quartile range for:
% Relative motion: Total
comp.motion.groupValues.totRelMean = mean(comp.motion.values.relative);
comp.motion.groupValues.totRelStd = std(comp.motion.values.relative);
comp.motion.groupValues.totRelMed = median(comp.motion.values.relative);
comp.motion.groupValues.totRelIqr = iqr(comp.motion.values.relative);
% Relative motion: Low group
comp.motion.groupValues.lowRelMean = mean(comp.motion.values.relative(models.options.PPIDs.low.idx));
comp.motion.groupValues.lowRelStd = std(comp.motion.values.relative(models.options.PPIDs.low.idx));
comp.motion.groupValues.lowRelMed = median(comp.motion.values.relative(models.options.PPIDs.low.idx));
comp.motion.groupValues.lowRelIqr = iqr(comp.motion.values.relative(models.options.PPIDs.low.idx));
% Relative motion: Mod group
comp.motion.groupValues.modRelMean = mean(comp.motion.values.relative(models.options.PPIDs.mod.idx));
comp.motion.groupValues.modRelStd = std(comp.motion.values.relative(models.options.PPIDs.mod.idx));
comp.motion.groupValues.modRelMed = median(comp.motion.values.relative(models.options.PPIDs.mod.idx));
comp.motion.groupValues.modRelIqr = iqr(comp.motion.values.relative(models.options.PPIDs.mod.idx));
% Absolute motion: Total
comp.motion.groupValues.totAbsMean = mean(comp.motion.values.absolute);
comp.motion.groupValues.totAbsStd = std(comp.motion.values.absolute);
comp.motion.groupValues.totAbsMed = median(comp.motion.values.absolute);
comp.motion.groupValues.totAbsIqr = iqr(comp.motion.values.absolute);
% Absolute motion: Low group
comp.motion.groupValues.lowAbsMean = mean(comp.motion.values.absolute(models.options.PPIDs.low.idx));
comp.motion.groupValues.lowAbsStd = std(comp.motion.values.absolute(models.options.PPIDs.low.idx));
comp.motion.groupValues.lowAbsMed = median(comp.motion.values.absolute(models.options.PPIDs.low.idx));
comp.motion.groupValues.lowAbsIqr = iqr(comp.motion.values.absolute(models.options.PPIDs.low.idx));
% Absolute motion: Mod group
comp.motion.groupValues.modAbsMean = mean(comp.motion.values.absolute(models.options.PPIDs.mod.idx));
comp.motion.groupValues.modAbsStd = std(comp.motion.values.absolute(models.options.PPIDs.mod.idx));
comp.motion.groupValues.modAbsMed = median(comp.motion.values.absolute(models.options.PPIDs.mod.idx));
comp.motion.groupValues.modAbsIqr = iqr(comp.motion.values.absolute(models.options.PPIDs.mod.idx));

% Compare motion between groups
comp.motion.groupComp.alpha = 0.05;
comp.motion.groupComp.tails = 'one';
comp.motion.adtests.relative.low = adtest(comp.motion.values.relative(models.options.PPIDs.low.idx));
comp.motion.adtests.relative.mod = adtest(comp.motion.values.relative(models.options.PPIDs.mod.idx));
if comp.motion.adtests.relative.low == 1 || comp.motion.adtests.relative.mod == 1
    comp.motion.groupComp.relative.test = 'Wcxn_rankSum';
    [comp.motion.groupComp.relative.p, comp.motion.groupComp.relative.h, comp.motion.groupComp.relative.stats] = ranksum(comp.motion.values.relative(models.options.PPIDs.low.idx), comp.motion.values.relative(models.options.PPIDs.mod.idx));
else
    comp.motion.groupComp.relative.test = 'tTest2';
    [comp.motion.groupComp.relative.h, comp.motion.groupComp.relative.p, comp.motion.groupComp.relative.ci, comp.motion.groupComp.relative.stats] = ttest2(comp.motion.values.relative(models.options.PPIDs.low.idx), comp.motion.values.relative(models.options.PPIDs.mod.idx));
end
comp.motion.adtests.absolute.low = adtest(comp.motion.values.absolute(models.options.PPIDs.low.idx));
comp.motion.adtests.absolute.mod = adtest(comp.motion.values.absolute(models.options.PPIDs.mod.idx));
if comp.motion.adtests.absolute.low == 1 || comp.motion.adtests.absolute.mod == 1
    comp.motion.groupComp.absolute.test = 'Wcxn_rankSum';
    [comp.motion.groupComp.absolute.p, comp.motion.groupComp.absolute.h, comp.motion.groupComp.absolute.stats] = ranksum(comp.motion.values.absolute(models.options.PPIDs.low.idx), comp.motion.values.absolute(models.options.PPIDs.mod.idx));
else
    comp.motion.groupComp.absolute.test = 'tTest2';
    [comp.motion.groupComp.absolute.h, comp.motion.groupComp.absolute.p, comp.motion.groupComp.absolute.ci, comp.motion.groupComp.absolute.stats] = ttest2(comp.motion.values.absolute(models.options.PPIDs.low.idx), comp.motion.values.absolute(models.options.PPIDs.mod.idx));
end

% Print motion comparison results to screen
fprintf('\n');
disp('-------------------------------------------------------------------------------------------');
fprintf('MOTION COMPARISON RESULTS (sig: %1.3f, tails: %s):\n', comp.motion.groupComp.alpha, comp.motion.groupComp.tails);
fprintf('Whole group relative motion: mean ± std = %1.2f ± %1.2f; median ± iqr = %1.2f ± %1.2f\n', comp.motion.groupValues.totRelMean, comp.motion.groupValues.totRelStd, comp.motion.groupValues.totRelMed, comp.motion.groupValues.totRelIqr);
fprintf('Low group relative motion: mean ± std = %1.2f ± %1.2f; median ± iqr = %1.2f ± %1.2f\n', comp.motion.groupValues.lowRelMean, comp.motion.groupValues.lowRelStd, comp.motion.groupValues.lowRelMed, comp.motion.groupValues.lowRelIqr);
fprintf('Mod group relative motion: mean ± std = %1.2f ± %1.2f; median ± iqr = %1.2f ± %1.2f\n', comp.motion.groupValues.modRelMean, comp.motion.groupValues.modRelStd, comp.motion.groupValues.modRelMed, comp.motion.groupValues.modRelIqr);
fprintf('Low vs. mod relative motion: %s p = %1.3f\n', comp.motion.groupComp.relative.test, comp.motion.groupComp.relative.p); 
fprintf('Whole group absolute motion: mean ± std = %1.2f ± %1.2f; median ± iqr = %1.2f ± %1.2f\n', comp.motion.groupValues.totAbsMean, comp.motion.groupValues.totAbsStd, comp.motion.groupValues.totAbsMed, comp.motion.groupValues.totAbsIqr);
fprintf('Low group absolute motion: mean ± std = %1.2f ± %1.2f; median ± iqr = %1.2f ± %1.2f\n', comp.motion.groupValues.lowAbsMean, comp.motion.groupValues.lowAbsStd, comp.motion.groupValues.lowAbsMed, comp.motion.groupValues.lowAbsIqr);
fprintf('Mod group absolute motion: mean ± std = %1.2f ± %1.2f; median ± iqr = %1.2f ± %1.2f\n', comp.motion.groupValues.modAbsMean, comp.motion.groupValues.modAbsStd, comp.motion.groupValues.modAbsMed, comp.motion.groupValues.modAbsIqr);
fprintf('Low vs. mod absolute motion: %s p = %1.3f\n', comp.motion.groupComp.absolute.test, comp.motion.groupComp.absolute.p); 
disp('-------------------------------------------------------------------------------------------');
fprintf('\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE RESULTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check directory existance and make if required
if ~exist(comp.options.paths.group_directory, 'dir')
    mkdir(comp.options.paths.group_directory);
end
if ~exist(comp.options.paths.group_directory_groupComp, 'dir')
    mkdir(comp.options.paths.group_directory_groupComp);
end

% Save results
save(comp.options.saveNames.groupComp, 'comp');

end