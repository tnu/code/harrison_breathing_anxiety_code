%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% BREATHING SCANNER TASK: CREATE NOISE MATRIX %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 26/03/2020
% -------------------------------------------------------------------------
% TO RUN:   noise = pbihb_createNoiseMatrix(options, physValues, varargin)
% INPUTS:   options     = PPID-specific matrix output from pbihb_setOptions
%           physTraces  = Structure with physiological traces from the
%                         learning task, from pbihb_physiology function
%           varargin    = Specify physio and / or ICA noise matrices
%                         e.g. 'physio', 'ica' or 'ica_orig'
%                         If matrices are not specified they are not added
% OUTPUTS:  noise       = Noise matrix
% -------------------------------------------------------------------------
% DESCRIPTION:
% This function creates the single subject noise matrix to be used in the
% PBIHB functional neuroimaging analyses. Basic noise regressors include
% convolved end-tidal CO2 traces, and optional additions include output
% from PhySIO and ICA analyses.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function noise = pbihb_createNoiseMatrix(options, physValues, varargin)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY OPTIONS AND LOAD REQUIRED DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

noise.options = options;
if isempty(physValues) == 1
    if isfile(options.saveNames.physTaskValues) == 1
        load(options.saveNames.physTaskValues);
    else
        error('No physValues datafile provided or found... Exiting')
    end        
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY INFO FOR RAW GAS TRACES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify onsets, durations and values for raw end tidal CO2 trace with 5 extra initial values
noise.endTidalCO2.raw.onsets = [(linspace(-noise.options.taskScan.tr * 4, 0, 5)), (linspace(noise.options.taskScan.tr, (noise.options.taskScan.vols - 1) * noise.options.taskScan.tr, (noise.options.taskScan.vols - 1)))]; % Add five prior volumes
noise.endTidalCO2.raw.durations = noise.options.taskScan.tr * ones([1 length(noise.endTidalCO2.raw.onsets)]);
noise.endTidalCO2.raw.values = [(physValues.values.endTidalCO2.valuesPerVolume(1) * ones(5,1)); physValues.values.endTidalCO2.valuesPerVolume];
noise.endTidalCO2.raw.valuesDemeaned = noise.endTidalCO2.raw.values - mean(noise.endTidalCO2.raw.values);

% Specify onsets, durations and values for raw end tidal O2 trace with 5 extra initial values
noise.endTidalO2.raw.onsets = [(linspace(-noise.options.taskScan.tr * 4, 0, 5)), (linspace(noise.options.taskScan.tr, (noise.options.taskScan.vols - 1) * noise.options.taskScan.tr, (noise.options.taskScan.vols - 1)))]; % Add five prior volumes
noise.endTidalO2.raw.durations = noise.options.taskScan.tr * ones([1 length(noise.endTidalCO2.raw.onsets)]);
noise.endTidalO2.raw.values = [(physValues.values.endTidalO2.valuesPerVolume(1) * ones(5,1)); physValues.values.endTidalO2.valuesPerVolume];
noise.endTidalO2.raw.valuesDemeaned = noise.endTidalO2.raw.values - mean(noise.endTidalO2.raw.values);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CONVOLVE GAS TRACES WITH ADJUSTED HRF (10 SEC TO PEAK) AND DERIVATIVES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get basis functions for CO2 convolution
noise.convolution.basisFunctions.CO2.dt = noise.options.taskScan.tr;
noise.convolution.basisFunctions.CO2.name = 'hrf (with time and dispersion derivatives)';
noise.convolution.basisFunctions.CO2.length = 36.2;
noise.convolution.basisFunctions.CO2.order = 3;
[noise.convolution.basisFunctions.CO2] = spm_get_bf_CO2(noise.convolution.basisFunctions.CO2);

% Convolve CO2 regressor with basis functions
noise.convolution.endTidalCO2.toConvolve.u = noise.endTidalCO2.raw.values;
noise.convolution.endTidalCO2.toConvolve.name = {'endTidalCO2'}; 
noise.convolution.endTidalCO2.convolvedLong = spm_Volterra(noise.convolution.endTidalCO2.toConvolve, noise.convolution.basisFunctions.CO2.bf);
noise.convolution.endTidalCO2.convolved = noise.convolution.endTidalCO2.convolvedLong(6:end,:);

% Convolve O2 regressor with basis functions
noise.convolution.endTidalO2.toConvolve.u = noise.endTidalO2.raw.values;
noise.convolution.endTidalO2.toConvolve.name = {'endTidalCO2'}; 
noise.convolution.endTidalO2.convolvedLong = spm_Volterra(noise.convolution.endTidalO2.toConvolve, noise.convolution.basisFunctions.CO2.bf);
noise.convolution.endTidalO2.convolved = noise.convolution.endTidalO2.convolvedLong(6:end,:);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE NOISE MATRIX (WITH ADDITIONS SPECIFIED BY FUNCTION INPUTS)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Start the noise matrix with the convolved CO2 regressors
noise.noiseMatrix.values(:,1:3) = noise.convolution.endTidalCO2.convolved;
noise.noiseMatrix.order{1} = 'CO2 convolution';

% Identify if any extra noise matrices to be added from function input
if exist('varargin', 'var')
    addNoise = [];
    for a = 1:length(varargin)
        noise.noiseMatrix.order{a+1} = varargin{a};
        if strcmp(varargin{a}, 'physio')
            addNoise = [addNoise load(noise.options.saveNames.physioRegressors.task)];
        end
        if strcmp(varargin{a}, 'ica')
            addNoise = [addNoise load(noise.options.saveNames.fslPreprocessing.task.noiseFile)];
        end
        if strcmp(varargin{a}, 'ica_orig')
            addNoise = [addNoise load(noise.options.saveNames.fslPreprocessing.task.noiseFile_orig)];
        end
    end
    noise.noiseMatrix.values = [noise.noiseMatrix.values addNoise];
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE OUTPUTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Save out the noise matrix
noiseMatrixOut = noise.noiseMatrix.values;
save(noise.options.saveNames.glmNoise, 'noiseMatrixOut', '-ascii');

% Save out all other info
save(noise.options.saveNames.glmNoiseInfo, 'noise');

end