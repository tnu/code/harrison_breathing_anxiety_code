%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% BREATHING SCANNER TASK: CREATING MODEL GLM %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 26/03/2020
% -------------------------------------------------------------------------
% TO RUN:   glmModel    = pbihb_createGlmModel(options, physValues)
% INPUTS:   options     = PPID-specific matrix output from pbihb_setOptions
%           physValues  = Structure with physiological traces from the
%                         learning task, from pbihb_physiology function
% OUTPUTS:  glmModel    = Structure with glm matrices added
%           figures     = Summary figures are generated to display the
%                         correlation matrices for key model trajectories 
%                         against motion traces
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script creates a model-based task design matrix for input into SPM.
% This GLM is manually convolved and does not use the automatic SPM
% functions for creating GLMs.
% This design includes:
%   1) A regressor for the presentation of all cues (block)
%   2) A modulatory regressor for the values of the positive cues (event), 
%      determined by the model trajectory --> Prediction of no resistance
%   3) A modulatory regressor for the values of the negative cues (event), 
%      determined by the model trajectory --> Prediction of resistance
%   4) A regressor for when the breathing resistance wasn't applied (block)
%   5) A regressor for when the breathing resistance was applied (block)
%   6) A modulatory regressor for the values of the positive stimuli 
%      (event), determined by the model trajectory --> Positive errors 
%      associated with no resistance
%   7) A modulatory regressor for the values of the negative stimuli 
%      (event), determined by the model trajectory --> Negative errors 
%      associated with resistance
%   8) A noise regressor for when the ratings were being made (block)
%   9+) A noise matrix containing the end-tidal CO2 values (one per volume)
%      that are convolved with the SPM basis functions, plus (if specified)
%      the physio noise matrix and ICA noise components
% Three additional GLMs are produced:
%   1) A GLM that contains only prediction trajectories (as above), without
%      any error trajectories (sanity check)
%   2) A GLM that contains only error trajectories (as above), without any
%      prediction trajectories (sanity check)
%   3) A GLM factorises the predictions and errors into linear and
%      quadratic terms, as an alternative to splitting the trajectories 
%      (sanity check)
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function glmModel = pbihb_createGlmModel(options, physValues)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY OPTIONS AND LOAD REQUIRED DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

glmModel.options = options;

% Check for existance of physValues variable
if isempty(physValues) == 1
    if isfile(options.saveNames.physTaskValues) == 1
        load(options.saveNames.physTaskValues);
    else
        error('No physValues datafile provided or found.. Exiting')
    end        
end

% Load model fits
if isfile(options.saveNames.modelFits) == 1
    load(options.saveNames.modelFits);
else
    error('No model fits datafile provided or found.. Exiting')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOCATE WINNING MODEL AND EXTRACT TRAJECTORIES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find PPID index
glmModel.traj.raw.PPIDidx = find(strcmp(glmModel.options.PPID, models.options.PPIDs.total));

% Pull out trajectory of winning model
glmModel.model.name = models.est.comp.total.winner.name;
for a = 1:length(models.est.fits)
    glmModel.fits{a} = models.est.fits{a}{glmModel.traj.raw.PPIDidx};
end
if strcmp(glmModel.model.name, 'Rescorla-Wagner') == 1
    glmModel.traj.raw.predict_orig = models.est.fits{models.est.comp.total.winner.idx}{glmModel.traj.raw.PPIDidx}.traj.vhat;
    glmModel.traj.raw.predict = glmModel.traj.raw.predict_orig - 0.5;
    glmModel.traj.raw.error = models.est.fits{models.est.comp.total.winner.idx}{glmModel.traj.raw.PPIDidx}.traj.da;
elseif strcmp(glmModel.model.name, '2-level HGF') == 1 || strcmp(glmModel.model.name, '3-level HGF') == 1
    glmModel.traj.raw.predict = models.est.fits{models.est.comp.total.winner.idx}{glmModel.traj.raw.PPIDidx}.traj.muhat;
    glmModel.traj.raw.error = models.est.fits{models.est.comp.total.winner.idx}{glmModel.traj.raw.PPIDidx}.traj.epsi;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPLIT TRAJECTORIES ACCORDING TO VALENCE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Flip trajectories for alternative clue
glmModel.traj.raw.predict_flipped = -glmModel.traj.raw.predict;
glmModel.traj.raw.error_flipped = -glmModel.traj.raw.error;
    
% Re-code predictions and prediction errors for stimulus meaning / valence
glmModel.traj.twoCues.total.predict.values = [];
glmModel.traj.twoCues.total.error.values = [];
for n = 1:length(glmModel.traj.raw.predict)                                                            
    if models.rawData{glmModel.traj.raw.PPIDidx}.params.cue(n) == 1                                          % If cue = 1 (expect resistance in first block)
        glmModel.traj.twoCues.total.predict.values(n,1) = glmModel.traj.raw.predict_flipped(n);              % Prediction starts as resistance / negative (from flipped trace)
        glmModel.traj.twoCues.total.error.values(n,1) = glmModel.traj.raw.error_flipped(n);                  % Prediction error (no resistance when you expect it) starts as positive (from flipped trace)
    elseif models.rawData{glmModel.traj.raw.PPIDidx}.params.cue(n) == 2                                      % Else if cue = 2 (do not expect resistance in first block)
        glmModel.traj.twoCues.total.predict.values(n,1) = glmModel.traj.raw.predict(n);                      % Prediction starts as positive (from original trace)
        glmModel.traj.twoCues.total.error.values(n,1) = glmModel.traj.raw.error(n);                          % Prediction error (resistance when you don't expect it) starts as negative (from original trace)
    end
end

% Split predictions and prediction errors according to stimulus meaning / valence
glmModel.traj.twoCues.split.positive.predict.values = glmModel.traj.twoCues.total.predict.values(glmModel.traj.twoCues.total.predict.values > 0); % Identify trials when the model says prediction is towards no-resistance
glmModel.traj.twoCues.split.negative.predict.values = glmModel.traj.twoCues.total.predict.values(glmModel.traj.twoCues.total.predict.values < 0); % Identify trials when the model says prediction is towards resistance
glmModel.traj.twoCues.split.positive.error.values = glmModel.traj.twoCues.total.error.values(models.rawData{glmModel.traj.raw.PPIDidx}.params.resist == 0); % Identify trials when no-resistance occurred
glmModel.traj.twoCues.split.negative.error.values = glmModel.traj.twoCues.total.error.values(models.rawData{glmModel.traj.raw.PPIDidx}.params.resist == 1); % Identify trials when resistance occurred

% De-mean all predictions and prediction errors
glmModel.traj.twoCues.total.predict.valuesDemeaned = glmModel.traj.twoCues.total.predict.values - mean(glmModel.traj.twoCues.total.predict.values);
glmModel.traj.twoCues.split.positive.predict.valuesDemeaned = glmModel.traj.twoCues.split.positive.predict.values - mean(glmModel.traj.twoCues.split.positive.predict.values);
glmModel.traj.twoCues.split.negative.predict.valuesDemeaned = glmModel.traj.twoCues.split.negative.predict.values - mean(glmModel.traj.twoCues.split.negative.predict.values);
glmModel.traj.twoCues.total.error.valuesDemeaned = glmModel.traj.twoCues.total.error.values - mean(glmModel.traj.twoCues.total.error.values);
glmModel.traj.twoCues.split.positive.error.valuesDemeaned = glmModel.traj.twoCues.split.positive.error.values - mean(glmModel.traj.twoCues.split.positive.error.values);
glmModel.traj.twoCues.split.negative.error.valuesDemeaned = glmModel.traj.twoCues.split.negative.error.values - mean(glmModel.traj.twoCues.split.negative.error.values);

% Specify onsets and volumes for total predictions and errors (according to valence)
glmModel.traj.twoCues.total.predict.onsets = models.rawData{glmModel.traj.raw.PPIDidx}.data.events.cue_pred_on';
glmModel.traj.twoCues.total.predict.volumes = round(glmModel.traj.twoCues.total.predict.onsets / glmModel.options.taskScan.tr);
glmModel.traj.twoCues.total.error.onsets = physValues.values.inspiratoryPressure.allStimuli.onsetTime;
glmModel.traj.twoCues.total.error.volumes = round(glmModel.traj.twoCues.total.error.onsets / glmModel.options.taskScan.tr);
glmModel.traj.twoCues.total.error.volumes = glmModel.traj.twoCues.total.error.volumes(glmModel.traj.twoCues.total.error.volumes <= glmModel.options.taskScan.vols);

% Specify onsets and volumes for split predictions and errors (according to valence)
glmModel.traj.twoCues.split.positive.predict.onsets = glmModel.traj.twoCues.total.predict.onsets(glmModel.traj.twoCues.total.predict.values > 0);
glmModel.traj.twoCues.split.positive.predict.volumes = round(glmModel.traj.twoCues.split.positive.predict.onsets / glmModel.options.taskScan.tr);
glmModel.traj.twoCues.split.negative.predict.onsets = glmModel.traj.twoCues.total.predict.onsets(glmModel.traj.twoCues.total.predict.values < 0);
glmModel.traj.twoCues.split.negative.predict.volumes = round(glmModel.traj.twoCues.split.negative.predict.onsets / glmModel.options.taskScan.tr);
glmModel.traj.twoCues.split.positive.error.onsets = physValues.values.inspiratoryPressure.noStimulus.onsetTime;
glmModel.traj.twoCues.split.positive.error.volumes = round(glmModel.traj.twoCues.split.positive.error.onsets / glmModel.options.taskScan.tr);
glmModel.traj.twoCues.split.positive.error.volumes = glmModel.traj.twoCues.split.positive.error.volumes(glmModel.traj.twoCues.split.positive.error.volumes <= glmModel.options.taskScan.vols);
glmModel.traj.twoCues.split.negative.error.onsets = physValues.values.inspiratoryPressure.stimulus.onsetTime;
glmModel.traj.twoCues.split.negative.error.volumes = round(glmModel.traj.twoCues.split.negative.error.onsets / glmModel.options.taskScan.tr);
glmModel.traj.twoCues.split.negative.error.volumes = glmModel.traj.twoCues.split.negative.error.volumes(glmModel.traj.twoCues.split.negative.error.volumes <= glmModel.options.taskScan.vols);

% Specify onsets and durations for cue and stimuli regressors
glmModel.events.cue.total.onsets = models.rawData{glmModel.traj.raw.PPIDidx}.data.events.cue_pred_on;
glmModel.events.cue.total.durations = models.rawData{glmModel.traj.raw.PPIDidx}.data.events.cue_pred_off - models.rawData{glmModel.traj.raw.PPIDidx}.data.events.cue_pred_on;
glmModel.events.stim.total.onsets = physValues.values.inspiratoryPressure.allStimuli.onsetTime;
glmModel.events.stim.total.durations = physValues.values.inspiratoryPressure.allStimuli.duration;
glmModel.events.stim.total.values = ones(size(glmModel.events.stim.total.onsets));
glmModel.events.stim.diff.onsets = physValues.values.inspiratoryPressure.allStimuli.onsetTime;
glmModel.events.stim.diff.durations = physValues.values.inspiratoryPressure.allStimuli.duration;
glmModel.events.stim.diff.values = -2 * models.rawData{glmModel.traj.raw.PPIDidx}.params.resist + 1;
glmModel.events.stim.noStimulus.onsets = physValues.values.inspiratoryPressure.noStimulus.onsetTime;
glmModel.events.stim.noStimulus.durations = physValues.values.inspiratoryPressure.noStimulus.duration;
glmModel.events.stim.stimulus.onsets = physValues.values.inspiratoryPressure.stimulus.onsetTime;
glmModel.events.stim.stimulus.durations = physValues.values.inspiratoryPressure.stimulus.duration;

% Specify onsets and durations for noise regressors
glmModel.events.noise.predict.onsets = models.rawData{glmModel.traj.raw.PPIDidx}.data.events.cue_pred_on(~isnan(models.rawData{glmModel.traj.raw.PPIDidx}.data.pred_rt)) + models.rawData{glmModel.traj.raw.PPIDidx}.data.pred_rt(~isnan(models.rawData{glmModel.traj.raw.PPIDidx}.data.pred_rt));
glmModel.events.noise.predict.durations = 0;
glmModel.events.noise.rating.onsets = models.rawData{glmModel.traj.raw.PPIDidx}.data.events.rating_on(~isnan(models.rawData{glmModel.traj.raw.PPIDidx}.data.rate_answer));
glmModel.events.noise.rating.durations = models.rawData{glmModel.traj.raw.PPIDidx}.data.events.rating_off(~isnan(models.rawData{glmModel.traj.raw.PPIDidx}.data.rate_answer)) - models.rawData{glmModel.traj.raw.PPIDidx}.data.events.rating_on(~isnan(models.rawData{glmModel.traj.raw.PPIDidx}.data.rate_answer));

% Create quadratic for prediction regressor
glmModel.traj.twoCues.total.predict.valuesSq = glmModel.traj.twoCues.total.predict.values .^ 2;
glmModel.traj.twoCues.total.predict.valuesSqDemeaned = glmModel.traj.twoCues.total.predict.valuesSq - mean(glmModel.traj.twoCues.total.predict.valuesSq);

% Create quadratic for error regressor
glmModel.traj.twoCues.total.error.valuesSq = glmModel.traj.twoCues.total.error.values .^ 2;
glmModel.traj.twoCues.total.error.valuesSqDemeaned = glmModel.traj.twoCues.total.error.valuesSq - mean(glmModel.traj.twoCues.total.error.valuesSq);

% Create orthogonal regressor and squared for errors against stimuli regressors
[b,bint,glmModel.traj.twoCues.total.error.valuesOrth] = regress(glmModel.traj.twoCues.total.error.values, [glmModel.events.stim.total.values, glmModel.events.stim.diff.values]);
[b,bint,glmModel.traj.twoCues.total.error.valuesOrthSq] = regress((glmModel.traj.twoCues.total.error.values .^ 2), [glmModel.events.stim.total.values, glmModel.events.stim.diff.values]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE MANUAL TASK TRACES (PSEUDO SAMPLING = 1000 Hz)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create base task regressors
glmModel.convolution.baseRegressor = zeros(1,(round(glmModel.options.taskScan.tr * glmModel.options.taskScan.vols) * 1000))';
glmModel.convolution.cue.total.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.predict.total.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.predict.totalSq.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.predict.positive.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.predict.negative.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.stim.total.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.stim.diff.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.stim.noStimulus.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.stim.stimulus.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.error.total.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.error.totalSq.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.error.totalOrth.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.error.totalOrthSq.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.error.positive.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.error.negative.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.noise.predict.toConvolve.u = glmModel.convolution.baseRegressor;
glmModel.convolution.noise.rating.toConvolve.u = glmModel.convolution.baseRegressor;

% Add in values for total cue
for a = 1:length(glmModel.events.cue.total.onsets)
    b = round(glmModel.events.cue.total.onsets(a) * 1000);
    c = b + round(glmModel.events.cue.total.durations(a) * 1000);
    glmModel.convolution.cue.total.toConvolve.u(b:c) = 1;
end

% Add in values for total predictions and total prediction squared
for a = 1:length(glmModel.traj.twoCues.total.predict.onsets)
    b = round(glmModel.traj.twoCues.total.predict.onsets(a) * 1000);
    c = b + (1 * 500);
    glmModel.convolution.predict.total.toConvolve.u(b:c) = glmModel.traj.twoCues.total.predict.values(a);
    glmModel.convolution.predict.totalSq.toConvolve.u(b:c) = glmModel.traj.twoCues.total.predict.valuesSq(a);
end

% Add in values for split positive predictions
for a = 1:length(glmModel.traj.twoCues.split.positive.predict.onsets)
    b = round(glmModel.traj.twoCues.split.positive.predict.onsets(a) * 1000);
    c = b + (1 * 500);
    glmModel.convolution.predict.positive.toConvolve.u(b:c) = glmModel.traj.twoCues.split.positive.predict.values(a);
end

% Add in values for split negative predictions
for a = 1:length(glmModel.traj.twoCues.split.negative.predict.onsets)
    b = round(glmModel.traj.twoCues.split.negative.predict.onsets(a) * 1000);
    c = b + (1 * 500);
    glmModel.convolution.predict.negative.toConvolve.u(b:c) = glmModel.traj.twoCues.split.negative.predict.values(a);
end

% Add in values for total and difference in stimulus
for a = 1:length(glmModel.events.stim.total.onsets)
    b = round(glmModel.events.stim.total.onsets(a) * 1000);
    c = b + round(glmModel.events.stim.total.durations(a) * 1000);
    glmModel.convolution.stim.total.toConvolve.u(b:c,1) = 1;
    glmModel.convolution.stim.diff.toConvolve.u(b:c,1) = glmModel.events.stim.diff.values(a);
end

% Add in values for no stimulus
for a = 1:length(glmModel.events.stim.noStimulus.onsets)
    b = round(glmModel.events.stim.noStimulus.onsets(a) * 1000);
    c = b + round(glmModel.events.stim.noStimulus.durations(a) * 1000);
    glmModel.convolution.stim.noStimulus.toConvolve.u(b:c,1) = 1;
end

% Add in values for stimulus (resistance)
for a = 1:length(glmModel.events.stim.stimulus.onsets)
    b = round(glmModel.events.stim.stimulus.onsets(a) * 1000);
    c = b + round(glmModel.events.stim.stimulus.durations(a) * 1000);
    glmModel.convolution.stim.stimulus.toConvolve.u(b:c,1) = 1;
end

% Add in values for total prediction errors, orthogonalised errors and
% error squared
for a = 1:length(glmModel.traj.twoCues.total.error.onsets)
    b = round(glmModel.traj.twoCues.total.error.onsets(a) * 1000);
    c = b + (1 * 500);
    glmModel.convolution.error.total.toConvolve.u(b:c) = glmModel.traj.twoCues.total.error.values(a);
    glmModel.convolution.error.totalSq.toConvolve.u(b:c) = glmModel.traj.twoCues.total.error.valuesSq(a);
    glmModel.convolution.error.totalOrth.toConvolve.u(b:c) = glmModel.traj.twoCues.total.error.valuesOrth(a);
    glmModel.convolution.error.totalOrthSq.toConvolve.u(b:c) = glmModel.traj.twoCues.total.error.valuesOrthSq(a);
end

% Add in values for split positive prediction errors
for a = 1:length(glmModel.traj.twoCues.split.positive.error.onsets)
    b = round(glmModel.traj.twoCues.split.positive.error.onsets(a) * 1000);
    c = b + (1 * 500);
    glmModel.convolution.error.positive.toConvolve.u(b:c) = glmModel.traj.twoCues.split.positive.error.values(a);
end

% Add in values for split negative prediction errors
for a = 1:length(glmModel.traj.twoCues.split.negative.error.onsets)
    b = round(glmModel.traj.twoCues.split.negative.error.onsets(a) * 1000);
    c = b + (1 * 500);
    glmModel.convolution.error.negative.toConvolve.u(b:c) = glmModel.traj.twoCues.split.negative.error.values(a);
end

% Add in values for button press prediction noise
for a = 1:length(glmModel.events.noise.predict.onsets)
    b = round(glmModel.events.noise.predict.onsets(a) * 1000);
    c = b + (1 * 500);
    glmModel.convolution.noise.predict.toConvolve.u(b:c,1) = 1;
end

% Add in values for rating noise
for a = 1:length(glmModel.events.noise.rating.onsets)
    b = round(glmModel.events.noise.rating.onsets(a) * 1000);
    c = b + round(glmModel.events.noise.rating.durations(a) * 1000);
    glmModel.convolution.noise.rating.toConvolve.u(b:c,1) = 1;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CONVOLVE TASK TRACES WITH STANDARD HRF AND DERIVATIVES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get basis functions for convolution
glmModel.convolution.basisFunctions.task.T = 1;
glmModel.convolution.basisFunctions.task.dt = 0.001; % Specify sampling rate
glmModel.convolution.basisFunctions.task.name = 'hrf (with time and dispersion derivatives)';
glmModel.convolution.basisFunctions.task.length = 32.2;
glmModel.convolution.basisFunctions.task.order = 3;
[glmModel.convolution.basisFunctions.task] = spm_get_bf(glmModel.convolution.basisFunctions.task);

% Convolve total cue regressor with basis functions
glmModel.convolution.cue.total.toConvolve.name = {'cueTot'};
glmModel.convolution.cue.total.convolved = spm_Volterra(glmModel.convolution.cue.total.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.cue.total.convolvedTR = downsample(glmModel.convolution.cue.total.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.cue.total.convolvedTR = glmModel.convolution.cue.total.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve total prediction regressor with basis functions
glmModel.convolution.predict.total.toConvolve.name = {'predictTot'};
glmModel.convolution.predict.total.convolved = spm_Volterra(glmModel.convolution.predict.total.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.predict.total.convolvedTR = downsample(glmModel.convolution.predict.total.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.predict.total.convolvedTR = glmModel.convolution.predict.total.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve total prediction squared regressor with basis functions
glmModel.convolution.predict.totalSq.toConvolve.name = {'predictTotSq'};
glmModel.convolution.predict.totalSq.convolved = spm_Volterra(glmModel.convolution.predict.totalSq.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.predict.totalSq.convolvedTR = downsample(glmModel.convolution.predict.totalSq.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.predict.totalSq.convolvedTR = glmModel.convolution.predict.totalSq.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve positive prediction regressor with basis functions
glmModel.convolution.predict.positive.toConvolve.name = {'predictPos'};
glmModel.convolution.predict.positive.convolved = spm_Volterra(glmModel.convolution.predict.positive.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.predict.positive.convolvedTR = downsample(glmModel.convolution.predict.positive.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.predict.positive.convolvedTR = glmModel.convolution.predict.positive.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve negative prediction regressor with basis functions
glmModel.convolution.predict.negative.toConvolve.name = {'predictNeg'}; 
glmModel.convolution.predict.negative.convolved = spm_Volterra(glmModel.convolution.predict.negative.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.predict.negative.convolvedTR = downsample(glmModel.convolution.predict.negative.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.predict.negative.convolvedTR = glmModel.convolution.predict.negative.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve total stimulus (resistance plus no resistance) regressor with basis functions
glmModel.convolution.stim.total.toConvolve.name = {'stimTot'}; 
glmModel.convolution.stim.total.convolved = spm_Volterra(glmModel.convolution.stim.total.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.stim.total.convolvedTR = downsample(glmModel.convolution.stim.total.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.stim.total.convolvedTR = glmModel.convolution.stim.total.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve difference stimulus (no resistance = 1, resistance = -1) regressor with basis functions
glmModel.convolution.stim.diff.toConvolve.name = {'stimDiff'}; 
glmModel.convolution.stim.diff.convolved = spm_Volterra(glmModel.convolution.stim.diff.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.stim.diff.convolvedTR = downsample(glmModel.convolution.stim.diff.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.stim.diff.convolvedTR = glmModel.convolution.stim.diff.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve no stimulus (no resistance) regressor with basis functions
glmModel.convolution.stim.noStimulus.toConvolve.name = {'noResist'}; 
glmModel.convolution.stim.noStimulus.convolved = spm_Volterra(glmModel.convolution.stim.noStimulus.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.stim.noStimulus.convolvedTR = downsample(glmModel.convolution.stim.noStimulus.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.stim.noStimulus.convolvedTR = glmModel.convolution.stim.noStimulus.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve stimulus (resistance) regressor with basis functions
glmModel.convolution.stim.stimulus.toConvolve.name = {'resist'}; 
glmModel.convolution.stim.stimulus.convolved = spm_Volterra(glmModel.convolution.stim.stimulus.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.stim.stimulus.convolvedTR = downsample(glmModel.convolution.stim.stimulus.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.stim.stimulus.convolvedTR = glmModel.convolution.stim.stimulus.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve total prediction error regressor with basis functions
glmModel.convolution.error.total.toConvolve.name = {'errorTot'}; 
glmModel.convolution.error.total.convolved = spm_Volterra(glmModel.convolution.error.total.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.error.total.convolvedTR = downsample(glmModel.convolution.error.total.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.error.total.convolvedTR = glmModel.convolution.error.total.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve total prediction error squared regressor with basis functions
glmModel.convolution.error.totalSq.toConvolve.name = {'errorTotSq'}; 
glmModel.convolution.error.totalSq.convolved = spm_Volterra(glmModel.convolution.error.totalSq.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.error.totalSq.convolvedTR = downsample(glmModel.convolution.error.totalSq.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.error.totalSq.convolvedTR = glmModel.convolution.error.totalSq.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve orthogonalised total prediction error regressor with basis functions
glmModel.convolution.error.totalOrth.toConvolve.name = {'errorTotOrth'}; 
glmModel.convolution.error.totalOrth.convolved = spm_Volterra(glmModel.convolution.error.totalOrth.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.error.totalOrth.convolvedTR = downsample(glmModel.convolution.error.totalOrth.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.error.totalOrth.convolvedTR = glmModel.convolution.error.totalOrth.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve orthogonalised total prediction error squared regressor with basis functions
glmModel.convolution.error.totalOrthSq.toConvolve.name = {'errorTotOrthSq'}; 
glmModel.convolution.error.totalOrthSq.convolved = spm_Volterra(glmModel.convolution.error.totalOrthSq.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.error.totalOrthSq.convolvedTR = downsample(glmModel.convolution.error.totalOrthSq.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.error.totalOrthSq.convolvedTR = glmModel.convolution.error.totalOrthSq.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve positive prediction error regressor with basis functions
glmModel.convolution.error.positive.toConvolve.name = {'errorPos'}; 
glmModel.convolution.error.positive.convolved = spm_Volterra(glmModel.convolution.error.positive.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.error.positive.convolvedTR = downsample(glmModel.convolution.error.positive.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.error.positive.convolvedTR = glmModel.convolution.error.positive.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve negative prediction error regressor with basis functions
glmModel.convolution.error.negative.toConvolve.name = {'errorNeg'}; 
glmModel.convolution.error.negative.convolved = spm_Volterra(glmModel.convolution.error.negative.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.error.negative.convolvedTR = downsample(glmModel.convolution.error.negative.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.error.negative.convolvedTR = glmModel.convolution.error.negative.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve prediction noise regressor with basis functions
glmModel.convolution.noise.predict.toConvolve.name = {'noisePredict'}; 
glmModel.convolution.noise.predict.convolved = spm_Volterra(glmModel.convolution.noise.predict.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.noise.predict.convolvedTR = downsample(glmModel.convolution.noise.predict.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.noise.predict.convolvedTR = glmModel.convolution.noise.predict.convolvedTR(1:glmModel.options.taskScan.vols,:);

% Convolve rating noise regressor with basis functions
glmModel.convolution.noise.rating.toConvolve.name = {'noiseRating'}; 
glmModel.convolution.noise.rating.convolved = spm_Volterra(glmModel.convolution.noise.rating.toConvolve, glmModel.convolution.basisFunctions.task.bf);
glmModel.convolution.noise.rating.convolvedTR = downsample(glmModel.convolution.noise.rating.convolved, round(glmModel.options.taskScan.tr * 1000), round(glmModel.options.taskScan.tr * 1000 / 2)); 
glmModel.convolution.noise.rating.convolvedTR = glmModel.convolution.noise.rating.convolvedTR(1:glmModel.options.taskScan.vols,:);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD NOISE MATRIX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isfile(glmModel.options.saveNames.glmNoise)
    noise = load(glmModel.options.saveNames.glmNoise);
else
    disp('No noise matrix found... No noise regressors added to model GLM')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE DESIGN MATRIX FOR MODELLING TASK DESIGN -- MANUAL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% NOTES:
% Prediction and error trajectories are split, and first predictioon value
% is unused
model.R = [glmModel.convolution.cue.total.convolvedTR glmModel.convolution.predict.positive.convolvedTR -glmModel.convolution.predict.negative.convolvedTR glmModel.convolution.stim.noStimulus.convolvedTR glmModel.convolution.stim.stimulus.convolvedTR glmModel.convolution.error.positive.convolvedTR -glmModel.convolution.error.negative.convolvedTR glmModel.convolution.noise.rating.convolvedTR noise];
model.names = {'cueTot', 'predictPos', 'predictNeg', 'noResist', 'resist', 'errorPos', 'errorNeg', 'noiseRating'};
model.extraContrasts.names = {'predAvg', 'errorAvg', 'predIntPos', 'predIntNeg', 'errorIntPos', 'errorIntNeg', 'predictPos_min', 'predictNeg_min'};
model.extraContrasts.weights{1} = [0 0 0 1 0 0 1 0 0]; % Remember to account for derivatives and modulators here
model.extraContrasts.weights{2} = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 0 0]; % Remember to account for derivatives and modulators here
model.extraContrasts.weights{3} = [0 0 0 1 0 0 -1 0 0]; % Remember to account for derivatives and modulators here
model.extraContrasts.weights{4} = [0 0 0 -1 0 0 1 0 0]; % Remember to account for derivatives and modulators here
model.extraContrasts.weights{5} = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -1 0 0]; % Remember to account for derivatives and modulators here
model.extraContrasts.weights{6} = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -1 0 0 1 0 0]; % Remember to account for derivatives and modulators here
model.extraContrasts.weights{7} = [0 0 0 -1 0 0 0 0 0]; % Remember to account for derivatives and modulators here
model.extraContrasts.weights{8} = [0 0 0 0 0 0 -1 0 0]; % Remember to account for derivatives and modulators here
save(glmModel.options.saveNames.glmModelMatrix, '-struct', 'model');
glmModel.designs.model = model;

% Output Figure (not including noise)
figure()
    set(gcf, 'Position', [0 0 1000 400]);
    ticks = 1:(length(model.names) * glmModel.convolution.basisFunctions.task.order);
    a = 0;
    for x = 1:length(model.names)
        a = a + 1;
        xlabels{a} = model.names{x};
        for b = 2:glmModel.convolution.basisFunctions.task.order
            xlabels{a+b} = [];
            a = a + 1;
        end
    end
    subplot(1,2,1)
    imagesc(zscore(model.R(:,1:(length(model.names)*glmModel.convolution.basisFunctions.task.order))), 3*[-1 1]);
    colormap('gray');
    colorbar();
    axis square
    title(['Model-based GLM: PPID ', glmModel.options.PPID], 'FontSize', 20);
    set(gca, 'XTick', ticks, 'XTickLabels', xlabels);
    xtickangle(45);
    subplot(1,2,2)
    imagesc(corrcoef(model.R(:,1:(length(model.names)*glmModel.convolution.basisFunctions.task.order))), [-1 1]);
    colormap('gray');
    colorbar();
    axis square
    title('Correlations', 'FontSize', 20);
    set(gca, 'XTick', ticks, 'XTickLabels', xlabels);
    xtickangle(45);
    set(gca, 'YTick', ticks, 'YTickLabels', xlabels);
    print(glmModel.options.saveNames.glmModelFig, '-dtiff');
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE DESIGN MATRIX FOR MODELLING TASK DESIGN (PREDICT ONLY) -- MANUAL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% NOTES:
% Prediction and error trajectories are split, and first predictioon value
% is unused
modelPred.R = [glmModel.convolution.cue.total.convolvedTR glmModel.convolution.predict.positive.convolvedTR -glmModel.convolution.predict.negative.convolvedTR glmModel.convolution.stim.noStimulus.convolvedTR glmModel.convolution.stim.stimulus.convolvedTR glmModel.convolution.noise.rating.convolvedTR noise];
modelPred.names = {'cueTot', 'predictPos', 'predictNeg', 'noResist', 'resist', 'noiseRating'};
modelPred.extraContrasts.names = {'predAvg', 'predIntPos', 'predIntNeg'};
modelPred.extraContrasts.weights{1} = [0 0 0 1 0 0 1 0 0]; % Remember to account for derivatives and modulators here
modelPred.extraContrasts.weights{2} = [0 0 0 1 0 0 -1 0 0]; % Remember to account for derivatives and modulators here
modelPred.extraContrasts.weights{3} = [0 0 0 -1 0 0 1 0 0]; % Remember to account for derivatives and modulators here
save(glmModel.options.saveNames.glmModelPredMatrix, '-struct', 'modelPred');
glmModel.designs.modelPred = modelPred;

% Output Figure (not including noise)
figure()
    set(gcf, 'Position', [0 0 1000 400]);
    ticks = 1:(length(modelPred.names) * glmModel.convolution.basisFunctions.task.order);
    a = 0;
    for x = 1:length(modelPred.names)
        a = a + 1;
        xlabels{a} = modelPred.names{x};
        for b = 2:glmModel.convolution.basisFunctions.task.order
            xlabels{a+b} = [];
            a = a + 1;
        end
    end
    subplot(1,2,1)
    imagesc(zscore(modelPred.R(:,1:(length(modelPred.names)*glmModel.convolution.basisFunctions.task.order))), 3*[-1 1]);
    colormap('gray');
    colorbar();
    axis square
    title(['Model-based GLM (pred): PPID ', glmModel.options.PPID], 'FontSize', 20);
    set(gca, 'XTick', ticks, 'XTickLabels', xlabels);
    xtickangle(45);
    subplot(1,2,2)
    imagesc(corrcoef(modelPred.R(:,1:(length(modelPred.names)*glmModel.convolution.basisFunctions.task.order))), [-1 1]);
    colormap('gray');
    colorbar();
    axis square
    title('Correlations', 'FontSize', 20);
    set(gca, 'XTick', ticks, 'XTickLabels', xlabels);
    xtickangle(45);
    set(gca, 'YTick', ticks, 'YTickLabels', xlabels);
    print(glmModel.options.saveNames.glmModelPredFig, '-dtiff');
    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE DESIGN MATRIX FOR MODELLING TASK DESIGN (ERROR ONLY) -- MANUAL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% NOTES:
% Prediction and error trajectories are split, and first predictioon value
% is unused
modelErr.R = [glmModel.convolution.cue.total.convolvedTR glmModel.convolution.stim.noStimulus.convolvedTR glmModel.convolution.stim.stimulus.convolvedTR glmModel.convolution.error.positive.convolvedTR -glmModel.convolution.error.negative.convolvedTR glmModel.convolution.noise.rating.convolvedTR noise];
modelErr.names = {'cueTot', 'noResist', 'resist', 'errorPos', 'errorNeg', 'noiseRating'};
modelErr.extraContrasts.names = {'errAvg', 'errIntPos', 'errIntNeg'};
modelErr.extraContrasts.weights{1} = [0 0 0 0 0 0 0 0 0 1 0 0 1 0 0]; % Remember to account for derivatives and modulators here
modelErr.extraContrasts.weights{2} = [0 0 0 0 0 0 0 0 0 1 0 0 -1 0 0]; % Remember to account for derivatives and modulators here
modelErr.extraContrasts.weights{3} = [0 0 0 0 0 0 0 0 0 -1 0 0 1 0 0]; % Remember to account for derivatives and modulators here
save(glmModel.options.saveNames.glmModelErrMatrix, '-struct', 'modelErr');
glmModel.designs.modelErr = modelErr;

% Output Figure (not including noise)
figure()
    set(gcf, 'Position', [0 0 1000 400]);
    ticks = 1:(length(modelErr.names) * glmModel.convolution.basisFunctions.task.order);
    a = 0;
    for x = 1:length(modelErr.names)
        a = a + 1;
        xlabels{a} = modelErr.names{x};
        for b = 2:glmModel.convolution.basisFunctions.task.order
            xlabels{a+b} = [];
            a = a + 1;
        end
    end
    subplot(1,2,1)
    imagesc(zscore(modelErr.R(:,1:(length(modelErr.names)*glmModel.convolution.basisFunctions.task.order))), 3*[-1 1]);
    colormap('gray');
    colorbar();
    axis square
    title(['Model-based GLM (err): PPID ', glmModel.options.PPID], 'FontSize', 20);
    set(gca, 'XTick', ticks, 'XTickLabels', xlabels);
    xtickangle(45);
    subplot(1,2,2)
    imagesc(corrcoef(modelErr.R(:,1:(length(modelErr.names)*glmModel.convolution.basisFunctions.task.order))), [-1 1]);
    colormap('gray');
    colorbar();
    axis square
    title('Correlations', 'FontSize', 20);
    set(gca, 'XTick', ticks, 'XTickLabels', xlabels);
    xtickangle(45);
    set(gca, 'YTick', ticks, 'YTickLabels', xlabels);
    print(glmModel.options.saveNames.glmModelErrFig, '-dtiff');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE DESIGN MATRIX FOR MODELLING TASK DESIGN (FACTOR FORMAT) -- MANUAL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% NOTES:
% Think of a factor structure used for a group GLM...
% For predictions: Cue total + prediction trajectory + prediction^2
% --> Positive predictions = positive contrast for prediction trajectory
% --> Negative predictions = negative contrast for prediction trajectory
% --> Pos + neg predictions = prediction^2 contrasts (this models a
%     quadratic / smooth absolute slope across conditions)
% For errors: No resistance + resistance + error trajectory + error 
% trajectory^2
% --> Positive stimulus (no resistance) = contrasts for no resistance
% --> Negative stimulus (resistance) = contrasts for resistance
% --> Positive errors = positive contrast for error trajectory
% --> Negative errors = negative contrast for error trajectory
% --> Pos + neg errors = error^2 (this models a quadratic / smooth
%     absolute slope across conditions)

modelFactors.R = [glmModel.convolution.cue.total.convolvedTR glmModel.convolution.predict.total.convolvedTR glmModel.convolution.predict.totalSq.convolvedTR glmModel.convolution.stim.noStimulus.convolvedTR glmModel.convolution.stim.stimulus.convolvedTR glmModel.convolution.error.total.convolvedTR glmModel.convolution.error.totalSq.convolvedTR glmModel.convolution.noise.rating.convolvedTR noise];
modelFactors.names = {'cueTot', 'predict', 'predictSq', 'noResist', 'resist', 'error', 'errorSq', 'noiseRating'};
save(glmModel.options.saveNames.glmModelFactorsMatrix, '-struct', 'modelFactors');
glmModel.designs.modelFactors = modelFactors;

% Output Figure (not including noise)
figure()
    set(gcf, 'Position', [0 0 1000 400]);
    ticks = 1:(length(modelFactors.names) * glmModel.convolution.basisFunctions.task.order);
    a = 0;
    for x = 1:length(modelFactors.names)
        a = a + 1;
        xlabels{a} = modelFactors.names{x};
        for b = 2:glmModel.convolution.basisFunctions.task.order
            xlabels{a+b} = [];
            a = a + 1;
        end
    end
    subplot(1,2,1)
    imagesc(zscore(modelFactors.R(:,1:(length(modelFactors.names)*glmModel.convolution.basisFunctions.task.order))), 3*[-1 1]);
    colormap('gray');
    colorbar();
    axis square
    title(['Model-based GLM (Factors): PPID ', glmModel.options.PPID], 'FontSize', 20);
    set(gca, 'XTick', ticks, 'XTickLabels', xlabels);
    xtickangle(45);
    subplot(1,2,2)
    imagesc(corrcoef(modelFactors.R(:,1:(length(modelFactors.names)*glmModel.convolution.basisFunctions.task.order))), [-1 1]);
    colormap('gray');
    colorbar();
    axis square
    title('Correlations', 'FontSize', 20);
    set(gca, 'XTick', ticks, 'XTickLabels', xlabels);
    xtickangle(45);
    set(gca, 'YTick', ticks, 'YTickLabels', xlabels);
    print(glmModel.options.saveNames.glmModelFactorsFig, '-dtiff');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE DESIGN MATRIX MODEL-BASED TASK DESIGN
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save(glmModel.options.saveNames.glmModelInfo, 'glmModel');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE CORRELATION MATRICES AND PLOTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load motion parameters
if isfile(glmModel.options.saveNames.motion.task_txt)
    motion.raw = load(glmModel.options.saveNames.motion.task_txt);
else
    disp('No motion file found... Correlations with model parameters not computed')
    return
end

% Identify motion traces from file
motion.x = motion.raw(:,1);
motion.y = motion.raw(:,2);
motion.z = motion.raw(:,3);
motion.pitch = motion.raw(:,4);
motion.roll = motion.raw(:,5);
motion.yaw = motion.raw(:,6);

% Plot and save correlations of prediction trajectories (total) with motion
glmModel.correlations.predictionVsMotion.total.variableNames = {'MotX', 'MotY', 'MotZ', 'MotP', 'MotR', 'MotYa', 'Pred'};
glmModel.correlations.predictionVsMotion.total.dataRaw = [motion.x(glmModel.traj.twoCues.total.predict.volumes), motion.y(glmModel.traj.twoCues.total.predict.volumes), motion.z(glmModel.traj.twoCues.total.predict.volumes), motion.pitch(glmModel.traj.twoCues.total.predict.volumes), motion.roll(glmModel.traj.twoCues.total.predict.volumes), motion.yaw(glmModel.traj.twoCues.total.predict.volumes), glmModel.traj.twoCues.total.predict.values];
glmModel.correlations.predictionVsMotion.total.dataZscore = zscore(glmModel.correlations.predictionVsMotion.total.dataRaw);
[glmModel.correlations.predictionVsMotion.total.Rvalues, glmModel.correlations.predictionVsMotion.total.Pvalues] = corrplot(glmModel.correlations.predictionVsMotion.total.dataZscore, 'testR', 'on', 'varNames', glmModel.correlations.predictionVsMotion.total.variableNames);
saveName = [glmModel.options.saveNames.models.correlations, '_model_predictionVsMotion']; % Create figure file name
print(saveName, '-dtiff');

% Plot and save correlations of prediction trajectories (split - positive) with motion
glmModel.correlations.predictionVsMotion.split.positive.variableNames = {'MotX', 'MotY', 'MotZ', 'MotP', 'MotR', 'MotYa', 'Pred+'};
glmModel.correlations.predictionVsMotion.split.positive.dataRaw = [motion.x(glmModel.traj.twoCues.split.positive.predict.volumes), motion.y(glmModel.traj.twoCues.split.positive.predict.volumes), motion.z(glmModel.traj.twoCues.split.positive.predict.volumes), motion.pitch(glmModel.traj.twoCues.split.positive.predict.volumes), motion.roll(glmModel.traj.twoCues.split.positive.predict.volumes), motion.yaw(glmModel.traj.twoCues.split.positive.predict.volumes), glmModel.traj.twoCues.split.positive.predict.values];
glmModel.correlations.predictionVsMotion.split.positive.dataZscore = zscore(glmModel.correlations.predictionVsMotion.split.positive.dataRaw);
[glmModel.correlations.predictionVsMotion.split.positive.Rvalues, glmModel.correlations.predictionVsMotion.total.Pvalues] = corrplot(glmModel.correlations.predictionVsMotion.split.positive.dataZscore, 'testR', 'on', 'varNames', glmModel.correlations.predictionVsMotion.split.positive.variableNames);
saveName = [glmModel.options.saveNames.models.correlations, '_model_predictionVsMotion_predPos']; % Create figure file name
print(saveName, '-dtiff');

% Plot and save correlations of prediction trajectories (split - negative (flipped)) with motion
glmModel.correlations.predictionVsMotion.split.negative.variableNames = {'MotX', 'MotY', 'MotZ', 'MotP', 'MotR', 'MotYa', 'Pred-'};
glmModel.correlations.predictionVsMotion.split.negative.dataRaw = [motion.x(glmModel.traj.twoCues.split.negative.predict.volumes), motion.y(glmModel.traj.twoCues.split.negative.predict.volumes), motion.z(glmModel.traj.twoCues.split.negative.predict.volumes), motion.pitch(glmModel.traj.twoCues.split.negative.predict.volumes), motion.roll(glmModel.traj.twoCues.split.negative.predict.volumes), motion.yaw(glmModel.traj.twoCues.split.negative.predict.volumes), -glmModel.traj.twoCues.split.negative.predict.values];
glmModel.correlations.predictionVsMotion.split.negative.dataZscore = zscore(glmModel.correlations.predictionVsMotion.split.negative.dataRaw);
[glmModel.correlations.predictionVsMotion.split.negative.Rvalues, glmModel.correlations.predictionVsMotion.total.Pvalues] = corrplot(glmModel.correlations.predictionVsMotion.split.negative.dataZscore, 'testR', 'on', 'varNames', glmModel.correlations.predictionVsMotion.split.negative.variableNames);
saveName = [glmModel.options.saveNames.models.correlations, '_model_predictionVsMotion_predNeg']; % Create figure file name
print(saveName, '-dtiff');

% Plot and save correlations of prediction error trajectories with motion
glmModel.correlations.errorVsMotion.total.variableNames = {'MotX', 'MotY', 'MotZ', 'MotP', 'MotR', 'MotYa', 'Error'};
glmModel.correlations.errorVsMotion.total.dataRaw = [motion.x(glmModel.traj.twoCues.total.error.volumes), motion.y(glmModel.traj.twoCues.total.error.volumes), motion.z(glmModel.traj.twoCues.total.error.volumes), motion.pitch(glmModel.traj.twoCues.total.error.volumes), motion.roll(glmModel.traj.twoCues.total.error.volumes), motion.yaw(glmModel.traj.twoCues.total.error.volumes), glmModel.traj.twoCues.total.error.values(1:length(glmModel.traj.twoCues.total.error.volumes))];
glmModel.correlations.errorVsMotion.total.dataZscore = zscore(glmModel.correlations.errorVsMotion.total.dataRaw);
[glmModel.correlations.errorVsMotion.total.Rvalues, glmModel.correlations.errorVsMotion.total.Pvalues] = corrplot(glmModel.correlations.errorVsMotion.total.dataZscore, 'testR', 'on', 'varNames', glmModel.correlations.errorVsMotion.total.variableNames);
saveName = [glmModel.options.saveNames.models.correlations, '_model_errorVsMotion']; % Create figure file name
print(saveName, '-dtiff');

% Plot and save correlations of prediction error trajectories (split - positive) with motion
glmModel.correlations.errorVsMotion.split.positive.variableNames = {'MotX', 'MotY', 'MotZ', 'MotP', 'MotR', 'MotYa', 'Err+'};
glmModel.correlations.errorVsMotion.split.positive.dataRaw = [motion.x(glmModel.traj.twoCues.split.positive.error.volumes), motion.y(glmModel.traj.twoCues.split.positive.error.volumes), motion.z(glmModel.traj.twoCues.split.positive.error.volumes), motion.pitch(glmModel.traj.twoCues.split.positive.error.volumes), motion.roll(glmModel.traj.twoCues.split.positive.error.volumes), motion.yaw(glmModel.traj.twoCues.split.positive.error.volumes), glmModel.traj.twoCues.split.positive.error.values(1:length(glmModel.traj.twoCues.split.positive.error.volumes))];
glmModel.correlations.errorVsMotion.split.positive.dataZscore = zscore(glmModel.correlations.errorVsMotion.split.positive.dataRaw);
[glmModel.correlations.errorVsMotion.split.positive.Rvalues, glmModel.correlations.errorVsMotion.total.Pvalues] = corrplot(glmModel.correlations.errorVsMotion.split.positive.dataZscore, 'testR', 'on', 'varNames', glmModel.correlations.errorVsMotion.split.positive.variableNames);
saveName = [glmModel.options.saveNames.models.correlations, '_model_errorVsMotion_PEpos']; % Create figure file name
print(saveName, '-dtiff');

% Plot and save correlations of prediction error trajectories (split - negative (flipped)) with motion
glmModel.correlations.errorVsMotion.split.negative.variableNames = {'MotX', 'MotY', 'MotZ', 'MotP', 'MotR', 'MotYa', 'Err-'};
glmModel.correlations.errorVsMotion.split.negative.dataRaw = [motion.x(glmModel.traj.twoCues.split.negative.error.volumes), motion.y(glmModel.traj.twoCues.split.negative.error.volumes), motion.z(glmModel.traj.twoCues.split.negative.error.volumes), motion.pitch(glmModel.traj.twoCues.split.negative.error.volumes), motion.roll(glmModel.traj.twoCues.split.negative.error.volumes), motion.yaw(glmModel.traj.twoCues.split.negative.error.volumes), -glmModel.traj.twoCues.split.negative.error.values(1:length(glmModel.traj.twoCues.split.negative.error.volumes))];
glmModel.correlations.errorVsMotion.split.negative.dataZscore = zscore(glmModel.correlations.errorVsMotion.split.negative.dataRaw);
[glmModel.correlations.errorVsMotion.split.negative.Rvalues, glmModel.correlations.errorVsMotion.total.Pvalues] = corrplot(glmModel.correlations.errorVsMotion.split.negative.dataZscore, 'testR', 'on', 'varNames', glmModel.correlations.errorVsMotion.split.negative.variableNames);
saveName = [glmModel.options.saveNames.models.correlations, '_model_errorVsMotion_PEneg']; % Create figure file name
print(saveName, '-dtiff');

end