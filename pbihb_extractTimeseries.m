%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% BREATHING SCANNER TASK: EXTRACT TIMESERIES %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 20/04/2020
% -------------------------------------------------------------------------
% TO RUN:    timeSeries = pbihb_extractTimeseries(options)
% INPUTS:    options    = PPID-specific matrix output from the
%                         pbihb_setOptions function
% OUTPUTS:   timeSeries = Structure containing all information for
%                         timeseries extraction, plus actual timeseries
%                         (matlab structures) located in the lower level
%                         SPM directory
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script extracts the timeseries for two anatomical areas of interest 
% (anterior insula and PAG) from the imaging data collected in the PBIHB 
% study. Local peaks are found and extracted for each region based on 
% contrasts for positive and negative predictions and errors (separately).
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function timeSeries = pbihb_extractTimeseries(options)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Save options
timeSeries.options = options;

% Specify SPM.mat
spm_mat = fullfile(timeSeries.options.paths.analysis_directories.model, 'SPM.mat');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD LOWER LEVEL GLM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

timeSeries.glm = load(timeSeries.options.saveNames.glmModelMatrix);
timeSeries.glm.fullNameList = {timeSeries.glm.names{:}, timeSeries.glm.extraContrasts.names{:}};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY REGION INFORMATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify names
timeSeries.regions(1).name = 'aIns';
timeSeries.regions(2).name = 'PAG';

% Specify sphere size
timeSeries.regions(1).size = timeSeries.options.masks.sphereSize.aIns;
timeSeries.regions(2).size = timeSeries.options.masks.sphereSize.PAG;

% Specify anatomical masks
timeSeries.regions(1).maskL = timeSeries.options.masks.aInsL;
timeSeries.regions(1).maskR = timeSeries.options.masks.aInsR;
timeSeries.regions(2).maskL = timeSeries.options.masks.PAGL;
timeSeries.regions(2).maskR = timeSeries.options.masks.PAGR;

% Specify contrasts of interest
timeSeries.contrasts(1).name = 'predictPos_min';
timeSeries.contrasts(2).name = 'predictNeg_min';
timeSeries.contrasts(3).name = 'errorPos';
timeSeries.contrasts(4).name = 'errorNeg';

% Specify contrast indices
timeSeries.contrasts(1).idx = find(strcmp(timeSeries.glm.fullNameList, timeSeries.contrasts(1).name));
timeSeries.contrasts(2).idx = find(strcmp(timeSeries.glm.fullNameList, timeSeries.contrasts(2).name));
timeSeries.contrasts(3).idx = find(strcmp(timeSeries.glm.fullNameList, timeSeries.contrasts(3).name));
timeSeries.contrasts(4).idx = find(strcmp(timeSeries.glm.fullNameList, timeSeries.contrasts(4).name));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET UP THE TIMESERIES EXTRACTIONS FOR EACH ROI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% https://en.wikibooks.org/wiki/SPM/Timeseries_extraction
% Useful for some of the pointlessly obtuse incantations required :-(

% Clear any existing matlabbatch
matlabbatch = {};

% Loop through all contrasts of interest
for conLoop = 1:length(timeSeries.contrasts)
    % Loop through all regions
    for regionLoop = 1:length(timeSeries.regions)
        % Create the structure
        extractbatch = struct();
        % Specify the single-subject spm
        extractbatch.spmmat = {spm_mat};
        extractbatch.adjust = 0;
        extractbatch.session = 1;
        extractbatch.name = [timeSeries.options.PPID, '_', timeSeries.regions(regionLoop).name, '_', timeSeries.contrasts(conLoop).name];
        % Region mask for left side
        extractbatch.roi{1}.mask.image = {timeSeries.regions(regionLoop).maskL};
        extractbatch.roi{1}.mask.threshold = 0.5;
        % Region mask for right side
        extractbatch.roi{2}.mask.image = {timeSeries.regions(regionLoop).maskR};
        extractbatch.roi{2}.mask.threshold = 0.5;
        % Contrast image
        extractbatch.roi{3}.spm.spmmat = {spm_mat};
        extractbatch.roi{3}.spm.contrast = timeSeries.contrasts(conLoop).idx;
        extractbatch.roi{3}.spm.threshdesc = 'none';
        extractbatch.roi{3}.spm.thresh = 1.0;  % On p-value, i.e. no thresh
        % And search for left side within contrast mask
        extractbatch.roi{4}.sphere.centre = [0 0 0];
        extractbatch.roi{4}.sphere.radius = timeSeries.regions(regionLoop).size;  % mm
        extractbatch.roi{4}.sphere.move.global.spm = 3;
        extractbatch.roi{4}.sphere.move.global.mask = 'i1';
        % And search for right side within contrast mask
        extractbatch.roi{5}.sphere.centre = [0 0 0];
        extractbatch.roi{5}.sphere.radius = timeSeries.regions(regionLoop).size;  % mm
        extractbatch.roi{5}.sphere.move.global.spm = 3;
        extractbatch.roi{5}.sphere.move.global.mask = 'i2';
        % Add the expression to include both sides
        extractbatch.expression = 'i4 | i5';
        % Add the commands to the batch
        matlabbatch{end+1}.spm.util.voi = extractbatch;
    end
end

% Add batch to structure for safe keeping
timeSeries.matlabbatch = matlabbatch;

% Print a message to screen
fprintf('\nRunning timeseries extraction for PPID %s...\n', timeSeries.options.PPID);

% Run the batch
spm_jobman('run', matlabbatch);

% Move the extracted timeseries to the timeseries folder and zip
delete(fullfile(timeSeries.options.paths.timeseries_directory, '*'));
movefile(fullfile(timeSeries.options.paths.analysis_directories.model, 'VOI*'), timeSeries.options.paths.timeseries_directory)
disp('Zipping all created VOI files...');
system(['gzip ', timeSeries.options.paths.timeseries_directory, '/*.nii']);
disp('... Finished!');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAVE THE TIMESERIES EXTRACTION INFO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save(timeSeries.options.saveNames.timeSeries, 'timeSeries');

% Return to analysis script directory    
cd(timeSeries.options.paths.analysis_scripts);

end