%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%% BREATHING SCANNER TASK: SET UP FOLDERS %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 06/02/2019
% -------------------------------------------------------------------------
% TO RUN:   pbihb_setUpFolders(options)
% INPUTS:   options     = PPID-specific matrix output from pbihb_setOptions
%                         Contains a preproc.options structure
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script sets up the folder structure for the imaging analysis of the
% PBIHB study. This must be run locally, with all original images copied
% to the path specified within 'preproc.options.paths.root_local_raw' (that 
% is set within the pbihb_setOptions function). The function takes images
% from the raw location based on key words within their structure name (as
% output by the scanner naming system), and re-names them according to the
% specifications set within pbihb_setOptions. The resulting subject folder 
% structure and files can then be copied to euler for further analyses.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_setUpFolders(options)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE REQUIRED DIRECTORIES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Make new working directory for subject
disp('Creating subject directory...');
[status, msg] = mkdir(fullfile(options.paths.root_fmri, ['TNU_PBIHB_', options.PPID]));

% Make new subdirectories
disp('Creating image directories...');
mkdir(options.paths.image_directory)
mkdir(fullfile(options.paths.image_directory, 'originals'))
disp('Creating physiology directory...');
mkdir(options.paths.physiology_directory)
disp('Creating task directory...');
mkdir(options.paths.task_directory)
disp('Creating scanner logs directory...');
mkdir(options.paths.scanner_logs)
disp('Creating preprocessing directory...');
mkdir(options.paths.preprocessing_directory)
mkdir(options.paths.physio_directory)
mkdir(options.paths.skullStrip_directory);
disp('Creating GLM directory...');
mkdir(options.paths.glm_directory)
disp('Creating analysis directories...');
mkdir(options.paths.analysis_directories.basic)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COPY IMAGE FILES TO ORIGINALS FOLDER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Copying imaging data to ''originals''...');

% Copy task fMRI
copyfile(fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'scandata', '*taskf*.nii'), fullfile(options.paths.image_directory, 'originals'));
currentDir = dir(fullfile(options.paths.image_directory, 'originals', '*task*.nii'));
if strcmp(currentDir(1).name, options.names.images.task_EPI) == 0
    movefile(fullfile(currentDir.folder, currentDir.name), fullfile(options.paths.image_directory, 'originals', options.names.images.task_EPI))
end
clear currentDir;

% Copy rest fMRI if present
restFile = dir (fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'scandata', '*restf*.nii'));
if ~isempty(restFile)
    copyfile(fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'scandata', '*restf*.nii'), fullfile(options.paths.image_directory, 'originals'));
    currentDir = dir(fullfile(options.paths.image_directory, 'originals', '*rest*.nii'));
    if strcmp(currentDir.name, options.names.images.rest_EPI) == 0
        movefile(fullfile(currentDir.folder, currentDir.name), fullfile(options.paths.image_directory, 'originals', options.names.images.rest_EPI))
    end
    clear currentDir;
else
    disp('No rest EPI to copy');
end

% Copy structural
copyfile(fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'scandata', '*struct*.nii'), fullfile(options.paths.image_directory, 'originals'));
currentDir = dir(fullfile(options.paths.image_directory, 'originals', '*struct*.nii'));
if strcmp(currentDir.name, options.names.images.struct) == 0
    movefile(fullfile(currentDir.folder, currentDir.name), fullfile(options.paths.image_directory, 'originals', options.names.images.struct_orig))
end
clear currentDir;

% Copy whole brain fMRI scans (task and rest)
wbEPIFile = dir (fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'scandata', '*wholeb*.nii'));
if ~isempty(wbEPIFile)
    copyfile(fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'scandata', '*wholeb*.nii'), fullfile(options.paths.image_directory, 'originals'));
    currentDir = dir(fullfile(options.paths.image_directory, 'originals', '*wholeb*.nii'));
    if strcmp(currentDir(1).name, options.names.images.task_wbEPI) == 0 % Rename task whole brain EPI
        movefile(fullfile(currentDir(1).folder, currentDir(1).name), fullfile(options.paths.image_directory, 'originals', options.names.images.task_wbEPI))
    end
    if length(currentDir) > 1
        if strcmp(currentDir(2).name, options.names.images.rest_wbEPI) == 0 % Rename rest whole brain EPI
            movefile(fullfile(currentDir(2).folder, currentDir(2).name), fullfile(options.paths.image_directory, 'originals', options.names.images.rest_wbEPI))
        end
        clear currentDir;
    else
        disp('No rest wholebrain EPI to copy');
    end
else
    disp('No wholebrain EPIs to copy');
end

% Copy fieldmaps (task and rest)
fmFile = dir (fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'scandata', '*tosetb0map*_typ18*.nii'));
if ~isempty(fmFile)
    copyfile(fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'scandata', '*tosetb0map*_typ18*.nii'), fullfile(options.paths.image_directory, 'originals'));
    currentDir = dir(fullfile(options.paths.image_directory, 'originals', '*tosetb0map*_typ18*.nii'));
    if strcmp(currentDir(1).name, options.names.images.task_fieldmap) == 0 % Rename task fieldmap
        movefile(fullfile(currentDir(1).folder, currentDir(1).name), fullfile(options.paths.image_directory, 'originals', options.names.images.task_fieldmap))
    end
    if length(currentDir) > 1
        if strcmp(currentDir(2).name, options.names.images.rest_fieldmap) == 0 % Rename rest whole brain EPI
            movefile(fullfile(currentDir(2).folder, currentDir(2).name), fullfile(options.paths.image_directory, 'originals', options.names.images.rest_fieldmap))
        end
        clear currentDir;
    else
        disp('No rest fieldmap to copy');
    end
else
    disp('No fieldmaps to copy');
end

% Copy fieldmap magnitude images (task and rest)
fmMagFile = dir (fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'scandata', '*tosetb0map*_typ0*.nii'));
if ~isempty(fmMagFile)
    copyfile(fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'scandata', '*tosetb0map*_typ0*.nii'), fullfile(options.paths.image_directory, 'originals'));
    currentDir = dir(fullfile(options.paths.image_directory, 'originals', '*tosetb0map*_typ0*.nii'));
    if strcmp(currentDir(1).name, options.names.images.task_fieldmap_mag) == 0 % Rename task fieldmap
        movefile(fullfile(currentDir(1).folder, currentDir(1).name), fullfile(options.paths.image_directory, 'originals', options.names.images.task_fieldmap_mag))
    end
    if length(currentDir) > 1
        if strcmp(currentDir(2).name, options.names.images.rest_fieldmap_mag) == 0 % Rename rest whole brain EPI
            movefile(fullfile(currentDir(2).folder, currentDir(2).name), fullfile(options.paths.image_directory, 'originals', options.names.images.rest_fieldmap_mag))
        end
        clear currentDir;
    else
        disp('No rest fieldmap magnitude image to copy');
    end
else
    disp('No fieldmap magnitude images to copy');
end

% Zip all images
disp('Zipping all original images...');
system(['gzip ', fullfile(options.paths.image_directory, 'originals', '/*.nii')]);
disp('... Finished.');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REORIENT ALL IMAGES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Reorienting all images...');

% Open a file for bash script
fileID = fopen('bashScript.sh','w');

% Add bash command
fprintf(fileID, '#!/bin/bash\n\n');

% Reorient all images
fprintf(fileID, ['fslreorient2std ', fullfile(options.paths.image_directory, 'originals', options.names.images.struct_orig), ' ', fullfile(options.paths.image_directory, options.names.images.struct_orig), '\n']);
if isfile(fullfile(options.paths.image_directory, 'originals', [options.names.images.task_EPI, '.gz']))
    fprintf(fileID, ['fslreorient2std ', fullfile(options.paths.image_directory, 'originals', options.names.images.task_EPI), ' ', fullfile(options.paths.image_directory, options.names.images.task_EPI), '\n']);
end
if isfile(fullfile(options.paths.image_directory, 'originals', [options.names.images.task_wbEPI, '.gz']))
    fprintf(fileID, ['fslreorient2std ', fullfile(options.paths.image_directory, 'originals', options.names.images.task_wbEPI), ' ', fullfile(options.paths.image_directory, options.names.images.task_wbEPI), '\n']);
    % Take only first volume of the wholebrain EPI
    fprintf(fileID, ['fslroi ', fullfile(options.paths.image_directory, options.names.images.task_wbEPI), ' ', fullfile(options.paths.image_directory, options.names.images.task_wbEPI), ' 0 1', '\n']);
end
if isfile(fullfile(options.paths.image_directory, 'originals', [options.names.images.task_fieldmap, '.gz']))
    fprintf(fileID, ['fslreorient2std ', fullfile(options.paths.image_directory, 'originals', options.names.images.task_fieldmap), ' ', fullfile(options.paths.image_directory, options.names.images.task_fieldmap), '\n']);
    fprintf(fileID, ['fslreorient2std ', fullfile(options.paths.image_directory, 'originals', options.names.images.task_fieldmap_mag), ' ', fullfile(options.paths.image_directory, options.names.images.task_fieldmap_mag), '\n']);
end
if isfile(fullfile(options.paths.image_directory, 'originals', [options.names.images.rest_EPI, '.gz']))
    fprintf(fileID, ['fslreorient2std ', fullfile(options.paths.image_directory, 'originals', options.names.images.rest_EPI), ' ', fullfile(options.paths.image_directory, options.names.images.rest_EPI), '\n']);
end
if isfile(fullfile(options.paths.image_directory, 'originals', [options.names.images.rest_wbEPI, '.gz']))
    fprintf(fileID, ['fslreorient2std ', fullfile(options.paths.image_directory, 'originals', options.names.images.rest_wbEPI), ' ', fullfile(options.paths.image_directory, options.names.images.rest_wbEPI), '\n']);
    % Take only first volume of the wholebrain EPI
    fprintf(fileID, ['fslroi ', fullfile(options.paths.image_directory, options.names.images.rest_wbEPI), ' ', fullfile(options.paths.image_directory, options.names.images.rest_wbEPI), ' 0 1', '\n']);
end
if isfile(fullfile(options.paths.image_directory, 'originals', [options.names.images.rest_fieldmap, '.gz']))
    fprintf(fileID, ['fslreorient2std ', fullfile(options.paths.image_directory, 'originals', options.names.images.rest_fieldmap), ' ', fullfile(options.paths.image_directory, options.names.images.rest_fieldmap), '\n']);
    fprintf(fileID, ['fslreorient2std ', fullfile(options.paths.image_directory, 'originals', options.names.images.rest_fieldmap_mag), ' ', fullfile(options.paths.image_directory, options.names.images.rest_fieldmap_mag), '\n']);
end

% Close the file
fclose(fileID);

% Run the bash script
! chmod 777 bashScript.sh
! ./bashScript.sh

% Remove the bash script
! rm bashScript.sh


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COPY OTHER FILES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copy and rename physiology task file to working directory
disp('Copying task physiology data...');
copyfile(fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'behavior', '*task_preproc_data*'), options.paths.physiology_directory);
currentDir = dir(fullfile(options.paths.physiology_directory, '*task_preproc_data*'));
if strcmp(currentDir.name, options.names.labChartData.task) == 0
    movefile(fullfile(currentDir.folder, currentDir.name), fullfile(options.paths.physiology_directory, options.names.labChartData.task))
end
clear currentDir;

% Copy and rename physiology rest file to working directory
disp('Copying rest physiology data...');
restPhysiologyFile = dir (fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'behavior', '*rest_preproc_data*'));
if ~isempty(restPhysiologyFile)
    copyfile(fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'behavior', '*rest_preproc_data*'), options.paths.physiology_directory);
    currentDir = dir(fullfile(options.paths.physiology_directory, '*rest_preproc_data*'));
    if strcmp(currentDir.name, options.names.labChartData.rest) == 0
        movefile(fullfile(currentDir.folder, currentDir.name), fullfile(options.paths.physiology_directory, options.names.labChartData.rest))
    end
    clear currentDir;
else
    disp('Skipping rest physiology data: Does not exist');
end

% Copy and rename task physiology comments file to working directory
disp('Copying task physiology data comments...');
copyfile(fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'behavior', '*task_preproc_comments*'), options.paths.physiology_directory);
currentDir = dir(fullfile(options.paths.physiology_directory, '*task_preproc_comments*'));
if strcmp(currentDir.name, options.names.labChartComments.task) == 0
    movefile(fullfile(currentDir.folder, currentDir.name), fullfile(options.paths.physiology_directory, options.names.labChartComments.task))
end
clear currentDir;

% Copy and rename physiology comments file to working directory
restPhysiologyComments = dir (fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'behavior', '*rest_preproc_comments*'));
if ~isempty(restPhysiologyComments)
    disp('Copying rest physiology data comments...');
    copyfile(fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'behavior', '*rest_preproc_comments*'), options.paths.physiology_directory);
    currentDir = dir(fullfile(options.paths.physiology_directory, '*rest_preproc_comments*'));
    if strcmp(currentDir.name, options.names.labChartComments.rest) == 0
        movefile(fullfile(currentDir.folder, currentDir.name), fullfile(options.paths.physiology_directory, options.names.labChartComments.rest))
    end
    clear currentDir;
end

% Copy and rename behavioural task file to working directory
disp('Copying task behavioural data...');
copyfile(fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'behavior', [options.PPID, '_task*']), options.paths.task_directory);
currentDir = dir(fullfile(options.paths.task_directory, [options.PPID, '_task*']));
if strcmp(currentDir(1).name, options.names.taskData) == 0
    movefile(fullfile(currentDir(1).folder, currentDir(1).name), (fullfile(options.paths.task_directory, options.names.taskData)))
end
clear currentDir;

% Copy and rename scanner logs to working directory
disp('Copying scanner logs...');
scanLog = dir (fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'physlog', '*.log'));
if ~isempty(scanLog) == 1
    copyfile(fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'physlog', '*task*.log'), options.paths.scanner_logs);
    currentDir = dir(fullfile(options.paths.scanner_logs, '*task*.log'));
    if strcmp(currentDir(1).name, options.names.scannerLogs.task) == 0
        movefile(fullfile(currentDir(1).folder, currentDir(1).name), fullfile(options.paths.scanner_logs, options.names.scannerLogs.task))
    end
    clear currentDir;
    logRestFile = dir (fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'physlog', '*rest*.log'));
    if ~isempty(logRestFile)
        copyfile(fullfile(options.paths.root_local_raw, ['TNU_PBIHB_', options.PPID], 'physlog', '*rest*.log'), options.paths.scanner_logs);
        currentDir = dir(fullfile(options.paths.scanner_logs, '*rest*.log'));
        if strcmp(currentDir.name, options.names.scannerLogs.rest) == 0
            movefile(fullfile(currentDir.folder, currentDir.name), fullfile(options.paths.scanner_logs, options.names.scannerLogs.rest))
        end
        clear currentDir;
    else
        disp('Skipping rest scanner logs: Do not exist');
    end
else
    disp('Skipping all scanner logs: Do not exist');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COPY ENTIRE SUBJECT DIRECTORY TO SETUP DIRECTORY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Copying directory structure to ''setup'' folder...');
copyfile(fullfile(options.paths.root_local, ['TNU_PBIHB_', options.PPID]), fullfile(options.paths.root_local_setup, ['TNU_PBIHB_', options.PPID]));

end