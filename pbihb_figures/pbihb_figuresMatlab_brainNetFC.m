%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% BREATHING SCANNER TASK: FUNCTIONAL CONNNECTIVITY FIGURES %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 13/07/2020
% -------------------------------------------------------------------------
% TO RUN:    pbihb_figuresMatlab_brainNetFC(options)
% INPUTS:    options    = PPID-specific matrix output from the
%                         pbihb_setOptions function
%            type       = 'exclude' or 'wholeGroup'
% OUTPUTS:   Matlab figures = BrainNet functional connectivity figures for
%                             the PBIHB study paper
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script uses the Matlab toolbox BrainNet to create the functional
% connectivity figures for the PBIHB study
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_figuresMatlab_brainNetFC(options, type)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET THE ADDITIONAL OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(type, 'exclude')
    groupDirFC_aIns = fullfile(options.paths.group_directory_imaging, 'PBIHB_groupsEx_FC_FE_aIns_predict_error');
    groupDirFC_PAG = fullfile(options.paths.group_directory_imaging, 'PBIHB_groupsEx_FC_FE_PAG_error');
    groupDirModel = fullfile(options.paths.group_directory_imaging, 'PBIHB_groupsEx_model');
elseif strcmp(type, 'wholeGroup')
    groupDirFC_aIns = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_FC_FE_aIns_predict_error');
    groupDirFC_PAG = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_FC_FE_PAG_error');
    groupDirModel = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_model');
end

% Specify paths and save names for aIns functional connectivity group figures
aIns.groupDiff.loadPredict = fullfile(groupDirFC_aIns, 'lowerLevel_con0001_predict_average', 'spm_*_005.csv');
aIns.groupDiff.loadPredictMax_left = fullfile(groupDirModel, 'lowerLevel_con0009_predAvg', 'spmT_0003_aInsLeft.txt');
aIns.groupDiff.loadPredictMax_right = fullfile(groupDirModel, 'lowerLevel_con0009_predAvg', 'spmT_0003_aInsRight.txt');
aIns.groupDiff.predictClusters = fullfile(groupDirFC_aIns, 'lowerLevel_con0001_predict_average', 'spmT_0005_thresh_GroupDiff_neg_clusterFWE.nii.gz');
aIns.groupDiff.predictNodes = fullfile(groupDirModel, 'lowerLevel_con0009_predAvg', 'spmT_0003_aIns.node');
aIns.groupDiff.predictEdges = fullfile(groupDirModel, 'lowerLevel_con0009_predAvg', 'spmT_0003_aIns.edge');
aIns.groupDiff.predictImage = fullfile(groupDirFC_aIns, 'lowerLevel_con0001_predict_average', 'PBIHB_groups_FC_FE_aIns_predict_error_predict_average_GroupDiff_3D.jpg');
aIns.posNegDiff.loadPredict = fullfile(groupDirFC_aIns, 'lowerLevel_con0002_predict_PosNegDiff', 'spm_*_003.csv');
aIns.posNegDiff.loadPredictMax_left = fullfile(groupDirModel, 'lowerLevel_con0012_predIntNeg', 'spmT_0002_aInsLeft.txt');
aIns.posNegDiff.loadPredictMax_right = fullfile(groupDirModel, 'lowerLevel_con0012_predIntNeg', 'spmT_0002_aInsRight.txt');
aIns.posNegDiff.predictClusters = fullfile(groupDirFC_aIns, 'lowerLevel_con0002_predict_PosNegDiff', 'spmT_0003_thresh_GroupMean_neg_clusterFWE.nii.gz');
aIns.posNegDiff.predictNodes = fullfile(groupDirModel, 'lowerLevel_con0012_predIntNeg', 'spmT_0002_aIns.node');
aIns.posNegDiff.predictEdges = fullfile(groupDirModel, 'lowerLevel_con0012_predIntneg', 'spmT_0002_aIns.edge');
aIns.posNegDiff.predictImage = fullfile(groupDirFC_aIns, 'lowerLevel_con0002_predict_PosNegDiff', 'PBIHB_groups_FC_FE_aIns_predict_error_predict_NegPosDiff_3D.jpg');

% Specify paths and save names for PAG functional connectivity figures
PAG.posNegDiff.loadError = fullfile(groupDirFC_PAG, 'lowerLevel_con0002_error_PosNegDiff', 'spm_*_002.csv');
PAG.posNegDiff.loadErrorMax_left = fullfile(groupDirModel, 'lowerLevel_con0010_errorAvg', 'spmT_0002_pagLeft.txt');
PAG.posNegDiff.loadErrorMax_right = fullfile(groupDirModel, 'lowerLevel_con0010_errorAvg', 'spmT_0002_pagRight.txt');
PAG.posNegDiff.errorClusters = fullfile(groupDirFC_PAG, 'lowerLevel_con0002_error_PosNegDiff', 'spmT_0002_thresh_GroupMean_pos_clusterFWE.nii.gz');
PAG.posNegDiff.errorNodes = fullfile(groupDirModel, 'lowerLevel_con0010_errorAvg', 'spmT_0002_pag.node');
PAG.posNegDiff.errorEdges = fullfile(groupDirModel, 'lowerLevel_con0010_errorAvg', 'spmT_0002_pag.edge');
PAG.posNegDiff.errorImage = fullfile(groupDirFC_PAG, 'lowerLevel_con0002_error_PosNegDiff', 'PBIHB_groups_FC_FE_PAG_error_PosNegDiff_3D.jpg');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD THE FC CLUSTER INFORMATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load aIns peak prediction information
fc.aIns.predict.groupDiff.seed.left = readtable(aIns.groupDiff.loadPredictMax_left);
fc.aIns.predict.groupDiff.seed.right = readtable(aIns.groupDiff.loadPredictMax_right);
fc.aIns.predict.posNegDiff.seed.left = readtable(aIns.posNegDiff.loadPredictMax_left);
fc.aIns.predict.posNegDiff.seed.right = readtable(aIns.posNegDiff.loadPredictMax_right);

% Load aIns FC clusters
currentDir = dir(aIns.groupDiff.loadPredict);
fc.aIns.predict.groupDiff.clusters.table = readtable(fullfile(currentDir.folder, currentDir.name));
fc.aIns.predict.groupDiff.clusters.table = fc.aIns.predict.groupDiff.clusters.table(2:end,:);
fc.aIns.predict.groupDiff.clusters.matrix = table2array(fc.aIns.predict.groupDiff.clusters.table);
fc.aIns.predict.groupDiff.clusters.matrix = str2double(fc.aIns.predict.groupDiff.clusters.matrix);
currentDir = dir(aIns.posNegDiff.loadPredict);
fc.aIns.predict.posNegDiff.clusters.table = readtable(fullfile(currentDir.folder, currentDir.name));
fc.aIns.predict.posNegDiff.clusters.table = fc.aIns.predict.posNegDiff.clusters.table(2:end,:);
fc.aIns.predict.posNegDiff.clusters.matrix = table2array(fc.aIns.predict.posNegDiff.clusters.table);
fc.aIns.predict.posNegDiff.clusters.matrix = str2double(fc.aIns.predict.posNegDiff.clusters.matrix);

% Load PAG peak error information
fc.PAG.error.posNegDiff.seed.left = readtable(PAG.posNegDiff.loadErrorMax_left);
fc.PAG.error.posNegDiff.seed.right = readtable(PAG.posNegDiff.loadErrorMax_right);

% Load PAG FC clusters
currentDir = dir(PAG.posNegDiff.loadError);
fc.PAG.error.posNegDiff.clusters.table = readtable(fullfile(currentDir.folder, currentDir.name));
fc.PAG.error.posNegDiff.clusters.table = fc.PAG.error.posNegDiff.clusters.table(2:end,:);
fc.PAG.error.posNegDiff.clusters.matrix = table2array(fc.PAG.error.posNegDiff.clusters.table);
fc.PAG.error.posNegDiff.clusters.matrix = str2double(fc.PAG.error.posNegDiff.clusters.matrix);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE NODE FILES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create node file for aIns group difference predictions
% Add the aIns nodes
predictNodes_groupDiff(1,:) = [fc.aIns.predict.groupDiff.seed.left.MAXX_mm_(1), fc.aIns.predict.groupDiff.seed.left.MAXY_mm_(1), fc.aIns.predict.groupDiff.seed.left.MAXZ_mm_(1), 3, options.masks.sphereSize.aIns];
predictNodes_groupDiff(2,:) = [fc.aIns.predict.groupDiff.seed.right.MAXX_mm_(1), fc.aIns.predict.groupDiff.seed.right.MAXY_mm_(1), fc.aIns.predict.groupDiff.seed.right.MAXZ_mm_(1), 3, options.masks.sphereSize.aIns];
count = 2;
% Add the clusters from aIns prediction nodes
for a = 1:size(fc.aIns.predict.groupDiff.clusters.matrix,1)
    if ~isnan(fc.aIns.predict.groupDiff.clusters.matrix(a,3))
        count = count + 1;
        predictNodes_groupDiff(count,:) = [fc.aIns.predict.groupDiff.clusters.matrix(a,12), fc.aIns.predict.groupDiff.clusters.matrix(a,13), fc.aIns.predict.groupDiff.clusters.matrix(a,14), 4, options.masks.sphereSize.aIns];
    end  
end
% Save the predictions node file
save(aIns.groupDiff.predictNodes, 'predictNodes_groupDiff', '-ascii');

% Create node file for aIns pos vs neg difference predictions
% Add the aIns nodes
predictNodes_posNegDiff(1,:) = [fc.aIns.predict.posNegDiff.seed.left.MAXX_mm_(1), fc.aIns.predict.posNegDiff.seed.left.MAXY_mm_(1), fc.aIns.predict.posNegDiff.seed.left.MAXZ_mm_(1), 3, options.masks.sphereSize.aIns];
predictNodes_posNegDiff(2,:) = [fc.aIns.predict.posNegDiff.seed.right.MAXX_mm_(1), fc.aIns.predict.posNegDiff.seed.right.MAXY_mm_(1), fc.aIns.predict.posNegDiff.seed.right.MAXZ_mm_(1), 3, options.masks.sphereSize.aIns];
count = 2;
% Add the clusters from aIns prediction nodes
for a = 1:size(fc.aIns.predict.posNegDiff.clusters.matrix,1)
    if ~isnan(fc.aIns.predict.posNegDiff.clusters.matrix(a,3))
        count = count + 1;
        predictNodes_posNegDiff(count,:) = [fc.aIns.predict.posNegDiff.clusters.matrix(a,12), fc.aIns.predict.posNegDiff.clusters.matrix(a,13), fc.aIns.predict.posNegDiff.clusters.matrix(a,14), 4, options.masks.sphereSize.aIns];
    end  
end
% Save the predictions node file
save(aIns.posNegDiff.predictNodes, 'predictNodes_posNegDiff', '-ascii');

% Create node file for PAG pos vs neg difference predictions
% Add the PAG nodes
errorNodes_posNegDiff(1,:) = [fc.PAG.error.posNegDiff.seed.left.MAXX_mm_(1), fc.PAG.error.posNegDiff.seed.left.MAXY_mm_(1), fc.PAG.error.posNegDiff.seed.left.MAXZ_mm_(1), 3, options.masks.sphereSize.aIns];
errorNodes_posNegDiff(2,:) = [fc.PAG.error.posNegDiff.seed.right.MAXX_mm_(1), fc.PAG.error.posNegDiff.seed.right.MAXY_mm_(1), fc.PAG.error.posNegDiff.seed.right.MAXZ_mm_(1), 3, options.masks.sphereSize.aIns];
count = 2;
% Add the clusters from aIns prediction nodes
for a = 1:size(fc.PAG.error.posNegDiff.clusters.matrix,1)
    if ~isnan(fc.PAG.error.posNegDiff.clusters.matrix(a,3))
        count = count + 1;
        errorNodes_posNegDiff(count,:) = [fc.PAG.error.posNegDiff.clusters.matrix(a,12), fc.PAG.error.posNegDiff.clusters.matrix(a,13), fc.PAG.error.posNegDiff.clusters.matrix(a,14), 4, options.masks.sphereSize.aIns];
    end  
end
% Save the predictions node file
save(PAG.posNegDiff.errorNodes, 'errorNodes_posNegDiff', '-ascii');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE EDGE FILES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create edge file for aIns group difference predictions
% Create the base edge file
predictEdges_groupDiff = zeros(size(predictNodes_groupDiff,1),size(predictNodes_groupDiff,1));
% Add the connections between aIns and clusters
predictEdges_groupDiff(3:end,1:2) = 1;
predictEdges_groupDiff(1:2,3:end) = 1;
% Save the edge files
save(aIns.groupDiff.predictEdges, 'predictEdges_groupDiff', '-ascii');

% Create edge file for aIns pos vs neg difference predictions
% Create the base edge file
predictEdges_posNegDiff = zeros(size(predictNodes_posNegDiff,1),size(predictNodes_posNegDiff,1));
% Add the connections between aIns and clusters
predictEdges_posNegDiff(3:end,1:2) = 1;
predictEdges_posNegDiff(1:2,3:end) = 1;
% Save the edge files
save(aIns.posNegDiff.predictEdges, 'predictEdges_posNegDiff', '-ascii');

% Create edge file for PAG pos vs neg difference errors
% Create the base edge file
errorEdges_posNegDiff = zeros(size(errorNodes_posNegDiff,1),size(errorNodes_posNegDiff,1));
% Add the connections between aIns and clusters
errorEdges_posNegDiff(3:end,1:2) = 1;
errorEdges_posNegDiff(1:2,3:end) = 1;
% Save the edge files
save(PAG.posNegDiff.errorEdges, 'errorEdges_posNegDiff', '-ascii');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE THE FUNCTIONAL CONNECTIVITY FIGURES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

surfaceFile = fullfile(options.paths.brainNet, 'Data', 'SurfTemplate', 'BrainMesh_ICBM152.nv');
optionsFile = fullfile(options.paths.figureCode, 'pbihb_brainNet_cfg.mat');

% Create the FC figures
BrainNet_MapCfg(surfaceFile, aIns.groupDiff.predictNodes, aIns.groupDiff.predictEdges, optionsFile, aIns.groupDiff.predictImage);
BrainNet_MapCfg(surfaceFile, aIns.posNegDiff.predictNodes, aIns.posNegDiff.predictEdges, optionsFile, aIns.posNegDiff.predictImage);
BrainNet_MapCfg(surfaceFile, PAG.posNegDiff.errorNodes, PAG.posNegDiff.errorEdges, optionsFile, PAG.posNegDiff.errorImage);


end