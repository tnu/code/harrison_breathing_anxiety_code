###########################################################################################

# PBIHB: MAKE LIGHTBOX SUMMARY FIGURES

############################################################################################

# DESCRIPTION:
# This function creates lightbox summary images using FSLeyes for the randomise analyses
# Usage: ./runLightBoxR $data $scripts $higherL $lowerL $conName
#	data = path to main data folder
#	scripts = path to PBIHB scripts folder
#	higherL = name of higher level design ('groups' 'groupsEx' 'regress' or 'explore')
#	lowerL = name of lower level design ('basic' 'model' or 'modelFactors')
#	conName = name of lower level contrast (number or name e.g. con0001 or cueTot)

############################################################################################
############################################################################################

# Define the function
runLightBoxR(){

	# Define local variables from inputs
	local data=$1
	local scripts=$2
	local higherL=$3
	local lowerL=$4
	local conName=$5
	
	dispMin=0.95
	dispMax=1.0

	# make sure filename supplied as command line arg else die
	[ $# -eq 0 ] && { echo "Usage: $0 data scripts higherL lowerL conName"; exit 1; }

	# Create path for data
	dataPath=$(find "${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_${higherL}_${lowerL}" -name "*${conName}*" | head -n1)

	# Create name lists for contrasts
	if [ "${higherL}" = "groups" ] || [ "${higherL}" = "groupsEx" ]; then
		conNames=(GroupMean GroupDiff)
	elif [ "${higherL}" = "regress" ]; then
		conNames=(GroupMean BreathAnx)
	elif [ "${higherL}" = "explore" ]; then
		conNames=(GroupMean staiT cesd gender)
	fi

	# Specify background image
	bgImage=${scripts}/pbihb_masks/MNI152_T1_1mm_brain.nii.gz
	bgMask=${scripts}/pbihb_masks/PBIHB_aInsBN_PAG_mask_inv.nii.gz

	# Start the counter
	declare -i count=1
	declare -i count2=2

	# Run the image processing for each contrast in higher level design
	for con in ${conNames[@]}; do
		fsleyes render -of "${dataPath}/PBIHB_${higherL}_${lowerL}_${conName}_${con}_RAND.png" \
			--scene lightbox \
			--zaxis Z --nrows 1 --ncols 5 --sliceSpacing 7.0 --zrange 56.0 120.0 \
			--worldLoc 0.0 0.0 0.0 --hideCursor --bgColour 1.0 1.0 1.0 \
			$bgImage -dr 0.0 10000 \
			"${dataPath}/randomise_${conName}/randomise_${conName}_tfce_corrp_tstat${count}.nii.gz" \
			--cmap red-yellow --displayRange $dispMin $dispMax \
			"${dataPath}/randomise_${conName}/randomise_${conName}_tfce_corrp_tstat${count2}.nii.gz" \
			--cmap blue-lightblue --displayRange $dispMin $dispMax \
			"${bgMask}" --alpha 20 \
			--cmap greyscale --displayRange 0 5 ;
			count=$(( count + 2 ))
			count2=$(( count2 + 2 ))
		done

	# Crop the image to remove white border
	for im in ${dataPath}/PBIHB_${higherL}_${lowerL}_${conName}_*.png; do
    		sips --cropToHeightWidth 325 800 "$im"
	done

}

# Invoke the function and pass arguments
runLightBoxR $1 $2 $3 $4 $5
