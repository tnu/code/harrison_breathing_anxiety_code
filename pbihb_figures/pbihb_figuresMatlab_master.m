%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% BREATHING SCANNER TASK: MATLAB FIGURES %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 06/07/2020
% -------------------------------------------------------------------------
% TO RUN:    pbihb_figuresMatlab_master(location)
% INPUTS:    location   = 'local' or 'euler'
% OUTPUTS:   Matlab figure components for PBIHB figures
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script creates the Matlab figure components for the PBIHB figures:
%   - Single subject example prediction and error trajectories from the
%     contingency model fits and de-coded trajectories
%	- Violin plots of the single subject contrast values in insula regions
%	- Violin plots of FDT results
%	- Violin plots of questionnaire results
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_figuresMatlab_master(location)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

options = pbihb_setOptions('all', location);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE FIGURES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Single subject example trajectories
pbihb_figuresMatlab_trajectories(options);
close all

% Single subject contrast values in insula regions
pbihb_figuresMatlab_barPlotsBLT(options, 'exclude');
close all

% FDT and questionnaire results
pbihb_figuresMatlab_barPlotsFDT(options);
close all

% Multi-modal figures
pbihb_figuresMatlab_multiModalPCA(options);
close all

end