%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% BREATHING SCANNER TASK: MATLAB TRAJECTORY FIGURES %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 08/07/2020
% -------------------------------------------------------------------------
% TO RUN:    pbihb_figuresMatlab_barPlotsBLT(options)
% INPUTS:    options    = PPID-specific matrix output from the
%                         pbihb_setOptions function
%            type       = 'total' or 'exclude'
% OUTPUTS:   Matlab figures = Bar plots for PBIHB figures
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script creates the Matlab figure components that show barplots of
% the significant insula contrast estimates from the PBIHB study:
%   - Bar plots of the blob insula with pos > neg prediction errors 
%   - Bar plots of the insula blob with mod < low prediction interaction
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_figuresMatlab_barPlotsBLT(options, type)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET THE ADDITIONAL OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify save names for matlab insula contrast estimates
if strcmp(type, 'total')
    options.saveNames.figures.insula.predictUncPosLeft = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'con0002_predictPosLeft.txt');
    options.saveNames.figures.insula.predictUncPosRight = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'con0002_predictPosRight.txt');
    options.saveNames.figures.insula.predictUncNegLeft = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'con0003_predictNegLeft.txt');
    options.saveNames.figures.insula.predictUncNegRight = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'con0003_predictNegRight.txt');
    options.saveNames.figures.barPlotPredUncLeft = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'PBIHB_predPlotLeft');
    options.saveNames.figures.barPlotPredUncRight = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'PBIHB_predPlotRight');
    options.saveNames.figures.barPlotPredUncViolinLeft = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'PBIHB_predPlotViolinLeft');
    options.saveNames.figures.barPlotPredUncViolinRight = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'PBIHB_predPlotViolinRight');
elseif strcmp(type, 'exclude')
    options.saveNames.figures.insula.predictUncPosExLeft = fullfile(options.paths.group_directory_imaging, 'PBIHB_groupsEx_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'con0002_predictPosLeft.txt');
    options.saveNames.figures.insula.predictUncPosExRight = fullfile(options.paths.group_directory_imaging, 'PBIHB_groupsEx_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'con0002_predictPosRight.txt');
    options.saveNames.figures.insula.predictUncNegExLeft = fullfile(options.paths.group_directory_imaging, 'PBIHB_groupsEx_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'con0003_predictNegLeft.txt');
    options.saveNames.figures.insula.predictUncNegExRight = fullfile(options.paths.group_directory_imaging, 'PBIHB_groupsEx_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'con0003_predictNegRight.txt');
    options.saveNames.figures.barPlotPredUncExLeft = fullfile(options.paths.group_directory_imaging, 'PBIHB_groupsEx_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'PBIHB_predPlotExLeft');
    options.saveNames.figures.barPlotPredUncExRight = fullfile(options.paths.group_directory_imaging, 'PBIHB_groupsEx_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'PBIHB_predPlotExRight');
    options.saveNames.figures.barPlotPredUncViolinExLeft = fullfile(options.paths.group_directory_imaging, 'PBIHB_groupsEx_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'PBIHB_predPlotViolinExLeft');
    options.saveNames.figures.barPlotPredUncViolinExRight = fullfile(options.paths.group_directory_imaging, 'PBIHB_groupsEx_model', 'lowerLevel_con0011_predIntPos', 'randomise_predIntPos', 'PBIHB_predPlotViolinExRight');
end
options.saveNames.figures.insula.predictDecPosLeft = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_basic', 'lowerLevel_con0010_cueYesNoDiff', 'randomise_cueYesNoDiff', 'con0002_predictDecPosLeft.txt');
options.saveNames.figures.insula.predictDecPosRight = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_basic', 'lowerLevel_con0010_cueYesNoDiff', 'randomise_cueYesNoDiff', 'con0002_predictDecPosRight.txt');
options.saveNames.figures.insula.predictDecNegLeft = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_basic', 'lowerLevel_con0010_cueYesNoDiff', 'randomise_cueYesNoDiff', 'con0001_predictDecNegLeft.txt');
options.saveNames.figures.insula.predictDecNegRight = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_basic', 'lowerLevel_con0010_cueYesNoDiff', 'randomise_cueYesNoDiff', 'con0001_predictDecNegRight.txt');
options.saveNames.figures.barPlotPredDecLeft = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_basic', 'lowerLevel_con0010_cueYesNoDiff', 'randomise_cueYesNoDiff', 'PBIHB_predDecPlotLeft');
options.saveNames.figures.barPlotPredDecRight = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_basic', 'lowerLevel_con0010_cueYesNoDiff', 'randomise_cueYesNoDiff', 'PBIHB_predDecPlotRight');
options.saveNames.figures.barPlotPredDecViolinLeft = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_basic', 'lowerLevel_con0010_cueYesNoDiff', 'randomise_cueYesNoDiff', 'PBIHB_predDecPlotViolinLeft');
options.saveNames.figures.barPlotPredDecViolinRight = fullfile(options.paths.group_directory_imaging, 'PBIHB_groups_basic', 'lowerLevel_con0010_cueYesNoDiff', 'randomise_cueYesNoDiff', 'PBIHB_predDecPlotViolinRight');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD THE DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load extracted mean timeseries for the predictions
if strcmp(type, 'total')
    insula.predUnc.posLeft = load(options.saveNames.figures.insula.predictUncPosLeft);
    insula.predUnc.posRight = load(options.saveNames.figures.insula.predictUncPosRight);
    insula.predUnc.negLeft = load(options.saveNames.figures.insula.predictUncNegLeft);
    insula.predUnc.negRight = load(options.saveNames.figures.insula.predictUncNegRight);
elseif strcmp(type, 'exclude')
    insula.predUnc.posExLeft = load(options.saveNames.figures.insula.predictUncPosExLeft);
    insula.predUnc.posExRight = load(options.saveNames.figures.insula.predictUncPosExRight);
    insula.predUnc.negExLeft = load(options.saveNames.figures.insula.predictUncNegExLeft);
    insula.predUnc.negExRight = load(options.saveNames.figures.insula.predictUncNegExRight);
end
insula.predDec.posLeft = load(options.saveNames.figures.insula.predictDecPosLeft);
insula.predDec.posRight = load(options.saveNames.figures.insula.predictDecPosRight);
insula.predDec.negLeft = load(options.saveNames.figures.insula.predictDecNegLeft);
insula.predDec.negRight = load(options.saveNames.figures.insula.predictDecNegRight);

% Load the GLM info
groupDiffGlmInfo = load(options.saveNames.groupDiffGLMinfo);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REMOVE THE EXCLUDED DATASETS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Remove indices
idx.lowEx = options.PPIDs.low.idx;
idx.modEx = options.PPIDs.mod.idx;
for a = 1:length(groupDiffGlmInfo.idxEx)
    if any(idx.lowEx == groupDiffGlmInfo.idxEx(a))
        idx.lowEx(end) = [];
        idx.modEx = idx.modEx - 1;
    end
    if any(idx.modEx == groupDiffGlmInfo.idxEx(a))
        idx.modEx(end) = [];
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY THE COLOURS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

colours.two = {[1 0.7 0]; [0.8 0 0]};
colours.four = {[1 0.7 0]; [0.8 0 0]; [1 0.7 0]; [0.8 0 0]};

    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE FIGURE FOR PREDICTIONS FOR ALL PARTICIPANTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(type, 'total')
    % Set the figure inputs and options
    insula.predUnc.totalNames = {'Low', 'Moderate'};
    insula.predUnc.totalNamesDouble = {'Low', 'Moderate', 'Low', 'Moderate'};
    insula.predUnc.totalLeft = [insula.predUnc.posLeft(options.PPIDs.low.idx), insula.predUnc.negLeft(options.PPIDs.low.idx) ...
        insula.predUnc.posLeft(options.PPIDs.mod.idx), insula.predUnc.negLeft(options.PPIDs.mod.idx)];
    insula.predUnc.totalRight = [insula.predUnc.posRight(options.PPIDs.low.idx), insula.predUnc.negRight(options.PPIDs.low.idx) ...
        insula.predUnc.posRight(options.PPIDs.mod.idx), insula.predUnc.negRight(options.PPIDs.mod.idx)];
    insula.predUnc.meanLeft = [mean(insula.predUnc.posLeft(options.PPIDs.low.idx)), mean(insula.predUnc.negLeft(options.PPIDs.low.idx)); ...
        mean(insula.predUnc.posLeft(options.PPIDs.mod.idx)), mean(insula.predUnc.negLeft(options.PPIDs.mod.idx))];
    insula.predUnc.meanRight = [mean(insula.predUnc.posRight(options.PPIDs.low.idx)), mean(insula.predUnc.negRight(options.PPIDs.low.idx)); ...
        mean(insula.predUnc.posRight(options.PPIDs.mod.idx)), mean(insula.predUnc.negRight(options.PPIDs.mod.idx))];
    insula.predUnc.errsLeft = [std(insula.predUnc.posLeft(options.PPIDs.low.idx))/sqrt(length(insula.predUnc.posLeft(options.PPIDs.low.idx))), ...
        std(insula.predUnc.negLeft(options.PPIDs.low.idx))/sqrt(length(insula.predUnc.negLeft(options.PPIDs.low.idx))); ...
        std(insula.predUnc.posLeft(options.PPIDs.mod.idx))/sqrt(length(insula.predUnc.posLeft(options.PPIDs.mod.idx))), ...
        std(insula.predUnc.negLeft(options.PPIDs.mod.idx))/sqrt(length(insula.predUnc.negLeft(options.PPIDs.mod.idx)))];
    insula.predUnc.errsRight = [std(insula.predUnc.posRight(options.PPIDs.low.idx))/sqrt(length(insula.predUnc.posRight(options.PPIDs.low.idx))), ...
        std(insula.predUnc.negRight(options.PPIDs.low.idx))/sqrt(length(insula.predUnc.negRight(options.PPIDs.low.idx))); ...
        std(insula.predUnc.posRight(options.PPIDs.mod.idx))/sqrt(length(insula.predUnc.posRight(options.PPIDs.mod.idx))), ...
        std(insula.predUnc.negRight(options.PPIDs.mod.idx))/sqrt(length(insula.predUnc.negRight(options.PPIDs.mod.idx)))];
    input_options = {'color', colours.four, 'nofig', 'names', insula.predUnc.totalNamesDouble, 'noind'};

    % Make the figure for the left side
    figure;
        set(gcf, 'Position', [0 0 600 600]);
        x = [1,1;2,2];
        h = bar(x, insula.predUnc.meanLeft, 'FaceColor', 'flat');
        h(1).LineWidth = 1;
        h(2).LineWidth = 1;
        hold on
        ngroups = size(insula.predUnc.meanLeft, 1);
        nbars = size(insula.predUnc.meanLeft, 2);
        % Calculating the width for each bar group
        groupwidth = min(0.8, nbars/(nbars + 1.5));
        for i = 1:nbars
            x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
            err = errorbar(x, insula.predUnc.meanLeft(:,i), insula.predUnc.errsLeft(:,i), '.');
            err.Color = [0 0 0];
            err.LineStyle = 'none';
            err.LineWidth = 1;
        end
        h(1).CData(1,:) = [1 0.7 0]; % group 1 1st bar
        h(1).CData(2,:) = [1 0.7 0]; % group 1 2nd bar
        h(2).CData(1,:) = [0.8 0 0];
        h(2).CData(2,:) = [0.8 0 0];
        ylim([-40, 0]);
        set(gca, 'FontSize', 26);
        set(gca, 'XTickLabel', insula.predUnc.totalNames);
        xlabel('Anxiety Group', 'FontSize', 38);
        ylabel('Contrast estimates (au)', 'FontSize', 38);
        text(1.25, 105, 'Low', 'FontSize', 38);
        text(2.9, 105, 'Moderate', 'FontSize', 38);
%         title('Prediction certainty estimates', 'FontSize', 42, 'FontWeight', 'normal');
        legend({'Pos', 'Neg'}, 'Location', 'southeast', 'FontSize', 38);
        print(options.saveNames.figures.barPlotPredUncLeft, '-dtiff');

    % Make the violin plot for the left side
    figure;
        set(gcf, 'Position', [0 0 600 600]);
        barplot_columns(insula.predUnc.totalLeft, input_options{:});
        hold on
        ylim([-50, 20]);
        set(gca, 'FontSize', 26);
        set(gca, 'XTickLabel', insula.predUnc.totalNamesDouble);
        xlabel('Anxiety Group', 'FontSize', 38);
        ylabel('Contrast estimates (au)', 'FontSize', 38);
%         title('Prediction certainty estimates', 'FontSize', 42, 'FontWeight', 'normal');
        print(options.saveNames.figures.barPlotPredUncViolinLeft, '-dtiff');
        
    % Make the figure for the right side
    figure;
        set(gcf, 'Position', [0 0 600 600]);
        x = [1,1;2,2];
        h = bar(x, insula.predUnc.meanRight, 'FaceColor', 'flat');
        h(1).LineWidth = 1;
        h(2).LineWidth = 1;
        hold on
        ngroups = size(insula.predUnc.meanRight, 1);
        nbars = size(insula.predUnc.meanRight, 2);
        % Calculating the width for each bar group
        groupwidth = min(0.8, nbars/(nbars + 1.5));
        for i = 1:nbars
            x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
            err = errorbar(x, insula.predUnc.meanRight(:,i), insula.predUnc.errsRight(:,i), '.');
            err.Color = [0 0 0];
            err.LineStyle = 'none';
            err.LineWidth = 1;
        end
        h(1).CData(1,:) = [1 0.7 0]; % group 1 1st bar
        h(1).CData(2,:) = [1 0.7 0]; % group 1 2nd bar
        h(2).CData(1,:) = [0.8 0 0];
        h(2).CData(2,:) = [0.8 0 0];
        ylim([-40, 0]);
        set(gca, 'FontSize', 26);
        set(gca, 'XTickLabel', insula.predUnc.totalNames);
        xlabel('Anxiety Group', 'FontSize', 38);
        ylabel('Contrast estimates (au)', 'FontSize', 38);
        text(1.25, 105, 'Low', 'FontSize', 38);
        text(2.9, 105, 'Moderate', 'FontSize', 38);
%         title('Prediction certainty estimates', 'FontSize', 42, 'FontWeight', 'normal');
        legend({'Pos', 'Neg'}, 'Location', 'southeast', 'FontSize', 38);
        print(options.saveNames.figures.barPlotPredUncRight, '-dtiff');

    % Make the violin plot for the right side
    figure;
        set(gcf, 'Position', [0 0 600 600]);
        barplot_columns(insula.predUnc.totalRight, input_options{:});
        hold on
        ylim([-50, 20]);
        set(gca, 'FontSize', 26);
        set(gca, 'XTickLabel', insula.predUnc.totalNamesDouble);
        xlabel('Anxiety Group', 'FontSize', 38);
        ylabel('Contrast estimates (au)', 'FontSize', 38);
%         title('Prediction certainty estimates', 'FontSize', 42, 'FontWeight', 'normal');
        print(options.saveNames.figures.barPlotPredUncViolinRight, '-dtiff');
end

    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE FIGURE FOR PREDICTIONS WITHOUT EXCLUDED PARTICIPANTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(type, 'exclude')
    % Set the figure inputs and options
    insula.predUnc.totalNamesEx = {'Low', 'Moderate'};
    insula.predUnc.totalNamesDoubleEx = {'Low', 'Moderate', 'Low', 'Moderate'};
    insula.predUnc.totalExLeft = [insula.predUnc.posExLeft(idx.lowEx), insula.predUnc.negExLeft(idx.lowEx) ...
        insula.predUnc.posExLeft(idx.modEx), insula.predUnc.negExLeft(idx.modEx)];
    insula.predUnc.totalExRight = [insula.predUnc.posExRight(idx.lowEx), insula.predUnc.negExRight(idx.lowEx) ...
        insula.predUnc.posExRight(idx.modEx), insula.predUnc.negExRight(idx.modEx)];
    insula.predUnc.meanExLeft = [mean(insula.predUnc.posExLeft(idx.lowEx)), mean(insula.predUnc.negExLeft(idx.lowEx)); ...
        mean(insula.predUnc.posExLeft(idx.modEx)), mean(insula.predUnc.negExLeft(idx.modEx))];
    insula.predUnc.meanExRight = [mean(insula.predUnc.posExRight(idx.lowEx)), mean(insula.predUnc.negExRight(idx.lowEx)); ...
        mean(insula.predUnc.posExRight(idx.modEx)), mean(insula.predUnc.negExRight(idx.modEx))];
    insula.predUnc.errsExLeft = [std(insula.predUnc.posExLeft(idx.lowEx))/sqrt(length(insula.predUnc.posExLeft(idx.lowEx))), ...
        std(insula.predUnc.negExLeft(idx.lowEx))/sqrt(length(insula.predUnc.negExLeft(idx.lowEx))); ...
        std(insula.predUnc.posExLeft(idx.modEx))/sqrt(length(insula.predUnc.posExLeft(idx.modEx))), ...
        std(insula.predUnc.negExLeft(idx.modEx))/sqrt(length(insula.predUnc.negExLeft(idx.modEx)))];
    insula.predUnc.errsExRight = [std(insula.predUnc.posExRight(idx.lowEx))/sqrt(length(insula.predUnc.posExRight(idx.lowEx))), ...
        std(insula.predUnc.negExRight(idx.lowEx))/sqrt(length(insula.predUnc.negExRight(idx.lowEx))); ...
        std(insula.predUnc.posExRight(idx.modEx))/sqrt(length(insula.predUnc.posExRight(idx.modEx))), ...
        std(insula.predUnc.negExRight(idx.modEx))/sqrt(length(insula.predUnc.negExRight(idx.modEx)))];
    input_options = {'color', colours.four, 'nofig', 'names', insula.predUnc.totalNamesDoubleEx, 'noind'};

    % Make the figure for the left side
    figure;
        set(gcf, 'Position', [0 0 600 600]);
        x = [1,1;2,2];
        h = bar(x, insula.predUnc.meanExLeft, 'FaceColor', 'flat');
        h(1).LineWidth = 1;
        h(2).LineWidth = 1;
        hold on
        ngroups = size(insula.predUnc.meanExLeft, 1);
        nbars = size(insula.predUnc.meanExLeft, 2);
        % Calculating the width for each bar group
        groupwidth = min(0.8, nbars/(nbars + 1.5));
        for i = 1:nbars
            x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
            err = errorbar(x, insula.predUnc.meanExLeft(:,i), insula.predUnc.errsExLeft(:,i), '.');
            err.Color = [0 0 0];
            err.LineStyle = 'none';
            err.LineWidth = 1;
        end
        h(1).CData(1,:) = [1 0.7 0]; % group 1 1st bar
        h(1).CData(2,:) = [1 0.7 0]; % group 1 2nd bar
        h(2).CData(1,:) = [0.8 0 0];
        h(2).CData(2,:) = [0.8 0 0];
        ylim([-45, 0]);
        set(gca, 'FontSize', 26);
        set(gca, 'XTickLabel', insula.predUnc.totalNamesEx);
        xlabel('Anxiety Group', 'FontSize', 38);
        ylabel('Contrast estimates (au)', 'FontSize', 38);
        text(1.25, 105, 'Low', 'FontSize', 38);
        text(2.9, 105, 'Moderate', 'FontSize', 38);
%         title('Prediction certainty estimates', 'FontSize', 42, 'FontWeight', 'normal');
        legend({'Pos', 'Neg'}, 'Location', 'southeast', 'FontSize', 38);
        print(options.saveNames.figures.barPlotPredUncExLeft, '-dtiff');

    % Make the violin plot for the left side
    figure;
        set(gcf, 'Position', [0 0 600 600]);
        barplot_columns(insula.predUnc.totalExLeft, input_options{:});
        hold on
        ylim([-50, 20]);
        set(gca, 'FontSize', 26);
        set(gca, 'XTickLabel', insula.predUnc.totalNamesDoubleEx);
        xlabel('Anxiety Group', 'FontSize', 38);
        ylabel('Contrast estimates (au)', 'FontSize', 38);
%         title('Prediction certainty estimates', 'FontSize', 42, 'FontWeight', 'normal');
        print(options.saveNames.figures.barPlotPredUncViolinExLeft, '-dtiff');
        
    % Make the figure for the right side
    figure;
        set(gcf, 'Position', [0 0 600 600]);
        x = [1,1;2,2];
        h = bar(x, insula.predUnc.meanExRight, 'FaceColor', 'flat');
        h(1).LineWidth = 1;
        h(2).LineWidth = 1;
        hold on
        ngroups = size(insula.predUnc.meanExRight, 1);
        nbars = size(insula.predUnc.meanExRight, 2);
        % Calculating the width for each bar group
        groupwidth = min(0.8, nbars/(nbars + 1.5));
        for i = 1:nbars
            x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
            err = errorbar(x, insula.predUnc.meanExRight(:,i), insula.predUnc.errsExRight(:,i), '.');
            err.Color = [0 0 0];
            err.LineStyle = 'none';
            err.LineWidth = 1;
        end
        h(1).CData(1,:) = [1 0.7 0]; % group 1 1st bar
        h(1).CData(2,:) = [1 0.7 0]; % group 1 2nd bar
        h(2).CData(1,:) = [0.8 0 0];
        h(2).CData(2,:) = [0.8 0 0];
        ylim([-45, 0]);
        set(gca, 'FontSize', 26);
        set(gca, 'XTickLabel', insula.predUnc.totalNamesEx);
        xlabel('Anxiety Group', 'FontSize', 38);
        ylabel('Contrast estimates (au)', 'FontSize', 38);
        text(1.25, 105, 'Low', 'FontSize', 38);
        text(2.9, 105, 'Moderate', 'FontSize', 38);
%         title('Prediction certainty estimates', 'FontSize', 42, 'FontWeight', 'normal');
        legend({'Pos', 'Neg'}, 'Location', 'southeast', 'FontSize', 38);
        print(options.saveNames.figures.barPlotPredUncExRight, '-dtiff');

    % Make the violin plot for the right side
    figure;
        set(gcf, 'Position', [0 0 600 600]);
        barplot_columns(insula.predUnc.totalExRight, input_options{:});
        hold on
        ylim([-50, 20]);
        set(gca, 'FontSize', 26);
        set(gca, 'XTickLabel', insula.predUnc.totalNamesDoubleEx);
        xlabel('Anxiety Group', 'FontSize', 38);
        ylabel('Contrast estimates (au)', 'FontSize', 38);
%         title('Prediction certainty estimates', 'FontSize', 42, 'FontWeight', 'normal');
        print(options.saveNames.figures.barPlotPredUncViolinExRight, '-dtiff');
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE FIGURE FOR PREDICTION DECISIONS FOR ALL PARTICIPANTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set the figure inputs and options
insula.predDec.totalNames = {'Low', 'Moderate'};
insula.predDec.totalNamesDouble = {'Low', 'Moderate', 'Low', 'Moderate'};
insula.predDec.totalLeft = [insula.predDec.posLeft(options.PPIDs.low.idx), insula.predDec.negLeft(options.PPIDs.low.idx) ...
    insula.predDec.posLeft(options.PPIDs.mod.idx), insula.predDec.negLeft(options.PPIDs.mod.idx)];
insula.predDec.totalRight = [insula.predDec.posRight(options.PPIDs.low.idx), insula.predDec.negRight(options.PPIDs.low.idx) ...
    insula.predDec.posRight(options.PPIDs.mod.idx), insula.predDec.negRight(options.PPIDs.mod.idx)];
insula.predDec.meanLeft = [mean(insula.predDec.posLeft(options.PPIDs.low.idx)), mean(insula.predDec.negLeft(options.PPIDs.low.idx)); ...
    mean(insula.predDec.posLeft(options.PPIDs.mod.idx)), mean(insula.predDec.negLeft(options.PPIDs.mod.idx))];
insula.predDec.meanRight = [mean(insula.predDec.posRight(options.PPIDs.low.idx)), mean(insula.predDec.negRight(options.PPIDs.low.idx)); ...
    mean(insula.predDec.posRight(options.PPIDs.mod.idx)), mean(insula.predDec.negRight(options.PPIDs.mod.idx))];
insula.predDec.errsLeft = [std(insula.predDec.posLeft(options.PPIDs.low.idx))/sqrt(length(insula.predDec.posLeft(options.PPIDs.low.idx))), ...
    std(insula.predDec.negLeft(options.PPIDs.low.idx))/sqrt(length(insula.predDec.negLeft(options.PPIDs.low.idx))); ...
    std(insula.predDec.posLeft(options.PPIDs.mod.idx))/sqrt(length(insula.predDec.posLeft(options.PPIDs.mod.idx))), ...
    std(insula.predDec.negLeft(options.PPIDs.mod.idx))/sqrt(length(insula.predDec.negLeft(options.PPIDs.mod.idx)))];
insula.predDec.errsRight = [std(insula.predDec.posRight(options.PPIDs.low.idx))/sqrt(length(insula.predDec.posRight(options.PPIDs.low.idx))), ...
    std(insula.predDec.negRight(options.PPIDs.low.idx))/sqrt(length(insula.predDec.negRight(options.PPIDs.low.idx))); ...
    std(insula.predDec.posRight(options.PPIDs.mod.idx))/sqrt(length(insula.predDec.posRight(options.PPIDs.mod.idx))), ...
    std(insula.predDec.negRight(options.PPIDs.mod.idx))/sqrt(length(insula.predDec.negRight(options.PPIDs.mod.idx)))];
input_options = {'color', colours.four, 'nofig', 'names', insula.predDec.totalNamesDouble, 'noind'};

% Make the figure for the left side
figure;
    set(gcf, 'Position', [0 0 600 600]);
    x = [1,1;2,2];
    h = bar(x, insula.predDec.meanLeft, 'FaceColor', 'flat');
    h(1).LineWidth = 1;
    h(2).LineWidth = 1;
    hold on
    ngroups = size(insula.predDec.meanLeft, 1);
    nbars = size(insula.predDec.meanLeft, 2);
    % Calculating the width for each bar group
    groupwidth = min(0.8, nbars/(nbars + 1.5));
    for i = 1:nbars
        x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
        err = errorbar(x, insula.predDec.meanLeft(:,i), insula.predDec.errsLeft(:,i), '.');
        err.Color = [0 0 0];
        err.LineStyle = 'none';
        err.LineWidth = 1;
    end
    h(1).CData(1,:) = [1 0.7 0]; % group 1 1st bar
    h(1).CData(2,:) = [1 0.7 0]; % group 1 2nd bar
    h(2).CData(1,:) = [0.8 0 0];
    h(2).CData(2,:) = [0.8 0 0];
    ylim([0, 3]);
    set(gca, 'FontSize', 26);
    set(gca, 'XTickLabel', insula.predDec.totalNames);
    xlabel('Anxiety Group', 'FontSize', 38);
    ylabel('Contrast estimates (au)', 'FontSize', 38);
    text(1.25, 105, 'Low', 'FontSize', 38);
    text(2.9, 105, 'Moderate', 'FontSize', 38);
%     title('Prediction decision estimates', 'FontSize', 42, 'FontWeight', 'normal');
    legend({'Pos', 'Neg'}, 'Location', 'northeast', 'FontSize', 38);
    print(options.saveNames.figures.barPlotPredDecLeft, '-dtiff');

% Make the violin plot for the left side
figure;
    set(gcf, 'Position', [0 0 600 600]);
    barplot_columns(insula.predDec.totalLeft, input_options{:});
    hold on
    ylim([0, 12]);
    set(gca, 'FontSize', 26);
    set(gca, 'XTickLabel', insula.predDec.totalNamesDouble);
    xlabel('Anxiety Group', 'FontSize', 38);
    ylabel('Contrast estimates (au)', 'FontSize', 38);
%     title('Prediction decision estimates', 'FontSize', 42, 'FontWeight', 'normal');
    print(options.saveNames.figures.barPlotPredDecViolinLeft, '-dtiff');
    
% Make the figure for the right side
figure;
    set(gcf, 'Position', [0 0 600 600]);
    x = [1,1;2,2];
    h = bar(x, insula.predDec.meanRight, 'FaceColor', 'flat');
    h(1).LineWidth = 1;
    h(2).LineWidth = 1;
    hold on
    ngroups = size(insula.predDec.meanRight, 1);
    nbars = size(insula.predDec.meanRight, 2);
    % Calculating the width for each bar group
    groupwidth = min(0.8, nbars/(nbars + 1.5));
    for i = 1:nbars
        x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
        err = errorbar(x, insula.predDec.meanRight(:,i), insula.predDec.errsRight(:,i), '.');
        err.Color = [0 0 0];
        err.LineStyle = 'none';
        err.LineWidth = 1;
    end
    h(1).CData(1,:) = [1 0.7 0]; % group 1 1st bar
    h(1).CData(2,:) = [1 0.7 0]; % group 1 2nd bar
    h(2).CData(1,:) = [0.8 0 0];
    h(2).CData(2,:) = [0.8 0 0];
    ylim([0, 3]);
    set(gca, 'FontSize', 26);
    set(gca, 'XTickLabel', insula.predDec.totalNames);
    xlabel('Anxiety Group', 'FontSize', 38);
    ylabel('Contrast estimates (au)', 'FontSize', 38);
    text(1.25, 105, 'Low', 'FontSize', 38);
    text(2.9, 105, 'Moderate', 'FontSize', 38);
%     title('Prediction decision estimates', 'FontSize', 42, 'FontWeight', 'normal');
    legend({'Pos', 'Neg'}, 'Location', 'northeast', 'FontSize', 38);
    print(options.saveNames.figures.barPlotPredDecRight, '-dtiff');

% Make the violin plot for the right side
figure;
    set(gcf, 'Position', [0 0 600 600]);
    barplot_columns(insula.predDec.totalRight, input_options{:});
    hold on
    ylim([0, 12]);
    set(gca, 'FontSize', 26);
    set(gca, 'XTickLabel', insula.predDec.totalNamesDouble);
    xlabel('Anxiety Group', 'FontSize', 38);
    ylabel('Contrast estimates (au)', 'FontSize', 38);
%     title('Prediction decision estimates', 'FontSize', 42, 'FontWeight', 'normal');
    print(options.saveNames.figures.barPlotPredDecViolinRight, '-dtiff');
    
    
end