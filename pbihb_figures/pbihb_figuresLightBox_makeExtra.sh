###########################################################################################

# PBIHB: MAKE LIGHTBOX SUMMARY FIGURES (EXTRA)

############################################################################################

# DESCRIPTION:
# This function creates the extra lightbox summary images using FSLeyes
# Usage: ./runLightBox $data $scripts $higherL $lowerL $conName $dispMin $dispMax
#	data = path to main data folder
#	scripts = path to PBIHB scripts folder
#	higherL = name of higher level design ('groups' 'groupsEx' 'regress' or 'explore')
#	lowerL = name of lower level design ('basic' 'model' or 'modelFactors')
#	conName = name of lower level contrast (number or name e.g. con0001 or cueTot)
#	dispMin = minimum for display
#	dispMax = maximum for display
#	slice = z slice to be used

############################################################################################
############################################################################################

# Define the function
runLightBoxExtra(){

	# Define local variables from inputs
	local data=$1
	local scripts=$2
	local higherL=$3
	local lowerL=$4
	local conName=$5
	local dispMin=$6
	local dispMax=$7
	local slice=$8

	# make sure filename supplied as command line arg else die
	[ $# -eq 0 ] && { echo "Usage: $0 data scripts higherL lowerL conName dispMin dispMax"; exit 1; }

	# Create path for data
	dataPath=$(find "${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_${higherL}_${lowerL}" -name "*${conName}*" | head -n1)

	# Create name lists for contrasts
	if [ "${higherL}" = "groups" ] || [ "${higherL}" = "groupsEx" ]; then
		conNames=(GroupMean GroupDiff)
	elif [ "${higherL}" = "regress" ]; then
		conNames=(GroupMean BreathAnx)
	elif [ "${higherL}" = "explore" ]; then
		conNames=(GroupMean staiT cesd gender)
	fi

	# Specify background image
	bgImage=${scripts}/pbihb_masks/MNI152_T1_1mm_brain.nii.gz
	bgMask=${scripts}/pbihb_masks/PBIHB_FOV_mask_inv.nii.gz

	# Run the image processing for each contrast in higher level design
	for con in ${conNames[@]}; do
		fsleyes render -of "${dataPath}/PBIHB_${higherL}_${lowerL}_${conName}_${con}_${slice}.png" \
			--scene lightbox \
			--zaxis Z --nrows 1 --ncols 1 \
			--worldLoc 0.0 0.0 $slice --hideCursor --bgColour 1.0 1.0 1.0 \
			$bgImage -dr 0.0 10000 \
			"${dataPath}/spm*_thresh_${con}_pos_clusterFWE.nii.gz" \
			--cmap red-yellow --displayRange $dispMin $dispMax \
			"${dataPath}/spm*_thresh_${con}_neg_clusterFWE.nii.gz" \
			--cmap blue-lightblue --displayRange $dispMin $dispMax \
			"${bgMask}" --alpha 50 \
			--cmap greyscale --displayRange 0 20 ;
		done

	# Crop the image to remove white border
	for im in ${dataPath}/PBIHB_${higherL}_${lowerL}_${conName}_*_${slice}.png; do
    		sips --cropToHeightWidth 550 600 "$im"
	done

}

# Invoke the function and pass arguments
runLightBoxExtra $1 $2 $3 $4 $5 $6 $7 $8
