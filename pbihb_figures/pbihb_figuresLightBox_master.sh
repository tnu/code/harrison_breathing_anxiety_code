###########################################################################################

# PBIHB: MASTER SCRIPT TO MAKE ALL LIGHTBOX SUMMARY FIGURES

############################################################################################

# DESCRIPTION:
# This script calls the lightbox function to make the specified summary images using FSLeyes
# Usage: ./runLightBox_master $data
#	data = path to main data folder
#	scripts = path to PBIHB scripts folder

############################################################################################
############################################################################################

# Define the function
runLightBox_master(){

	# Define local variables from inputs
	local data=$1
	local scripts=$2

	# make sure filename supplied as command line arg else die
	[ $# -eq 0 ] && { echo "Usage: $0 data scripts"; exit 1; }

	####################################################################################
	# BASIC DESIGN

	# Make lightbox figure for group diff + basic design: resistance
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups basic resistanceBlock 0 10;

	# Make lightbox figure for group diff + basic design: resistance surprise
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups basic resistanceSurprise 0 10;

	# Make lightbox figure for group diff + basic design: no resistance block
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups basic noResistanceBlock 0 10;

	# Make lightbox figure for group diff + basic design: no resistance surprise
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups basic noResistanceSurprise 0 10;

	# Make lightbox figure for group diff + basic design: prediction average yes / no
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups basic avgCue 0 10;

	# Make lightbox figure for group diff + basic design: prediction yes > no
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups basic cueYesNoDiff 0 10;

	# Make lightbox figure for group diff + basic design: surprise pos > neg
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups basic surpriseDiffPos 0 10;

	# Make lightbox figure for group diff + basic design: surprise neg > pos
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups basic surpriseDiffNeg 0 10;

	# Make lightbox figure for group diff + basic design: surprise average pos / neg
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups basic surpriseAvg 0 10;


	####################################################################################
	# MODEL DESIGN

	# Make lightbox figure for group diff + model design: positive prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups model predictPos_min 0 10;

	# Make lightbox figure for group diff + model design: negative prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups model predictNeg_min 0 10;

	# Make lightbox figure for group diff + model design: average prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups model predAvg 0 10;

	# Make lightbox figure for group diff + model design: prediction interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups model predIntPos 0 10;

	# Make lightbox figure for group diff + model design: prediction interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups model predIntNeg 0 10;

	# Make lightbox figure for group diff + model design: positive error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups model errorPos 0 10;

	# Make lightbox figure for group diff + model design: negative error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups model errorNeg 0 10;

	# Make lightbox figure for group diff + model design: average error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups model errorAvg 0 10;

	# Make lightbox figure for group diff + model design: error interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups model errorIntPos 0 10;

	# Make lightbox figure for group diff + model design: error interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups model errorIntNeg 0 10;

	# Make lightbox figure for group diff + model design (excluded subjects): positive prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx model predictPos_min 0 10;

	# Make lightbox figure for group diff + model design (excluded subjects): negative prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx model predictNeg_min 0 10;

	# Make lightbox figure for group diff + model design (excluded subjects): average prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx model predAvg 0 10;

	# Make lightbox figure for group diff + model design (excluded subjects): prediction interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx model predIntPos 0 10;

	# Make lightbox figure for group diff + model design (excluded subjects): prediction interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx model predIntNeg 0 10;

	# Make lightbox figure for group diff + model design (excluded subjects): positive error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx model errorPos 0 10;

	# Make lightbox figure for group diff + model design (excluded subjects): negative error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx model errorNeg 0 10;

	# Make lightbox figure for group diff + model design (excluded subjects): average error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx model errorAvg 0 10;

	# Make lightbox figure for group diff + model design (excluded subjects): error interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx model errorIntPos 0 10;

	# Make lightbox figure for group diff + model design (excluded subjects): error interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx model errorIntNeg 0 10;

	# Make lightbox figure for group regress + model design: positive prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts regress model predictPos_min 0 10;

	# Make lightbox figure for group regress + model design: negative prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts regress model predictNeg_min 0 10;

	# Make lightbox figure for group regress + model design: average prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts regress model predAvg 0 10;

	# Make lightbox figure for group regress + model design: prediction interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts regress model predIntPos 0 10;

	# Make lightbox figure for group regress + model design: prediction interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts regress model predIntNeg 0 10;

	# Make lightbox figure for group regress + model design: positive error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts regress model errorPos 0 10;

	# Make lightbox figure for group regress + model design: negative error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts regress model errorNeg 0 10;

	# Make lightbox figure for group regress + model design: average error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts regress model errorAvg 0 10;

	# Make lightbox figure for group regress + model design: error interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts regress model errorIntPos 0 10;

	# Make lightbox figure for group regress + model design: error interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts regress model errorIntNeg 0 10;

	# Make lightbox figure for group explore + model design: positive prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts explore model predictPos_min 0 10;

	# Make lightbox figure for group explore + model design: negative prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts explore model predictNeg_min 0 10;

	# Make lightbox figure for group explore + model design: average prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts explore model predAvg 0 10;

	# Make lightbox figure for group explore + model design: prediction interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts explore model predIntPos 0 10;

	# Make lightbox figure for group explore + model design: prediction interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts explore model predIntNeg 0 10;

	# Make lightbox figure for group explore + model design: positive error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts explore model errorPos 0 10;

	# Make lightbox figure for group explore + model design: negative error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts explore model errorNeg 0 10;

	# Make lightbox figure for group explore + model design: average error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts explore model errorAvg 0 10;

	# Make lightbox figure for group explore + model design: error interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts explore model errorIntPos 0 10;

	# Make lightbox figure for group explore + model design: error interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts explore model errorIntNeg 0 10;


	####################################################################################
	# FUNCTIONAL CONNECTIVITY

	# Make lightbox figure for group diff + modelFC design: aIns average prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups FC_FE_aIns_predict_error predict_average 0 10;

	# Make lightbox figure for group diff + modelFC design: aIns positive vs negative prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups FC_FE_aIns_predict_error predict_PosNegDiff 0 10;

	# Make lightbox figure for group diff + modelFC design: aIns average error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups FC_FE_aIns_predict_error error_average 0 10;

	# Make lightbox figure for group diff + modelFC design: aIns positive vs negative error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups FC_FE_aIns_predict_error error_PosNegDiff 0 10;

	# Make lightbox figure for group diff + modelFC design: aIns prediction vs error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups FC_FE_aIns_predict_error predict_error_diff 0 10;

	# Make lightbox figure for group diff + modelFC design: PAG average error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups FC_FE_PAG_error error_average 0 10;

	# Make lightbox figure for group diff + modelFC design: PAG positive vs negative error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groups FC_FE_PAG_error error_PosNegDiff 0 10;

	# Make lightbox figure for group diff + modelFC design (excluded subjects): aIns average prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx FC_FE_aIns_predict_error predict_average 0 10;

	# Make lightbox figure for group diff + modelFC design (excluded subjects): aIns positive vs negative prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx FC_FE_aIns_predict_error predict_PosNegDiff 0 10;

	# Make lightbox figure for group diff + modelFC design (excluded subjects): aIns average error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx FC_FE_aIns_predict_error error_average 0 10;

	# Make lightbox figure for group diff + modelFC design (excluded subjects): aIns positive vs negative error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx FC_FE_aIns_predict_error error_PosNegDiff 0 10;

	# Make lightbox figure for group diff + modelFC design (excluded subjects): aIns prediction vs error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx FC_FE_aIns_predict_error predict_error_diff 0 10;

	# Make lightbox figure for group diff + modelFC design (excluded subjects): PAG average error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx FC_FE_PAG_error error_average 0 10;

	# Make lightbox figure for group diff + modelFC design (excluded subjects): PAG positive vs negative error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_make.sh $data $scripts groupsEx FC_FE_PAG_error error_PosNegDiff 0 10;


	####################################################################################
	# RANDOMISE FIGURES

	# Make images readable
	chmod a+x ${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_*/lowerLevel_con00*/randomise_*/*

	# Make lightbox figure for group diff + model design: positive prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groups model predictPos_min;

	# Make lightbox figure for group diff + model design: negative prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groups model predictNeg_min;

	# Make lightbox figure for group diff + model design: average prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groups model predAvg;

	# Make lightbox figure for group diff + model design: prediction interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groups model predIntPos;

	# Make lightbox figure for group diff + model design: prediction interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groups model predIntNeg;

	# Make lightbox figure for group diff + model design: positive error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groups model errorPos;

	# Make lightbox figure for group diff + model design: negative error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groups model errorNeg;

	# Make lightbox figure for group diff + model design: average error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groups model errorAvg;

	# Make lightbox figure for group diff + model design: error interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groups model errorIntPos;

	# Make lightbox figure for group diff + model design: error interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groups model errorIntNeg;

	# Make lightbox figure for group diff + model design (excluded subjects): positive prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groupsEx model predictPos_min;

	# Make lightbox figure for group diff + model design (excluded subjects): negative prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groupsEx model predictNeg_min;

	# Make lightbox figure for group diff + model design (excluded subjects): average prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groupsEx model predAvg;

	# Make lightbox figure for group diff + model design (excluded subjects): prediction interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groupsEx model predIntPos;

	# Make lightbox figure for group diff + model design (excluded subjects): prediction interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groupsEx model predIntNeg;

	# Make lightbox figure for group diff + model design (excluded subjects): positive error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groupsEx model errorPos;

	# Make lightbox figure for group diff + model design (excluded subjects): negative error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groupsEx model errorNeg;

	# Make lightbox figure for group diff + model design (excluded subjects): average error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groupsEx model errorAvg;

	# Make lightbox figure for group diff + model design (excluded subjects): error interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groupsEx model errorIntPos;

	# Make lightbox figure for group diff + model design (excluded subjects): error interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts groupsEx model errorIntNeg;

	# Make lightbox figure for group regress + model design: positive prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts regress model predictPos_min;

	# Make lightbox figure for group regress + model design: negative prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts regress model predictNeg_min;

	# Make lightbox figure for group regress + model design: average prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts regress model predAvg;

	# Make lightbox figure for group regress + model design: prediction interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts regress model predIntPos;

	# Make lightbox figure for group regress + model design: prediction interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts regress model predIntNeg;

	# Make lightbox figure for group regress + model design: positive error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts regress model errorPos;

	# Make lightbox figure for group regress + model design: negative error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts regress model errorNeg;

	# Make lightbox figure for group regress + model design: average error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts regress model errorAvg;

	# Make lightbox figure for group regress + model design: error interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts regress model errorIntPos;

	# Make lightbox figure for group regress + model design: error interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts regress model errorIntNeg;

	# Make lightbox figure for group explore + model design: positive prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts explore model predictPos_min;

	# Make lightbox figure for group explore + model design: negative prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts explore model predictNeg_min;

	# Make lightbox figure for group explore + model design: average prediction
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts explore model predAvg;

	# Make lightbox figure for group explore + model design: prediction interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts explore model predIntPos;

	# Make lightbox figure for group explore + model design: prediction interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts explore model predIntNeg;

	# Make lightbox figure for group explore + model design: positive error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts explore model errorPos;

	# Make lightbox figure for group explore + model design: negative error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts explore model errorNeg;

	# Make lightbox figure for group explore + model design: average error
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts explore model errorAvg;

	# Make lightbox figure for group explore + model design: error interaction positive
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts explore model errorIntPos;

	# Make lightbox figure for group explore + model design: error interaction negative
	${scripts}/pbihb_figures/pbihb_figuresLightBox_makeR.sh $data $scripts explore model errorIntNeg;

}

# Invoke the function and pass arguments
runLightBox_master $1 $2
