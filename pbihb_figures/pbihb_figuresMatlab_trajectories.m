%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% BREATHING SCANNER TASK: MATLAB TRAJECTORY FIGURES %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 06/07/2020
% -------------------------------------------------------------------------
% TO RUN:    pbihb_figuresMatlab_trajectories(options)
% INPUTS:    options    = PPID-specific matrix output from the
%                         pbihb_setOptions function
% OUTPUTS:   Matlab figures = Example trajectories for PBIHB figures
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script creates the Matlab figure components that show example 
% trajectories for the PBIHB figures:
%   - Single subject example prediction and error trajectories from the
%     contingency model fits 
%   - Single subject example prediction and error trajectories from the
%     de-coded trajectories (positive and negative trajectories)
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_figuresMatlab_trajectories(options)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET THE ADDITIONAL OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify save names for matlab trajectory figures
options.saveNames.figures.trajPredPairs = fullfile(options.paths.group_directory_models, 'PBIHB_trajPredPairs');
options.saveNames.figures.trajErrPairs = fullfile(options.paths.group_directory_models, 'PBIHB_trajErrPairs');
options.saveNames.figures.trajTransform = fullfile(options.paths.group_directory_models, 'PBIHB_trajTransform');
options.saveNames.figures.trajPredValence = fullfile(options.paths.group_directory_models, 'PBIHB_trajPredValence');
options.saveNames.figures.trajErrValence = fullfile(options.paths.group_directory_models, 'PBIHB_trajErrValence');
options.saveNames.figures.trajPredDec = fullfile(options.paths.group_directory_models, 'PBIHB_trajPredDec');
options.saveNames.figures.trajSurprise = fullfile(options.paths.group_directory_models, 'PBIHB_trajSurprise');
options.saveNames.figures.modelLMEcomp = fullfile(options.paths.group_directory_models, 'PBIHB_modelLMEcomp');
options.saveNames.figures.trajErrDecisions = fullfile(options.paths.group_directory_models, 'PBIHB_trajErrDecisions');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD THE DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load raw model fits and group comparison data
load(options.saveNames.modelFits);
load(options.saveNames.groupComp);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FIND EXAMPLE PARTICIPANT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find the participant with the closest perceptual model value to the mean
[minVal, idx] = min(abs(comp.models.values.prcModelValues - comp.models.groupValues.totPrcMean));
exVal = comp.models.values.prcModelValues(idx);

% Pull out relevant stimulus and decision information
glmModel.decisions = (2 * -models.rawData{idx}.data.pred_answer + 1)';
glmModel.stimuli = 2 * -models.rawData{idx}.params.resist + 1;
glmModel.surprise = (glmModel.stimuli - glmModel.decisions) / 2;


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE THE TRAJECTORY PLOTS FOR CONTINGENCIES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create the probability trajectory
probs = zeros(1,80);
probs(1:30) = 0.8;
probs(31:41) = 0.2;
probs(42:55) = 0.8;
probs(56:68) = 0.2;
probs(69:80) = 0.8;

figure;
    set(gcf, 'Position', [0 0 1400 300]);
    % Plot the probabilities
    yyaxis('right')
    plot(probs, 'k', 'LineWidth', 1.5)
    ylim([-0.05 1.05]);
    ylabel('Probabilities')
    hold on
    % Plot winning model
    yyaxis('left');
    if strcmp(models.est.comp.total.winner.name, 'Rescorla-Wagner')
        plot(models.est.fits{models.est.comp.total.winner.idx}{idx}.traj.vhat, 'r', 'LineWidth', 2);
        ylabel('Prediction strength ({\it\nu})')
    else
        plot(models.est.fits{models.est.comp.total.winner.idx}{idx}.traj.muhat(:,2), 'r', 'LineWidth', 2);
        ylabel('Prediction strength ({\it\mu})')
    end
    trials = 1:80;
    scatter(trials, models.est.fits{models.est.comp.total.winner.idx}{idx}.y, 'k', 'MarkerFaceColor', 'k');
    errors = models.est.fits{models.est.comp.total.winner.idx}{idx}.y == ~models.est.fits{models.est.comp.total.winner.idx}{idx}.u;
    scatter(trials(errors == 1), models.est.fits{models.est.comp.total.winner.idx}{idx}.y(errors == 1), 'MarkerEdgeColor', [1 1 1], 'MarkerFaceColor', [1 1 1]);
    scatter(trials, models.est.fits{models.est.comp.total.winner.idx}{idx}.u, 'k');
    set(gca, 'FontSize', 20)
    ylim([-0.05 1.05]);
    ax = gca;
    ax.YAxis(1).Color = 'k';
    ax.YAxis(2).Color = 'k';
    xlabel('Trial number');
    box on
    title('Example fitted prediction trajectory', 'FontSize', 24, 'FontWeight', 'normal');
    print(options.saveNames.figures.trajPredPairs, '-dtiff');
    
figure;
    set(gcf, 'Position', [0 0 1400 300]);
    % Plot the probabilities
    yyaxis('right')
    plot(probs, 'k', 'LineWidth', 1.5)
    ylim([-0.1 1.1]);
    ylabel('Probabilities')
    hold on
    % Plot winning model
    yyaxis('left');
    if strcmp(models.est.comp.total.winner.name, 'Rescorla-Wagner')
        plot(models.est.fits{models.est.comp.total.winner.idx}{idx}.traj.da, 'r', 'LineWidth', 2);
        ylabel('Prediction error strength (\delta)')
    else
        plot(models.est.fits{models.est.comp.total.winner.idx}{idx}.traj.epsi(:,2), 'r', 'LineWidth', 2);
        ylabel(['Prediction error strength (', char(949), ')'])
    end
    trials = 1:80;
    scatter(trials, (models.est.fits{models.est.comp.total.winner.idx}{idx}.y * 2 - 1), 'k', 'MarkerFaceColor', 'k');
    errors = models.est.fits{models.est.comp.total.winner.idx}{idx}.y == ~models.est.fits{models.est.comp.total.winner.idx}{idx}.u;
    scatter(trials(errors == 1), (models.est.fits{models.est.comp.total.winner.idx}{idx}.y(errors == 1) * 2 - 1), 'MarkerEdgeColor', [1 1 1], 'MarkerFaceColor', [1 1 1]);
    scatter(trials, (models.est.fits{models.est.comp.total.winner.idx}{idx}.u * 2 - 1), 'k');
    set(gca, 'FontSize', 20);
    ylim([-1.1 1.1]);
    ax = gca;
    ax.YAxis(1).Color = 'k';
    ax.YAxis(2).Color = 'k';
    xlabel('Trial number');
    box on
    title('Example fitted prediction error trajectory', 'FontSize', 24, 'FontWeight', 'normal')
    print(options.saveNames.figures.trajErrPairs, '-dtiff');
    

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE THE TRAJECTORY PLOTS FOR VALENCE: MODEL-BASED
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pull out trajectory of winning model in example participant
glmModel.model.name = models.est.comp.total.winner.name;
if strcmp(glmModel.model.name, 'Rescorla-Wagner') == 1
    glmModel.traj.raw.predict_orig = models.est.fits{models.est.comp.total.winner.idx}{idx}.traj.vhat;
    glmModel.traj.raw.predict = glmModel.traj.raw.predict_orig - 0.5;
    glmModel.traj.raw.error = models.est.fits{models.est.comp.total.winner.idx}{idx}.traj.da;
elseif strcmp(glmModel.model.name, '2-level HGF') == 1 || strcmp(glmModel.model.name, '3-level HGF') == 1
    glmModel.traj.raw.predict = models.est.fits{models.est.comp.total.winner.idx}{idx}.traj.muhat(:,2);
    glmModel.traj.raw.error = models.est.fits{models.est.comp.total.winner.idx}{idx}.traj.epsi(:,2);
end

% Flip trajectories for alternative clue
glmModel.traj.raw.predict_flipped = -glmModel.traj.raw.predict;
glmModel.traj.raw.error_flipped = -glmModel.traj.raw.error;
    
% Re-code predictions and prediction errors for stimulus meaning / valence
glmModel.traj.twoCues.total.predict.values = [];
glmModel.traj.twoCues.total.error.values = [];
for n = 1:length(glmModel.traj.raw.predict)                                                            
    if models.rawData{idx}.params.cue(n) == 1                                          % If cue = 1 (expect resistance in first block)
        glmModel.traj.twoCues.total.predict.values(n,1) = glmModel.traj.raw.predict_flipped(n);              % Prediction starts as resistance / negative (from flipped trace)
        glmModel.traj.twoCues.total.error.values(n,1) = glmModel.traj.raw.error_flipped(n);                  % Prediction error (no resistance when you expect it) starts as positive (from flipped trace)
    elseif models.rawData{idx}.params.cue(n) == 2                                      % Else if cue = 2 (do not expect resistance in first block)
        glmModel.traj.twoCues.total.predict.values(n,1) = glmModel.traj.raw.predict(n);                      % Prediction starts as positive (from original trace)
        glmModel.traj.twoCues.total.error.values(n,1) = glmModel.traj.raw.error(n);                          % Prediction error (resistance when you don't expect it) starts as negative (from original trace)
    end
end

% Split predictions and prediction errors according to stimulus meaning / valence
glmModel.traj.twoCues.split.positive.predict.values = glmModel.traj.twoCues.total.predict.values(glmModel.traj.twoCues.total.predict.values > 0); % Identify trials when the model says prediction is towards no-resistance
glmModel.traj.twoCues.split.negative.predict.values = glmModel.traj.twoCues.total.predict.values(glmModel.traj.twoCues.total.predict.values < 0); % Identify trials when the model says prediction is towards resistance
glmModel.traj.twoCues.split.positive.error.values = glmModel.traj.twoCues.total.error.values(models.rawData{idx}.params.resist == 0); % Identify trials when no-resistance occurred
glmModel.traj.twoCues.split.negative.error.values = glmModel.traj.twoCues.total.error.values(models.rawData{idx}.params.resist == 1); % Identify trials when resistance occurred

% Make trajectory de-coding figure (supplementary)
figure;
    set(gcf, 'Position', [0 0 1400 800]);
    subplot(2,1,1)
    hold on
    % Plot example model prediction trajectory
    if strcmp(models.est.comp.total.winner.name, 'Rescorla-Wagner')
        plot(glmModel.traj.raw.predict + 0.5, 'Color', [0.7 0.7 0.7], 'LineWidth', 1.5);
        plot(glmModel.traj.raw.predict_flipped + 0.5, 'Color', [0.7 0.7 0.7], 'LineWidth', 1.5, 'LineStyle', '--');
        plot(glmModel.traj.twoCues.total.predict.values + 0.5, 'r', 'LineWidth', 1.5);
        ylabel({'Prediction strength ({\it\nu})'; 'Resistance <--> No-resistance'});
        hline = refline([0 0.5]);
    else
        plot(glmModel.traj.raw.predict, 'Color', [0.7 0.7 0.7], 'LineWidth', 1.5);
        plot(glmModel.traj.raw.predict_flipped, 'Color', [0.7 0.7 0.7], 'LineWidth', 1.5, 'LineStyle', '--');
        plot(glmModel.traj.twoCues.total.predict.values, 'r', 'LineWidth', 2);
        ylabel({'Prediction strength ({\it\mu})'; 'Resistance <--> No-resistance'});
        hline = refline([0 0]);
    end
    hline.Color = 'k';
    hline.LineWidth = 2;
    hline.LineStyle = ':';
    trials = 1:80;
    cues = models.rawData{idx}.params.cue -1;
    scatter(trials(cues > 0.5), cues(cues > 0.5), 'MarkerFaceColor', [0.7 0.7 0.7], 'MarkerEdgeColor', [0.7 0.7 0.7]);
    scatter(trials(cues < 0.5), cues(cues < 0.5) + 1, 'MarkerEdgeColor', [0.7 0.7 0.7]);
    set(gca, 'FontSize', 15)
    ylim([-0.05 1.05]);
    xlabel('Trial number');
    box on
    title('Example prediction trajectory transformed into stimulus space', 'FontSize', 25, 'FontWeight', 'normal');
    subplot(2,1,2)
    hold on
    % Plot example model prediction error trajectory
    plot(glmModel.traj.raw.error, 'Color', [0.7 0.7 0.7], 'LineWidth', 1.5);
    plot(glmModel.traj.raw.error_flipped, 'Color', [0.7 0.7 0.7], 'LineWidth', 1.5, 'LineStyle', '--');
    plot(glmModel.traj.twoCues.total.error.values, 'r', 'LineWidth', 2);
    hline = refline([0 0]);
    hline.Color = 'k';
    hline.LineWidth = 2;
    hline.LineStyle = ':';
    if strcmp(models.est.comp.total.winner.name, 'Rescorla-Wagner')
        ylabel({'Prediction error strength ({\it\delta})'; 'Resistance <--> No-resistance'});
    else
        ylabel({'Prediction error strength (', char(949), ')'; 'Resistance <--> No-resistance'});
    end
    scatter(trials(cues > 0.5), cues(cues > 0.5), 'MarkerFaceColor', [0.7 0.7 0.7], 'MarkerEdgeColor', [0.7 0.7 0.7]);
    scatter(trials(cues < 0.5), cues(cues < 0.5) + 1, 'MarkerEdgeColor', [0.7 0.7 0.7]);
    set(gca, 'FontSize', 15)
    ylim([-1.1 1.1]);
    xlabel('Trial number');
    box on
    title('Example prediction error trajectory transformed into stimulus space', 'FontSize', 25, 'FontWeight', 'normal');
    print(options.saveNames.figures.trajTransform, '-dtiff');

% Make prediction trajectory figure
figure;
    set(gcf, 'Position', [0 0 1400 350]);
    hold on
    % Plot winning model
    if strcmp(models.est.comp.total.winner.name, 'Rescorla-Wagner')
        plot(glmModel.traj.twoCues.total.predict.values + 0.5, 'Color', [0.7 0.7 0.7], 'LineWidth', 1.5);
        ylabel({'Prediction strength ({\it\nu})'; 'Resistance <--> No-resistance'});
        hline = refline([0 0.5]);
    else
        plot(glmModel.traj.twoCues.total.predict.values, 'Color', [0.7 0.7 0.7], 'LineWidth', 2);
        ylabel({'Prediction strength ({\it\mu})'; 'Resistance <--> No-resistance'});
        hline = refline([0 0]);
    end
    hline.Color = 'k';
    hline.LineWidth = 2;
    hline.LineStyle = ':';
    yyaxis('right')
    ylim([-0.5 0.5])
    stem(trials(glmModel.traj.twoCues.total.predict.values > 0), glmModel.traj.twoCues.total.predict.values(glmModel.traj.twoCues.total.predict.values > 0), '-b', 'filled', 'LineWidth', 1.5);
    stem(trials(glmModel.traj.twoCues.total.predict.values < 0), glmModel.traj.twoCues.total.predict.values(glmModel.traj.twoCues.total.predict.values < 0), '-r', 'filled', 'LineWidth', 1.5);
    ax = gca;
    ax.YAxis(1).Color = 'k';
    ax.YAxis(2).Color = 'k';
    set(gca, 'FontSize', 22)
    xlabel('Trial number');
    box on
    title('Example positive (blue) and negative (red) prediction trajectories', 'FontSize', 30, 'FontWeight', 'normal');
    print(options.saveNames.figures.trajPredValence, '-dtiff');

% Make prediction error trajectory figure
figure;
    set(gcf, 'Position', [0 0 1400 350]);
    hold on
    % Plot winning model
    plot(glmModel.traj.twoCues.total.error.values, 'Color', [0.7 0.7 0.7], 'LineWidth', 2);
    hline = refline([0 0]);
    hline.Color = 'k';
    hline.LineWidth = 2;
    hline.LineStyle = ':';
    if strcmp(models.est.comp.total.winner.name, 'Rescorla-Wagner')
        ylabel({'Prediction error strength ({\it\delta})'; 'Resistance <--> No-resistance'});
    else
        ylabel({'Prediction error strength (', char(949), ')'; 'Resistance <--> No-resistance'});
    end
    yyaxis('right')
    ylim([-1 1])
    yticks([-1 0 1])
    stem(trials(glmModel.traj.twoCues.total.error.values > 0), glmModel.traj.twoCues.total.error.values(glmModel.traj.twoCues.total.error.values > 0), '-b', 'filled', 'LineWidth', 1.5);
    stem(trials(glmModel.traj.twoCues.total.error.values < 0), glmModel.traj.twoCues.total.error.values(glmModel.traj.twoCues.total.error.values < 0), '-r', 'filled', 'LineWidth', 1.5);
    ax = gca;
    ax.YAxis(1).Color = 'k';
    ax.YAxis(2).Color = 'k';
    set(gca, 'FontSize', 22)
    xlabel('Trial number');
    box on
    title('Example positive (blue) and negative (red) prediction error trajectories', 'FontSize', 30, 'FontWeight', 'normal');
    print(options.saveNames.figures.trajErrValence, '-dtiff');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE THE TRAJECTORY PLOTS FOR ERRORS VS DECISIONS (VALENCE SPACE)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate the proportion of incorrect responses at each trial
clear errors
for a = 1:length(options.PPIDs.total)
    errors(a,:) = models.est.fits{models.est.comp.total.winner.idx}{a}.y ~= models.est.fits{models.est.comp.total.winner.idx}{a}.u;
end
errorsProp = nanmean(errors);
[R,p] = corrcoef(errorsProp, abs(glmModel.traj.twoCues.total.error.values));
fprintf('Correlation between proportion of errors and prediction error (original dataset):\n R = %.2f, p = %d\n', R(1,2), p(1,2));

figure;
    set(gcf, 'Position', [0 0 1400 300]);
    % Plot absolute prediction error trajectory from mean trace
    plot(abs(glmModel.traj.twoCues.total.error.values), 'k', 'LineWidth', 2);
    hold on
    % Plot proportion of incorrect responses
    plot(errorsProp, 'r', 'LineWidth', 2);
    ylim([0, 1.2]);
    set(gca, 'FontSize', 20);
    ax = gca;
    ax.YAxis(1).Color = 'k';
    xlabel('Trial number');
    box on
    figureText = sprintf('R = %.2f; p < 0.001', R(1,2));
    text(65, 1.05, figureText, 'FontSize', 20);
    title('Absolute mean prediction error trajectory (black) and proportion of incorrect responses (red) of the original dataset (n = 60)', 'FontSize', 20, 'FontWeight', 'normal')
    print(options.saveNames.figures.trajErrDecisions, '-dtiff');
    
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE THE TRAJECTORY PLOTS FOR VALENCE: DECISIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Make prediction decisions figure
figure;
    set(gcf, 'Position', [0 0 1400 350]);
    hold on
    % Plot stimulus outcomes and decisions
    stem(glmModel.stimuli, 'Color', [0.7 0.7 0.7], 'LineWidth', 1.5);
    stem(trials(glmModel.decisions > 0.5), glmModel.decisions(glmModel.decisions > 0.5), 'filled', 'Color', 'b', 'LineWidth', 2);
	stem(trials(glmModel.decisions < 0.5), glmModel.decisions(glmModel.decisions < 0.5), 'filled', 'Color', 'r', 'LineWidth', 2);
    hline1 = refline([0 0]);
    hline1.Color = [1 1 1];
    hline1.LineWidth = 2;
    hline2 = refline([0 0]);
    hline2.Color = 'k';
    hline2.LineWidth = 2;
    hline2.LineStyle = ':';
    ylabel({'Prediction decisions'; 'Resistance <--> No-resistance'});
    set(gca, 'FontSize', 22)
    ylim([-1.25 1.25]);
    xlabel('Trial number');
    box on
    title('Example no-resistance (pos) and resistance (neg) prediction decision trajectories', 'FontSize', 30, 'FontWeight', 'normal');
    print(options.saveNames.figures.trajPredDec, '-dtiff');
    
figure;
    set(gcf, 'Position', [0 0 1400 350]);
    hold on
    % Plot stimulus outcomes and surprise
    stem(glmModel.stimuli, 'Color', [0.7 0.7 0.7], 'LineWidth', 1.5);
    hline1 = refline([0 0]);
    hline1.Color = [1 1 1];
    hline1.LineWidth = 2;
    hline2 = refline([0 0]);
    hline2.Color = 'k';
    hline2.LineWidth = 2;
    hline2.LineStyle = ':';
    stem(trials(glmModel.surprise > 0.5), glmModel.surprise(glmModel.surprise > 0.5), 'filled', 'Color', 'b', 'LineWidth', 2);
	stem(trials(glmModel.surprise < -0.5), glmModel.surprise(glmModel.surprise < -0.5), 'filled', 'Color', 'r', 'LineWidth', 2);
    ylabel({'Surprising events'; 'Resistance <--> No-resistance'});
    set(gca, 'FontSize', 22)
    ylim([-1.25 1.25]);
    xlabel('Trial number');
    box on
    title('Example no-resistance (pos) and resistance (neg) surprise trajectories', 'FontSize', 30, 'FontWeight', 'normal');
    print(options.saveNames.figures.trajSurprise, '-dtiff');
    
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE THE MODEL COMPARISON PLOTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set the colours
colours = {[0.8 0 0]; [0.8 0 0]; [0.8 0 0]};

% Set the figure inputs and options
LMEcomp.names = {'RW vs HGF2', 'RW vs HGF3', 'HGF2 vs. HGF3'};
LMEcomp.total = [models.est.LME(:,3) - models.est.LME(:,2), models.est.LME(:,3) - models.est.LME(:,1), models.est.LME(:,2) - models.est.LME(:,1)];
input_options1 = {'color', colours, 'nofig', 'names', LMEcomp.names};
input_options2 = {'color', colours, 'nofig', 'names', models.est.names, 'noind', 'noviolin'};

% Calculate winner percentages
[~,winners] = max(models.est.LME, [], 2);
winnerSum = [sum(winners==1)/length(winners), sum(winners==2)/length(winners), sum(winners==3)/length(winners)];
upper95 = binoinv(0.95, length(winners), 1/3) / length(winners);
lower95 = binoinv(0.05, length(winners), 1/3) / length(winners);

% Make figure for model results
figure;
    set(gcf, 'Position', [0 0 1200 400]);
    % Plot LME comparisons
    subplot(1,3,1);
    barplot_columns(LMEcomp.total, input_options1{:});
    set(gca, 'FontSize', 15)
    xtickangle(35);
    ylabel('LME difference values', 'FontSize', 20);
    title('LME comparisons', 'FontSize', 25, 'FontWeight', 'normal');
    subplot(1,3,2);
    barplot_columns(LMEcomp.total, input_options1{:});
    ylim([-4, 4]);
    set(gca, 'FontSize', 15)
    xtickangle(35);
    ylabel('LME difference values', 'FontSize', 20);
    title('LME comparisons (zoomed)', 'FontSize', 25, 'FontWeight', 'normal');
    subplot(1,3,3);
    barplot_columns(winnerSum, input_options2{:});
    ylim([0, 1]);
    line([0 3.5], [upper95 upper95], 'linestyle', '--', 'color', 'k', 'linewidth', 1.5);
    line([0 3.5], [lower95 lower95], 'linestyle', '--', 'color', 'k', 'linewidth', 1.5);
    set(gca, 'FontSize', 15)
    xtickangle(35);
    box on
    ylabel('Proportion of winning models', 'FontSize', 20);
    title('Proportion of winning models', 'FontSize', 25, 'FontWeight', 'normal');
    print(options.saveNames.figures.modelLMEcomp, '-dtiff');

 
end