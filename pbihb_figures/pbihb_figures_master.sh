###########################################################################################

# PBIHB: MASTER SCRIPT TO MAKE ALL PBIHB SUMMARY FIGURES

############################################################################################

# DESCRIPTION:
# This script makes the basic components of the figures for the PBIHB paper

############################################################################################
############################################################################################

# Specify necessary variables

data=/Volumes/TNU_data/PBIHB_data_local/PBIHB_working/fmri
scripts=/Users/faullol/Documents/just_breathe/zurich/gitTNU/pbihb_scanner_analysis
location=local

############################################################################################
# RUN THE SCRIPTS TO MAKE THE FIGURE COMPONENTS

# Make lightbox summary figures for all contrasts of interest (takes a while)
${scripts}/pbihb_figures/pbihb_figuresLightBox_master.sh $data $scripts

# Make insula contrast figures from randomise results (without excluded subjects)
${scripts}/pbihb_figures/pbihb_figuresInsula.sh $data $scripts groupsEx

# Pull out the peak voxel locations for FC figures (whole group)
${scripts}/pbihb_figures/pbihb_roiPeaks.sh $data $scripts groups

# Pull out the peak voxel locations for FC figures (without excluded subjects)
${scripts}/pbihb_figures/pbihb_roiPeaks.sh $data $scripts groupsEx

# Run the Matlab figure scripts
matlab -nosplash -nodesktop -r "addpath('${scripts}/pbihb_figures/'); pbihb_figuresMatlab_master('$location'); exit"
#	- Single subject example prediction and error trajectories from the contingency model fits
#	- Plots of the single subject contrasts in insula regions
#	- Violin plots of FDT results
#	- Violin plots of questionnaire results
#	- BrainNet functional connectivity figures

# Make any extra lightbox summary figures for contrasts of interest
${scripts}/pbihb_figures/pbihb_figuresLightBox_makeExtra.sh $data $scripts groupsEx model predIntNeg 0 10 27;


