%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% BREATHING SCANNER TASK: MULTI-MODAL PCA FIGURES %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 08/07/2020
% -------------------------------------------------------------------------
% TO RUN:    pbihb_figuresMatlab_barPlotsFDT(options)
% INPUTS:    options    = PPID-specific matrix output from the
%                         pbihb_setOptions function
% OUTPUTS:   Matlab figures = Bar plots for PBIHB figures
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script creates the Matlab figure components that show barplots of
% the FDT task from the PBIHB study, including measures of breathing
% sensitivity, decision bias, confidence and metacognitive performance
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_figuresMatlab_multiModalPCA(options)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD THE DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load PCA analysis results
load(options.saveNames.pcaResults);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET THE ADDITIONAL OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

options.saveNames.figures.pcaCorr = fullfile(options.paths.group_directory_groupComp, 'PBIHB_multiModal_corr_results');
options.saveNames.figures.pcaFactors = fullfile(options.paths.group_directory_groupComp, 'PBIHB_multiModal_factor_results');
options.saveNames.figures.pcaPermute = fullfile(options.paths.group_directory_groupComp, 'PBIHB_multiModal_factor_permutations');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE FIGURE FOR CORRELATION MATRIX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;
    set(gcf, 'Position', [0 0 850 700]);
    bwr = @(n, chance_p) interp1([-1 chance_p 1], [0 0 0.8; 1 1 1; 0.8 0 0], linspace(-1, 1, n), 'linear');
    imagesc(pcaResults.corrcoef.R, [-1 1]);
    hold on; axis square;
    colormap(bwr(64, 0));
    colorbar;
    set(gca, 'clim', [-1 1])
    set(gca, 'Fontsize', 12);
    title('Correlation matrix', 'FontSize', 25, 'FontWeight', 'normal');
    div1 = line([4.5 4.5], [0.5 16.5]);
    div1.LineWidth = 2;
    div1.Color = 'k';
    div2 = line([8.5 8.5], [0.5 16.5]);
    div2.LineWidth = 2;
    div2.Color = 'k';
    div3 = line([12.5 12.5], [0.5 16.5]);
    div3.LineWidth = 2;
    div3.Color = 'k';
    div1 = line([0.5 16.5], [4.5 4.5]);
    div1.LineWidth = 2;
    div1.Color = 'k';
    div2 = line([0.5 16.5], [8.5 8.5]);
    div2.LineWidth = 2;
    div2.Color = 'k';
    div3 = line([0.5 16.5], [12.5 12.5]);
    div3.LineWidth = 2;
    div3.Color = 'k';
    ax = gca;
    ax.XTick = 1:16;
    ax.XTickLabel = '';
    ax.YTick = 1:16;
    ax.YTickLabel = pcaResults.matrix.names;
    % Add significance signals to the matrix
    [rows,cols]=size(pcaResults.corrcoef.R);
    for r = 1:rows
        for c = 1:cols
            if pcaResults.corrcoef.p(r,c) < 0.05
                plot(r, c, 'k.', 'MarkerSize', 18)
            end
            if pcaResults.corrcoef.pFDR(r,c) < 0.05
                plot(r, c, 'w.', 'MarkerSize', 8)
            end
        end
    end
    % Add text to describe groups of measures
    text(0.9, 17.2, 'Subjective', 'FontSize', 20)
    text(1.5, 18, 'Affect', 'FontSize', 20)
    text(4.9, 17.2, 'Subjective', 'FontSize', 20)
    text(5.5, 18, 'Intero.', 'FontSize', 20)
    text(8.9, 17.2, 'Percep. &', 'FontSize', 20)
    text(9.2, 18, 'Metacog.', 'FontSize', 20)
    text(12.8, 17.2, 'Ant. Insula', 'FontSize', 20)
    text(13.3, 18, 'Activity', 'FontSize', 20)
    print(options.saveNames.figures.pcaCorr, '-dtiff');
    

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE FIGURE FOR SIGNIFICANT PCA COMPONENTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set overall weight colours
coloursWeights = {[0.8 0 0], [0 0 0.8]};
coloursAlpha = 0.7;

% Specify group colours
coloursGroups = {[1 0.7 0]; [0.8 0 0]};
names = {'Low', 'Mod'};
input_options = {'color', coloursGroups, 'nofig', 'names', names};

figure;
    set(gcf, 'Position', [0 0 800 700]);
    % Plot first component weights
    subplot(2,3,[1 2]);
    [coeffs, idx] = sort(abs(pcaResults.coeff(:,1)), 'descend');
    b = bar(coeffs);
    set(gca, 'Fontsize', 12);
    % Set colours
    b.FaceColor = 'flat';
    b.FaceAlpha = coloursAlpha;
    for a = 1:length(pcaResults.coeff(:,1))
        if pcaResults.coeff(idx(a),1) > 0
            b.CData(a,:) = coloursWeights{1};
        else
            b.CData(a,:) = coloursWeights{2};
        end
    end
    ax = gca;
    ax.XTick = 1:16;
    ax.XTickLabel = pcaResults.matrix.namesShort(idx);
    ax.XTickLabelRotation = 45;
    set(get(ax, 'ylabel'), 'string', 'Absolute coefficient', 'FontSize', 20)
    title('Principal Component 1', 'FontSize', 25, 'FontWeight', 'normal');
    ylim([0, 0.55]);
    xlim([0.25, 16.75]);
    % Add dummy bar plot to make legends match colors
    hold on
    nColors = size(coloursWeights,2);
    labels = {'Positive coefficient'; 'Negative coefficient'};
    xb = bar(nan(2,nColors));
    for i=1:nColors
      xb(i).FaceColor = coloursWeights{i};
      xb(i).FaceAlpha = coloursAlpha;
    end
    legend(xb, labels, 'location', 'northeast');
    % Plot first component group difference
    subplot(2,3,3);
    barplot_columns([pcaResults.score(pcaResults.options.PPIDidx.lowNew,1), pcaResults.score(pcaResults.options.PPIDidx.modNew,1)], input_options{:});
    ylim([-5, 10]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    clear ylabel
    ylabel('Component 1 scores', 'FontSize', 20);
    title('Group scores', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot second component weights
    subplot(2,3,[4 5]);
    [coeffs, idx] = sort(abs(pcaResults.coeff(:,2)), 'descend');
    b = bar(coeffs);
    set(gca, 'Fontsize', 12);
    % Set colours
    b.FaceColor = 'flat';
    b.FaceAlpha = coloursAlpha;
    for a = 1:length(pcaResults.coeff(:,2))
        if pcaResults.coeff(idx(a),2) > 0
            b.CData(a,:) = coloursWeights{1};
        else
            b.CData(a,:) = coloursWeights{2};
        end
    end
    ax = gca;
    ax.XTick = 1:16;
    ax.XTickLabel = pcaResults.matrix.namesShort(idx);
    ax.XTickLabelRotation = 45;
    set(get(ax, 'ylabel'), 'string', 'Absolute coefficient', 'FontSize', 20)
    title('Principal Component 2', 'FontSize', 25, 'FontWeight', 'normal');
    ylim([0, 0.65]);
    xlim([0.25, 16.75]);
    % Plot second component group difference
    subplot(2,3,6);
    barplot_columns([pcaResults.score(pcaResults.options.PPIDidx.lowNew,2), pcaResults.score(pcaResults.options.PPIDidx.modNew,2)], input_options{:});
    ylim([-5, 5]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    clear ylabel
    ylabel('Component 2 scores', 'FontSize', 20);
    title('Group scores', 'FontSize', 25, 'FontWeight', 'normal');
    print(options.saveNames.figures.pcaFactors, '-dtiff');
    
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE FIGURE FOR PCA PERMUTATION TEST
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure();
    set(gcf, 'Position', [0 0 600 500]);
    hold on;
    plot(pcaResults.permute.explained', 'Color', [0.8 0.8 0.8], 'LineWidth', 1.5);
    plot(pcaResults.explained, 'k', 'LineWidth', 2);
    xlim([1, size(pcaResults.matrix.scores,2)]);
    set(gca, 'FontSize', 15)
	xlabel('Principal component', 'FontSize', 20);
    ylabel('Variance explained', 'FontSize', 20);
    title('Variance explained: Permutation tests', 'FontSize', 25, 'FontWeight', 'normal');
    labels = {'Permutations'; 'Fitted data'};
    xb = plot(nan(2,2), 'LineWidth', 4);
    xb(1).Color = [0.8 0.8 0.8];
    xb(2).Color = 'k';
    legend(xb, labels, 'location', 'northeast', 'FontSize', 20);
    box on
    print(options.saveNames.figures.pcaPermute, '-dtiff');


end