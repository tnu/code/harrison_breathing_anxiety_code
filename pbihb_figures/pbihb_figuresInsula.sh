###########################################################################################

# PBIHB: GET SINGLE SUBJECT CONTRAST ESTIMATES FOR INSULA ROIS

############################################################################################

# DESCRIPTION:
# This function pulls out the single-subject contrast estimates from significant contrasts
# in the PBIHB ROIs (insula)
# Usage: ./makeInsulaFigs $data $scripts
#	data = path to main data folder
#	scripts = path to PBIHB scripts folder
#	model = group model ('groups' or 'groupsEx')

############################################################################################
############################################################################################

# Define the function
makeInsulaFigs(){

	# Define local variables from inputs
	local data=$1
	local scripts=$2
	local model=$3

	dispMin=0.95
	dispMax=1.0

	# make sure filename supplied as command line arg else die
	[ $# -eq 0 ] && { echo "Usage: $0 data scripts model"; exit 1; }

	####################################################################################
	# EXTRACT TIMESERIES

	# Specify data paths
	if [ "${model}" = "groups" ]; then
		dataPathUnc=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groups_model
	fi
	if [ "${model}" = "groupsEx" ]; then
		dataPathUnc=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupsEx_model
	fi
	dataPathDec=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groups_basic


	# Specify insula mask files
	maskInsulaLeft=${scripts}/pbihb_masks/PBIHB_aInsBN_mask_left.nii
	maskInsulaRight=${scripts}/pbihb_masks/PBIHB_aInsBN_mask_right.nii

	# Specify files for prediction uncertainty
	dataInsAnxPred=${dataPathUnc}/lowerLevel_con0011_predIntPos/randomise_predIntPos/randomise_predIntPos_tfce_corrp_tstat4.nii.gz
	dataInsAnxPredMaskLeft=${dataPathUnc}/lowerLevel_con0011_predIntPos/randomise_predIntPos/randomise_predIntPos_tfce_corrp_tstat4_maskLeft.nii.gz
	dataInsAnxPredMaskRight=${dataPathUnc}/lowerLevel_con0011_predIntPos/randomise_predIntPos/randomise_predIntPos_tfce_corrp_tstat4_maskRight.nii.gz
	dataSSInsPredPos=${dataPathUnc}/lowerLevel_con0002_predictPos/lowerLevel_con0002_predictPos_4Dfile.nii.gz
	dataSSInsPredNeg=${dataPathUnc}/lowerLevel_con0003_predictNeg/lowerLevel_con0003_predictNeg_4Dfile.nii.gz
	dataSSoutInsPredPosLeft=${dataPathUnc}/lowerLevel_con0011_predIntPos/randomise_predIntPos/con0002_predictPosLeft.txt
	dataSSoutInsPredPosRight=${dataPathUnc}/lowerLevel_con0011_predIntPos/randomise_predIntPos/con0002_predictPosRight.txt
	dataSSoutInsPredNegLeft=${dataPathUnc}/lowerLevel_con0011_predIntPos/randomise_predIntPos/con0003_predictNegLeft.txt
	dataSSoutInsPredNegRight=${dataPathUnc}/lowerLevel_con0011_predIntPos/randomise_predIntPos/con0003_predictNegRight.txt

	# Specify files for prediction decision
	dataInsAnxPredDec=${dataPathDec}/lowerLevel_con0010_cueYesNoDiff/randomise_cueYesNoDiff/randomise_cueYesNoDiff_tfce_corrp_tstat3.nii.gz
	dataInsAnxPredDecMaskLeft=${dataPathDec}/lowerLevel_con0010_cueYesNoDiff/randomise_cueYesNoDiff/randomise_cueYesNoDiff_tfce_corrp_tstat3_maskLeft.nii.gz
	dataInsAnxPredDecMaskRight=${dataPathDec}/lowerLevel_con0010_cueYesNoDiff/randomise_cueYesNoDiff/randomise_cueYesNoDiff_tfce_corrp_tstat3_maskRight.nii.gz
	dataSSInsPredDecPos=${dataPathDec}/lowerLevel_con0001_cueYes/lowerLevel_con0001_cueYes_4Dfile.nii.gz
	dataSSInsPredDecNeg=${dataPathDec}/lowerLevel_con0002_cueNo/lowerLevel_con0002_cueNo_4Dfile.nii.gz
	dataSSoutInsPredDecPosLeft=${dataPathDec}/lowerLevel_con0010_cueYesNoDiff/randomise_cueYesNoDiff/con0001_predictDecNegLeft.txt
	dataSSoutInsPredDecPosRight=${dataPathDec}/lowerLevel_con0010_cueYesNoDiff/randomise_cueYesNoDiff/con0001_predictDecNegRight.txt
	dataSSoutInsPredDecNegLeft=${dataPathDec}/lowerLevel_con0010_cueYesNoDiff/randomise_cueYesNoDiff/con0002_predictDecPosLeft.txt
	dataSSoutInsPredDecNegRight=${dataPathDec}/lowerLevel_con0010_cueYesNoDiff/randomise_cueYesNoDiff/con0002_predictDecPosRight.txt

	# Threshold and binarise the randomise data to create significance masks for each hemisphere
	fslmaths ${dataInsAnxPred} -thr 0.95 -mas ${maskInsulaLeft} -bin ${dataInsAnxPredMaskLeft}
	fslmaths ${dataInsAnxPred} -thr 0.95 -mas ${maskInsulaRight} -bin ${dataInsAnxPredMaskRight}
	fslmaths ${dataInsAnxPredDec} -thr 0.95 -mas ${maskInsulaLeft} -bin ${dataInsAnxPredDecMaskLeft}
	fslmaths ${dataInsAnxPredDec} -thr 0.95 -mas ${maskInsulaRight} -bin ${dataInsAnxPredDecMaskRight}

	# Extract the single-subject contrast estimates for the insula in each hemisphere
	fslmeants -i ${dataSSInsPredPos} -o ${dataSSoutInsPredPosLeft} -m ${dataInsAnxPredMaskLeft}
	fslmeants -i ${dataSSInsPredPos} -o ${dataSSoutInsPredPosRight} -m ${dataInsAnxPredMaskRight}
	fslmeants -i ${dataSSInsPredNeg} -o ${dataSSoutInsPredNegLeft} -m ${dataInsAnxPredMaskLeft}
	fslmeants -i ${dataSSInsPredNeg} -o ${dataSSoutInsPredNegRight} -m ${dataInsAnxPredMaskRight}
	fslmeants -i ${dataSSInsPredDecPos} -o ${dataSSoutInsPredDecPosLeft} -m ${dataInsAnxPredDecMaskLeft}
	fslmeants -i ${dataSSInsPredDecPos} -o ${dataSSoutInsPredDecPosRight} -m ${dataInsAnxPredDecMaskRight}
	fslmeants -i ${dataSSInsPredDecNeg} -o ${dataSSoutInsPredDecNegLeft} -m ${dataInsAnxPredDecMaskLeft}
	fslmeants -i ${dataSSInsPredDecNeg} -o ${dataSSoutInsPredDecNegRight} -m ${dataInsAnxPredDecMaskRight}

	####################################################################################
	# CREATE FSLEYES IMAGES OF INSULA

	# Specify background image
	bgImage=${scripts}/pbihb_masks/MNI152_T1_1mm_brain.nii.gz

	# Create the left fsleyes image for mod vs low anxiety prediction uncertainty
	fsleyes render -of "${dataPathUnc}/lowerLevel_con0011_predIntPos/randomise_predIntPos/randomise_predIntPos_tfce_corrp_tstat4_left.png" \
		--scene lightbox \
		--zaxis X --nrows 1 --ncols 1 \
		--worldLoc -31 18 7 --hideCursor --bgColour 1.0 1.0 1.0 \
		$bgImage -dr 0.0 8500 \
		"${dataInsAnxPred}" \
		--cmap red-yellow --displayRange $dispMin $dispMax ;
	sips --cropToHeightWidth 550 600 "${dataPathUnc}/lowerLevel_con0011_predIntPos/randomise_predIntPos/randomise_predIntPos_tfce_corrp_tstat4_left.png"

	# Create the right fsleyes image for mod vs low anxiety prediction uncertainty
	fsleyes render -of "${dataPathUnc}/lowerLevel_con0011_predIntPos/randomise_predIntPos/randomise_predIntPos_tfce_corrp_tstat4_right.png" \
		--scene lightbox \
		--zaxis X --nrows 1 --ncols 1 \
		--worldLoc 42 18 -6 --hideCursor --bgColour 1.0 1.0 1.0 \
		$bgImage -dr 0.0 8500 \
		"${dataInsAnxPred}" \
		--cmap red-yellow --displayRange $dispMin $dispMax ;
	sips --cropToHeightWidth 550 600 "${dataPathUnc}/lowerLevel_con0011_predIntPos/randomise_predIntPos/randomise_predIntPos_tfce_corrp_tstat4_right.png"

	# Create the middle fsleyes image for mod vs low anxiety prediction uncertainty
	fsleyes render -of "${dataPathUnc}/lowerLevel_con0011_predIntPos/randomise_predIntPos/randomise_predIntPos_tfce_corrp_tstat4_middle.png" \
		--scene lightbox \
		--zaxis Y --nrows 1 --ncols 1 \
		--worldLoc 5 19 -1 --hideCursor --bgColour 1.0 1.0 1.0 \
		$bgImage -dr 0.0 8500 \
		"${dataInsAnxPred}" \
		--cmap red-yellow --displayRange $dispMin $dispMax ;
	sips --cropToHeightWidth 550 600 "${dataPathUnc}/lowerLevel_con0011_predIntPos/randomise_predIntPos/randomise_predIntPos_tfce_corrp_tstat4_middle.png"

	# Create the left fsleyes image for mod vs low anxiety prediction decisions
	fsleyes render -of "${dataPathDec}/lowerLevel_con0010_cueYesNoDiff/randomise_cueYesNoDiff/randomise_cueYesNoDiff_tfce_corrp_tstat3_left.png" \
		--scene lightbox \
		--zaxis X --nrows 1 --ncols 1 \
		--worldLoc -39 18 7 --hideCursor --bgColour 1.0 1.0 1.0 \
		$bgImage -dr 0.0 8500 \
		"${dataInsAnxPredDec}" \
		--cmap red-yellow --displayRange $dispMin $dispMax ;
	sips --cropToHeightWidth 550 600 "${dataPathDec}/lowerLevel_con0010_cueYesNoDiff/randomise_cueYesNoDiff/randomise_cueYesNoDiff_tfce_corrp_tstat3_left.png"

	# Create the right fsleyes image for mod vs low anxiety prediction decisions
	fsleyes render -of "${dataPathDec}/lowerLevel_con0010_cueYesNoDiff/randomise_cueYesNoDiff/randomise_cueYesNoDiff_tfce_corrp_tstat3_right.png" \
		--scene lightbox \
		--zaxis X --nrows 1 --ncols 1 \
		--worldLoc 39 18 -6 --hideCursor --bgColour 1.0 1.0 1.0 \
		$bgImage -dr 0.0 8500 \
		"${dataInsAnxPredDec}" \
		--cmap red-yellow --displayRange $dispMin $dispMax ;
	sips --cropToHeightWidth 550 600 "${dataPathDec}/lowerLevel_con0010_cueYesNoDiff/randomise_cueYesNoDiff/randomise_cueYesNoDiff_tfce_corrp_tstat3_right.png"

	# Create the middle fsleyes image for mod vs low anxiety prediction decisions
	fsleyes render -of "${dataPathDec}/lowerLevel_con0010_cueYesNoDiff/randomise_cueYesNoDiff/randomise_cueYesNoDiff_tfce_corrp_tstat3_middle.png" \
		--scene lightbox \
		--zaxis Y --nrows 1 --ncols 1 \
		--worldLoc 5 17 -1 --hideCursor --bgColour 1.0 1.0 1.0 \
		$bgImage -dr 0.0 8500 \
		"${dataInsAnxPredDec}" \
		--cmap red-yellow --displayRange $dispMin $dispMax ;
	sips --cropToHeightWidth 550 600 "${dataPathDec}/lowerLevel_con0010_cueYesNoDiff/randomise_cueYesNoDiff/randomise_cueYesNoDiff_tfce_corrp_tstat3_middle.png"

}

# Invoke the function and pass arguments
makeInsulaFigs $1 $2 $3
