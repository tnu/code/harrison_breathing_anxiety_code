%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% BREATHING SCANNER TASK: MATLAB TRAJECTORY FIGURES %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 08/07/2020
% -------------------------------------------------------------------------
% TO RUN:    pbihb_figuresMatlab_barPlotsFDT(options)
% INPUTS:    options    = PPID-specific matrix output from the
%                         pbihb_setOptions function
% OUTPUTS:   Matlab figures = Bar plots for PBIHB figures
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script creates the Matlab figure components that show barplots of
% the FDT task from the PBIHB study, including measures of breathing
% sensitivity, decision bias, confidence and metacognitive performance
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pbihb_figuresMatlab_barPlotsFDT(options)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD THE DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load FDT and questionnaire analysis
load(options.saveNames.fdt);


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFY THE OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify save names for matlab FDT and questionnaire figures
options.saveNames.figures.barPlotFDT = fullfile(options.paths.fdt.results, 'PBIHB_FDT_results');
options.saveNames.figures.barPlotQuest.staiT = fullfile(options.paths.fdt.results, 'PBIHB_quest_resultsStaiT');
options.saveNames.figures.barPlotQuest.anxDep = fullfile(options.paths.fdt.results, 'PBIHB_quest_resultsAnxDep');
options.saveNames.figures.barPlotQuest.intero = fullfile(options.paths.fdt.results, 'PBIHB_quest_resultsIntero');
options.saveNames.figures.barPlotQuest.extra = fullfile(options.paths.fdt.results, 'PBIHB_quest_resultsExtra');
options.saveNames.figures.barPlotQuest.subScalesASI = fullfile(options.paths.fdt.results, 'PBIHB_quest_resultsSubsASI');
options.saveNames.figures.barPlotQuest.subScalesMAIA = fullfile(options.paths.fdt.results, 'PBIHB_quest_resultsSubsMAIA');
options.saveNames.figures.barPlotQuest.subScalesBPQ = fullfile(options.paths.fdt.results, 'PBIHB_quest_resultsSubsBPQ');
options.saveNames.figures.barPlotQuest.subScalesPCSR = fullfile(options.paths.fdt.results, 'PBIHB_quest_resultsSubsPCSR');
options.saveNames.figures.barPlotQuest.subScalesPVQR = fullfile(options.paths.fdt.results, 'PBIHB_quest_resultsSubsPVQR');

% Specify colours
colours.two = {[1 0.7 0]; [0.8 0 0]};
colours.four = {[1 0.7 0]; [0.8 0 0]; [1 0.7 0]; [0.8 0 0]};
names = {'Low', 'Mod'};
input_options = {'color', colours.two, 'nofig', 'names', names};


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE FIGURE FOR FDT RESULTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set the figure inputs
FDTresults.filters = [fdt.params.values.low(:,1), fdt.params.values.mod(:,1)];
FDTresults.bias = [fdt.params.values.low(:,2), fdt.params.values.mod(:,2)];
FDTresults.confidence = [fdt.params.values.low(:,3), fdt.params.values.mod(:,3)];
FDTresults.mratio = [fdt.params.values.low(:,4), fdt.params.values.mod(:,4)];

% Make violin figure for FDT results
figure;
    set(gcf, 'Position', [0 0 1500 400]);
    % Plot threshold filter sensitivity
    subplot(1,4,1);
    barplot_columns(FDTresults.filters, input_options{:});
    ylim([0, 14]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Threshold filter (number)', 'FontSize', 20);
    title('Perceptual threshold', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot decision bias
    subplot(1,4,2);
    barplot_columns(FDTresults.bias, input_options{:});
    ylim([-1.2, 2]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Decision bias \it c', 'FontSize', 20);
    title('Decision bias', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot average confidence
    subplot(1,4,3);
    barplot_columns(FDTresults.confidence, input_options{:});
    ylim([0, 14]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Confidence score', 'FontSize', 20);
    title('Meta. bias', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot Mratio
    subplot(1,4,4);
    barplot_columns(FDTresults.mratio, input_options{:});
    ylim([0.6, 1.2]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Mratio', 'FontSize', 20);
    title('Meta. performance', 'FontSize', 25, 'FontWeight', 'normal');
    print(options.saveNames.figures.barPlotFDT, '-dtiff');

    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE FIGURE FOR TRAIT  ANXIETY SCREENING QUESTIONNAIRE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set the figure inputs for anxiety / deression questionnaires
idx = find(contains(fdt.quest.names.total, 'staiT'));
questResults.sreeningStaiT = [fdt.quest.finalScores.total(options.PPIDs.low.idx,idx), fdt.quest.finalScores.total(options.PPIDs.mod.idx,idx)];

% Make violin figure for screening questionnaire results
figure;
    set(gcf, 'Position', [0 0 400 400]);
    % Plot trait anxiety
     barplot_columns(questResults.sreeningStaiT, input_options{:});
%     histogram(questResults.sreeningStaiT(fdt.options.PPIDs.low.idx), 'FaceColor', colours.two{1});
%     hold on
%     histogram(questResults.sreeningStaiT(fdt.options.PPIDs.mod.idx), 'FaceColor', colours.two{2});
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('STAI-T score', 'FontSize', 20);
    title('Trait anxiety','FontSize', 25, 'FontWeight', 'normal');
    print(options.saveNames.figures.barPlotQuest.staiT, '-dtiff');
    
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE FIGURES FOR QUESTIONNAIRES: TOTAL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set the figure inputs for anxiety / deression questionnaires
idx = find(contains(fdt.quest.names.total, 'staiS'));
questResults.stais = [fdt.quest.finalScores.total(options.PPIDs.low.idx,idx), fdt.quest.finalScores.total(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.total, 'gad7'));
questResults.gad7 = [fdt.quest.finalScores.total(options.PPIDs.low.idx,idx), fdt.quest.finalScores.total(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.total, 'asi'));
questResults.asi = [fdt.quest.finalScores.total(options.PPIDs.low.idx,idx), fdt.quest.finalScores.total(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.total, 'cesd'));
questResults.cesd = [fdt.quest.finalScores.total(options.PPIDs.low.idx,idx), fdt.quest.finalScores.total(options.PPIDs.mod.idx,idx)];

% Make violin figure for anxiety / depression questionnnaire results
figure;
    set(gcf, 'Position', [0 0 1500 400]);
    % Plot state anxiety
    subplot(1,4,1);
    barplot_columns(questResults.stais, input_options{:});
    ylim([15, 75]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('STAI-S score', 'FontSize', 20);
    title('State anxiety', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot gad7
    subplot(1,4,2);
    barplot_columns(questResults.gad7, input_options{:});
    ylim([0, 15]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('GAD7 score', 'FontSize', 20);
    title('GAD7', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot asi
    subplot(1,4,3);
    barplot_columns(questResults.asi, input_options{:});
    ylim([0, 60]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('ASI score', 'FontSize', 20);
    title('Anxiety sensitivity', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot cesd
    subplot(1,4,4);
    barplot_columns(questResults.cesd, input_options{:});
    ylim([0, 45]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('CES-D score', 'FontSize', 20);
    title('Depression', 'FontSize', 25, 'FontWeight', 'normal');
    print(options.saveNames.figures.barPlotQuest.anxDep, '-dtiff');

% Set the figure inputs for interoceptive questionnaires 
idx = find(contains(fdt.quest.names.total, 'maia'));
questResults.maia = [fdt.quest.finalScores.total(options.PPIDs.low.idx,idx), fdt.quest.finalScores.total(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.total, 'bpq'));
questResults.bpq = [fdt.quest.finalScores.total(options.PPIDs.low.idx,idx), fdt.quest.finalScores.total(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.total, 'pcsR'));
questResults.pcsR = [fdt.quest.finalScores.total(options.PPIDs.low.idx,idx), fdt.quest.finalScores.total(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.total, 'pvqR'));
questResults.pvqR = [fdt.quest.finalScores.total(options.PPIDs.low.idx,idx), fdt.quest.finalScores.total(options.PPIDs.mod.idx,idx)];

% Make violin figure for interoceptive questionnnaire results
figure;
    set(gcf, 'Position', [0 0 1500 400]);
    % Plot MAIA
    subplot(1,4,1);
    barplot_columns(questResults.maia, input_options{:});
    ylim([10, 250]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('MAIA score', 'FontSize', 20);
    title('MAIA', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot BPQ
    subplot(1,4,2);
    barplot_columns(questResults.bpq, input_options{:});
    ylim([0, 310]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('BPQ score', 'FontSize', 20);
    title('BPQ', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot catastrophising
    subplot(1,4,3);
    barplot_columns(questResults.pcsR, input_options{:});
    ylim([0, 70]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('PCS-R score', 'FontSize', 20);
    title('Breathing catastrophising', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot vigilance
    subplot(1,4,4);
    barplot_columns(questResults.pvqR, input_options{:});
    ylim([0, 90]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('PVQ-R score', 'FontSize', 20);
    title('Breathing vigilance', 'FontSize', 25, 'FontWeight', 'normal');
    print(options.saveNames.figures.barPlotQuest.intero, '-dtiff');

% Set the figure inputs for remaining questionnaires 
idx = find(contains(fdt.quest.names.total, 'panasP'));
questResults.panasP = [fdt.quest.finalScores.total(options.PPIDs.low.idx,idx), fdt.quest.finalScores.total(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.total, 'panasN'));
questResults.panasN = [fdt.quest.finalScores.total(options.PPIDs.low.idx,idx), fdt.quest.finalScores.total(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.total, 'fss'));
questResults.fss = [fdt.quest.finalScores.total(options.PPIDs.low.idx,idx), fdt.quest.finalScores.total(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.total, 'gss'));
questResults.gss = [fdt.quest.finalScores.total(options.PPIDs.low.idx,idx), fdt.quest.finalScores.total(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.total, 'cdrisc'));
questResults.cdrisc = [fdt.quest.finalScores.total(options.PPIDs.low.idx,idx), fdt.quest.finalScores.total(options.PPIDs.mod.idx,idx)];

% Make violin figure for additional questionnnaire results
figure;
    set(gcf, 'Position', [0 0 1500 400]);
    % Plot PANAS+
    subplot(1,5,1);
    barplot_columns(questResults.panasP, input_options{:});
    ylim([12, 65]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('PANAS+ score', 'FontSize', 20);
    title('Positive affect', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot PANAS-
    subplot(1,5,2);
    barplot_columns(questResults.panasN, input_options{:});
    ylim([5, 38]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('PANAS- score', 'FontSize', 20);
    title('Negative affect', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot fss
    subplot(1,5,3);
    barplot_columns(questResults.fss, input_options{:});
    ylim([0, 70]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('FSS score', 'FontSize', 20);
    title('Fatigue', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot gss
    subplot(1,5,4);
    barplot_columns(questResults.gss, input_options{:});
    ylim([12, 55]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('GSS score', 'FontSize', 20);
    title('Self efficacy', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot CD-risc
    subplot(1,5,5);
    barplot_columns(questResults.cdrisc, input_options{:});
    ylim([20, 130]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('CD-RISC score', 'FontSize', 20);
    title('Resilience', 'FontSize', 25, 'FontWeight', 'normal');
    print(options.saveNames.figures.barPlotQuest.extra, '-dtiff');
    
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAKE FIGURES FOR QUESTIONNAIRES: SUB-SCALES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set the figure inputs for the asi sub-scales
idx = find(contains(fdt.quest.names.subScales, 'asiSocial'));
questResults.asiSocial = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'asiCognitive'));
questResults.asiCognitive = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'asiSomatic'));
questResults.asiSomatic = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];

% Make violin figure for the asi sub-scale results
figure;
    set(gcf, 'Position', [0 0 1500 400]);
    % Plot asi: social
    subplot(1,3,1);
    barplot_columns(questResults.asiSocial, input_options{:});
    ylim([0, 30]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('ASI: Social score', 'FontSize', 20);
    title('ASI: Social', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot asi: cognitive
    subplot(1,3,2);
    barplot_columns(questResults.asiCognitive, input_options{:});
    ylim([0, 20]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('ASI: Cognitive score', 'FontSize', 20);
    title('ASI: Cognitive', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot asi: somatic
    subplot(1,3,3);
    barplot_columns(questResults.asiSomatic, input_options{:});
    ylim([0, 25]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('ASI: Somatic score', 'FontSize', 20);
    title('ASI: Somatic', 'FontSize', 25, 'FontWeight', 'normal');
    print(options.saveNames.figures.barPlotQuest.subScalesASI, '-dtiff');

% Set the figure inputs for the maia sub-scales
idx = find(contains(fdt.quest.names.subScales, 'maiaNoticing'));
questResults.maiaNoticing = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'maiaNotDistract'));
questResults.maiaNotDistract = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'maiaNotWorrying'));
questResults.maiaNotWorrying = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'maiaAttnReg'));
questResults.maiaAttnReg = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'maiaEmotAware'));
questResults.maiaEmotAware = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'maiaSelfReg'));
questResults.maiaSelfReg = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'maiaBodyList'));
questResults.maiaBodyList = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'maiaTrust'));
questResults.maiaTrust = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];

% Make violin figure for the maia sub-scale results
figure;
    set(gcf, 'Position', [0 0 1500 800]);
    % Plot maia: noticing
    subplot(2,4,1);
    barplot_columns(questResults.maiaNoticing, input_options{:});
    ylim([0, 35]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Noticing score', 'FontSize', 20);
    title('Noticing', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot maia: not distracting
    subplot(2,4,2);
    barplot_columns(questResults.maiaNotDistract, input_options{:});
    ylim([0, 25]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Not distracting score', 'FontSize', 20);
    title('Not distracting', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot maia: not worrying
    subplot(2,4,3);
    barplot_columns(questResults.maiaNotWorrying, input_options{:});
    ylim([0, 25]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Not worrying score', 'FontSize', 20);
    title('Not worrying', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot maia: attention regulating
    subplot(2,4,4);
    barplot_columns(questResults.maiaAttnReg, input_options{:});
    ylim([0, 55]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Attention regulation score', 'FontSize', 20);
    title('Attention regulation', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot maia: emotional awareness
    subplot(2,4,5);
    barplot_columns(questResults.maiaEmotAware, input_options{:});
    ylim([0, 40]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Emotional awareness score', 'FontSize', 20);
    title('Emotional awareness', 'FontSize', 25, 'FontWeight', 'normal');
    % Plot maia: self regulating
    subplot(2,4,6);
    barplot_columns(questResults.maiaSelfReg, input_options{:});
    ylim([0, 30]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Self regulation score', 'FontSize', 20);
    title('Self regulation', 'Fontsize', 25, 'FontWeight', 'normal');
    % Plot maia: body listening
    subplot(2,4,7);
    barplot_columns(questResults.maiaBodyList, input_options{:});
    ylim([0, 28]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Body listening score', 'FontSize', 20);
    title('Body listening', 'Fontsize', 25, 'FontWeight', 'normal');
    % Plot maia: trust
    subplot(2,4,8);
    barplot_columns(questResults.maiaTrust, input_options{:});
    ylim([0, 35]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Trust score', 'FontSize', 20);
    title('Trust', 'Fontsize', 25, 'FontWeight', 'normal');
    print(options.saveNames.figures.barPlotQuest.subScalesMAIA, '-dtiff');
  
% Set the figure inputs for the bpq sub-scales
idx = find(contains(fdt.quest.names.subScales, 'bpqAware'));
questResults.bpqAware = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'bpqStressResp'));
questResults.bpqStressResp = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'bpqANS'));
questResults.bpqANS = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'bpqStressStyle1'));
questResults.bpqStressStyle1 = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'bpqStressStyle2'));
questResults.bpqStressStyle2 = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'bpqHealth'));
questResults.bpqHealth = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];

% Make violin figure for the bpq sub-scale results
figure;
    set(gcf, 'Position', [0 0 1500 800]);
    % Plot bpq: awareness
    subplot(2,3,1);
    barplot_columns(questResults.bpqAware, input_options{:});
    ylim([0, 250]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Awareness score', 'FontSize', 20);
    title('Awareness', 'Fontsize', 25, 'FontWeight', 'normal');
    % Plot bpq: stress response
    subplot(2,3,2);
    barplot_columns(questResults.bpqStressResp, input_options{:});
    ylim([0, 50]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Stress response score', 'FontSize', 20);
    title('Stress response', 'Fontsize', 25, 'FontWeight', 'normal');
    % Plot bpq: ans
    subplot(2,3,3);
    barplot_columns(questResults.maiaNotWorrying, input_options{:});
    ylim([0, 25]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('ANS Reactivity score', 'FontSize', 20);
    title('ANS Reactivity', 'Fontsize', 25, 'FontWeight', 'normal');
    % Plot bpq: stress style 1
    subplot(2,3,4);
    barplot_columns(questResults.bpqStressStyle1, input_options{:});
    ylim([0, 35]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Stress style I score', 'FontSize', 20);
    title('Stress style I', 'Fontsize', 25, 'FontWeight', 'normal');
    % Plot bpq: stress style 2
    subplot(2,3,5);
    barplot_columns(questResults.bpqStressStyle2, input_options{:});
    ylim([0, 15]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Stress style II score', 'FontSize', 20);
    title('Stress style II', 'Fontsize', 25, 'FontWeight', 'normal');
    % Plot bpq: health history
    subplot(2,3,6);
    barplot_columns(questResults.bpqHealth, input_options{:});
    ylim([0, 20]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Health history score', 'FontSize', 20);
    title('Health history', 'Fontsize', 25, 'FontWeight', 'normal');
    print(options.saveNames.figures.barPlotQuest.subScalesBPQ, '-dtiff');

% Set the figure inputs for the pcsR sub-scales
idx = find(contains(fdt.quest.names.subScales, 'pcsRHelpless'));
questResults.pcsRHelpless = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'pcsRMag'));
questResults.pcsRMag = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'pcsRRumin'));
questResults.pcsRRumin = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];

% Make violin figure for the pcsR sub-scale results
figure;
    set(gcf, 'Position', [0 0 1500 400]);
    % Plot pcsR: helpessness
    subplot(1,3,1);
    barplot_columns(questResults.pcsRHelpless, input_options{:});
    ylim([0, 27]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Helpessness score', 'FontSize', 20);
    title('Helplessness', 'Fontsize', 25, 'FontWeight', 'normal');
    % Plot pcsR: magnification
    subplot(1,3,2);
    barplot_columns(questResults.pcsRMag, input_options{:});
    ylim([0, 20]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Magnification score', 'FontSize', 20);
    title('Magnification', 'Fontsize', 25, 'FontWeight', 'normal');
    % Plot pcsR: rumination
    subplot(1,3,3);
    barplot_columns(questResults.pcsRRumin, input_options{:});
    ylim([0, 30]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Rumination score', 'FontSize', 20);
    title('Rumination', 'Fontsize', 25, 'FontWeight', 'normal');
    print(options.saveNames.figures.barPlotQuest.subScalesPCSR, '-dtiff');

% Set the figure inputs for the pvqR sub-scales
idx = find(contains(fdt.quest.names.subScales, 'pvqRSymptoms'));
questResults.pvqRSymptoms = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];
idx = find(contains(fdt.quest.names.subScales, 'pvqChanges'));
questResults.pvqChanges = [fdt.quest.finalScores.subScales(options.PPIDs.low.idx,idx), fdt.quest.finalScores.subScales(options.PPIDs.mod.idx,idx)];

% Make violin figure for the pvqR sub-scale results
figure;
    set(gcf, 'Position', [0 0 1000 400]);
    % Plot pvqR: symptoms
    subplot(1,2,1);
    barplot_columns(questResults.pvqRSymptoms, input_options{:});
    ylim([0, 45]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Symptoms score', 'FontSize', 20);
    title('Symptoms', 'Fontsize', 25, 'FontWeight', 'normal');
    % Plot pvqR: changes
    subplot(1,2,2);
    barplot_columns(questResults.pvqChanges, input_options{:});
    ylim([0, 50]);
    set(gca, 'FontSize', 15)
	xlabel('Anxiety group', 'FontSize', 20);
    ylabel('Changes score', 'FontSize', 20);
    title('Changes', 'Fontsize', 25, 'FontWeight', 'normal');
    print(options.saveNames.figures.barPlotQuest.subScalesPVQR, '-dtiff');
    
    
end