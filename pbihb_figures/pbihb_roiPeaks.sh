###########################################################################################

# PBIHB: GET PEAK COORDINATES FOR ROI PREDICTION AND ERROR ACTIVITY

############################################################################################

# DESCRIPTION:
# This function locates the clusters in the anterior insula and PAG associated with average
# prediction and prediction error-related activity
# Usage: ./roiPeaks $data $scripts $model
#	data = path to main data folder
#	scripts = path to PBIHB scripts folder
#	model = group model ('groups' or 'groupsEx')

############################################################################################
############################################################################################

# Define the function
roiPeaks(){

	# Define local variables from inputs
	local data=$1
	local scripts=$2
	local model=$3

	# make sure filename supplied as command line arg else die
	[ $# -eq 0 ] && { echo "Usage: $0 data scripts model"; exit 1; }

	# Specify masks
	aInsMaskLeft=${scripts}/pbihb_masks/PBIHB_aInsBN_mask_left.nii
	aInsMaskRight=${scripts}/pbihb_masks/PBIHB_aInsBN_mask_right.nii
	pagMaskLeft=${scripts}/pbihb_masks/PBIHB_PAG_mask_left.nii
	pagMaskRight=${scripts}/pbihb_masks/PBIHB_PAG_mask_right.nii

	# Specify data path
	if [ "${model}" = "groups" ]; then
		dataPath=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groups_model
	else
		if [ "${model}" = "groupsEx" ]; then
			dataPath=${data}/PBIHB_BLT_results/PBIHB_imaging/PBIHB_groupsEx_model
		fi
	fi

	# Specify data
	dataPredAvg=${dataPath}/lowerLevel_con0009_predAvg/spmT_0003.nii.gz
	dataErrAvg=${dataPath}/lowerLevel_con0010_errorAvg/spmT_0002.nii.gz
	dataPredPosNeg=${dataPath}/lowerLevel_con0011_predIntPos/spmT_0002.nii.gz
	dataPredNegPos=${dataPath}/lowerLevel_con0012_predIntNeg/spmT_0002.nii.gz
	dataErrPosNeg=${dataPath}/lowerLevel_con0013_errorIntPos/spmT_0002.nii.gz
	dataErrNegPos=${dataPath}/lowerLevel_con0014_errorIntNeg/spmT_0002.nii.gz

	# Specify aIns masked data
	aInsDataPredAvgLeft=${dataPath}/lowerLevel_con0009_predAvg/spmT_0003_aInsLeft.nii.gz
	aInsDataPredAvgRight=${dataPath}/lowerLevel_con0009_predAvg/spmT_0003_aInsRight.nii.gz
	aInsDataErrAvgLeft=${dataPath}/lowerLevel_con0010_errorAvg/spmT_0002_aInsLeft.nii.gz
	aInsDataErrAvgRight=${dataPath}/lowerLevel_con0010_errorAvg/spmT_0002_aInsRight.nii.gz
	aInsDataPredPosNegLeft=${dataPath}/lowerLevel_con0011_predIntPos/spmT_0002_aInsLeft.nii.gz
	aInsDataPredPosNegRight=${dataPath}/lowerLevel_con0011_predIntPos/spmT_0002_aInsRight.nii.gz
	aInsDataPredNegPosLeft=${dataPath}/lowerLevel_con0012_predIntNeg/spmT_0002_aInsLeft.nii.gz
	aInsDataPredNegPosRight=${dataPath}/lowerLevel_con0012_predIntNeg/spmT_0002_aInsRight.nii.gz
	aInsDataErrPosNegLeft=${dataPath}/lowerLevel_con0013_errorIntPos/spmT_0002_aInsLeft.nii.gz
	aInsDataErrPosNegRight=${dataPath}/lowerLevel_con0013_errorIntPos/spmT_0002_aInsRight.nii.gz
	aInsDataErrNegPosLeft=${dataPath}/lowerLevel_con0014_errorIntNeg/spmT_0002_aInsLeft.nii.gz
	aInsDataErrNegPosRight=${dataPath}/lowerLevel_con0014_errorIntNeg/spmT_0002_aInsRight.nii.gz

	# Specify PAG masked data
	pagDataErrAvgLeft=${dataPath}/lowerLevel_con0010_errorAvg/spmT_0002_pagLeft.nii.gz
	pagDataErrAvgRight=${dataPath}/lowerLevel_con0010_errorAvg/spmT_0002_pagRight.nii.gz
	pagDataErrPosNegLeft=${dataPath}/lowerLevel_con0013_errorIntPos/spmT_0002_pagLeft.nii.gz
	pagDataErrPosNegRight=${dataPath}/lowerLevel_con0013_errorIntPos/spmT_0002_pagRight.nii.gz
	pagDataErrNegPosLeft=${dataPath}/lowerLevel_con0014_errorIntNeg/spmT_0002_pagLeft.nii.gz
	pagDataErrNegPosRight=${dataPath}/lowerLevel_con0014_errorIntNeg/spmT_0002_pagRight.nii.gz

	# Specify aIns save names
	aInsInfoPredAvgLeft=${dataPath}/lowerLevel_con0009_predAvg/spmT_0003_aInsLeft.txt
	aInsInfoPredAvgRight=${dataPath}/lowerLevel_con0009_predAvg/spmT_0003_aInsRight.txt
	aInsInfoErrAvgLeft=${dataPath}/lowerLevel_con0010_errorAvg/spmT_0002_aInsLeft.txt
	aInsInfoErrAvgRight=${dataPath}/lowerLevel_con0010_errorAvg/spmT_0002_aInsRight.txt
	aInsInfoPredPosNegLeft=${dataPath}/lowerLevel_con0011_predIntPos/spmT_0002_aInsLeft.txt
	aInsInfoPredPosNegRight=${dataPath}/lowerLevel_con0011_predIntPos/spmT_0002_aInsRight.txt
	aInsInfoPredNegPosLeft=${dataPath}/lowerLevel_con0012_predIntNeg/spmT_0002_aInsLeft.txt
	aInsInfoPredNegPosRight=${dataPath}/lowerLevel_con0012_predIntNeg/spmT_0002_aInsRight.txt
	aInsInfoErrPosNegLeft=${dataPath}/lowerLevel_con0013_errorIntPos/spmT_0002_aInsLeft.txt
	aInsInfoErrPosNegRight=${dataPath}/lowerLevel_con0013_errorIntPos/spmT_0002_aInsRight.txt
	aInsInfoErrNegPosLeft=${dataPath}/lowerLevel_con0014_errorIntNeg/spmT_0002_aInsLeft.txt
	aInsInfoErrNegPosRight=${dataPath}/lowerLevel_con0014_errorIntNeg/spmT_0002_aInsRight.txt

	# Specify PAG save names
	pagInfoErrAvgLeft=${dataPath}/lowerLevel_con0010_errorAvg/spmT_0002_pagLeft.txt
	pagInfoErrAvgRight=${dataPath}/lowerLevel_con0010_errorAvg/spmT_0002_pagRight.txt
	pagInfoErrPosNegLeft=${dataPath}/lowerLevel_con0013_errorIntPos/spmT_0002_pagLeft.txt
	pagInfoErrPosNegRight=${dataPath}/lowerLevel_con0013_errorIntPos/spmT_0002_pagRight.txt
	pagInfoErrNegPosLeft=${dataPath}/lowerLevel_con0014_errorIntNeg/spmT_0002_pagLeft.txt
	pagInfoErrNegPosRight=${dataPath}/lowerLevel_con0014_errorIntNeg/spmT_0002_pagRight.txt
	
	# Mask the thresholded SPMs for average predictions and errors
	fslmaths ${dataPredAvg} -mas ${aInsMaskLeft} ${aInsDataPredAvgLeft}
	fslmaths ${dataPredAvg} -mas ${aInsMaskRight} ${aInsDataPredAvgRight}
	fslmaths ${dataErrAvg} -mas ${aInsMaskLeft} ${aInsDataErrAvgLeft}
	fslmaths ${dataErrAvg} -mas ${aInsMaskRight} ${aInsDataErrAvgRight}
	fslmaths ${dataErrAvg} -mas ${pagMaskLeft} ${pagDataErrAvgLeft}
	fslmaths ${dataErrAvg} -mas ${pagMaskRight} ${pagDataErrAvgRight}

	# Mask the thresholded SPMs for pos vs neg predictions and errors
	fslmaths ${dataPredPosNeg} -mas ${aInsMaskLeft} ${aInsDataPredPosNegLeft}
	fslmaths ${dataPredPosNeg} -mas ${aInsMaskRight} ${aInsDataPredPosNegRight}
	fslmaths ${dataPredNegPos} -mas ${aInsMaskLeft} ${aInsDataPredNegPosLeft}
	fslmaths ${dataPredNegPos} -mas ${aInsMaskRight} ${aInsDataPredNegPosRight}
	fslmaths ${dataErrPosNeg} -mas ${aInsMaskLeft} ${aInsDataErrPosNegLeft}
	fslmaths ${dataErrPosNeg} -mas ${aInsMaskRight} ${aInsDataErrPosNegRight}
	fslmaths ${dataErrNegPos} -mas ${aInsMaskLeft} ${aInsDataErrNegPosLeft}
	fslmaths ${dataErrNegPos} -mas ${aInsMaskRight} ${aInsDataErrNegPosRight}
	fslmaths ${dataErrPosNeg} -mas ${pagMaskLeft} ${pagDataErrPosNegLeft}
	fslmaths ${dataErrPosNeg} -mas ${pagMaskRight} ${pagDataErrPosNegRight}
	fslmaths ${dataErrNegPos} -mas ${pagMaskLeft} ${pagDataErrNegPosLeft}
	fslmaths ${dataErrNegPos} -mas ${pagMaskRight} ${pagDataErrNegPosRight}

	# Extract the cluster information for average predictions and errors
	cluster -i ${aInsDataPredAvgLeft} -t 1 --mm > ${aInsInfoPredAvgLeft}
	cluster -i ${aInsDataPredAvgRight} -t 1 --mm > ${aInsInfoPredAvgRight}
	cluster -i ${aInsDataErrAvgLeft} -t 1 --mm > ${aInsInfoErrAvgLeft}
	cluster -i ${aInsDataErrAvgRight} -t 1 --mm > ${aInsInfoErrAvgRight}
	cluster -i ${pagDataErrAvgLeft} -t 1 --mm > ${pagInfoErrAvgLeft}
	cluster -i ${pagDataErrAvgRight} -t 1 --mm > ${pagInfoErrAvgRight}

	# Extract the cluster information for pos vs neg predictions and errors
	cluster -i ${aInsDataPredPosNegLeft} -t 1 --mm > ${aInsInfoPredPosNegLeft}
	cluster -i ${aInsDataPredPosNegRight} -t 1 --mm > ${aInsInfoPredPosNegRight}
	cluster -i ${aInsDataPredNegPosLeft} -t 1 --mm > ${aInsInfoPredNegPosLeft}
	cluster -i ${aInsDataPredNegPosRight} -t 1 --mm > ${aInsInfoPredNegPosRight}
	cluster -i ${aInsDataErrPosNegLeft} -t 1 --mm > ${aInsInfoErrPosNegLeft}
	cluster -i ${aInsDataErrPosNegRight} -t 1 --mm > ${aInsInfoErrPosNegRight}
	cluster -i ${aInsDataErrNegPosLeft} -t 1 --mm > ${aInsInfoErrNegPosLeft}
	cluster -i ${aInsDataErrNegPosRight} -t 1 --mm > ${aInsInfoErrNegPosRight}
	cluster -i ${pagDataErrPosNegLeft} -t 1 --mm > ${pagInfoErrPosNegLeft}
	cluster -i ${pagDataErrPosNegRight} -t 1 --mm > ${pagInfoErrPosNegRight}
	cluster -i ${pagDataErrNegPosLeft} -t 1 --mm > ${pagInfoErrNegPosLeft}
	cluster -i ${pagDataErrNegPosRight} -t 1 --mm > ${pagInfoErrNegPosRight}

}

# Invoke the function and pass arguments
roiPeaks $1 $2 $3
