############################################################################################

# PBIHB: EXTRACTION OF ICA NOISE COMPONENTS FOR USE AS CONFOUNDS

############################################################################################

# DESCRIPTION:
# This function extracts the identified ICA noise components for the PBIHB study.
# Requires a full preprocessing (including ICA and FIX noise classification).
# Usage: ./fslEnts $subj $directory $training $type $thresh
#	subj = variable containing PPID
#	directory = preprocessing feat directory (containing PPID)
#	training = training file name
#	type = rest or task
#	thresh = threshold used for classification

############################################################################################
############################################################################################


# Define the function
fslEnts(){

	# Define local variables from inputs
	local subj=$1
	local directory=$2
	local training=$3
	local type=$4
	local thresh=$5
	
	# make sure filename supplied as command line arg else die
	[ $# -eq 0 ] && { echo "Usage: $0 subj directory training type thresh"; exit 1; }
	
	echo ---------------------------------------
	echo FSL NOISE EXTRACTION
	echo ---------------------------------------
	
	# Run fsl_ents to extract the noise from FIX file
	echo Extracting noise components...
	fsl_ents ${directory}/filtered_func_data.ica \
		-o ${directory}/PBIHB_${subj}_icaNoiseFile_${type}.txt \
		${directory}/fix4melview_${training}_thr${thresh}.txt

	echo ---------------------------------------
	echo FSL NOISE EXTRACTION FINISHED!
	echo ---------------------------------------

}

# Invoke the function and pass arguments
fslEnts $1 $2 $3 $4 $5
