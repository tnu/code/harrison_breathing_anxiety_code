# ANALYSIS OF DATA FOR BREATHING ANXIETY MANUSCRIPT

Author: Olivia Harrison

Created: 16/03/2021

This software is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details: <http://www.gnu.org/licenses/>

-----

### INCLUDED SOFTWARE

This toolbox includes a copy of the HMeta-d Toolbox (https://github.com/metacoglab/HMeta-d) under the MIT Licence, and SPM12 software (https://github.com/spm/spm12/blob/master/LICENCE.txt) under the GPU Licence. Please see the included licence and README files within each of the toolbox folders for further information and required referencing.

-----

### REREFENCING

If you use any of the tasks in this manuscript, please reference Harrison et al., "Interoception of breathing and its relationship with anxiety" (in preparation).

If you use the inspiratory resistance circuit designed for this task, please reference Rieger et al. (2020): https://doi.org/10.3389/fnhum.2020.00161

If you use the Filter Detection Task, please reference Harrison et al.,: https://www.biorxiv.org/content/10.1101/2020.06.29.176941v1

-----

### GENERAL OVERVIEW

This code covers the analysis of questionnaire data, data from the 'Filter Detection Task' (FDT: an interoceptive sensitivity and metacognition task), the novel 'Breathing Learning Task' (BLT: an associative learning paradigm between visual cues and inspiratory resistances), and the brain activity (recorded using 7 Tesla fMRI) associated with the BLT. Data are compared between two groups of individuals: those with very low and moderate levels of anxiety. A final multi-modal analysis is then conducted across all task modalities, to assess the multiple levels of the relationship between breathing-related interoception and anxiety.

-----
