############################################################################################

# PBIHB: FSL PREPROCESSING

############################################################################################

# DESCRIPTION:
# This function runs the image preprocessing in FSL for the PBIHB study
# Usage: ./fslPreproc $subj $data $scripts $type
#	subj = variable containing PPID
#	data = path to main data folder
#	scripts = path to main scripts folder
#	type = task or rest

############################################################################################
############################################################################################


# Define the function
fslPreproc(){

	# Define local variables from inputs
	local subj=$1
	local data=$2
	local scripts=$3
	local type=$4

	# Define variables for FIX classification
	training=PBIHB_training_task
	thresh=30
	
	# make sure filename supplied as command line arg else die
	[ $# -eq 0 ] && { echo "Usage: $0 subj data scripts type"; exit 1; }

	echo ---------------------------------------
	echo FSL PREPROCESSING: $type
	echo ---------------------------------------

	# Set template location
	template=${scripts}/pbihb_FSL_templates/pbihb_FSLpreproc_template_${type}.fsf

	# Set image names
	input_image=${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_${type}.nii.gz
	wholebrain_image=${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_${type}_wholebrain.nii.gz
	struct_image=${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_struct_brain.nii.gz
	standard_image=$FSLDIR/data/standard/MNI152_T1_1mm_brain
	output_directory=${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_preproc_FSL_${type}.feat

	# Check if output directory exists, and if so rename
	if [ -d "$output_directory" ] 
	then
    		echo "Directory $output_directory exists... Renaming to old" 
		mv $output_directory ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_preproc_FSL_${type}_old.feat
	fi

	# Check availability of whole-brain image
	if [ -f "$wholebrain_image" ]; then
    		echo "Whole-brain image for ${type} exists"
	else 
		task=task
		if [ "${type}" = "${task}" ]; then
			wholebrain_image=${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_rest_wholebrain.nii.gz
		else
			wholebrain_image=${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_images/PBIHB_${subj}_task_wholebrain.nii.gz
		fi
    		echo "Whole-brain image used is $wholebrain_image"
	fi

	# Add substitutions to image template file
	cat ${template} | sed s:INPUTDATAFILE:${input_image}:g \
		| sed s:WHOLEBRAIN:${wholebrain_image}:g \
		| sed s:STRUCT:${struct_image}:g \
		| sed s:FSLSTANDARD:${standard_image}:g \
		| sed s:OUTPUTDIR:${output_directory}:g > \
		${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_preproc_FSL_${type}.fsf

	# Run the FSL preprocessing analysis
	echo Running FSL preprocessing for ${subj} ${type}...
	feat ${data}/TNU_PBIHB_${subj}/PBIHB_${subj}_preprocessing/PBIHB_${subj}_preproc_FSL_${type}.fsf
	echo ... Finished!

	# Output the movement parameters as a text file
	awk '{ print $4 " " $5 " " $6 " " $1 " " $2 " " $3}' ${output_directory}/mc/prefiltered_func_data_mcf.par  > \
		${output_directory}/mc/prefiltered_func_data_mcf.txt

	# Re-run the registration using the weighting mask
	echo Moving original registration directory...
	cp -R ${output_directory}/reg ${output_directory}/reg_orig
	echo Inverting weighting mask...
	invwarp -w ${output_directory}/reg/highres2standard_warp.nii.gz \
		-o ${output_directory}/reg/standard2highres_warp.nii.gz \
		-r ${output_directory}/reg/highres.nii.gz
	applywarp -i ${scripts}/pbihb_FSL_templates/PBIHB_weighting_mask.nii.gz \
		-o ${output_directory}/reg/PBIHB_${subj}_weighting_mask.nii.gz \
		-r ${output_directory}/reg/highres.nii.gz \
		-w ${output_directory}/reg/standard2highres_warp.nii.gz
	echo Creating new whole-brain EPI to structural registration...
	flirt -ref ${output_directory}/reg/highres.nii.gz \
		-in ${output_directory}/reg/initial_highres.nii.gz \
		-dof 12 -cost bbr \
		-wmseg ${output_directory}/reg/initial_highres2highres_fast_wmseg.nii.gz \
		-init ${output_directory}/reg/initial_highres2highres_init.mat \
		-omat ${output_directory}/reg/initial_highres2highres.mat \
		-out ${output_directory}/reg/initial_highres2highres \
		-schedule ${FSLDIR}/etc/flirtsch/bbr.sch \
		-refweight ${output_directory}/reg/PBIHB_${subj}_weighting_mask.nii.gz
	echo Updating registrations...
	updatefeatreg ${output_directory} -gifs
	echo REGISTRATION UPDATE FINISHED! Please check carefully

	# Remove ICA report images (take up lots of space!)
	echo Removing report images to save space...
	rm ${output_directory}/filtered_func_data.ica/report/*

	# Run component FIX extraction (FIX step 1)
	echo Running FIX component extraction...
	${scripts}/fix/fix -f ${output_directory}

	# Run FIX classification (FIX step 2)
	echo Running component classification with ${training}.RData...
	${scripts}/fix/fix -c ${output_directory} ${scripts}/fix_PBIHB_trainingFiles/${training}.RData ${thresh}

	# Run noise extraction to isolate ICA noise components
	${scripts}/pbihb_fslEnts.sh ${subj} ${output_directory} ${training} ${type} ${thresh}

	echo ---------------------------------------
	echo FSL PREPROCESSING FINISHED!
	echo ---------------------------------------

}

# Invoke the function and pass arguments
fslPreproc $1 $2 $3 $4
