%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% BREATHING SCANNER TASK: BLT MODEL VALIDATION %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 27/08/2021
% -------------------------------------------------------------------------
% TO RUN:   comp        = pbihb_validateModel(location)
% INPUTS:   location    = 'local' or 'euler'
% OUTPUTS:  Model validation checks
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script runs the model validation checks, where a logistic regression
% is run from the mean trajectory on new data from a pilot study in Otago.
% NB: Model fits must be completed (using pbihb_fitModels.m) before running
% this analysis.
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function validate = pbihb_validateModel(location)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Secify options
validate.options = pbihb_setOptions('all', location);

% Specify additional path for validation data
validate.options.paths.newData = '/Volumes/TNU_data/PBIHB_data_local/PBIHB_validate';

% Specify additional file name for validation figure
figureSaveName = fullfile(validate.options.paths.newData, 'PBIHB_trajErrDecisions_validation');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOAD DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load models and comparison data
load(validate.options.saveNames.modelFits);
load(validate.options.saveNames.groupComp);

% Load validation data
list = dir(fullfile(validate.options.paths.newData, '*.mat'));
for a = 1:length(list)
    newDataRaw{a} = load(fullfile(validate.options.paths.newData, list(a).name));
    newData(a,:) = newDataRaw{a}.data.pred_answer - 50;
    newData(a,newDataRaw{a}.data.pred_answer==999) = NaN;
    newData(a,newDataRaw{a}.data.pred_answer<50) = 1;
    newData(a,newDataRaw{a}.data.pred_answer>50) = 0;
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GET EXAMPLE TRAJECTORY TO USE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find the participant with the closest perceptual model value to the mean
[minVal, idx] = min(abs(comp.models.values.prcModelValues - comp.models.groupValues.totPrcMean));
exVal = comp.models.values.prcModelValues(idx);

% Pull out trajectory of winning model in example participant
glmModel.model.name = models.est.comp.total.winner.name;
if strcmp(glmModel.model.name, 'Rescorla-Wagner') == 1
    glmModel.traj.raw.predict_orig = models.est.fits{models.est.comp.total.winner.idx}{idx}.traj.vhat;
    glmModel.traj.raw.predict = glmModel.traj.raw.predict_orig - 0.5;
    glmModel.traj.raw.error = models.est.fits{models.est.comp.total.winner.idx}{idx}.traj.da;
elseif strcmp(glmModel.model.name, '2-level HGF') == 1 || strcmp(glmModel.model.name, '3-level HGF') == 1
    glmModel.traj.raw.predict = models.est.fits{models.est.comp.total.winner.idx}{idx}.traj.muhat(:,2);
    glmModel.traj.raw.error = models.est.fits{models.est.comp.total.winner.idx}{idx}.traj.epsi(:,2);
end

% Flip trajectories for alternative clue
glmModel.traj.raw.predict_flipped = -glmModel.traj.raw.predict;
glmModel.traj.raw.error_flipped = -glmModel.traj.raw.error;
    
% Re-code predictions and prediction errors for stimulus meaning / valence
glmModel.traj.twoCues.total.predict.values = [];
glmModel.traj.twoCues.total.error.values = [];
for n = 1:length(glmModel.traj.raw.predict)                                                            
    if models.rawData{idx}.params.cue(n) == 1                                          % If cue = 1 (expect resistance in first block)
        glmModel.traj.twoCues.total.predict.values(n,1) = glmModel.traj.raw.predict_flipped(n);              % Prediction starts as resistance / negative (from flipped trace)
        glmModel.traj.twoCues.total.error.values(n,1) = glmModel.traj.raw.error_flipped(n);                  % Prediction error (no resistance when you expect it) starts as positive (from flipped trace)
    elseif models.rawData{idx}.params.cue(n) == 2                                      % Else if cue = 2 (do not expect resistance in first block)
        glmModel.traj.twoCues.total.predict.values(n,1) = glmModel.traj.raw.predict(n);                      % Prediction starts as positive (from original trace)
        glmModel.traj.twoCues.total.error.values(n,1) = glmModel.traj.raw.error(n);                          % Prediction error (resistance when you don't expect it) starts as negative (from original trace)
    end
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN LOGISTIC REGRESSION ON NEW DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[B,dev,stats] = glmfit(glmModel.traj.twoCues.total.predict.values, [nansum(newData, 1)' (size(newData,1) * ones(80,1))], 'binomial', 'logit');
fprintf('------------------------------------------------------------------\n');
fprintf('LOGISTIC REGRESSION RESULTS\n');
fprintf('Beta estimates:\n');
fprintf('   Intercept: beta = %d � %d\n   Prediction: beta = %d � %d\n', stats.beta(1), stats.se(1), stats.beta(2), stats.se(2));
fprintf('T statistics:\n');
fprintf('   Intercept: t = %d\n   Prediction: t = %d\n', stats.t);
fprintf('P values:\n');
fprintf('   Intercept: p = %d\n   Prediction: p = %d\n', stats.p);
fprintf('------------------------------------------------------------------\n');


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE THE TRAJECTORY PLOTS FOR ERRORS VS DECISIONS (VALENCE SPACE)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate the proportion of incorrect responses at each trial
clear errors
for a = 1:(size(newData,1))
    errors(a,:) = abs(1-newData(a,:)) == ~newDataRaw{a}.params.resist';
end
errorsProp = nanmean(errors);
[R,p] = corrcoef(errorsProp, abs(glmModel.traj.twoCues.total.error.values));
fprintf('Correlation between proportion of errors and prediction error (original dataset):\n R = %.2f, p = %d\n', R(1,2), p(1,2));

figure;
    set(gcf, 'Position', [0 0 1400 300]);
    % Plot absolute prediction error trajectory from mean trace
    plot(abs(glmModel.traj.twoCues.total.error.values), 'k', 'LineWidth', 2);
    hold on
    % Plot proportion of incorrect responses
    plot(errorsProp, 'r', 'LineWidth', 2);
    ylim([0, 1.2]);
    set(gca, 'FontSize', 20);
    ax = gca;
    ax.YAxis(1).Color = 'k';
    xlabel('Trial number');
    box on
    figureText = sprintf('R = %.2f; p < 0.001', R(1,2));
    text(65, 1.05, figureText, 'FontSize', 20);
    title('Absolute mean prediction error trajectory (black) and proportion of incorrect responses (red) of a validation dataset (n = 15)', 'FontSize', 20, 'FontWeight', 'normal')
    print(figureSaveName, '-dtiff');
    

end