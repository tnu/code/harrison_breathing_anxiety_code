%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% BREATHING SCANNER TASK: SPM ANALYSIS (BASIC OR MODEL) %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% Olivia Faull
% Created: 06/02/2019
% -------------------------------------------------------------------------
% TO RUN:   analysis    = pbihb_spmAnalysis(options, type, varargin)
% INPUTS:   options     = PPID-specific matrix output from pbihb_setOptions
%           type        = 'basic', 'basicSPM', 'model', 'modelPred', 
%                         'modelErr', 'modelFactors' or 'FC'
%           varargin    = If 'type' is 'FC', need to specify: 
%                           1) The seed: 'aIns' or 'PAG'
%                           2) The contrast: 'predictPos', 'predictNeg', 
%                              'errorPos' or 'errorNeg'
% OUTPUTS:  analysis    = Structure with spm batch specifications and 
%                         results added
% -------------------------------------------------------------------------
% DESCRIPTION:
% This script runs the basic analysis for the 7 Tesla functional task data
% collected in the PBIHB study (in spm), using the following:
%   1) Model specification from the specified GLM
%   2) Model estimation of this GLM
%   3) Setting contrasts of interest
%   4) Estimation and output of thresholded results
% -------------------------------------------------------------------------

% This software is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version. This software is distributed in the hope that 
% it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% See the GNU General Public License for more details: 
% <http://www.gnu.org/licenses/>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function analysis = pbihb_spmAnalysis(options, type, varargin)

%% LOAD GLM AND NOISE MATRICES

% Add options
analysis.options = options;

% Load GLM
if strcmp(type, 'basic') == 1
    analysis.name = 'basic';
    analysis.design = load(analysis.options.saveNames.glmBasicMatrix);
    saveName = analysis.options.saveNames.analysis.basic;
    saveFolder = analysis.options.paths.analysis_directories.basic;
    matrixName = analysis.options.saveNames.glmBasicMatrix;
elseif strcmp(type, 'basicSPM') == 1
    analysis.name = 'basicSPM';
    analysis.design = load(analysis.options.saveNames.glmBasicSpmMatrix);
    saveName = analysis.options.saveNames.analysis.basicSpm;
    saveFolder = analysis.options.paths.analysis_directories.basicSpm;
    matrixName = analysis.options.saveNames.glmBasicSpmMatrix;
elseif strcmp(type, 'model') == 1
    analysis.name = 'model';
    analysis.design = load(analysis.options.saveNames.glmModelMatrix);
    saveName = analysis.options.saveNames.analysis.model;
    saveFolder = analysis.options.paths.analysis_directories.model;
    matrixName = analysis.options.saveNames.glmModelMatrix;
elseif strcmp(type, 'modelPred') == 1
    analysis.name = 'modelPred';
    analysis.design = load(analysis.options.saveNames.glmModelPredMatrix);
    saveName = analysis.options.saveNames.analysis.modelPred;
    saveFolder = analysis.options.paths.analysis_directories.modelPred;
    matrixName = analysis.options.saveNames.glmModelPredMatrix;
elseif strcmp(type, 'modelErr') == 1
    analysis.name = 'modelErr';
    analysis.design = load(analysis.options.saveNames.glmModelErrMatrix);
    saveName = analysis.options.saveNames.analysis.modelErr;
    saveFolder = analysis.options.paths.analysis_directories.modelErr;
    matrixName = analysis.options.saveNames.glmModelErrMatrix;
elseif strcmp(type, 'modelFactors') == 1
    analysis.name = 'modelFactors';
    analysis.design = load(analysis.options.saveNames.glmModelFactorsMatrix);
    saveName = analysis.options.saveNames.analysis.modelFactors;
    saveFolder = analysis.options.paths.analysis_directories.modelFactors;
    matrixName = analysis.options.saveNames.glmModelFactorsMatrix;
elseif strcmp(type, 'FC') == 1
    roi = varargin{1};
    contrast = varargin{2};
    analysis.name = ['FC_', roi, '_', contrast];
    analysis.design = load([analysis.options.saveNames.glmModelMatrixFC, roi, '_', contrast, '.mat']);
    saveFolder = [analysis.options.paths.analysis_directories.modelFC, roi, '_', contrast];
    saveName = fullfile(saveFolder, ['PBIHB_', analysis.options.PPID, '_analysis_modelFC_', roi, '_', contrast]);
    matrixName = [analysis.options.saveNames.glmModelMatrixFC, roi, '_', contrast, '.mat'];   
else
    disp('GLM type invalid... Exiting');
    return
end

% Check directory existance and make if required
if ~exist(saveFolder, 'dir')
   mkdir(saveFolder)
elseif isfile(fullfile(saveFolder, 'SPM.mat'))
    delete(fullfile(saveFolder, 'SPM.mat'));
end

% Load noise matrix
if isfile(analysis.options.saveNames.glmNoise)
    analysis.noise = load(analysis.options.saveNames.glmNoise);
else
    disp('No noise matrix found... No noise regressors added to model GLM')
end

% Specify EPI image to be used and check unzipped version exists
sEPI = (fullfile(analysis.options.paths.image_directory, analysis.options.names.images.task_EPI_stdSm));
if ~isfile(sEPI)
    disp('WARNING: No unzipped smoothed standard space EPI scan exists... Unzipping ...');
    pbihb_zippingFunctions(analysis.options, 'unzip');
    disp('... Finished!');
end


%% 1) RUN MODEL SPECIFICATION

matlabbatch{1}.spm.stats.fmri_spec.dir = {saveFolder};
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = analysis.options.taskScan.tr;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = analysis.options.taskScan.slices;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = analysis.options.taskScan.sliceMiddle;
for nameIdx = 1:analysis.options.taskScan.vols
    vol = num2str(nameIdx);
    matlabbatch{1}.spm.stats.fmri_spec.sess.scans{nameIdx,1} = [sEPI, ',', vol];
end
if strcmp(type, 'basicSPM') == 1
    matlabbatch{1}.spm.stats.fmri_spec.sess.multi = cellstr(matrixName);
    matlabbatch{1}.spm.stats.fmri_spec.sess.cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
    matlabbatch{1}.spm.stats.fmri_spec.sess.regress = struct('name', {}, 'val', {});
    matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg = cellstr(analysis.options.saveNames.glmNoise);
else
    matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg = cellstr(matrixName);
end
matlabbatch{1}.spm.stats.fmri_spec.sess.hpf = analysis.options.taskScan.hpf;
matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [1 1];
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mthresh = -inf;
matlabbatch{1}.spm.stats.fmri_spec.mask = cellstr(analysis.options.masks.standard);
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';


%% 2) RUN MODEL ESTIMATION

matlabbatch{2}.spm.stats.fmri_est.spmmat(1) = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;


%% 3) SET CONTRASTS OF INTEREST

matlabbatch{3}.spm.stats.con.spmmat(1) = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));

% Calculate the length of the design matrix = (conditions + modulators) x basis functions + noise matrix + constant
if strcmp(type, 'basicSPM') == 1
    glmMatrix = (length(analysis.design.onsets) + analysis.design.numMod) * (1 + length(matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs)) + size(analysis.noise, 2) + 1;
else
    glmMatrix = size(analysis.design.R, 2) + 1;
end

% Set the contrast counter
conCount = 0;

% For each specified condition and modulator, create a positive t-contrast
if ~strcmp(type, 'FC') % Don't add main contrasts for FC analyses
    for nameIdx = 1:length(analysis.design.names)
        conCount = conCount + 1; % Counter
        % Add the names for the condition
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.name = analysis.design.names{nameIdx};
        analysis.design.finalMatrixOrder{conCount} = matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.name;
        % Add the weights for the positive condition contrast
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights = zeros(1, glmMatrix);
        if conCount == 1
            weightIdx = conCount;
        else
            weightIdx = find(matlabbatch{3}.spm.stats.con.consess{conCount-1}.tcon.weights ~= 0) + 3;
        end
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights(weightIdx) = 1;
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.sessrep = 'none';
        % Check if there is a modulator and add
        if isfield(analysis.design, 'pmod') == 1 && ~isempty(analysis.design.pmod(nameIdx).name)
            for pmodIdx = 1:length(analysis.design.pmod(nameIdx).name)
                conCount = conCount + 1;
                matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.name = char(analysis.design.pmod(nameIdx).name{pmodIdx});
                analysis.design.finalMatrixOrder{conCount} = matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.name;
                matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights = zeros(1, glmMatrix);
                weightIdx = weightIdx + 3;
                matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights(weightIdx) = 1;
                matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.sessrep = 'none';
            end
        end
    end
end

% For each of the extra contrasts specified, create a t-contrast
if isfield(analysis.design, 'extraContrasts') == 1
    if iscell(analysis.design.extraContrasts.names) % If multiple extra contrasts
        for nameIdx = 1:length(analysis.design.extraContrasts.names)
            conCount = conCount + 1; % Update the counter
            % Add the base weights for the contrast
            matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights = zeros(1, glmMatrix);
            % Add the names for the condition
            matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.name = [analysis.design.extraContrasts.names{nameIdx}];
            matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights = [analysis.design.extraContrasts.weights{nameIdx} matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights(1:(end - length(analysis.design.extraContrasts.weights{nameIdx})))];
            analysis.design.finalMatrixOrder{conCount} = matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.name;
        end
    else % If only one extra contrast
        conCount = conCount + 1; % Update the counter
        % Add the base weights for the contrast
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights = zeros(1, glmMatrix);
        % Add the names for the condition
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.name = analysis.design.extraContrasts.names;
        matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights = [analysis.design.extraContrasts.weights matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights(1:(end - length(analysis.design.extraContrasts.weights)))];
        analysis.design.finalMatrixOrder{conCount} = matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.name;
    end
end

% Add additional contrasts and F-tests for non-FC analyses
if ~strcmp(type, 'FC') % Don't add main contrasts for FC analyses
    
    % For each specified condition and modulator, create an f-test
    fweightIndex = 1;
    input = eye(3);
    for nameIdx = 1:length(analysis.design.names)
        conCount = conCount + 1;
        % Add the name for the condition
        matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.name = [analysis.design.names{nameIdx}, '_Ftest'];
        analysis.design.finalMatrixOrder{conCount} = matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.name;
        matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.weights = zeros(glmMatrix);
        matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.weights(fweightIndex:(fweightIndex+2),fweightIndex:(fweightIndex+2)) = input;
        matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.sessrep = 'none';
        fweightIndex = fweightIndex + 3; % Update the counter
        % Check if there is a modulator and add
        if isfield(analysis.design, 'pmod') == 1 && ~isempty(analysis.design.pmod(nameIdx).name)
            for pmodIdx = 1:length(analysis.design.pmod(nameIdx).name)
                conCount = conCount + 1;
                matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.name = [char(analysis.design.pmod(nameIdx).name{pmodIdx}), '_Ftest'];
                matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.weights = zeros(glmMatrix);
                matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.weights(fweightIndex:(fweightIndex+2),fweightIndex:(fweightIndex+2)) = input;
                matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.sessrep = 'none';
                fweightIndex = fweightIndex + 3; % Update the counter
            end
        end
    end

    % Add contrasts and f-test for CO2
    conCount = conCount + 1;
    weightIdx = weightIdx + 3;
    matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.name = 'CO2';
    analysis.design.finalMatrixOrder{conCount} = matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.name;
    matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights = zeros(1, glmMatrix);
    matlabbatch{3}.spm.stats.con.consess{conCount}.tcon.weights(weightIdx) = 1;
    matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.sessrep = 'none';
    conCount = conCount + 1;
    matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.name = 'CO2_Ftest';
    matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.weights = zeros(glmMatrix);
    matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.weights(fweightIndex:(fweightIndex+2),fweightIndex:(fweightIndex+2)) = input;
    matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.sessrep = 'none';

%     % Add an f-test for all noise (minus constant)
%     if size(analysis.noise, 2) > 3
%         conCount = conCount + 1;
%         matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.name = 'all_other_noise_Ftest';
%         matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.weights = matlabbatch{3}.spm.stats.con.consess{conCount -1}.fcon.weights; % Start with CO2 weights
%         noiseMatrixEye = eye(size(analysis.noise, 2) - 3);
%         matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.weights((glmMatrix -length(noiseMatrixEye)):(glmMatrix -1), (glmMatrix -length(noiseMatrixEye)):(glmMatrix -1)) = noiseMatrixEye;
%         matlabbatch{3}.spm.stats.con.consess{conCount}.fcon.sessrep = 'none';
%     end

end

matlabbatch{3}.spm.stats.con.delete = 0;


%% 4) ESTIMATE AND OUTPUT THRESHOLDED RESULTS

matlabbatch{4}.spm.stats.results.spmmat(1) = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{4}.spm.stats.results.conspec.titlestr = '';
matlabbatch{4}.spm.stats.results.conspec.contrasts = Inf;
matlabbatch{4}.spm.stats.results.conspec.threshdesc = 'FWE';
matlabbatch{4}.spm.stats.results.conspec.thresh = 0.05;
matlabbatch{4}.spm.stats.results.conspec.extent = 0;
matlabbatch{4}.spm.stats.results.conspec.conjunction = 1;
matlabbatch{4}.spm.stats.results.conspec.mask.none = 1;
matlabbatch{4}.spm.stats.results.units = 1;
matlabbatch{4}.spm.stats.results.export{1}.ps = true;
matlabbatch{4}.spm.stats.results.export{2}.tspm.basename = 'thresh';


%% SAVE THE SPECS AND RUN THE JOBS

analysis.matlabbatch = matlabbatch;
spm_jobman('run', matlabbatch);

save(saveName, 'analysis');


%% REMOVE ALL BETA FILES TO SAVE SPACE

% Remove all beta files to save space
disp('Deleting all beta analysis files...');
delete(fullfile(saveFolder, 'beta*.nii'));
disp('... Finished!');


%% RETURN TO ANALYSIS DIRECTORY
    
cd(analysis.options.paths.analysis_scripts);

end
